<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pricings', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('plans')
                ->onDelete('cascade');

            $table->integer('term');
            $table->string('period');
            $table->float('price', 8, 2)->unsigned();
            $table->float('price_renews', 8, 2)->unsigned()->nullable();

            $table->bigInteger('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }
};
