<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('monitor_status_pages', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->uuid('monitor_id');
            $table->foreign('monitor_id')->references('uuid')->on('monitors')
                ->onDelete('cascade');
            $table->string('type', 100);
            $table->uuid('rel_id');
            $table->foreign('rel_id')->references('uuid')->on('status_pages')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }
};
