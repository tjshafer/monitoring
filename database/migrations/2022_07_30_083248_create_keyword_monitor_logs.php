<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('keyword_monitor_logs', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');
            $table->string('rel_id');
            $table->tinyInteger('is_up');
            $table->timestamp('previous_time');
            $table->timestamp('current_time')->nullable();
            $table->bigInteger('down_time');
            $table->bigInteger('response_time');
            $table->string('response_json');
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
