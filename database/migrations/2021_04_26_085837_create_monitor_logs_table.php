<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('monitor_logs', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->uuid('rel_id');
            $table->foreign('rel_id')->references('uuid')->on('monitors')
                ->onDelete('cascade');

            $table->string('type');
            $table->tinyInteger('is_up');
            $table->bigInteger('response_time')
                ->default('0');
            $table->dateTime('previous_time');
            $table->dateTime('current_time');
            $table->bigInteger('down_time')
                ->default('0');
            $table->softDeletes();
            $table->timestamps();
            $table->index(['uuid', 'rel_id', 'created_at']);
        });
    }
};
