<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ssl_monitor_logs', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->uuid('rel_id');
            $table->foreign('rel_id')->references('uuid')->on('ssl_monitors')
                ->onDelete('cascade');

            $table->dateTime('ssl_expiry')->nullable();
            $table->boolean('ssl_status');

            $table->timestamps();
        });
    }
};
