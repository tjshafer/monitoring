<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->longText('description');
            $table->boolean('status')->default(true);
            $table->integer('server_count')->nullable();
            $table->integer('webpage_count')->nullable();
            $table->integer('api_count')->nullable();
            $table->integer('user_count')->nullable();
            $table->integer('display_order')->nullable();
            $table->integer('dns_count')->nullable();
            $table->integer('keyword_count')->nullable();
            $table->timestamps();
        });
    }
};
