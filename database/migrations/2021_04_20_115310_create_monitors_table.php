<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('monitors', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->string('name', 100);
            $table->string('type');
            $table->string('url', 250);
            $table->string('port', 6)->nullable();
            $table->string('methode', 100)->nullable();
            $table->string('json', 100)->nullable();
            $table->integer('expected_response_code')->nullable();
            $table->tinyInteger('test_result');
            $table->string('previous_status', 10)->default('up');
            $table->bigInteger('avg_response_time')->default('0');
            $table->bigInteger('interval');
            $table->text('email')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }
};
