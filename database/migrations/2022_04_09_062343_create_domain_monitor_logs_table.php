<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('domain_monitor_logs', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->uuid('rel_id');
            $table->foreign('rel_id')->references('uuid')->on('domain_monitors')
                ->onDelete('cascade');

            $table->dateTime('domain_expiry')->nullable();
            $table->boolean('domain_status');

            $table->timestamps();
        });
    }
};
