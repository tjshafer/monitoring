<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gateway_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('gateway_id')->unsigned();
            $table->foreign('gateway_id')->references('id')->on('gateways')
                ->onDelete('cascade');
            $table->string('name');
            $table->string('display_name', 100)->unique();
            $table->string('type')->default('text');
            $table->text('value')->nullable();
            $table->text('description');
            $table->timestamps();
        });
    }
};
