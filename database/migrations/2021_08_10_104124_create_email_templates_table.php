<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('email_templates', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');
            $table->string('name');
            $table->longText('subject');
            $table->binary('message');
            $table->boolean('status')->default(true);

            $table->bigInteger('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade');
            $table->boolean('system_template')->default(false);
            $table->longText('merge_fields')->nullable();
            $table->timestamps();
        });
    }
};
