<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('keyword_monitoring', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');
            $table->string('name');
            $table->string('url');
            $table->string('keyword');
            $table->boolean('is_case_sensitive');
            $table->string('alert_when');
            $table->bigInteger('interval');
            $table->string('previous_status')->nullable();
            $table->bigInteger('avg_response_time');
            $table->bigInteger('user_id');
            $table->string('email');
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
