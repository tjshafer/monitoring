<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('domain_monitors', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->string('name', 100);
            $table->string('url', 250);
            $table->dateTime('domain_expiry')->nullable();
            $table->text('email')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }
};
