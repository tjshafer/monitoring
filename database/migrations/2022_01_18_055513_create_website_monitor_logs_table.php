<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('website_monitor_logs', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->uuid('rel_id');
            $table->foreign('rel_id')->references('uuid')->on('monitor_websites')
                ->onDelete('cascade');

            $table->dateTime('ssl_expiry')->nullable();
            $table->boolean('ssl_status');
            $table->dateTime('domain_expiry')->nullable();
            $table->boolean('domain_status');

            $table->timestamps();
        });
    }
};
