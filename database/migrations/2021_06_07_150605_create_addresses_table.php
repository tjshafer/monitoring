<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('phone')->nullable();
            $table->text('address_1');
            $table->text('address_2')->nullable();
            $table->string('city');
            $table->integer('postal_code');
            $table->bigInteger('country_id')->references('id')->on('countries')
                ->onDelete('cascade');
            $table->bigInteger('state_id')->references('id')->on('states')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }
};
