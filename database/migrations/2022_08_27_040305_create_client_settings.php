<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('client_settings', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');
            $table->bigInteger('user_id');
            $table->string('key');
            $table->string('value');
            $table->timestamps();
        });
    }
};
