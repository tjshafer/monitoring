<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dns_cname_records', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->string('website_id');
            $table->foreign('website_id')->references('uuid')->on('dns_monitoring')
                ->onDelete('cascade');
            $table->string('value');
            $table->string('previous_status')->nullable();
            $table->bigInteger('avg_response_time');
            $table->timestamps();
        });
    }
};
