<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->bigInteger('incident_id')->unique();
            $table->uuid('rel_id');
            $table->foreign('rel_id')->references('uuid')->on('monitors')
                ->onDelete('cascade');

            $table->text('subject');
            $table->text('description');
            $table->boolean('status')->default(false);
            $table->boolean('is_auto')->default(false);
            $table->softDeletes();
            $table->timestamps();
            $table->index(['uuid', 'created_at']);
        });
    }
};
