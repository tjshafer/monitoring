<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('site_id')->default(0);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('status_id')->default(1);
            $table->string('phone')->nullable();
            $table->text('address_1');
            $table->text('address_2')->nullable();
            $table->string('city');
            $table->integer('postal_code');
            $table->bigInteger('country_id')->references('id')->on('countries')
                ->onDelete('cascade');
            $table->bigInteger('state_id')->references('id')->on('states')
                ->onDelete('cascade');
            $table->string('currency')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }
};
