<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('email_subscriptions', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->uuid('status_page_id');
            $table->foreign('status_page_id')->references('uuid')->on('status_pages')
                ->onDelete('cascade');
            $table->text('email');
            $table->timestamps();
        });
    }
};
