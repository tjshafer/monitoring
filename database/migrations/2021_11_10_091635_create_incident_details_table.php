<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('incident_details', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');

            $table->uuid('incident_id');
            $table->foreign('incident_id')->references('uuid')->on('incidents')
                ->onDelete('cascade');
            $table->string('status');
            $table->text('description');
            $table->timestamps();
        });
    }
};
