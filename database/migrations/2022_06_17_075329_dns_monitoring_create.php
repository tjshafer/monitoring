<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dns_monitoring', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');
            $table->string('website_domain');
            $table->bigInteger('interval');
            $table->string('email');
            $table->timestamps();
        });
    }
};
