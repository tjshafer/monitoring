<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('status_pages', function (Blueprint $table) {
            $table->longText('description')->nullable()->after('name');
            $table->string('sub_domain')->unique()->nullable()->after('description');
            $table->text('logo')->nullable()->after('sub_domain');
        });
    }
};
