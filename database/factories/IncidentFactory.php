<?php

namespace Database\Factories;

use App\Helpers\Uuid;
use App\Models\Incident;
use App\Models\Monitor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class IncidentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Incident::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'uuid' => Uuid::getUuid(),
            // create a new user
            'rel_id' => function () {
                return Monitor::factory()->create()->uuid;
            },
            'incident_id' => $this->faker->unique()->randomNumber(5),
            'subject' => $this->faker->sentence($nbWords = 4, $variableNbWords = true),
            'description' => $this->faker->sentence($nbWords = 7, $variableNbWords = true),

            'status' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }
}
