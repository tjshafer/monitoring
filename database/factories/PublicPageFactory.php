<?php

namespace Database\Factories;

use App\Models\Monitor;
use App\Models\PublicPage;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PublicPageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PublicPage::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(),
            // create a new user
            'rel_id' => function () {
                return Monitor::all()->unique()->random()->uuid;
            },
            // create a new order
            'name' => $this->faker->name(),
            'status' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }
}
