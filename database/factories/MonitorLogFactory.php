<?php

namespace Database\Factories;

use App\Helpers\Uuid;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class MonitorLogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MonitorLog::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'uuid' => Uuid::getUuid(),
            // create a new user
            'rel_id' => function () {
                return Monitor::all()->random()->uuid;
            },
            'type' => $this->faker->randomElement(['webpage', 'server', 'api']),
            // create a new order
            'is_up' => $this->faker->randomElement([true, false]),
            'response_time' => $this->faker->randomNumber(3),
            'previous_time' => Carbon::now()->format('Y-m-d H:i:s'),
            'current_time' => Carbon::now()->format('Y-m-d H:i:s'),
            'down_time' => $this->faker->randomElement([0, 5, 10, 15]),
            'created_at' => Carbon::today()->subDays(rand(0, 100)),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }
}
