<?php

namespace Database\Factories;

use App\Helpers\Uuid;
use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'uuid' => Uuid::getUuid(),
            // 'order_id' => $this->faker->unique()->randomNumber(6),
            'user_id' => function () {
                return User::factory()->create()->id;
            },
            'amount' => $this->faker->randomElement([0.00, 19.00, 99.00, 199.00]),
            'gateway' => 'paypal',
            'status' => $this->faker->randomElement(['active', 'pending']),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }
}
