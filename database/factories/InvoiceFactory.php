<?php

namespace Database\Factories;

use App\Helpers\Uuid;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class InvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'uuid' => Uuid::getUuid(),
            // create a new user
            'user_id' => function () {
                return User::all()->random()->id;
            },
            // create a new order
            'order_id' => function () {
                return Order::factory()->create()->uuid;
            },
            'amount' => $this->faker->randomElement([0.00, 19.00, 99.00, 199.00]),
            'due_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'payment_status' => $this->faker->randomElement(['paid', 'unpaid']),
            'is_renew' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }
}
