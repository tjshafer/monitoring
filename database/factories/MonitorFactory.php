<?php

namespace Database\Factories;

use App\Helpers\Uuid;
use App\Models\Monitor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class MonitorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Monitor::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'uuid' => Uuid::getUuid(),
            // create a new user
            'user_id' => '1',
            // create a new order
            'type' => $this->faker->randomElement(['server', 'webpage', 'api']),
            'name' => $this->faker->name(),
            'url' => $this->faker->domainName(),
            'port' => $this->faker->randomNumber(2),
            'methode' => $this->faker->randomElement(['Get', 'Post']),
            'expected_response_code' => $this->faker->randomNumber(3),
            'previous_status' => $this->faker->randomElement(['up', 'down']),
            'test_result' => 1,
            'avg_response_time' => $this->faker->randomNumber(3),
            'interval' => $this->faker->randomNumber(1),
            'email' => $this->faker->unique()->safeEmail(),
            'created_at' => Carbon::now()->subDays(100)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }
}
