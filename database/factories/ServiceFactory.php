<?php

namespace Database\Factories;

use App\Helpers\Uuid;
use App\Models\Order;
use App\Models\Service;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'uuid' => Uuid::getUuid(),
            // create a new user
            'user_id' => function () {
                return User::all()->random()->id;
            },
            // create a new order
            'order_id' => function () {
                return Order::all()->random()->uuid;
            },
            'plan_id' => $this->faker->randomElement([1, 2, 3, 4]),
            'status_id' => $this->faker->randomElement([1, 2]),
            'expiry_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'next_invoice_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }
}
