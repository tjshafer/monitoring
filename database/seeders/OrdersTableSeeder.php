<?php

namespace Database\Seeders;

use App\Helpers\Uuid;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // DB::table('orders')->insert([
        //     [
        //         'uuid' => Uuid::getUuid(),
        //         'order_id' => 111,
        //         'user_id' => 1,
        //         'amount' => 0,
        //         'gateway' => 'payPal',
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //         'uuid' => Uuid::getUuid(),
        //         'order_id' => 222,
        //         'user_id' => 2,
        //         'amount' => 29,
        //         'gateway' => 'payPal',
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //         'uuid' => Uuid::getUuid(),
        //         'order_id' => 333,
        //         'user_id' => 2,
        //         'amount' => 29,
        //         'gateway' => 'stripe',
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],

        // ]);
    }
}
