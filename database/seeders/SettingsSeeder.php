<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
            [
                'id' => 1,
                'name' => 'order_accept',
                'value' => 1,
                'type' => 'radio',
                'description' => 'Enable this to use order accept feature from admin side',

                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'name' => 'auto_incident',
                'value' => 0,
                'type' => 'radio',
                'description' => 'Enable this to add an incident automatically when a server downs',

                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'name' => 'system_logo',
                'value' => '',
                'type' => 'attachment',
                'description' => 'The system logo must be a file of type: .svg, .png',

                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id' => 4,
                'name' => 'statuspage_with_subdomain',
                'value' => 0,
                'type' => 'radio',
                'description' => 'Enable this to use subdomains for each statuspage',

                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'id' => 5,
                'name' => 'footer_text',
                'value' => '',
                'type' => 'text',
                'description' => 'Add the footer text to be displayed',

                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id' => 6,
                'name' => 'privacy_policy',
                'value' => 'Add a privacy policy',
                'type' => 'text_area',
                'description' => 'Add the detailed privacy policy here',

                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id' => 7,
                'name' => 'terms_of_use',
                'value' => 'Add the terms of use',
                'type' => 'text_area',
                'description' => 'You can add the terms of use from here',

                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
