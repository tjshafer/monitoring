<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CurrencySeeder::class,
            AdminsTableSeeder::class,
            StatusesTableSeeder::class,
            PlansTableSeeder::class,
            LanguageSeeder::class,
            EmailTemplateSeeder::class,
            GatewaySeeder::class,
            GatewayDetailsSeeder::class,
            CountrySeeder::class,
            StateSeeder::class,
            IntervalTableSeeder::class,
            UsersTableSeeder::class,
            SettingsSeeder::class,
            // MonitorTableSeeder::class,
            // MonitorLogTableSeeder::class,
        ]);
    }
}
