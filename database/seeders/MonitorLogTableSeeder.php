<?php

namespace Database\Seeders;

use App\Models\MonitorLog;
use Illuminate\Database\Seeder;

class MonitorLogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MonitorLog::factory()
            ->count(20000)
            ->create();
    }
}
