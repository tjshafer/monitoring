<?php

namespace Database\Seeders;

use App\Helpers\Uuid;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class InvoiceDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // DB::table('invoice_details')->insert([
        //     [
        //         'uuid' => Uuid::getUuid(),
        //         'service_id' => 100,
        //         'invoice_id' => 33,
        //         'amount' => 0,
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //         'uuid' => Uuid::getUuid(),
        //         'service_id' => 200,
        //         'invoice_id' => 11,
        //         'amount' => 29,
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //         'uuid' => Uuid::getUuid(),
        //         'service_id' => 300,
        //         'invoice_id' => 22,
        //         'amount' => 29,
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],

        // ]);
    }
}
