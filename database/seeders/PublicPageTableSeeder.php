<?php

namespace Database\Seeders;

use App\Models\PublicPage;
use Illuminate\Database\Seeder;

class PublicPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PublicPage::factory()
            ->count(30)
            ->create();
    }
}
