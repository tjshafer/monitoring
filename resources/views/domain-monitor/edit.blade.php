@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Domain Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('domain_monitor.index') }}">{{ __('Domain Monitor') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Edit domain') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Edit domain') }}</h2>
    <p class="section-lead">{{ __('You can update a domain from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Edit domain') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('domain_monitor.update', $domain->uuid) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Name') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name', $domain->name) }}" autocomplete="name" autofocus>

                                @error('name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can enter a name of your choice for the domain') }}.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('URL') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror"
                                    name="url" value="{{ old('url', $domain->url) }}" autocomplete="url" autofocus>

                                @error('url')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can enter the domain. Note that you have to enter the exact domain name, ') }}. <br>
                                    Eg: {{ __('codedeploy.io') }}
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Email') }}:</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email', $domain->email) }}" autocomplete="email" autofocus>

                                @error('email')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted">
                                    Eg: {{ Auth::user()->email }}
                                </small>
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can enter email id, To this email id status update emails will be sent.') }}
                                    <br>
                                    {{ __('If no email is mentioned here, mail will be sent to') }}<br />
                                    {{ Auth::user()->email }}
                                </small>
                            </div>
                        </div>

                        @if (config('app.env') != 'demo')
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Update') }}</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection