@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Domain Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('domain_monitor.index') }}">{{ __('Domain Monitor') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('List of Domains') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('List of Domains') }}</h2>
    <p class="section-lead">
        {{__('Total number of domains')}}: {{ $domains->total() }}
    </p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.user')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4 class="d-sm-inline-block">{{ __('List of Domains') }}</h4>
                    <div class="inline-block float-sm-right mt-2 mt-sm-0">
                        <a href="{{ route('domain_monitor.create') }}" class="btn btn-icon btn-custom"><i
                                class="far fa-edit"></i>{{ __('Add Domain') }}</a>
                        <a href="{{ route('domain_bulk_create') }}" class="btn btn-icon btn-custom mr-1"><i
                                class="far fa-edit"></i>{{ __('Bulk Add') }}</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="search-bar">
                        <form action="/domain_monitor" method="get">
                            <div class="input-group mb-2">
                                <input type="text" id="search" name="search" class="form-control search-bar-input"
                                    value="{{ request()->input('search') }}" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-custom search-bar-button"><i
                                            class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                    @if (!count($domains))
                    <div class="card-body">
                        <div class="empty-state" data-height="400">
                            <div class="empty-state-icon bg-danger">
                                <i class="fas fa-question"></i>
                            </div>
                            <h2>{{ __('No domain added yet under') }} !!</h2>
                            <p class="lead">
                                {{ __('Sorry we cant find any data, to get rid of this message, make at least 1 entry') }}.
                            </p>
                            <a href="{{ route('domain_monitor.create') }}"
                                class="btn btn-custom mt-4">{{ __('Create new One') }}</a>
                        </div>
                    </div>

                    @else
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center text-capitalize">
                                    <th>{{ __('Name') }}</th>
                                    <th>
                                        {{ __('Domain') }}
                                    </th>
                                    <th>
                                        {{ __('Domain status') }}
                                    </th>
                                    <th>
                                        {{ __('Domain expiry date') }}
                                    </th>
                                    <th>
                                        {{ __('Last checked') }}
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($domains as $domain)
                                <tr class="text-left">
                                    <td>{{ $domain->name }}</td>
                                    <td>{{ $domain->url }}</td>
                                    <td>
                                        @if($domain->domain_expiry && $domain->domain_expiry >= $current_date)
                                        <span class="text-success">{{ __('Active') }}</span>
                                        @elseif($domain->domain_expiry && $domain->domain_expiry < $current_date)
                                        <span class="text-danger">{{ __('Expired') }}</span>
                                        @else
                                        {{ __('NA') }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($domain_expiry_remaining[$domain->uuid] &&
                                        $domain_expiry_remaining[$domain->uuid] <= 7) <span class="text-danger">
                                            {{ __((1+$domain_expiry_remaining[$domain->uuid])." days left") }}</span>
                                            @else
                                            {{ $domain->domain_expiry ? \Illuminate\Support\Carbon::parse($domain->domain_expiry)->format('d-M-Y') : "NA" }}
                                            @endif
                                    </td>

                                    <td>{{$domain->updated_at->diffForHumans()}}</td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('domain_monitor.edit', [$domain->uuid]) }}"
                                            class="btn btn-sm bg-transparent"><i class="far fa-edit text-primary"
                                                aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('domain_monitor.destroy', [$domain->uuid]) }}"
                                            method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm bg-transparent"
                                                onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash text-danger" aria-hidden="true"
                                                    title="{{ __('Delete') }}"></i>
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br>
                        {{ $domains->appends($request->all())->links("pagination::bootstrap-4") }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection