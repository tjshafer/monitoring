@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Server Incidents') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('server_incident.index') }}">{{ __('Server Incidents') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Update Incident') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Update Incident') }}</h2>
    <p class="section-lead">{{ __('Update your incident here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Update Incident') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('server_incident.update', [$incident->uuid]) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Server') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="server" type="text"
                                    class="form-control @error('server') is-invalid @enderror" name="server"
                                    value="{{ $incident->monitors->name }}" autocomplete="server" autofocus
                                    readonly>
                                @error('server')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Subject') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="subject" type="text"
                                    class="form-control @error('subject') is-invalid @enderror" name="subject"
                                    value="{{ old('subject', $incident->subject) }}" autocomplete="subject" autofocus>
                                @error('subject')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can mention the basic problem in a small sentence') }}. <br>
                                    {{ __('Example: Infrastructure outage incident report') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Description') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea id="description"
                                    class="summernote-simple @error('description') is-invalid @enderror"
                                    name="description">{{ old('description', $incident->description) }}</textarea>
                                @error('description')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you should describe the issue clearly') }}.
                                </small>
                            </div>
                        </div>

                        @if (config('app.env') != 'demo')
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Update') }}</button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection