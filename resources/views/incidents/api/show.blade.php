@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('API Incidents') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('api_incident.index') }}">{{ __('API Incidents') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Incident Details') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Update Incident') }}</h2>
    <p class="section-lead">{{ __('Update your incident status here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Update Incident') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('updateApiDetails', [$incident->uuid]) }}">
                        @csrf

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('API') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="api" type="text" class="form-control @error('api') is-invalid @enderror"
                                    name="api" value="{{ $incident->monitors->name }}" autocomplete="api" autofocus
                                    readonly>
                                @error('api')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label for="status"
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Status') }}</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric" id="status" name="status">
                                    <option value="">{{ __('Select status') }}</option>
                                    <option value="monitoring" {{ old('status') == "monitoring" ? 'selected' : '' }}>
                                        {{ __('Monitoring') }}</option>
                                    <option value="update" {{ old('status') == "update" ? 'selected' : '' }}>
                                        {{ __('Update') }}</option>
                                    <option value="identified" {{ old('status') == "identified" ? 'selected' : '' }}>
                                        {{ __('Identified') }}</option>
                                    <option value="resolved" {{ old('status') == "resolved" ? 'selected' : '' }}>
                                        {{ __('Resolved') }}</option>
                                </select>
                                @error('status')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select the update status of the icident') }}. <br>
                                    {{ __('Not that if you select resolved status, Then the icident status will also change to resolved') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Description') }}:</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea id="updated_description"
                                    class="summernote-simple @error('updated_description') is-invalid @enderror"
                                    name="updated_description">{{ old('updated_description') }}</textarea>
                                @error('updated_description')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you should describe the update of the incident') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Update') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Incident Updates') }}</h2>
    <p class="section-lead">
        {{ __('All incident updates are listed here') }}.
    </p>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="inline-block">{{ __('Incident Updates') }}</h4>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center text-capitalize">
                                    <th class="width-10">{{ __('Status') }}</th>
                                    <th class="width-50">{{ __('Description') }}</th>
                                    <th>{{ __('Updated at') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($incidentDetails as $details)
                                <tr class="text-center">
                                    <td class="text-left text-capitalize">{{ $details->status }}</td>
                                    <td class="text-left">{!! strip_tags($details->description) !!}</td>
                                    <td>{{ $details->created_at->diffForHumans() }}</td>
                                    <td class="justify-content-center form-inline">
                                        <form action="{{ route('deleteApiDetails', [$details->uuid]) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm bg-transparent"
                                                onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash text-danger" aria-hidden="true"
                                                    title="{{ __('Delete') }}"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                <tr class="text-center">
                                    <td class="text-left text-capitalize">{{ __('Investigating') }}</td>
                                    <td class="text-left">{!! strip_tags($incident->description) !!}</td>
                                    <td>{{ $incident->created_at->diffForHumans() }}</td>
                                    <td class="justify-content-center form-inline">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        {{ $incidentDetails->appends($request->all())->links("pagination::bootstrap-4") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection