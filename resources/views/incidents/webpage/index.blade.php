@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Webpage Incidents') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('webpage_incident.index') }}">{{ __('Webpage Incidents') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('List of Incidents') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('List of Incidents') }}</h2>
    <p class="section-lead">
        {{__('Total number of webpage incidents')}}: {{ $incidents->total() }}
    </p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.user')
            @include('common.errors')
            <div class="card">

                <div class="card-header">
                    <h4 class="inline-block">{{ __('List of Incidents') }}</h4>
                    <a href="{{ route('webpage_incident.create') }}"
                        class="btn btn-icon btn-custom float-right inline-block"><i
                            class="far fa-edit"></i>{{ __('Add Incident') }}</a>
                </div>
                <div class="card-body">
                    <form action="/webpage_incident" method="get">
                        @csrf
                        <input type="hidden" name="action" value="modify_incident" />
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="page">{{ __('Webpages') }}</label>
                                <select class="form-control selectric" id="page" name="rel_id">
                                    <option value="0">{{ __('All') }}</option>
                                    @foreach ($pages as $page)
                                    @if (request()->input('rel_id') == $page->uuid)
                                    <option selected value="{{ $page->uuid }}">
                                        {{ __($page->name) }}</option>
                                    @else
                                    <option value="{{ $page->uuid }}">{{ __($page->name) }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="status">{{ __('Status') }}</label>
                                <select class="form-control selectric" id="status" name="status">
                                    <option value="">{{ __('All') }}</option>
                                    <option value="true" {{ request()->input('status') == "true" ? 'selected' : '' }}>
                                        {{ __('Resolved') }}</option>
                                    <option value="false" {{ request()->input('status') == "false" ? 'selected' : '' }}>
                                        {{ __('Pending') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group m-2 row float-left">
                            <div>
                                <button type="submit" class="btn btn-custom">{{ __('Filter') }}</button>
                            </div>
                        </div>
                    </form>

                    @if (!count($incidents))
                    <div class="empty-state" data-height="400">
                        <div class="empty-state-icon bg-danger">
                            <i class="fas fa-question"></i>
                        </div>
                        <h2>{{ __('No data found') }} !!</h2>
                        <p class="lead">
                            {{ __('Sorry we cant find any data, to get rid of this message, make at least 1 entry') }}.
                        </p>
                        <a href="{{ route('webpage_incident.create') }}"
                            class="btn btn-custom mt-4">{{ __('Create new One') }}</a>
                    </div>
                    @else
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center text-capitalize">
                                    <th>{{ __('Webpage') }}</th>
                                    <th>{{ __('Subject') }}</th>
                                    <th>{{ __('Description') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Last update') }}</th>
                                    <th>{{ __('Follow up') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($incidents as $incident)
                                <tr>
                                    <td>{{ Str::of($incident->monitors->name)->limit(20) }}</td>
                                    <td class="width-20">{{ Str::of($incident->subject)->limit(30) }}</td>
                                    <td class="width-20">{!! strip_tags(Str::of($incident->description)->limit(30)) !!}</td>
                                    <td  class="text-center">
                                        @if($incident->status == true)
                                        <span class="text-success-dark font-weight-bold"> {{ __('Resolved') }} </span>
                                        @else
                                        <span class="text-danger font-weight-bold"> {{ __('Pending') }} </span>
                                        @endif
                                    </td>
                                    <td>{{ $incident->updated_at->diffForHumans() }}</td>
                                    <td  class="text-center"><a href="{{ route('webpage_incident.show', [$incident->uuid]) }}">
                                            {{ __('Follow up') }} </a></td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('webpage_incident.edit', [$incident->uuid]) }}"
                                            class="btn btn-sm bg-transparent"><i class="far fa-edit text-primary"
                                                aria-hidden="true" title="{{ __('Edit') }}"></i></a>
                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('webpage_incident.destroy', [$incident->uuid]) }}"
                                            method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm bg-transparent"
                                                onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash text-danger" aria-hidden="true"
                                                    title="{{ __('Delete') }}"></i>
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br>
                        {{ $incidents->appends($request->all())->links("pagination::bootstrap-4") }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection