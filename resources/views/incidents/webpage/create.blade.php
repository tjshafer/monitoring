@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Webpage Incidents') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('webpage_incident.index') }}">{{ __('Webpage Incidents') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Add Incident') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Add Incident') }}</h2>
    <p class="section-lead">{{ __('Add your incidents here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add Incident') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('webpage_incident.store') }}">
                        @csrf

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Select Server') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric" id="page" name="rel_id">
                                    @foreach ($pages as $page)
                                    @if (old('rel_id') == $page->uuid)
                                    <option selected value="{{ $page->uuid }}">
                                        {{ __($page->name) }}</option>
                                    @else
                                    <option value="{{ $page->uuid }}">{{ __($page->name) }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                                
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select the webpage name') }}. <br>
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Subject') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="subject" type="text" class="form-control @error('subject') is-invalid @enderror"
                                    name="subject" value="{{ old('subject') }}" autocomplete="subject" autofocus>
                                @error('subject')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can mention the basic problem in a small sentence') }}. <br>
                                    {{ __('Example: Infrastructure outage incident report') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Description') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea id="description"
                                    class="summernote-simple @error('description') is-invalid @enderror"
                                    name="description">{{ old('description') }}</textarea>
                                @error('description')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you should describe the issue clearly') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Add') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection