<div>
    <b>{{ __('Hello :first_name :last_name', ['first_name' => $website->user->first_name, 'last_name' => $website->user->last_name]) }},
    </b><br />
    {{ __('Your domain validity will expire in :days days', ['days' => $expireIn]) }}

    <br />
    {{ __('Domain name: :name',['name' => $website->url]) }}

    <br />
    {{ __('Expiry date: :expiry',['expiry' => $website->domain_expiry]) }}

    <br />
    <br />
    {{ __('Regards') }}
    <br />
    <br />
    {{ __(':admin', ['admin' => config('mail.from.name')]) }}
</div>