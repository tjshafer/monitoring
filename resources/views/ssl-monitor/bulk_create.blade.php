@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('SSL Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('ssl_monitor.index') }}">{{ __('SSL Monitor') }}</a></div>
        <div class="breadcrumb-item">{{ __('Add Bulk SSL') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Add Bulk SSL') }}</h2>
    <p class="section-lead">{{ __('You can add SSL list as .csv file from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add SSL') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('ssl_bulk_store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 text-capitalize">{{ __('CSV File') }}:</label>
                                <div class="col-sm-12 col-md-7">
                                    <input id="icon" name="csv_file" type="file" class="form-control file"
                                        data-show-caption="true" autocomplete="value"
                                        autofocus>
                                    @error('csv_file')
                                    <div class="text-danger pt-1">{{ $message }}</div>
                                    @enderror

                                    <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Using bulk add, you can add multiple ssl at once. You have to enter the ssl details in a csv file.
                                            format  of the csv file is as shown below, one header will be there and then one ssl details per row.)') }}.
                                    <br>
                                    Eg: <strong>{{ __('Name') }}, {{ __('URL') }}, {{ __('ssl_port (default:433, can be skipped)') }}</strong><br>
                                    {{ __('v11, v12, v13') }}<br>
                                    {{ __('v21, v22, v23') }}<br>
                                    {{ __('......') }}<br>
                                </small>
                                </div>
                            </div>

                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Add') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection