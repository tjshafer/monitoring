@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Intervals') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item">{{ __('Intervals') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Intervals') }}</h2>
    <p class="section-lead">
        {{__('Total number of intervals')}}: {{ $intervals->total() }}
    </p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.admin')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add Interval') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('interval.store') }}">
                        @csrf

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Interval') }} ({{ __('in minutes') }}):*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="interval" type="text"
                                    class="form-control @error('interval') is-invalid @enderror" name="interval"
                                    value="{{ old('interval') }}" autocomplete="interval" autofocus>
                                @error('interval')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                                <small id="port_help1" class="form-text text-muted">
                                    {{ __('eg : 5') }}
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Display value') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="display_name" type="text"
                                    class="form-control @error('display_name') is-invalid @enderror" name="display_name"
                                    value="{{ old('display_name') }}" autocomplete="display_name" autofocus>
                                @error('display_name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                                <small id="port_help1" class="form-text text-muted">
                                    {{ __('eg : 5 Minutes, 2 Hours') }}
                                </small>
                            </div>
                        </div>

                        @if (config('app.env') != 'demo')
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Add') }}</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="inline-block">{{ __('Available Intervals') }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center text-capitalize">
                                    <th>{{ __('Interval') }}</th>
                                    <th>{{ __('Display value') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($intervals as $interval)
                                <tr class="text-center">

                                    <td>{{ __($interval->interval) }} {{ __('minutes') }}</td>
                                    <td>{{ __($interval->display_name) }}</td>
                                    <td>
                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('interval.destroy', [$interval->id]) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button  class="btn bg-transparent" onclick="return confirm('Are you sure?')" >
                                                <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        {{ $intervals->appends($request->all())->links("pagination::bootstrap-4") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection