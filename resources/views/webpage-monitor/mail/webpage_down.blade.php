<div>
  <b>{{ __('Hello :name', ['name' => $page->user->first_name]) }},</b><br/>
  {{ __('Webpage down email for :name', ['name' => $page->name]) }}
  <br/>
  {{ __('URL: :url', ['url' => $page->url]) }}
  <br/>
  {{ __('Response code: :code', ['code' => $page->response_code]) }}
  <br/>
  {{ __('Time: :time', ['time' => \Illuminate\Support\Carbon::now()->toDateTimeString()]) }}
  <br/>
  {{ __('Please contact support team.') }}

  <br/>
  <br/>
  {{ __('Regards') }}
  <br/>
  {{ __(':admin', ['admin' => config('mail.from.name')]) }}
</div>