@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Webpage Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('webpage_monitor.index') }}">{{ __('Webpage Monitor') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('List of Webpages') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('List of Webpages') }}</h2>
    <p class="section-lead">
        {{__('Total number of webpages')}}: {{ $pages->total() }}
    </p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.user')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4 class="d-sm-inline-block">{{ __('List of Webpages') }}</h4>
                    <div class="inline-block float-sm-right mt-2 mt-sm-0">
                        <a href="{{ route('webpage_monitor.create') }}" class="btn btn-icon btn-custom"><i
                                class="far fa-edit"></i>{{ __('Add Webpage') }}</a>
                        <a href="{{ route('webpage_bulk_create') }}" class="btn btn-icon btn-custom mr-1"><i
                                class="far fa-edit"></i>{{ __('Bulk Add') }}</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="search-bar">
                        <form action="/webpage_monitor" method="get">
                            <div class="input-group mb-2">
                                <input type="text" id="search" name="search" class="form-control search-bar-input"
                                    value="{{ request()->input('search') }}" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-custom search-bar-button"><i
                                            class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                    @if (!count($pages))
                    <div class="card-body">
                        <div class="empty-state" data-height="400">
                            <div class="empty-state-icon bg-danger">
                                <i class="fas fa-question"></i>
                            </div>
                            <h2>{{ __('No webpage added yet under') }} !!</h2>
                            <p class="lead">
                                {{ __('Sorry we cant find any data, to get rid of this message, make at least 1 entry') }}.
                            </p>
                            <a href="{{ route('webpage_monitor.create') }}"
                                class="btn btn-custom mt-4">{{ __('Create new One') }}</a>
                        </div>
                    </div>

                    @else
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center text-capitalize">
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Expected response code') }}</th>
                                    <th>{{ __('Response Time') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>
                                        {{ __('Details') }}
                                    </th>
                                    <th>
                                        {{ __('Public page') }}
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pages as $page)
                                <tr class="text-center">
                                    <td class="text-left">{{ $page->name }}</td>
                                    <td>{{ $page->expected_response_code }}</td>
                                    <td>
                                        {{ $page->avg_response_time }} ms
                                    </td>
                                    <td>
                                        @if ($page->previous_status == 'up')
                                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                                        @elseif (($page->previous_status == 'down'))
                                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                                        @else
                                        {{ __('NA') }}
                                        @endif
                                    </td>
                                    <td><a href="{{ route('webpage_monitor.show', [$page->uuid]) }}"
                                            class="btn bg-transparent">
                                            <i class="fas fa-eye text-primary"></i></a></td>
                                    <td><a href="{{ route('monitorPublicPage', [$page->name]) }}"
                                            class="btn bg-transparent">
                                            <i class="fas fa-eye text-primary"></i></a></td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('webpage_monitor.edit', [$page->uuid]) }}"
                                            class="btn btn-sm bg-transparent"><i class="far fa-edit text-primary"
                                                aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('webpage_monitor.destroy', [$page->uuid]) }}"
                                            method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm bg-transparent"
                                                onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash text-danger" aria-hidden="true"
                                                    title="{{ __('Delete') }}"></i>
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br>
                        {{ $pages->appends($request->all())->links("pagination::bootstrap-4") }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection