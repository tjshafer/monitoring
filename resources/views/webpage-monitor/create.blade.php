@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Webpage Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('webpage_monitor.index') }}">{{ __('Webpage Monitor') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Add webpage') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Add Webpage') }}</h2>
    <p class="section-lead">{{ __('You can add a webpage from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add Webpage') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('webpage_monitor.store') }}">
                        @csrf
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Name') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                                @error('name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                {{ __('Here you can enter a name of your choice for the webpage') }}.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('URL') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror"
                                    name="url" value="{{ old('url') }}" autocomplete="url" autofocus>

                                @error('url')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Enter domain as shown in browser(some will have www, some not)') }}. <br>
                                    Eg: {{ __('https://www.google.com, https://monitoring.zone') }}
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Expected response code') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="expected_response_code" type="text"
                                    class="form-control @error('expected_response_code') is-invalid @enderror"
                                    name="expected_response_code" value="{{ old('expected_response_code') }}" autocomplete="expected_response_code"
                                    autofocus>

                                @error('expected_response_code')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to provide HTTP response status code of the URL, It should be some numbers') }}. <br>
                                    Eg: {{ __('200') }}
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Ping interval') }}({{ __('in minutes') }}):*</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric" id="interval" name="interval">
                                    @foreach ($intervals as $interval)
                                    @if (old('interval') == $interval->interval)
                                    <option selected value="{{ $interval->interval }}">
                                        {{ __($interval->display_name) }}</option>
                                    @else
                                    <option value="{{ $interval->interval }}">{{ __($interval->display_name) }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>

                                @error('interval')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select the time interval you want to check the status of the webpage') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Email') }}:</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" autocomplete="email" autofocus>

                                @error('email')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted">
                                    Eg: {{ Auth::user()->email }}
                                </small>
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can enter email id, To this email id status update emails will be sent.') }}
                                    <br>
                                    {{ __('If no email is mentioned here, mail will be sent to') }}<br />
                                    {{ Auth::user()->email }}
                                </small>
                            </div>
                        </div>

                        @if ($testFailed = Session::get('test_failed'))
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Skip test connection') }}:*</label>
                            <div class="col-sm-6 col-md-6 form-inline">
                                <input id="skip_test_connection" type="checkbox" class="form-control is-invalid"
                                    name="skip_test_connection" value="1">
                            </div>
                        </div>
                        @endif

                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Add') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection