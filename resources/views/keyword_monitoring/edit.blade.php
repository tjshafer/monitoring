@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Keyword Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('keyword_monitor.index') }}">{{ __('Keyword Monitor') }}</a></div>
        <div class="breadcrumb-item">{{ __('Edit Keyword') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Edit Keyword') }}</h2>
    <p class="section-lead">{{ __('You can edit a keyword from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Edit Keyword') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('keyword_monitor.update', [$monitor->uuid]) }}">
                        @csrf

                        @method('PUT')

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Name') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name',$monitor->name) }}" autocomplete="name" autofocus>

                                @error('name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                {{ __('Here you can enter a name of your choice for the keyword') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('URL') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="url" type="text"
                                    class="form-control @error('url') is-invalid @enderror"
                                    name="url" value="{{ old('url',$monitor->url) }}"
                                    autocomplete="url" autofocus>

                                @error('url')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="address_help1" class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Enter domain as shown in browser(some will have www, some not).') }}
                                </small>
                                <small id="address_help2" class="form-text text-muted">

                                    {{ __('Eg: https://www.google.com, https://monitoring.zone') }}
                                </small>
                                
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Keyword') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="keyword" type="text" class="form-control @error('keyword') is-invalid @enderror"
                                    name="keyword" value="{{ old('keyword',$monitor->keyword) }}" autocomplete="keyword" autofocus>

                                @error('keyword')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="keyword_help1" class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Keyword should not be more than 255 characters.') }}
                                </small>
                                
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Case Sensitive') }}:*</label>
                                <div class="col-sm-12 col-md-7">
                                    <div class="custom-radio custom-control">
                                        <input class="custom-control-input" type="radio" name="case" id="caseYes"
                                            value=1 {{ old('case', $monitor->is_case_sensitive) == 1 ? "checked" : "" }}>
                                        <label class="custom-control-label" for="caseYes">
                                            {{ __('Yes') }}
                                        </label>
                                    </div>
                                    <div class="custom-radio custom-control">
                                        <input class="custom-control-input" type="radio" name="case" id="caseNo"
                                            value=0 {{ old('case', $monitor->is_case_sensitive) == 0 ? "checked" : "" }}>
                                        <label class="custom-control-label" for="caseNo">
                                            {{ __('No') }}
                                        </label>
                                    </div>
                                    
                                </div>
                            </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Expected Result') }}:</label>
                            <div class="col-sm-12 col-md-7">
                                <select name="alert" id="alert" class="form-control selectric">
                                    <option value="keyword_exist" {{ old('alert', $monitor->alert_when) == 'keyword_exist' ? "selected" : "" }}>{{ __('Keyword exists') }}</option>
                                    <option value="keyword_not_exist" {{ old('alert', $monitor->alert_when) == 'keyword_not_exist' ? "selected" : "" }}>{{ __('Keyword does not exist') }}</option>
                                </select>

                                @error('alert')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select when to alert ') }}.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Ping interval') }}({{ __('in minutes') }}):*</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric" id="interval" name="interval">
                                    @foreach ($intervals as $interval)
                                    @if ($interval->interval == old('interval', $monitor->interval))
                                    <option selected value="{{ $interval->interval }}">
                                        {{ __($interval->display_name) }}</option>
                                    @else
                                    <option value="{{ $interval->interval }}">{{ __($interval->display_name) }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>

                                @error('interval')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select the time interval you want to check the status of the keyword') }}.
                                </small>
                            </div>
                        </div>
                        
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Update') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection