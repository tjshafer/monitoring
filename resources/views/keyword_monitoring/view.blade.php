@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Keyword Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('keyword_monitor.index') }}">{{ __('Keyword Monitor') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Keyword Details') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Keyword Details') }}</h2>
    <p class="section-lead">
        {{__('Key metrics of')}} {{ $keyword->name }}
    </p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.user')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4 class="inline-block">{{ $keyword->name }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center">
                                <th>{{ __('URL') }}</th>
                                <th>{{ __('Keyword') }}</th>
                                <th>{{ __('Expected Result') }}</th>
                                <th>{{__('Case Sensitive')}}</th>
                                <th>{{__('Ping Interval')}}</th>
                                <th>{{__('Response Time')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                <td>{{ $keyword->url }}</td>
                                <td>{{ Str::of($keyword->keyword)->limit(), 255 }}</td>
                                <td>{{ $keyword->alert_when=='keyword_exist'?'Keyword exists':'Keyword does not exist ' }}</td>
                                <td>{{ $keyword->is_case_sensitive?'True':'False'}}</td>
                                <td>{{ $keyword->interval}} mins</td>
                                <td>{{ $keyword->avg_response_time}} ms</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-4 col-sm-12">
                            <div class="card card-statistic-3 bg-custom-light">
                                <div>
                                    <div class="card-icon shadow-custom">
                                        <i class="fas fa-arrow-circle-up"></i>
                                    </div>
                                    <div class="card-wrap">
                                        <div class="card-header">
                                            <h4>{{ __('Up Time') }}</h4>
                                        </div>
                                        <div class="card-body">
                                            {{ $upTime }}%
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="card card-statistic-3 bg-custom-light">
                                <div>
                                    @if($keyword->previous_status == 'up')
                                    <div class="card-icon shadow-custom">
                                        <i class="fas fa-check-circle"></i>
                                    </div>
                                    <div class="card-wrap">
                                        <div class="card-header">
                                            <h4>{{ __('Current Status') }}</h4>
                                        </div>
                                        <div class="card-body text-success-dark">
                                            {{ __('Up') }}
                                        </div>
                                    </div>
                                    @else
                                    <div class="card-icon shadow-custom">
                                        <i class="fas fa-times-circle"></i>
                                    </div>
                                    <div class="card-wrap">
                                        <div class="card-header">
                                            <h4>{{ __('Current Status') }}</h4>
                                        </div>
                                        <div class="card-body text-danger">
                                            {{ __('Down') }}
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="card card-statistic-3 bg-custom-light">
                                <div>
                                    <div class="card-icon shadow-custom">
                                        <i class="fas fa-arrow-circle-down"></i>
                                    </div>
                                    <div class="card-wrap">
                                        <div class="card-header">
                                            <h4>{{ __('Down Time') }}</h4>
                                        </div>
                                        <div class="card-body">
                                            {{ $downTime }}%
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="card card-statistic-3 bg-custom-light">
                                <div>
                                    <div class="card-icon shadow-custom">
                                        <i class="fas fa-bolt"></i>
                                    </div>
                                    <div class="card-wrap">
                                        <div class="card-header">
                                            <h4>{{ __('Avg. Response Time') }}</h4>
                                        </div>
                                        <div class="card-body">
                                            {{ $avgResponseTime }} ms
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="card card-statistic-3 bg-custom-light">
                                <div>
                                    <div class="card-icon shadow-custom">
                                        <i class="fas fa-calendar-check"></i>
                                    </div>
                                    <div class="card-wrap">
                                        <div class="card-header">
                                            <h4>{{ __('Last Checked') }}</h4>
                                        </div>
                                        <div class="card-body">
                                            {{$lastChecked ? $lastChecked->diffForHumans() : "Not checked yet"}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="inline-block">{{ __('Last Logs') }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('#') }}</th>
                                    <th>{{ __('Response Time') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Down time') }}</th>
                                    <th>{{ __('Checked at') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($lastLogs as $id => $logs)
                                <tr class="text-center">
                                    <td>{{ $id+1 }}</td>
                                    <td>{{ $logs->response_time }} ms</td>
                                    <td>
                                        @if ($logs->is_up == true)
                                        {{ __('Up') }}
                                        @else
                                        {{ __('Down') }}
                                        @endif
                                    </td>
                                    <td>{{ $logs->down_time }} {{ __('minutes') }}</td>
                                    <td>{{$logs->created_at->diffForHumans()}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection