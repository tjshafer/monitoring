@extends('layouts.new_theme')

@section('content')

<div class="section-header">
  <h1>{{ __('Keyword Monitor') }}</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
    <div class="breadcrumb-item"><a href="{{ route('keyword_monitor.index') }}">{{ __('Keyword Monitor') }}</a></div>
    <div class="breadcrumb-item">{{ __('List of Keywords') }}</div>
  </div>
</div>

<div class="section-body">
  <h2 class="section-title">{{ __('List of Keywords') }}</h2>
  <p class="section-lead">
    {{__('Total number of keywords')}}: {{ $keywords->total() }}
  </p>

  <div class="row">
    <div class="col-12">
      @include('common.demo')
      @include('common.user')
      @include('common.errors')
      <div class="card">
        <div class="card-header">
          <h4 class="d-sm-inline-block">{{ __('List of Keywords') }}</h4>
          <div class="inline-block float-sm-right mt-2 mt-sm-0">
            <a href="{{ route('keyword_monitor.create') }}" class="btn btn-icon btn-custom"><i
                class="far fa-edit"></i>{{ __('Add Keyword') }}</a>
           
          </div>
        </div>
        <div class="card-body">

          <div class="search-bar">
            <form action="/keyword_monitor" method="get">
              <div class="input-group mb-2">
                <input type="text" name="search" class="form-control search-bar-input" placeholder="Search"
                  value="{{ request()->input('search') }}">
                <div class="input-group-btn">
                  <button class="btn btn-custom search-bar-button"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>

          @if (!count($keywords))
          <div class="card-body">
            <div class="empty-state" data-height="400">
              <div class="empty-state-icon bg-danger">
                <i class="fas fa-question"></i>
              </div>
              <h2>{{ __('No Keywords added yet under') }} !!</h2>
              <p class="lead">
                {{ __('Sorry we cant find any data, to get rid of this message, make at least 1 entry') }}.
              </p>
              <a href="{{ route('keyword_monitor.create') }}" class="btn btn-custom mt-4">{{ __('Create new One') }}</a>
            </div>
          </div>

          @else
          <br>
          
          @if($keyExist>0)
            <h6 class="d-sm-inline-block">{{ __('Case Keywords Exist') }}</h6>
          <br>
          <div class="table-responsive">
            <table class="table table-striped" id="table-1">
              <thead>
                <tr class="text-center text-capitalize">
                  <th>{{ __('Name') }}</th>
                  <th>{{ __('URL') }}</th>
                  <th>{{ __('Keyword') }}</th>
                  <th>{{__('Status')}}</th>
                  <th>{{__('Details')}}</th>
                  <th>{{__('Last Checked')}}</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($keywords as $keyword)
                  @if($keyword->alert_when=='keyword_exist')
                    <tr class="text-center">
                      <td class="text-left">{{ $keyword->name }}</td>
                      <td>{{ $keyword->url }}</td>
                      <td>{{ Str::of($keyword->keyword)->limit(), 255 }}</td>
                      <td>
                        @if ($keyword->previous_status == 'up')
                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                        @elseif (($keyword->previous_status == 'down'))
                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                        @else
                        {{ __('NA') }}
                        @endif
                      </td>
                      <td><a href="{{ route('keyword_monitor.show', [$keyword->uuid]) }}" class="btn bg-transparent">
                          <i class="fas fa-eye text-primary"></i></a></td>
                      <td>{{ $keyword->updated_at -> diffForHumans()}}</td>
                      <td class="justify-content-center form-inline">
                        <a href="{{ route('keyword_monitor.edit', [$keyword->uuid]) }}" class="btn btn-sm bg-transparent"><i
                            class="far fa-edit text-primary" aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                        @if (config('app.env') != 'demo')
                        <form action="{{ route('keyword_monitor.destroy', [$keyword->uuid]) }}" method="POST">
                          @method('DELETE')
                          @csrf
                          <button class="btn btn-sm bg-transparent" onclick="return confirm('Are you sure?')">
                            <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                          </button>
                        </form>
                        @endif
                      </td>
                    </tr>
                    @endif
                    
                @endforeach
              </tbody>
            </table>
            
          </div>
          <br>
          @endif
          @if($keyNotExist>0)
            <h6 class="d-sm-inline-block">{{ __('Case Keywords Do Not Exist') }}</h6>
          <br>
          <div class="table-responsive">
            <table class="table table-striped" id="table-1">
              <thead>
                <tr class="text-center text-capitalize">
                  <th>{{ __('Name') }}</th>
                  <th>{{ __('URL') }}</th>
                  <th>{{ __('Keyword') }}</th>
                  <th>{{__('Status')}}</th>
                  <th>{{__('Details')}}</th>
                  <th>{{__('Last Checked Time')}}</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($keywords as $keyword)
                  @if($keyword->alert_when=='keyword_not_exist')
                    <tr class="text-center">
                      <td class="text-left">{{ $keyword->name }}</td>
                      <td>{{ $keyword->url }}</td>
                      <td>{{ Str::of($keyword->keyword)->limit(), 255 }}</td>
                      <td>
                        @if ($keyword->previous_status == 'up')
                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                        @elseif (($keyword->previous_status == 'down'))
                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                        @else
                        {{ __('NA') }}
                        @endif
                      </td>
                      <td><a href="{{ route('keyword_monitor.show', [$keyword->uuid]) }}" class="btn bg-transparent">
                          <i class="fas fa-eye text-primary"></i></a></td>
                      <td>{{ $keyword->updated_at -> diffForHumans()}}</td>
                      <td class="justify-content-center form-inline">
                        <a href="{{ route('keyword_monitor.edit', [$keyword->uuid]) }}" class="btn btn-sm bg-transparent"><i
                            class="far fa-edit text-primary" aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                        @if (config('app.env') != 'demo')
                        <form action="{{ route('keyword_monitor.destroy', [$keyword->uuid]) }}" method="POST">
                          @method('DELETE')
                          @csrf
                          <button class="btn btn-sm bg-transparent" onclick="return confirm('Are you sure?')">
                            <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                          </button>
                        </form>
                        @endif
                      </td>
                    </tr>
                    @endif
                    
                @endforeach
              </tbody>
            </table>
            <br>
            {{ $keywords->appends($request->all())->links("pagination::bootstrap-4") }}
          </div>
          @endif
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection