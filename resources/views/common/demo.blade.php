@if (config('app.env') == 'demo')
<div class="alert alert-warning demo-alert text-center" role="alert">
<strong>{{ __('Note: ') }}</strong> {{ __('This is a demo version of Monitoring.Zone. Some features will not work in this version') }}
</div>
@endif