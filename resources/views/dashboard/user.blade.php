@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Dashboard') }}</h1>
</div>

@include('common.user')
<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            {{-- <div class="card-header">
                <h3 class="text-custom">{{ __('Servers') }}</h3>
        </div> --}}
        <div>
            <a href="{{ route('server_monitor.index') }}">
                <div class="card-icon bg-custom">
                    <i class="fas fa-server"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total Servers') }}</h4>
                </div>
                <div class="card-body">
                    {{ $servers }}
                </div>
            </div>
        </div>
        <div>
            <a href="{{ route('server_monitor.index') }}">
                <div class="card-icon bg-success-dark">
                    <i class="fas fa-arrow-alt-circle-up"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total Up Servers') }}</h4>
                </div>
                <div class="card-body">
                    {{ $upServer }}
                </div>
            </div>
        </div>
        <div>
            <a href="{{ route('server_monitor.index') }}">
                <div class="card-icon bg-danger">
                    <i class="fas fa-arrow-alt-circle-down"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total Down Servers') }}</h4>
                </div>
                <div class="card-body">
                    {{ $downServer }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
        <div>
            <a href="{{ route('webpage_monitor.index') }}">
                <div class="card-icon bg-custom">
                    <i class="fas fa-globe"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total Webpages') }}</h4>
                </div>
                <div class="card-body">
                    {{ $pages }}
                </div>
            </div>
        </div>
        <div>
            <a href="{{ route('webpage_monitor.index') }}">
                <div class="card-icon bg-success-dark">
                    <i class="fas fa-arrow-alt-circle-up"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total Up Webpages') }}</h4>
                </div>
                <div class="card-body">
                    {{ $upPage }}
                </div>
            </div>
        </div>
        <div>
            <a href="{{ route('webpage_monitor.index') }}">
                <div class="card-icon bg-danger">
                    <i class="fas fa-arrow-alt-circle-down"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total Down Webpages') }}</h4>
                </div>
                <div class="card-body">
                    {{ $downPage }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
        <div>
            <a href="{{ route('api_monitor.index') }}">
                <div class="card-icon bg-custom">
                    <i class="fab fa-quinscape"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total APIs') }}</h4>
                </div>
                <div class="card-body">
                    {{ $apis }}
                </div>
            </div>
        </div>
        <div>
            <a href="{{ route('api_monitor.index') }}">
                <div class="card-icon bg-success-dark">
                    <i class="fas fa-arrow-alt-circle-up"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total Up APIs') }}</h4>
                </div>
                <div class="card-body">
                    {{ $upApi }}
                </div>
            </div>
        </div>
        <div>
            <a href="{{ route('api_monitor.index') }}">
                <div class="card-icon bg-danger">
                    <i class="fas fa-arrow-alt-circle-down"></i>
                </div>
            </a>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>{{ __('Total Down APIs') }}</h4>
                </div>
                <div class="card-body">
                    {{ $downApi }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="col-lg-4 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
        <div>
            <a href="{{ route('website_monitor.index') }}">
<div class="card-icon bg-custom">
    <i class="fas fa-window-maximize"></i>
</div>
</a>
<div class="card-wrap">
    <div class="card-header">
        <h4>{{ __('Total Websites') }}</h4>
    </div>
    <div class="card-body">
        {{ $websites }}
    </div>
</div>
</div>
<div>
    <a href="{{ route('website_monitor.index') }}">
        <div class="card-icon bg-danger">
            <i class="fas fa-times-circle"></i>
        </div>
    </a>
    <div class="card-wrap">
        <div class="card-header">
            <h4>{{ __('Domain Expired Webpages') }}</h4>
        </div>
        <div class="card-body">
            {{ $domainExpired }}
        </div>
    </div>
</div>
<div>
    <a href="{{ route('website_monitor.index') }}">
        <div class="card-icon bg-danger">
            <i class="fas fa-times-circle"></i>
        </div>
    </a>
    <div class="card-wrap">
        <div class="card-header">
            <h4>{{ __('Ssl Expired Webpages') }}</h4>
        </div>
        <div class="card-body">
            {{ $sslExpired }}
        </div>
    </div>
</div>
</div>
</div> --}}

<div class="col-12">
    @if ($service)
    @if ($service->status_id == "1" && $service->orders->status == "active")
    <div class="card  bg-success-dark">
        <div class="hero align-items-center text-white">
            <div class="hero-inner text-center">
                <h2>{{ __("You have activated") }} {{ $service->plans->name }} {{ __('subscription for') }}
                    {{ $service->pricing->term }} {{ __($service->pricing->period) }}.</h2>
                <p class="lead">
                    {{ __("Your plan will expire on") }}
                    {{ \Illuminate\Support\Carbon::parse($service->expiry_date)->format('d-M-Y') }}.
                </p>
            </div>
        </div>
    </div>
    @elseif ($service->status_id == "1" && $service->orders->status != "active")
    <div class="card  bg-danger">
        <div class="hero align-items-center text-white">
            <div class="hero-inner text-center">
                <h2>{{ __("Your order is not confirmed") }}.</h2>
                <p class="lead">
                    {{ __("Your order for") }} {{ __($latestOrder->pricing->plans->name) }} {{ __('subscription for') }}
                    {{ $service->pricing->term }} {{ __($service->pricing->period) }}
                    {{ __('is not confirmed. Please wait sometimes or contact support') }}.
                </p>
            </div>
        </div>
    </div>
    @else
    <div class="card  bg-danger">
        <div class="hero align-items-center text-white">
            <div class="hero-inner text-center">
                <h2>{{ __("Your plan is expired") }}.</h2>
                <p class="lead">
                    {{ __('Please subscribe to a plan to use our service') }}.<a
                        href="{{ route('available_plans.index') }}" class="btn bg-transparent text-primary">
                        {{ __('Click here') }}.</a>
                </p>
            </div>
        </div>
    </div>
    @endif
    @else
    <div class="card  bg-danger">
        <div class="hero align-items-center text-white">
            <div class="hero-inner text-center">
                <h2>{{ __('You have no subscriptions yet') }}</h2>
                <p class="lead">
                    {{ __('Please subscribe to a plan to use our service') }}.<a
                        href="{{ route('available_plans.index') }}" class="btn bg-transparent text-primary">
                        {{ __('Click here') }}.</a>
                </p>
            </div>
        </div>
    </div>
    @endif
</div>

@if ($service)
@if ($latestOrder && $latestOrder->status == "pending" && $latestOrder->parent_order_id != 0)
<div class="col-12">
    <div class="card  bg-danger">
        <div class="hero align-items-center text-white">
            <div class="hero-inner text-center">
                <h2>{{ __("Your order is not confirmed") }}.</h2>
                <p class="lead">
                    {{ __("Your order for") }} {{ __($latestOrder->pricing->plans->name) }} {{ __('subscription for') }}
                    {{ $service->pricing->term }} {{ __($service->pricing->period) }}
                    {{ __('is not confirmed. Please wait sometimes or contact support') }}.
                </p>
            </div>
        </div>
    </div>
</div>
@endif
@endif

<div class="col-12">
    <div class="card">
        <div class="card-header mt-3">
            <h3 class="text-custom">{{ __('Incidents Details') }}</h3>
            {{-- <div class="card-header-action">
                    <div class="btn-group">
                        <a href="#" class="btn btn-primary">Week</a>
                        <a href="#" class="btn">Month</a>
                    </div>
                </div> --}}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card card-statistic-2 height-220 bg-custom-light">
                        <div class="card-stats">
                            <div class="card-stats-title bg-custom-light">
                                {{ __('Server Incidents') }}
                            </div>
                            <div class="card-stats-items">
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count text-danger">{{ $pendingServerIncidents }}
                                    </div>
                                    <div class="card-stats-item-label">{{ __('Pending') }}</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label"><a href="{{ route('server_incident.index') }}"
                                            class="btn btn-outline-custom btn-sm" role="button"
                                            aria-pressed="true">{{ __('View') }}</a></div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count text-success-dark">
                                        {{ $resolvedServerIncidents }}</div>
                                    <div class="card-stats-item-label">{{ __('Resolved') }}</div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="card-icon shadow-primary bg-custom">
                                <i class="fas fa-exclamation-circle"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>{{ __('Total Incidents') }}</h4>
                                </div>
                                <div class="card-body">
                                    {{ $serverIncidentsCount }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card card-statistic-2 height-220 bg-custom-light">
                        <div class="card-stats">
                            <div class="card-stats-title bg-custom-light">
                                {{ __('Webpage Incidents') }}
                            </div>
                            <div class="card-stats-items">
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count  text-danger">{{ $pendingWebpageIncidents }}
                                    </div>
                                    <div class="card-stats-item-label">{{ __('Pending') }}</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label"><a href="{{ route('webpage_incident.index') }}"
                                            class="btn btn-outline-custom btn-sm" role="button"
                                            aria-pressed="true">{{ __('View') }}</a>
                                    </div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count text-success-dark">
                                        {{ $resolvedWebpageIncidents }}</div>
                                    <div class="card-stats-item-label">{{ __('Resolved') }}</div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="card-icon shadow-primary bg-custom">
                                <i class="fas fa-exclamation-circle"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>{{ __('Total Incidents') }}</h4>
                                </div>
                                <div class="card-body">
                                    {{ $webpageIncidentsCount }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card card-statistic-2 height-220 bg-custom-light">
                        <div class="card-stats">
                            <div class="card-stats-title bg-custom-light">
                                {{ __('API Incidents') }}
                            </div>
                            <div class="card-stats-items">
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count  text-danger">{{ $pendingApiIncidents }}</div>
                                    <div class="card-stats-item-label">{{ __('Pending') }}</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label"><a href="{{ route('api_incident.index') }}"
                                            class="btn btn-outline-custom btn-sm" role="button"
                                            aria-pressed="true">{{ __('View') }}</a>
                                    </div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count text-success-dark">{{ $resolvedApiIncidents }}
                                    </div>
                                    <div class="card-stats-item-label">{{ __('Resolved') }}</div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="card-icon shadow-primary bg-custom">
                                <i class="fas fa-exclamation-circle"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>{{ __('Total Incidents') }}</h4>
                                </div>
                                <div class="card-body">
                                    {{ $apiIncidentsCount }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection