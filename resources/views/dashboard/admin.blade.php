@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Dashboard') }}</h1>
</div>

@include('common.admin')
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2 height-220">
            <div class="card-stats">
                <div class="card-stats-title">{{ __('Order Statistics') }}
                </div>
                <div class="card-stats-items">
                    <div class="card-stats-item">
                        <div class="card-stats-item-count">{{ $pendingOrders }}</div>
                        <div class="card-stats-item-label">{{ __('Pending') }}</div>
                    </div>
                    <div class="card-stats-item">
                        <div class="card-stats-item-count">{{ $completedOrders }}</div>
                        <div class="card-stats-item-label">{{ __('Completed') }}</div>
                    </div>
                    <div class="card-stats-item">
                        <div class="card-stats-item-count">{{ $activeOrders }}</div>
                        <div class="card-stats-item-label">{{ __('Active') }}</div>
                    </div>
                </div>
            </div>
            <div>
                <a href="{{ route('orders.index') }}">
                    <div class="card-icon shadow-primary bg-custom">
                        <i class="fas fa-archive"></i>
                    </div>
                </a>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>{{ __('Total Orders') }}</h4>
                    </div>
                    <div class="card-body">
                        {{ $orders }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2 height-220">
            <div class="card-stats">
                <div class="card-stats-title">{{ __('Paid Orders') }}
                </div>
                <div class="card-stats-items">
                    <div class="card-stats-item">
                        <div class="card-stats-item-count">{{ $activePaidOrders }}</div>
                        <div class="card-stats-item-label">{{ __('Active') }}</div>
                    </div>
                    <div class="card-stats-item">
                    </div>
                    <div class="card-stats-item">
                        <div class="card-stats-item-count">{{ $inactivePaidOrders }}</div>
                        <div class="card-stats-item-label">{{ __('Inactive') }}</div>
                    </div>
                </div>
            </div>
            <div>
                <a href="{{ route('orders.index') }}">
                    <div class="card-icon shadow-primary bg-custom">
                        <i class="fas fa-archive"></i>
                    </div>
                </a>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>{{ __('Total Paid Orders') }}</h4>
                    </div>
                    <div class="card-body">
                        {{ $paidOrders }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2 height-220">
            <div class="card-stats">
                <div class="card-stats-title">{{ __('Trial Orders') }}
                </div>
                <div class="card-stats-items">
                    <div class="card-stats-item">
                        <div class="card-stats-item-count">{{ $activeTrialOrders }}</div>
                        <div class="card-stats-item-label">{{ __('Active') }}</div>
                    </div>
                    <div class="card-stats-item">
                    </div>
                    <div class="card-stats-item">
                        <div class="card-stats-item-count">{{ $inactiveTrialOrders }}</div>
                        <div class="card-stats-item-label">{{ __('Inactive') }}</div>
                    </div>
                </div>
            </div>
            <div>
                <a href="{{ route('orders.index') }}">
                    <div class="card-icon shadow-primary bg-custom">
                        <i class="fas fa-archive"></i>
                    </div>
                </a>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>{{ __('Total Trial Orders') }}</h4>
                    </div>
                    <div class="card-body">
                        {{ $trialOrders }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12">
    <div class="card  bg-success-dark">
        <div class="hero align-items-center text-white">
            <div class="hero-inner text-center">
                <h2>{{ __('Congratulations') }}</h2>
                @if($users != 0)
                <p class="lead">
                    {{ $users }}
                    {{ __('users have successfully registered with our system. You can view the users with below link') }}.
                </p>
                <div class="mt-4">
                    <a href="{{ route('users.index') }}" class="btn btn-outline-white btn-lg btn-icon icon-left"><i
                            class="fas fa-sign-in-alt"></i> {{ __('View User') }}</a>
                </div>
                @else
                <p class="lead">
                    {{ __('You have successfully installed the app. Good luck with sales') }}.
                </p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="card height-450">
            <div class="card-header">
                <h4>{{ __('Income Statistics') }}</h4>
            </div>
            <div class="card-body">
                <div class="row  bg-secondary height-190">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 pt-4 text-center">
                        <div>
                            <h5>{{ __('Total Income') }}</h5>
                        </div>
                        <div class="card-body">
                            <h1>${{ number_format($totalIncome, 2) }}</h1>
                            <a href="{{ route('orders.index') }}">{{ __('view orders') }}</a>
                        </div>
                    </div>
                </div>
                <div class="statistic-details mt-sm-4">
                    <div class="statistic-details-item">
                        <span class="text-muted"><span class="text-primary"><i class="fas fa-square"></i></span>
                            {{ $totalIncome != 0 ? round($todayIncome*100/$totalIncome, 2) : 0}}%</span>
                        <div class="detail-value">${{ number_format($todayIncome, 2) }}</div>
                        <div class="detail-name">{{ __('Todays Sales') }}</div>
                    </div>
                    <div class="statistic-details-item">
                        <span class="text-muted"><span class="text-info"><i class="fas fa-square"></i></span>
                            {{ $totalIncome != 0 ? round($thisWeekIncome*100/$totalIncome, 2) : 0}}%</span>
                        <div class="detail-value">${{ number_format($thisWeekIncome, 2) }}</div>
                        <div class="detail-name">{{ __('This Weeks Sales') }}</div>
                    </div>
                    <div class="statistic-details-item">
                        <span class="text-muted"><span class="text-success-dark"><i class="fas fa-square"></i></span>
                            {{ $totalIncome != 0 ? round($thisMonthIncome*100/$totalIncome, 2) : 0 }}%</span>
                        <div class="detail-value">${{ number_format($thisMonthIncome, 2) }}</div>
                        <div class="detail-name">{{ __('This Months Sales') }}</div>
                    </div>
                    <div class="statistic-details-item">
                        <span class="text-muted"><span class="text-custom"><i class="fas fa-square"></i></span>
                            {{ $totalIncome != 0 ? round($thisYearIncome*100/$totalIncome, 2) : 0 }}%</span>
                        <div class="detail-value">${{ number_format($thisYearIncome, 2) }}</div>
                        <div class="detail-name">{{ __('This Years Sales') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card  bg-admin-custom-card height-450">
            <div class="col-12 pt-5">
                <div class="card card-statistic-1">
                    <div>
                        <div class="card-icon bg-custom">
                            <i class="fas fa-server"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>{{ __('Total Servers') }}</h4>
                            </div>
                            <div class="card-body">
                                {{ $servers }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card card-statistic-1">
                    <div>
                        <div class="card-icon bg-custom">
                            <i class="fas fa-globe"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>{{ __('Total Webpages') }}</h4>
                            </div>
                            <div class="card-body">
                                {{ $pages }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card card-statistic-1">
                    <div>
                        <div class="card-icon bg-custom">
                            <i class="fas fa-dot-circle"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>{{ __('Total APIs') }}</h4>
                            </div>
                            <div class="card-body">
                                {{ $apis }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-12">
                <div class="card card-statistic-1">
                    <div>
                        <div class="card-icon bg-custom">
                            <i class="fas fa-dot-circle"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>{{ __('Total Websites') }}</h4>
                            </div>
                            <div class="card-body">
                                {{ $websites }}
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</div>
</div>
@endsection