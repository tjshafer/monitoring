<div>
    <b>{{ __('Hello :name', ['name' => $user->first_name]) }},</b><br/>
    {{ __('Your plan is expired') }}

    <br/>
    {{ __('To renew the plan, please login with your details at :app_url',['app_url' => config('app.url')]) }}

    <br/>
    <br/>
    {{ __('Regards') }}
    <br/>
    <br/>
    {{ __(':admin', ['admin' => config('mail.from.name')]) }}
</div>