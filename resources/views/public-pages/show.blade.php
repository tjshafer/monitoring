@extends('layouts.public')

@section('content')

<div class="col-10 offset-1 mt-5">
    <div class="section-header">
        <h1>{{ __('Public Page') }}</h1>
    </div>

    <div class="section-body">
        <h2 class="section-title">{{ __('API Details') }}</h2>
        <p class="section-lead">
            {{ __('Up time percentage of the API is listed below') }}.
        </p>

        <div class="row">
            <div class="col-12">
                @include('common.demo')
                @include('common.errors')
                <div class="card">
                    <div class="card-body mt-4">
                        <div class="table-responsive">
                            <table class="table table-striped bg-gray-shade" id="table-1">
                                <thead>
                                    <tr class="text-center">
                                        <th scope="col">{{ __('Monitor name') }}</th>
                                        <th scope="col">{{ __('Monitor type') }}</th>
                                        @if ($publicPage->monitors->expected_response_code)
                                        <th scope="col">{{ __('Response code') }}</th>
                                        @endif
                                        <th scope="col">{{ __('Week status') }}</th>
                                        @foreach ($reports as $report)
                                        <th scope="col">{{ Illuminate\Support\Carbon::parse($report['date'])->format('d-M') }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $publicPage->name }}</td>
                                        <td class="text-capitalize">{{ $publicPage->monitors->type }}</td>
                                        @if ($publicPage->monitors->expected_response_code)
                                        <td>{{ $publicPage->monitors->expected_response_code }}</td>
                                        @endif
                                        <td>{{ $weekStatus }}%</td>
                                        @foreach ($reports as $report)
                                        <td>
                                            @if($createdDate > $report['date'])
                                            NA
                                            @else
                                            {{ $report['status'] }}%
                                            @endif
                                        </td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row mt-4">
                            <div class="col-lg-4 col-sm-12">
                                <div class="card card-statistic-3 bg-custom-light">
                                    <div>
                                        <div class="card-icon shadow-custom">
                                            <i class="fas fa-arrow-circle-up"></i>
                                        </div>
                                        <div class="card-wrap">
                                            <div class="card-header">
                                                <h4>{{ __('Up Time') }} %</h4>
                                            </div>
                                            <div class="card-body">
                                                {{ $upTime }}%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="card card-statistic-3 bg-custom-light">
                                    <div>
                                        <div class="card-icon shadow-custom">
                                            <i class="fas fa-bolt"></i>
                                        </div>
                                        <div class="card-wrap">
                                            <div class="card-header">
                                                <h4>{{ __('Avg. response time') }}</h4>
                                            </div>
                                            <div class="card-body">
                                                {{ $avgResponseTime }} ms
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="card card-statistic-3 bg-custom-light">
                                    <div>
                                        <div class="card-icon shadow-custom">
                                            <i class="fas fa-exclamation-circle"></i>
                                        </div>
                                        <div class="card-wrap">
                                            <div class="card-header">
                                                <h4>{{ __('Incident count') }}</h4>
                                            </div>
                                            <div class="card-body">
                                                {{ count($incidents) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-body">
        <h2 class="section-title">{{ __('Status History') }} - {{ __('Incidents') }}</h2>
        <p class="section-lead">
            {{__('Total number of incidents reported')}}: {{ count($incidents) }}
        </p>
        <div class="row">
            <div class="col-12 mt-4">
                <div class="activities">
                    @if (count($incidents))
                    @foreach ($incidents as $incident)
                    <div class="activity">
                        @if($incident->status == true)
                        <div class="activity-icon bg-success text-white shadow-custom">
                            <i class="far fa-check-circle fs-2x"></i>
                        </div>
                        @else
                        <div class="activity-icon bg-danger text-white shadow-custom">
                            <i class="far fa-times-circle fs-2x"></i>
                        </div>
                        @endif
                        <div class="activity-detail">
                            <div class="mb-2">
                                <span class="text-job text-primary">{{ $incident->created_at->diffForHumans() }}</span>
                                <span class="bullet"></span>
                                <span class="font-weight-bold">{!! strip_tags($incident->description) !!}</span>
                            </div>
                            @foreach ($incidentDetails[$incident->uuid] as $details)
                            <p><span class="font-weight-bold text-capitalize">{{ $details->status }}:
                                </span>{!! strip_tags($details->description) !!}. <span
                                    class="text-job text-primary">{{ $details->created_at->diffForHumans() }}</span></p>
                            @endforeach
                            <p><span class="font-weight-bold text-capitalize">{{ __('Investigating') }}:
                                </span>{!! strip_tags($incident->description) !!}.</p>

                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="activity">
                        <div class="activity-icon bg-success-dark text-white shadow-custom">
                            <i class="far fa-smile fs-2x"></i>
                        </div>
                        <div class="activity-detail">
                            <div class="mb-2">
                                <span class="font-weight-bold">{{ __('There is no incidents yet') }}</span>
                            </div>

                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection