@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Server Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('server_monitor.index') }}">{{ __('Server Monitor') }}</a></div>
        <div class="breadcrumb-item">{{ __('Edit Server') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Edit Server') }}</h2>
    <p class="section-lead">{{ __('You can update a server from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Edit Server') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('server_monitor.update', $server->uuid) }}">
                        @csrf

                        @method('PUT')

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Name') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name', $server->name) }}" autocomplete="name" autofocus>

                                @error('name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                {{ __('Here you can enter a name of your choice for the server') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Server Address') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="address" type="text"
                                            class="form-control @error('url') is-invalid @enderror"
                                            name="url" value="{{old('url', $server->url)}}" autocomplete="address"
                                            autofocus>

                                @error('url')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="address_help1" class="form-text text-muted">
                                    {{ __('eg1: google.com (without https or http)') }}
                                </small>
                                <small id="address_help2" class="form-text text-muted">

                                    {{ __('eg2: 134.88.9.1') }}
                                </small>
                                <small id="address_help3" class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Server Address can be website domain or IP addres.') }}
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Port') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="port" type="text" class="form-control @error('port') is-invalid @enderror"
                                    name="port" value="{{old('port', $server->port)}}" autocomplete="port" autofocus>

                                @error('port')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="port_help1" class="form-text text-muted">
                                    {{ __('eg : 80') }}
                                </small>
                                <small id="port_help1" class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you have to enter to which port we have to connect. It should be some numbers.') }}
                                </small>
                                <small id="port_help1" class="form-text text-muted">
                                    <a target="_blank"
                                        href="https://web.mit.edu/rhel-doc/4/RH-DOCS/rhel-sg-en-4/ch-ports.html">
                                        {{ __('Please see list of port') }}
                                    </a>
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Ping interval') }}({{ __('in minutes') }}):*</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric" id="interval" name="interval">
                                    @foreach ($intervals as $interval)
                                    @if ($interval->interval == old('interval', $server->interval))
                                    <option selected value="{{ $interval->interval }}">
                                        {{ __($interval->display_name) }}</option>
                                    @else
                                    <option value="{{ $interval->interval }}">{{ __($interval->display_name) }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>

                                @error('interval')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select the time interval you want to check the status of the server') }}.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Server Email') }}:</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="email" type="text"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email', $server->email) }}" autocomplete="email" autofocus>

                                @error('email')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="email_help1" class="form-text text-muted">
                                    {{ __('eg: server1@domain.com') }}
                                </small>
                                <small id="Server email help-2" class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can enter email id, To this email id server down emails will be sent.') }}
                                    <br>
                                    {{ __('If no email is mentioned here, server down mail will be sent to') }}<br />
                                    {{ Auth::user()->email }}
                                </small>
                            </div>
                        </div>

                        @if ($testFailed = Session::get('test_failed'))
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Skip test connection') }}:*</label>
                            <div class="col-sm-6 col-md-6 form-inline">
                                <input id="skip_test_connection" type="checkbox" class="form-control is-invalid"
                                    name="skip_test_connection" value="1">
                            </div>
                        </div>
                        @endif

                        @if (config('app.env') != 'demo')
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Update') }}</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection