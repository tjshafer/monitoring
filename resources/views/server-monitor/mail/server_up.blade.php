<div>
  <b>{{ __('Hello :name', ['name' => $server->user->first_name]) }},</b><br/>
  {{ __('Server up email for :name', ['name' => $server->name]) }}
  <br/>
  {{ __('Server address :server_address', ['server_address' => $server->url]) }}
  <br/>
  {{ __('Port: :port', ['port' => $server->port]) }}
  <br/>
  {{ __('Time: :time', ['time' => \Illuminate\Support\Carbon::now()->toDateTimeString()]) }}

  <br/>
  <br/>
  {{ __('Regards') }}
  <br/>
  {{ __(':admin', ['admin' => config('mail.from.name')]) }}
</div>