@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Alert Manager') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="">{{ __('Dashboard') }}</a></div>
        
        <div class="breadcrumb-item">{{ __('Alert Manager') }}</div>
    </div>
</div>
<div class="section-body">
  <h2 class="section-title">{{ __('Alert Manager') }}</h2>
  <p class="section-lead">
  </p>

  <div class="row">
    <div class="col-12">
      @include('common.demo')
      @include('common.errors')
      <div class="card">
        <div class="card-header">
          <h4 class="d-sm-inline-block">{{ __('Alert Manager') }}</h4>
        </div>
        <div class="card-body">
                    <form method="POST" action="">
                        @csrf


                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Global Emails') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea class="form-control @error('emails') is-invalid @enderror" 
                                id="emails" name="emails" rows=6>{{ $emails != null ? $emails->value : ''}}</textarea>

                                @error('emails')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                {{ __('Enter multiple emails seperated by commas') }}.
                                </small>
                            </div>
                        </div>
                        @if(count($twilio))
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Global SMS Numbers') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea class="form-control @error('numbers') is-invalid @enderror" 
                                id="numbers" name="numbers" required rows=6>{{ $numbers != null ? $numbers->value : ''}}</textarea>

                                @error('numbers')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                {{ __('Enter multiple numbers seperated by commas') }}.
                                </small>
                            </div>
                        </div>
                        @endif
                        
                       
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>

          
        </div>
      </div>
    </div>
  </div>
</div>
@endsection