@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Profile') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item">{{ __('Profile') }}</div>
    </div>
</div>
<div class="section-body">
    <h2 class="section-title">{{ __('Hi') }}, {{ $admin->first_name }}!</h2>
    <p class="section-lead">
        {{ __('Change information about yourself on this page') }}.
    </p>
    
    @include('common.demo')
    @include('common.admin')
    @include('common.errors')
    <div class="row mt-sm-4">
        <div class="col-12 col-md-12 col-lg-4">
            <div class="card profile-widget">
                <div class="profile-widget-header">
                    <img alt="image" src="/images/avatar-1.png" class="rounded-circle profile-widget-picture">
                </div>
                <div class="profile-widget-description">
                    <div class="profile-widget-name text-capitalize">{{ $admin->first_name }} {{ $admin->last_name }}</div>
                    <div class="ml-2">
                        <div>{{ $admin->email }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-8 pt-lg-5-custom">
            <div class="card">
                <form method="POST" action="{{ route('adminProfileUpdate') }}">
                    @csrf
                    <div class="card-header">
                        <h4>{{ __('Edit Profile') }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label>{{ __('First Name') }}*</label>
                                <input id="first_name" type="text"
                                    class="form-control @error('first_name') is-invalid @enderror" name="first_name"
                                    value="{{ old('first_name', $admin->first_name) }}" autocomplete="first_name" autofocus>
                                @error('first_name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label>{{ __('Last Name') }}*</label>
                                <input id="last_name" type="text"
                                    class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                    value="{{ old('last_name', $admin->last_name) }}" autocomplete="last_name" autofocus>
                                @error('last_name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label>{{ __('Email') }}*</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email', $admin->email) }}" autocomplete="name" autofocus>
                                @error('email')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label>{{ __('Old password') }}*</label>
                                <input id="old_password" type="password"
                                    class="form-control @error('old_password') is-invalid @enderror" name="old_password"
                                    value="" autocomplete="old_password" autofocus
                                    placeholder="{{ __('Enter if you want to change') }}">
                                @error('old_password')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label>{{ __('New password') }}*</label>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    value="" autocomplete="password" autofocus>
                                @error('password')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6 col-12">
                                <label>{{ __('Confirm password') }}*</label>
                                <input id="c_password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="c_password"
                                    value="" autocomplete="c_password" autofocus>
                                @error('c_password')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    @if (config('app.env') != 'demo')
                    <div class="card-footer text-right">
                        <button class="btn btn-custom">{{ __('Save Changes') }}</button>
                    </div>
                    @endif
                </form>
            </div>
        </div>

    </div>
</div>
@endsection