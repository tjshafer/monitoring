@extends('layouts.new_theme')

@section('content')

<div class="section-header">
  <h1>{{ __('DNS Monitor for ') }}{{$server->website_domain}}</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
    <div class="breadcrumb-item"><a href="{{ route('dns_server_monitor.index') }}">{{ __('DNS Monitor') }}</a></div>
    <div class="breadcrumb-item"><a href="{{ route('manage', [$server->uuid]) }}">{{ __('Server Details') }}</a></div>
    <div class="breadcrumb-item">{{ __('Edit TXT Records') }}</div>
  </div>
</div>
<div class="section-body">
    <h2 class="section-title">{{ __('Edit TXT Record') }}</h2>
    <p class="section-lead">{{ __('You can edit TXT value from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Edit TXT Record') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('update_txt_record',[$record->uuid]) }}">
                        @csrf


                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Value') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="value" type="text"
                                    class="form-control @error('value') is-invalid @enderror"
                                    name="value" value="{{ old('value',$record->value) }}"
                                    autocomplete="value" autofocus>

                                @error('value')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="ip_address_help1" class="form-text text-muted">
                                    {{ __('Any text that an administrator wants to associate with their domain.') }}
                                </small>
                                <small id="ip_address_help2" class="form-text text-muted">

                                    {{ __(' eg: This is an awesome domain! Definitely not spammy.') }}
                                </small>
                              
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Update') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection