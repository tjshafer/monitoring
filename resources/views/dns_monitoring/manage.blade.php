@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('DNS Monitor for ') }}{{ $server ->website_domain }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('dns_server_monitor.index') }}">{{ __('DNS Monitor') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Server Details') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Server Details') }}</h2>
    <p class="section-lead">
        {{__('Key metrics of')}} {{ $server ->website_domain }}
    </p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.user')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="inline-block row">{{ $server->website_domain }}</h4><hr> -->
                    <div class="row ">
                    <a href="{{route('add_a_record',[$server->uuid])}}" class="btn btn-icon btn-custom  mr-1 col-sm-2"><i
                        class="far fa-edit"></i>{{ __('Add A Record') }}</a>
                    <a href="{{route('add_aaaa_record',[$server->uuid])}}" class="btn btn-icon btn-custom  mr-1 col-sm-2"><i
                        class="far fa-edit"></i>{{ __('Add AAAA Record') }}</a>
                    <a href="{{route('add_cname_record',[$server->uuid])}}" class="btn btn-icon btn-custom mr-1 col-sm-2"><i
                        class="far fa-edit"></i>{{ __('Add CNAME Record') }}</a>
                    <a href="{{route('add_mx_record',[$server->uuid])}}" class="btn btn-icon btn-custom  mr-1 col-sm-2"><i
                        class="far fa-edit"></i>{{ __('Add MX Record') }}</a>
                    <a href="{{route('add_ns_record',[$server->uuid])}}" class="btn btn-icon btn-custom mr-1 col-sm-2"><i
                        class="far fa-edit"></i>{{ __('Add NS Record') }}</a>
                    <a href="{{route('add_txt_record',[$server->uuid])}}" class="btn btn-icon btn-custom  mr-1 col"><i
                        class="far fa-edit"></i>{{ __('Add TXT Record') }}</a>
                    </div>
                    
                </div>
               
    </div>
    @if(count($dns_a_records)+count($dns_aaaa_records)+count($dns_cname_records)+count($dns_mx_records)+count($dns_ns_records)+count($dns_txt_records)==0)
        <div class="card-body">
            <div class="empty-state" data-height="400">
              <div class="empty-state-icon bg-danger">
                <i class="fas fa-question"></i>
              </div>
              <h2>{{ __('No records added! Please add record by clicking a button listed above') }} !!</h2>
              <p class="lead">
                {{ __('Sorry we cant find any data, to get rid of this message, make at least 1 entry') }}.
              </p>
             
            </div>
    </div>  
    @else
    @if(count($dns_a_records)>0)
    <div class="card">
               <div class="card-header">
                    <h4 class="inline-block"> {{ __('A Records') }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('IP Address List') }}</th>
                                    <th>{{__('Resolved IP')}}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Last Checked') }}</th>
                                    <th>{{__('Details')}}</th>
                                    <th></th>
                                    <!-- <th>{{ __('Public page') }}</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($dns_a_records as $dns_a_record)
                                <tr class="text-center">
                                    <td>{{ $dns_a_record->ip_address }}</td>
                                    <td>{{$dns_a_record->last_resolved_ip}}</td>
                                    <td>
                                        @if ( $dns_a_record->previous_status == 'up') 
                                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                                        @elseif ($dns_a_record->previous_status == 'down')
                                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                                        @else
                                        {{ __('NA') }}
                                        @endif
                                    </td>
                                    <td> {{ $dns_a_record->updated_at ->diffForHumans()}} </td>
                                    <td><a href="{{ route('show_a_record', $dns_a_record->uuid) }}" class="btn bg-transparent">
                                    <i class="fas fa-eye text-primary"></i></a></td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('edit_a_record', $dns_a_record->uuid) }}" class="btn btn-sm bg-transparent"><i
                                            class="far fa-edit text-primary" aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('delete_a_record', [$dns_a_record->uuid]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm bg-transparent" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                                        </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    
               </div>
        </div>
        @endif
        @if(count($dns_aaaa_records)>0)
        <div class="card">
               <div class="card-header">
                    <h4 class="inline-block"> {{ __('AAAA Records') }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                        <thead>
                                <tr class="text-center">
                                    <th>{{ __('IP Address List') }}</th>
                                    <th>{{__('Resolved IP')}}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Last Checked') }}</th>
                                    <th>{{__('Details')}}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($dns_aaaa_records as $dns_aaaa_record)
                             <tr class="text-center">
                                    <td>{{ $dns_aaaa_record->ipv6_address }}</td>
                                    <td>{{$dns_aaaa_record->last_resolved_ip}}</td>
                                    <td>
                                        @if ( $dns_aaaa_record->previous_status == 'up') 
                                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                                        @elseif ($dns_aaaa_record->previous_status == 'down')
                                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                                        @else
                                        {{ __('NA') }}
                                        @endif
                                    </td>
                                    <td> {{ $dns_aaaa_record->updated_at ->diffForHumans()}} </td>
                                    <td><a href="{{ route('show_aaaa_record', [$dns_aaaa_record->uuid]) }}" class="btn bg-transparent">
                                    <i class="fas fa-eye text-primary"></i></a></td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('edit_aaaa_record', $dns_aaaa_record->uuid) }}" class="btn btn-sm bg-transparent"><i
                                            class="far fa-edit text-primary" aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('delete_aaaa_record', [$dns_aaaa_record->uuid]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm bg-transparent" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                                        </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

               </div>
        </div>
        @endif
        @if(count($dns_cname_records)>0)
        <div class="card">
               <div class="card-header">
                    <h4 class="inline-block"> {{ __('CNAME Records') }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('Value') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Last Checked') }}</th>
                                    <th>{{__('Details')}}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($dns_cname_records as $dns_cname_record)
                            <tr class="text-center">
                                    <td>{{ $dns_cname_record->value }}</td>
                                    <td>
                                        @if ( $dns_cname_record->previous_status == 'up') 
                                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                                        @elseif ($dns_cname_record->previous_status == 'down')
                                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                                        @else
                                        {{ __('NA') }}
                                        @endif
                                    </td>
                                    <td> {{ $dns_cname_record->updated_at ->diffForHumans()}} </td>
                                    <td><a href="{{ route('show_cname_record', [$dns_cname_record->uuid]) }}" class="btn bg-transparent">
                                    <i class="fas fa-eye text-primary"></i></a></td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('edit_cname_record', $dns_cname_record->uuid) }}" class="btn btn-sm bg-transparent"><i
                                            class="far fa-edit text-primary" aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('delete_cname_record', [$dns_cname_record->uuid]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm bg-transparent" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                                        </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

               </div>
        </div>
        @endif
        @if(count($dns_mx_records)>0)
        <div class="card">
               <div class="card-header">
                    <h4 class="inline-block"> {{ __('MX Records') }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('Priority') }}</th>
                                    <th>{{ __('Exchange')}}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Last Checked') }}</th>
                                    <th>{{__('Details')}}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($dns_mx_records as $dns_mx_record)
                            <tr class="text-center">
                                    <td>{{ $dns_mx_record->priority }}</td>
                                    <td>{{ $dns_mx_record->exchange }}</td>
                                    <td>
                                        @if ( $dns_mx_record->previous_status == 'up') 
                                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                                        @elseif ($dns_mx_record->previous_status == 'down')
                                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                                        @else
                                        {{ __('NA') }}
                                        @endif
                                    </td>
                                    <td> {{ $dns_mx_record->updated_at->diffForHumans() }} </td>
                                    <td><a href="{{ route('show_mx_record', [$dns_mx_record->uuid]) }}" class="btn bg-transparent">
                                    <i class="fas fa-eye text-primary"></i></a></td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('edit_mx_record', $dns_mx_record->uuid) }}" class="btn btn-sm bg-transparent"><i
                                            class="far fa-edit text-primary" aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('delete_mx_record', [$dns_mx_record->uuid]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm bg-transparent" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                                        </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

               </div>
        </div>
        @endif
        @if(count($dns_ns_records)>0)
        <div class="card">
               <div class="card-header">
                    <h4 class="inline-block"> {{ __('NS Records') }} </h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('Value') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Last Checked') }}</th>
                                    <th>{{__('Details')}}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($dns_ns_records as $dns_ns_record)
                             <tr class="text-center">
                                    <td>{{ $dns_ns_record->value }}</td>
                                    <td>
                                        @if ( $dns_ns_record->previous_status == 'up') 
                                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                                        @elseif ($dns_ns_record->previous_status == 'down')
                                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                                        @else
                                        {{ __('NA') }}
                                        @endif
                                    </td>
                                    <td> {{ $dns_ns_record->updated_at->diffForHumans() }} </td>
                                    <td><a href="{{ route('show_ns_record', [$dns_ns_record->uuid]) }}" class="btn bg-transparent">
                                    <i class="fas fa-eye text-primary"></i></a></td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('edit_ns_record', $dns_ns_record->uuid) }}" class="btn btn-sm bg-transparent"><i
                                            class="far fa-edit text-primary" aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('delete_ns_record', [$dns_ns_record->uuid]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm bg-transparent" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                                        </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

               </div>
        </div>
        @endif
        @if(count($dns_txt_records)>0)
        <div class="card">
               <div class="card-header">
                    <h4 class="inline-block"> {{ __('TXT Records') }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('Value') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Last Checked') }}</th>
                                    <th>{{__('Details')}}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($dns_txt_records as $dns_txt_record)
                            <tr class="text-center">
                                    <td>{{ $dns_txt_record->value }}</td>
                                    <td>
                                        @if ( $dns_txt_record->previous_status == 'up') 
                                        <i class="fa fa-circle fa-2x text-success-dark" title="{{ __('Up') }}"></i>
                                        @elseif ($dns_txt_record->previous_status == 'down')
                                        <i class="fa fa-circle fa-2x text-danger" title="{{ __('Down') }}"></i>
                                        @else
                                        {{ __('NA') }}
                                        @endif
                                    </td>
                                    <td> {{ $dns_txt_record->updated_at->diffForHumans() }} </td>
                                    <td><a href="{{ route('show_txt_record', [$dns_txt_record->uuid]) }}" class="btn bg-transparent">
                                    <i class="fas fa-eye text-primary"></i></a></td>
                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('edit_txt_record', $dns_txt_record->uuid) }}" class="btn btn-sm bg-transparent"><i
                                            class="far fa-edit text-primary" aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('delete_txt_record', [$dns_txt_record->uuid]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm bg-transparent" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash text-danger" aria-hidden="true" title="{{ __('Delete') }}"></i>
                                        </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

               </div>
        </div>
        @endif
        @endif
</div>
@endsection