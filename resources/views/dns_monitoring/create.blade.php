@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('DNS Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('dns_server_monitor.index') }}">{{ __('DNS Monitor') }}</a></div>
        <div class="breadcrumb-item">{{ __('Add Server') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Add Server') }}</h2>
    <p class="section-lead">{{ __('You can add a server from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add Server') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('dns_server_monitor.store') }}">
                        @csrf


                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Server Address') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="url" type="text"
                                    class="form-control @error('url') is-invalid @enderror"
                                    name="url" value="{{ old('url') }}"
                                    autocomplete="url" autofocus>

                                @error('url')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="address_help1" class="form-text text-muted">
                                    {{ __('eg1: google.com (without https or http)') }}
                                </small>
                                <small id="address_help2" class="form-text text-muted">

                                    {{ __('eg2: 134.88.9.1') }}
                                </small>
                                <small id="address_help3" class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Server Address can be website domain or IP addres.') }}
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Ping interval') }}({{ __('in minutes') }}):*</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric" id="interval" name="interval">
                                    @foreach ($intervals as $interval)
                                    @if (old('interval') == $interval->interval)
                                    <option selected value="{{ $interval->interval }}">
                                        {{ __($interval->display_name) }}</option>
                                    @else
                                    <option value="{{ $interval->interval }}">{{ __($interval->display_name) }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>

                                @error('interval')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select the time interval you want to check the status of the server') }}.
                                </small>
                            </div>
                        </div>
                    
                        @if ($testFailed = Session::get('test_failed'))
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Skip test connection') }}:*</label>
                            <div class="col-sm-6 col-md-6 form-inline">
                                <input id="skip_test_connection" type="checkbox" class="form-control is-invalid"
                                    name="skip_test_connection" value="1">
                            </div>
                        </div>
                        @endif

                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Add') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection