@extends('layouts.new_theme')

@section('content')

<div class="section-header">
  <h1>{{ __('DNS Monitor for ') }}{{$server->website_domain}}</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
    <div class="breadcrumb-item"><a href="{{ route('dns_server_monitor.index') }}">{{ __('DNS Monitor') }}</a></div>
    <div class="breadcrumb-item"><a href="{{ route('manage', [$server->uuid]) }}">{{ __('Server Details') }}</a></div>
    <div class="breadcrumb-item">{{ __('Edit AAAA Records') }}</div>
  </div>
</div>
<div class="section-body">
    <h2 class="section-title">{{ __('Edit AAAA Record') }}</h2>
    <p class="section-lead">{{ __('You can edit an IPV6 address from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Edit dns_aaaa_records Record') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('update_aaaa_record',[$record->uuid]) }}">
                        @csrf


                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('IP Address') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="ip_address" type="text"
                                    class="form-control @error('ip_address') is-invalid @enderror"
                                    name="ip_address" value="{{ old('ip_address',$record->ipv6_address) }}"
                                    autocomplete="ip_address" autofocus>

                                @error('ip_address')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="ip_address_help1" class="form-text text-muted">
                                    {{ __(' Format:  y : y : y : y : y : y : y : y where y is a segment and can be any hexadecimal value between 0 and FFFF') }}
                                </small>
                                <small id="ip_address_help2" class="form-text text-muted">

                                    {{ __(' eg: 2001:0db8:85a3:0000:0000:8a2e:0370:7334') }}
                                </small>
                              
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Update') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection