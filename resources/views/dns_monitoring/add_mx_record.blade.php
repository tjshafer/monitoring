@extends('layouts.new_theme')

@section('content')

<div class="section-header">
  <h1>{{ __('DNS Monitor for ') }}{{$server->website_domain}}</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
    <div class="breadcrumb-item"><a href="{{ route('dns_server_monitor.index') }}">{{ __('DNS Monitor') }}</a></div>
    <div class="breadcrumb-item"><a href="{{ route('manage', [$server->uuid]) }}">{{ __('Server Details') }}</a></div>
    <div class="breadcrumb-item">{{ __('Add MX Records') }}</div>
  </div>
</div>
<div class="section-body">
    <h2 class="section-title">{{ __('Add MX Record') }}</h2>
    <p class="section-lead">{{ __('You can add MX priority and exchange values from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add MX Record') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('store_mx_record',[$server->uuid]) }}">
                        @csrf


                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Priority') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="priority" type="text"
                                    class="form-control @error('priority') is-invalid @enderror"
                                    name="priority" value="{{ old('priority') }}"
                                    autocomplete="priority" autofocus>

                                @error('priority')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="ip_address_help1" class="form-text text-muted">
                                    {{ __(' The priority number determines which order a server will receive mail in.') }}
                                </small>
                                <small id="ip_address_help2" class="form-text text-muted">

                                    {{ __(' eg: 0,1,2...') }}
                                </small>
                              
                            </div>
                        </div>
                            <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Exchange') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="exchange" type="text"
                                    class="form-control @error('exchange') is-invalid @enderror"
                                    name="exchange" value="{{ old('exchange') }}"
                                    autocomplete="exchange" autofocus>

                                @error('exchange')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small id="ip_address_help1" class="form-text text-muted">
                                    {{ __('Name of the specific mail server the mail is being sent to') }}
                                </small>
                                <small id="ip_address_help2" class="form-text text-muted">

                                    {{ __(' eg: ASPMX.L.GOOGLE.COM') }}
                                </small>
                              
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Add') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection