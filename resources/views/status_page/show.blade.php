@extends('layouts.status')

@section('content')
<div class="col-md-10 offset-md-1">
    <div class="section-header custom-border-radius">
        <h3 class="m-0"><i class="far fa-eye icon-custom"></i> {{ __('Statuspage') }}
        </h3>
    </div>

    @if ($downMonitors == 0)
    <div class="page-status status-none text-center">
        <span class="status font-large">
            {{ __('All Systems Operational') }}
        </span>
        <span class="last-updated-stamp  font-small"></span>
    </div>
    @else
    <div class="page-status status-none bg-danger text-center">
        <span class="status font-large">
            {{ $downMonitors }} {{ __('out of') }} {{ $totalMonitors }} {{ __('systems down') }}
        </span>
        <span class="last-updated-stamp  font-small"></span>
    </div>
    @endif

    <div class="components-section font-regular">
        <i class="component-status hidden major_outage"></i>
        <div class="components-uptime-link history-footer-link">
            {{ __('Uptime over the past') }} <var data-var="num" data-pluralize="90">90</var> {{ __('days') }}.
        </div>
        <div class="card custom-border-radius">
            <div class="components-container one-column p-2">

                @foreach ($monitors as $monitor)
                <div class="component-container border-color">

                    <div data-component-id="yr5d23nndt8f" class="component-inner-container status-green showcased"
                        data-component-status="operational" data-js-hook="">

                        <span class="name">
                            {{ $monitor->monitors->name }}
                        </span>

                        @if ($monitor->monitors->previous_status == 'up')
                        <span class="component-status">
                            <span class='dot'></span>
                            {{ __('Operational') }}
                        </span>
                        @else
                        <span class="component-status text-danger">
                            {{ __('Down') }}
                        </span>
                        @endif

                        <span class="tool icon-indicator fa fa-check" title="Operational"></span>

                        <div class="shared-partial uptime-90-days-wrapper">
                            <svg class="availability-time-line-graphic" id="uptime-component-hchd2wyk50h7"
                                preserveAspectRatio="none" height="34" viewBox="0 0 448 34">
                                @for ($i=0; $i<90; $i++) 
                                
                                @if (!array_key_exists($i, $reports))
                                    <rect
                                        uk-tooltip="<div class='uk-text-muted font-20'>{{ Illuminate\Support\Carbon::now()->subDays(89-$i)->startOfDay()->format('d-M-y') }}</div>NA"
                                        height="34" width="3" x="{{ $i*5 }}" y="0" ry="3" fill="#6c798f"
                                        class="uptime-day component-hchd2wyk50h7 hover-1" data-html="true">
                                    </rect>
                                    @elseif(!array_key_exists($monitor->monitor_id, $reports[$i]))

                                    <rect
                                        uk-tooltip="<div class='uk-text-muted font-20'>{{ Illuminate\Support\Carbon::now()->subDays(89-$i)->startOfDay()->format('d-M-y') }}</div>NA"
                                        height="34" width="3" x="{{ $i*5 }}" y="0" ry="3" fill="#6c798f"
                                        class="uptime-day component-hchd2wyk50h7 hover-1" data-html="true">
                                    </rect>
                                    @elseif(Illuminate\Support\Carbon::parse($monitor->monitors->created_at)->format('Y-m-d') >
                                    $reports[$i][$monitor->monitor_id]['date'])
                                    <rect
                                        uk-tooltip="<div class='uk-text-muted font-20'>{{ Illuminate\Support\Carbon::parse($reports[$i][$monitor->monitor_id]['date'])->format('d-M-y') }}</div>NA"
                                        height="34" width="3" x="{{ $i*5 }}" y="0" ry="3" fill="#6c798f"
                                        class="uptime-day component-hchd2wyk50h7 hover-1" data-html="true">
                                    </rect>
                                    @elseif ($reports[$i][$monitor->monitor_id]['perc'] <= 50.00) <rect
                                        uk-tooltip="<div class='uk-text-muted font-20'>{{ Illuminate\Support\Carbon::parse($reports[$i][$monitor->monitor_id]['date'])->format('d-M-y') }}</div>{{ $reports[$i][$monitor->monitor_id]['perc'] }}{{ "%" }}"
                                        height="34" width="3" x="{{ $i*5 }}" y="0" ry="3" fill="#c90d00"
                                        class="uptime-day component-hchd2wyk50h7 hover-2" data-html="true">
                                        </rect>
                                        @elseif($reports[$i][$monitor->monitor_id]['perc'] > 50.00 &&
                                        $reports[$i][$monitor->monitor_id]['perc'] <= 80.00) <rect height="34" width="3"
                                            x="{{ $i*5 }}" y="0" ry="3" fill="#ed5d53"
                                            class="uptime-day component-hchd2wyk50h7 hover-3" data-html="true"
                                            uk-tooltip="<div class='uk-text-muted font-20'>{{ Illuminate\Support\Carbon::parse($reports[$i][$monitor->monitor_id]['date'])->format('d-M-y') }}</div>{{ $reports[$i][$monitor->monitor_id]['perc'] }}{{ "%" }}">
                                            </rect>
                                            @elseif($reports[$i][$monitor->monitor_id]['perc'] > 80.00 &&
                                            $reports[$i][$monitor->monitor_id]['perc'] <= 90.00) <rect height="34"
                                                width="3" x="{{ $i*5 }}" y="0" ry="3" fill="#f7928b"
                                                class="uptime-day component-hchd2wyk50h7 hover-4" data-html="true"
                                                uk-tooltip="<div class='uk-text-muted font-20'>{{ Illuminate\Support\Carbon::parse($reports[$i][$monitor->monitor_id]['date'])->format('d-M-y') }}</div>{{ $reports[$i][$monitor->monitor_id]['perc'] }}{{ "%" }}">
                                                </rect>
                                                @elseif($reports[$i][$monitor->monitor_id]['perc'] > 90.00 &&
                                                $reports[$i][$monitor->monitor_id]['perc'] <= 99.00) <rect height="34"
                                                    width="3" x="{{ $i*5 }}" y="0" ry="3" fill="#28d455"
                                                    class="uptime-day component-hchd2wyk50h7 hover-5" data-html="true"
                                                    uk-tooltip="<div class='uk-text-muted font-20'>{{ Illuminate\Support\Carbon::parse($reports[$i][$monitor->monitor_id]['date'])->format('d-M-y') }}</div>{{ $reports[$i][$monitor->monitor_id]['perc'] }}{{ "%" }}">
                                                    </rect>
                                                    @else
                                                    <rect height="34" width="3" x="{{ $i*5 }}" y="0" ry="3"
                                                        fill="#1d8f3b" class="uptime-day component-hchd2wyk50h7 hover-6"
                                                        data-html="true"
                                                        uk-tooltip="<div class='uk-text-muted font-20'>{{ Illuminate\Support\Carbon::parse($reports[$i][$monitor->monitor_id]['date'])->format('d-M-y') }}</div>{{ $reports[$i][$monitor->monitor_id]['perc'] }}{{ "%" }}">
                                                    </rect>
                                                    @endif
                                                    @endfor
                            </svg>
                            <div class="legend legend-group">
                                <div class="legend-item light legend-item-date-range">
                                    <span class="availability-time-line-legend-day-count">90</span> {{ __('days ago') }}
                                </div>
                                <div class="spacer"></div>
                                <div class="legend-item legend-item-uptime-value">
                                    <span class="d d-sm-none">
                                        {{ $oneMonthStatus[$monitor->monitor_id]  }}%
                                    </span>
                                    <span class="d-none d-sm-inline d-lg-none">
                                        {{ $twoMonthStatus[$monitor->monitor_id]  }}%
                                    </span>
                                    <span class="d-none d-lg-inline">
                                        {{ $threeMonthStatus[$monitor->monitor_id]  }}%
                                    </span>
                                    <var data-var="uptime-percent">{{ __('uptime') }}</var>
                                </div>
                                <div class="spacer"></div>
                                <div class="legend-item light legend-item-date-range">{{ __('Today') }}</div>
                            </div>

                        </div>

                    </div>
                </div>

                @endforeach
            </div>
        </div>
    </div>
    <div class="section-header custom-border-radius">
        <h3 class="m-0"><i class="far fa-arrow-alt-circle-up icon-custom"></i>
            {{ __('Overall Uptime') }}</h3>
    </div>
    <div class="row">
        <div class="col-xl-4 col-sm-12">
            <div class="card card-statistic-3">
                <div>
                    <div class="card-icon shadow-custom">
                        <i class="fas fa-chevron-circle-up"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>{{ __('Today') }}</h4>
                        </div>
                        <div class="card-body">
                            {{ $todayStatus  }}%
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-sm-12">
            <div class="card card-statistic-3">
                <div>
                    <div class="card-icon shadow-custom">
                        <i class="fas fa-chevron-circle-up"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>{{ __('Last 7 days') }}</h4>
                        </div>
                        <div class="card-body">
                            {{ $lastWeekStatus }}%
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-sm-12">
            <div class="card card-statistic-3">
                <div>
                    <div class="card-icon shadow-custom">
                        <i class="fas fa-chevron-circle-up"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>{{ __('Last 30 days') }}</h4>
                        </div>
                        <div class="card-body">
                            {{ $lastMonthStatus }}%
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="section-body">
        <div class="section-header custom-border-radius">
            <h3 class="m-0"><i class="fas fa-exclamation-circle icon-custom"></i> {{ __('Latest Incidents') }}</h3>
        </div>
        <div class="row">
            <div class="col-12 mt-4">
                <div class="activities">
                    @if (count($incidents))
                    @foreach ($incidents as $incident)
                    <div class="activity">
                        @if($incident->status == true)
                        <div class="activity-icon bg-success text-white shadow-custom">
                            <i class="far fa-check-circle fs-2x"></i>
                        </div>
                        @else
                        <div class="activity-icon bg-danger text-white shadow-custom">
                            <i class="far fa-times-circle fs-2x"></i>
                        </div>
                        @endif
                        <div class="activity-detail">
                            <div class="mb-2">
                                <span
                                    class="text-job text-primary">{{ Illuminate\Support\Carbon::parse($incident->created_at)->format('M d, Y') }}</span>
                                <span class="bullet"></span>
                                <span class="font-weight-bold text-custom">{{ $incident->monitors->name }}<span> <i
                                            class="fas fa-arrow-right"></i> <span class="text-dark">{!!
                                            strip_tags($incident->subject) !!}</span>
                            </div>
                            @foreach ($incidentDetails[$incident->uuid] as $details)
                            <p><span class="font-weight-bold text-capitalize">{{ $details->status }}:
                                </span>{!! strip_tags($details->description) !!}. <span
                                    class="text-job text-primary">{{ $details->created_at->diffForHumans() }}</span></p>
                            @endforeach
                            <p><span class="font-weight-bold text-capitalize">{{ __('Investigating') }}:
                                </span>{!! strip_tags($incident->description) !!}.</p>

                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="activity">
                        <div class="activity-icon bg-success-dark text-white shadow-custom">
                            <i class="far fa-smile fs-2x"></i>
                        </div>
                        <div class="activity-detail">
                            <div class="mb-2">
                                <span class="font-weight-bold">{{ __('There is no incidents yet') }}</span>
                            </div>

                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection