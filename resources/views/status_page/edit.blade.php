@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Status Pages') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('status_page.index') }}">{{ __('Status Pages') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Edit Status Page') }}</div>
    </div>
</div>

<div class="section-body">

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Edit Status Page') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('status_page.update', $uuid) }}" enctype="multipart/form-data">
                        @csrf

                        @method('PUT')

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Name') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name', $name) }}" autocomplete="name" autofocus>

                                @error('name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 text-capitalize">{{ __('Description') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea class="form-control @error('description') is-invalid @enderror"
                                    id="description" rows="5"
                                    name="description">{{ old('description', $description) }} </textarea>
                                @error('description')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        @if ($enableSubdomain)
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 text-capitalize">{{ __('Sub domain') }}:*</label>
                                <div class="col-sm-12 col-md-7">
                                <input id="sub_domain" type="text"
                                    class="form-control @error('sub_domain') is-invalid @enderror" name="sub_domain"
                                    value="{{ old('sub_domain', $sub_domain) }}"
                                    autocomplete="sub_domain" autofocus>

                                @error('sub_domain')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Enter your statuspage subdomain') }}.
                                    <br>
                                    {{ __('Eg: status.mydomain.com') }}.
                                </small>
                            </div>

                        </div>
                        @endif

                        <div class="form-group row mb-4" id="servers">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Servers') }}:</label>
                            <div class="col-sm-12 col-md-7" v-if="servers">
                                <input type="hidden" ref="server_ref" id="server_ref_id" value="" name="server_ids" />
                                <select-servers multiple :options="servers" taggable push-servers
                                    v-model="selected_servers" :reduce="servers => servers.uuid" label="name"
                                    @input="chooseMe">
                                </select-servers>
                            </div>
                        </div>

                        <div class="form-group row mb-4" id="webpages">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Webpages') }}:</label>
                            <div class="col-sm-12 col-md-7" v-if="webpages">
                                <input type="hidden" ref="webpage_ref" id="webpage_ref_id" value=""
                                    name="webpage_ids" />
                                <select-webpages multiple :options="webpages" taggable push-webpages
                                    v-model="selected_webpages" :reduce="webpages => webpages.uuid" label="name"
                                    @input="chooseMe">
                                </select-webpages>
                            </div>
                        </div>

                        <div class="form-group row mb-4" id="apis">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Apis') }}:</label>
                            <div class="col-sm-12 col-md-7" v-if="apis">
                                <input type="hidden" ref="api_ref" id="api_ref_id" value="" name="api_ids" />
                                <select-apis multiple :options="apis" taggable push-apis v-model="selected_apis"
                                    :reduce="apis => apis.uuid" label="name" @input="chooseMe">
                                </select-apis>
                            </div>
                        </div>

                        @if ($enableSubdomain)
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 text-capitalize">{{ __('Logo') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="logo" name="logo" type="file" class="form-control file"
                                    data-show-caption="true" value="{{ __($logo) }}" autocomplete="logo" autofocus>
                                @error('logo')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        @endif

                        @if (config('app.env') != 'demo')
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Update') }}</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var uuid = '<?php echo $uuid; ?>';
    var current_servers = '@json($selected_servers)';
    var current_webpages = '@json($selected_webpages)';
    var current_apis = '@json($selected_apis)';
</script>
<script src="{{ asset('js/servers.js') }}"></script>
<script src="{{ asset('js/webpages.js') }}"></script>
<script src="{{ asset('js/apis.js') }}"></script>
@endsection