@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Status Pages') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('plans.index') }}">{{ __('Status Pages') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('List of Status Pages') }}</div>
    </div>
</div>

<div class="section-body">

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.user')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4 class="inline-block">{{ __('List of Status Pages') }}</h4>
                    <a href="{{ route('status_page.create') }}"
                        class="btn btn-icon btn-custom float-right inline-block"><i
                            class="far fa-edit"></i>{{ __('Add Status Page') }}</a>
                </div>
                <div class="card-body">

                    @if (!count($statusPages))
                    <div class="card-body">
                        <div class="empty-state" data-height="400">
                            <div class="empty-state-icon bg-danger">
                                <i class="fas fa-question"></i>
                            </div>
                            <h2>{{ __('No data found') }} !!</h2>
                            <p class="lead">
                                {{ __('Sorry we cant find any data, to get rid of this message, make at least 1 entry') }}.
                            </p>
                            <a href="{{ route('status_page.create') }}"
                                class="btn btn-custom mt-4">{{ __('Create new one') }}</a>
                        </div>
                    </div>

                    @else
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center text-capitalize">
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('View') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($statusPages as $page)
                                <tr>
                                    <td class="text-capitalize">{{ __($page->name) }}</td>

                                    @if ($enableSubdomain && $page->sub_domain)
                                    <td class="text-center"><a href="{{ "https://$page->sub_domain" }}" data-toggle="tooltip" target="blank"
                                            class="btn bg-transparent">
                                            <i class="fas fa-eye text-primary"></i></a></td>
                                    @else
                                    <td class="text-center"><a href="{{ route('showStatusPage', [$page->slug]) }}" data-toggle="tooltip"
                                        class="btn bg-transparent">
                                        <i class="fas fa-eye text-primary"></i></a></td>
                                    @endif

                                    <td class="justify-content-center form-inline">
                                        <a href="{{ route('status_page.edit', [$page->uuid]) }}"
                                            class="btn btn-sm bg-transparent"><i class="far fa-edit text-primary"
                                                aria-hidden="true" title="{{ __('Edit') }}"></i></a>

                                        @if (config('app.env') != 'demo')
                                        <form action="{{ route('status_page.destroy', [$page->uuid]) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm bg-transparent"
                                                onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash text-danger" aria-hidden="true"
                                                    title="{{ __('Delete') }}"></i>
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br>
                        {{ $statusPages->appends($request->all())->links("pagination::bootstrap-4") }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endsection