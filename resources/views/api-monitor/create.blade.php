@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('API Monitor') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="{{ route('api_monitor.index') }}">{{ __('API Monitor') }}</a>
        </div>
        <div class="breadcrumb-item">{{ __('Add API') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Add API') }}</h2>
    <p class="section-lead">{{ __('You can add an API from here') }}.</p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add API') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('api_monitor.store') }}">
                        @csrf
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('API Name') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                                @error('name')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                {{ __('Here you can enter a name of your choice for the api') }}.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('URL') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror"
                                    name="url" value="{{ old('url') }}" autocomplete="url" autofocus>

                                @error('url')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can enter the API URL') }}. <br>
                                    Eg: {{ __('https://reqres.in/api/products/3') }}
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Methode') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric" id="methode" name="methode">
                                    <option value="GET" selected>GET</option>
                                    <option value="POST" {{ old('methode') == 'POST' ? 'selected' : '' }}>POST
                                    </option>
                                    <option value="PUT" {{ old('methode') == 'PUT' ? 'selected' : '' }}>PUT
                                    </option>
                                    <option value="DELETE" {{ old('methode') == 'DELETE' ? 'selected' : '' }}>
                                        DELETE</option>
                                </select>

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select HTTP method to be performed on the object identified by the URL') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Expected response code') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="expected_response_code" type="text"
                                    class="form-control @error('expected_response_code') is-invalid @enderror"
                                    name="expected_response_code" value="{{ old('expected_response_code') }}" autocomplete="expected_response_code"
                                    autofocus>

                                @error('expected_response_code')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                                
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to provide HTTP response status code of the URL, It should be some numbers') }}. <br>
                                    Eg: {{ __('200') }}
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('JSON data') }}:</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea class="form-control @error('json') is-invalid @enderror" id="json" name="json"
                                    autocomplete="json" autofocus>{{ old('json') }}</textarea>

                                @error('json')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Please paste any json data that should be posted to the API here (optional)') }}. <br>
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Ping interval') }}({{ __('in minutes') }}):*</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric" id="interval" name="interval">
                                    @foreach ($intervals as $interval)
                                    @if (old('interval') == $interval->interval)
                                    <option selected value="{{ $interval->interval }}">
                                        {{ __($interval->display_name) }}</option>
                                    @else
                                    <option value="{{ $interval->interval }}">{{ __($interval->display_name) }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>

                                @error('interval')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                                
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you need to select the time interval you want to check the status of the API') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Email') }}:</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" autocomplete="email" autofocus>

                                @error('email')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="form-text text-muted">
                                    Eg: {{ Auth::user()->email }}
                                </small>
                                <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('Here you can enter email id, To this email id status update emails will be sent.') }}
                                    <br>
                                    {{ __('If no email is mentioned here, mail will be sent to') }}<br />
                                    {{ Auth::user()->email }}
                                </small>
                            </div>
                        </div>

                        @if ($testFailed = Session::get('test_failed'))
                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Skip test connection') }}:*</label>
                            <div class="col-sm-6 col-md-6 form-inline">
                                <input id="skip_test_connection" type="checkbox" class="form-control is-invalid"
                                    name="skip_test_connection" value="1">
                            </div>
                        </div>
                        @endif

                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Add') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection