
<div>
  <b>{{ __('Hello :name', ['name' => $api->user->first_name]) }},</b><br/>
  {{ __('API down email for :name', ['name' => $api->name]) }}
  <br/>
  {{ __('URL: :url', ['url' => $api->url]) }}
  <br/>
  {{ __('Response code: :code', ['code' => $api->expected_response_code]) }}
  <br/>
  {{ __('Time: :time', ['time' => \Illuminate\Support\Carbon::now()->toDateTimeString()]) }}
  <br/>
  {{ __('Please contact support team.') }}

  <br/>
  <br/>
  {{ __('Regards') }}
  <br/>
  {{ __(':admin', ['admin' => config('mail.from.name')]) }}
</div>