@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Plans') }}</h1>
    <div class="section-header-breadcrumb">
        {{-- <div class="breadcrumb-item active"><a href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a></div> --}}
    <div class="breadcrumb-item"><a href="{{ route('pricing.index') }}">{{ __('Pricing') }}</a></div>
    <div class="breadcrumb-item">{{ __('Add Pricing') }}</div>
</div>
</div>

<div class="section-body">

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add Pricing') }}</h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-pills" id="myTab3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link"
                                href="{{ route('plans.edit', [$plan_id]) }}"></i><span>{{ __('Edit') }}</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active"
                                href="{{ route('pricing_index', [$plan_id]) }}"><span>{{ __('Pricing') }}</span></a>
                        </li>
                    </ul>
                    <div class="custom-divider"></div>
                    <div class="tab-content pt-3" id="myTabContent">
                        <form method="POST" action="{{ route('pricing.store') }}">
                            @csrf

                            <div>
                                <input type="hidden" name="plan_id" value="{{ $plan_id }}" />

                                <div class="form-group row mb-4">
                                    <label for="address"
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Enter Term') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input id="term" type="text"
                                            class="form-control @error('term') is-invalid @enderror" name="term"
                                            value="{{ old('term') }}" autocomplete="term" autofocus>
                                        @error('term')
                                        <div class="text-danger pt-1">{{ $message }}</div>
                                        @enderror
                                        <small class="text-secondary"><i class="fa fa-exclamation-circle"
                                                aria-hidden="true"></i>
                                            {{ __('Term must be a number that indicates the count of period') }}  <br>
                                            {{ __('Eg: 2') }} {{ __('means 2 Days if the periode selected is Day(s)') }}
                                        </small>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Period') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control selectric" id="period" name="period">
                                            <option value="Day(s)" selected>{{ __('Day(s)') }}</option>
                                            <option value="Week(s)" {{ old('period') == 'Week(s)' ? 'selected' : '' }}>
                                                {{ __('Week(s)') }}
                                            </option>
                                            <option value="Month(s)"
                                                {{ old('period') == 'Month(s)' ? 'selected' : '' }}>
                                                {{ __('Month(s)') }}
                                            </option>
                                            <option value="Year(s)" {{ old('period') == 'Year(s)' ? 'selected' : '' }}>
                                                {{ __('Year(s)') }}
                                            </option>
                                        </select>

                                        {{-- <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                            aria-hidden="true"></i>
                                        {{ __('Here you need to select HTTP method to be performed on the object identified by the URL') }}.
                                        </small> --}}
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label for="address"
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Price') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input id="price" type="text"
                                            class="form-control @error('price') is-invalid @enderror" name="price"
                                            value="{{ old('price') }}" autocomplete="price" autofocus>
                                        @error('price')
                                        <div class="text-danger pt-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="form-group row mb-4">
                                    <label for="address"
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Price Renews') }}:</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input id="price_renews" type="text"
                                            class="form-control @error('price_renews') is-invalid @enderror"
                                            name="price_renews" value="{{ old('price_renews') }}"
                                            autocomplete="price_renews" autofocus>
                                        @error('price_renews')
                                        <div class="text-danger pt-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div> --}}

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Select Currency') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control selectric" id="currency" name="currency_id">
                                            @foreach ($currencies as $currency)
                                            @if (old('currency_id') == $currency->id)
                                            <option selected value="{{ $currency->id }}">
                                                {{ __($currency->currency) }}</option>
                                            @else
                                            <option value="{{ $currency->id }}">{{ __($currency->currency) }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>

                                        @error('currency_id')
                                        <div class="text-danger pt-1">{{ $message }}</div>
                                        @enderror

                                        <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                                aria-hidden="true"></i>
                                            {{ __('Here you need to select the currency.') }}.
                                            <br>
                                        </small>
                                    </div>
                                </div>

                                @if (config('app.env') != 'demo')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-custom"> {{ __('Add') }}</button>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection