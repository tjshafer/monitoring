@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Plans') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item"><a href="">{{ __('Plans') }}</a></div>
        <div class="breadcrumb-item">{{ __('Plans') }}</div>
    </div>
</div>

<div class="section-body">

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Edit Plan') }}</h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-pills" id="myTab3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active"
                                href="{{ route('plans.edit', [$plan->id]) }}"></i><span>{{ __('Edit') }}</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                                href="{{ route('pricing_index', [$plan->id]) }}"><span>{{ __('Pricing') }}</span></a>
                        </li>
                    </ul>
                    <div class="custom-divider"></div>
                    <div class="tab-content pt-5" id="myTabContent">
                        <form method="POST" action="{{ route('plans.update', [$plan->id]) }}">
                            @csrf
                            @method('PUT')

                            <div>
                                <div class="form-group row mb-4">
                                    <label for="address"
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Name') }}*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input id="name" type="text"
                                            class="form-control @error('name') is-invalid @enderror" name="name"
                                            value="{{ old('name', $plan->name) }}" autocomplete="name" autofocus>
                                        @error('name')
                                        <div class="text-danger pt-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Description') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea id="description"
                                            class="form-control height-auto @error('description') is-invalid @enderror"
                                            name="description">{{ old('description', $plan->description) }}</textarea>
                                        @error('description')
                                        <div class="text-danger pt-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Server Count') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="row">
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="servers"
                                                    id="unlimited_servers" value=""
                                                    {{ old('servers', $plan->server_count) == null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="unlimited_servers">
                                                    {{ __('Unlimited') }}
                                                </label>
                                            </div>
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="servers"
                                                    id="limited_servers" value="1"
                                                    {{ old('servers', $plan->server_count) != null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="limited_servers">
                                                    <input type="text"
                                                        class="form-control @error('server_count') is-invalid @enderror"
                                                        name="server_count"
                                                        value="{{ old('server_count', $plan->server_count) }}"
                                                        autocomplete="server_count" autofocus>
                                                    @error('server_count')
                                                    <div class="text-danger pt-1">{{ $message }}</div>
                                                    @enderror
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Webpage Count') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="row">
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="webpages"
                                                    id="unlimited_webpages" value=""
                                                    {{ old('webpages', $plan->webpage_count) == null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="unlimited_webpages">
                                                    {{ __('Unlimited') }}
                                                </label>
                                            </div>
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="webpages"
                                                    id="limited_webpage" value="1"
                                                    {{ old('webpages', $plan->webpage_count) != null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="limited_webpage">
                                                    <input type="text"
                                                        class="form-control @error('webpage_count') is-invalid @enderror"
                                                        name="webpage_count"
                                                        value="{{ old('webpage_count', $plan->webpage_count) }}"
                                                        autocomplete="webpage_count" autofocus>
                                                </label>
                                                @error('webpage_count')
                                                <div class="text-danger pt-1">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('API Count') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="row">
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="apis"
                                                    id="unlimited_apis" value=""
                                                    {{ old('apis', $plan->api_count) == null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="unlimited_apis">
                                                    {{ __('Unlimited') }}
                                                </label>
                                            </div>
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="apis"
                                                    id="limited_apis" value="1"
                                                    {{ old('apis', $plan->api_count) != null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="limited_apis">
                                                    <input type="text"
                                                        class="form-control @error('api_count') is-invalid @enderror"
                                                        name="api_count" value="{{ old('api_count', $plan->api_count) }}"
                                                        autocomplete="api_count" autofocus>
                                                </label>
                                                @error('api_count')
                                                <div class="text-danger pt-1">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('SSL Count') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="row">
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="ssl"
                                                    id="unlimited_ssl" value=""
                                                    {{ old('ssl', $plan->ssl_count) == null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="unlimited_ssl">
                                                    {{ __('Unlimited') }}
                                                </label>
                                            </div>
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="ssl"
                                                    id="limited_ssl" value="1"
                                                    {{ old('ssl', $plan->ssl_count) != null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="limited_ssl">
                                                    <input type="text"
                                                        class="form-control @error('ssl_count') is-invalid @enderror"
                                                        name="ssl_count"
                                                        value="{{ old('ssl_count', $plan->ssl_count) }}"
                                                        autocomplete="ssl_count" autofocus >
                                                </label>
                                                @error('ssl_count')
                                                <div class="text-danger pt-1">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Domain Count') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="row">
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="domains"
                                                    id="unlimited_domains" value=""
                                                    {{ old('domains', $plan->domain_count) == null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="unlimited_domains">
                                                    {{ __('Unlimited') }}
                                                </label>
                                            </div>
                                            <div class="custom-radio custom-control ml-3">
                                                <input class="custom-control-input" type="radio" name="domains"
                                                    id="limited_domains" value="1"
                                                    {{ old('domains', $plan->domain_count) != null ? 'checked' : "" }}>
                                                <label class="custom-control-label" for="limited_domains">
                                                    <input type="text"
                                                        class="form-control @error('domain_count') is-invalid @enderror"
                                                        name="domain_count"
                                                        value="{{ old('domain_count', $plan->domain_count) }}"
                                                        autocomplete="domain_count" autofocus >
                                                </label>
                                                @error('domain_count')
                                                <div class="text-danger pt-1">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Subdomain Count') }}:*</label>
                                <div class="col-sm-12 col-md-7">
                                    <div class="row">
                                        <div class="custom-radio custom-control ml-3">
                                            <input class="custom-control-input" type="radio" name="subdomains"
                                                id="unlimited_subdomains" value="" {{ old('subdomains', $plan->subdomain_count) == null ? 'checked' : "" }}>
                                            <label class="custom-control-label" for="unlimited_subdomains">
                                                {{ __('Unlimited') }}
                                            </label>
                                        </div>
                                        <div class="custom-radio custom-control ml-3">
                                            <input class="custom-control-input" type="radio" name="subdomains"
                                                id="limited_subdomains" value="1" {{ old('subdomains', $plan->subdomain_count) != null ? 'checked' : "" }}>
                                            <label class="custom-control-label" for="limited_subdomains">
                                                <input type="text"
                                                    class="form-control @error('subdomain_count') is-invalid @enderror"
                                                    name="subdomain_count" value="{{ old('subdomain_count', $plan->subdomain_count) }}"
                                                    autocomplete="subdomain_count" autofocus>
                                            </label>
                                            @error('subdomain_count')
                                            <div class="text-danger pt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('DNS Count') }}:*</label>
                                <div class="col-sm-12 col-md-7">
                                    <div class="row">
                                        <div class="custom-radio custom-control ml-3">
                                            <input class="custom-control-input" type="radio" name="dns"
                                                id="unlimited_dns" value="" {{ old('dns', $plan->dns_count) == null ? 'checked' : "" }}>
                                            <label class="custom-control-label" for="unlimited_dns">
                                                {{ __('Unlimited') }}
                                            </label>
                                        </div>
                                        <div class="custom-radio custom-control ml-3">
                                            <input class="custom-control-input" type="radio" name="dns"
                                                id="limited_dns" value="1"  {{ old('dns', $plan->dns_count) != null ? 'checked' : "" }}>
                                            <label class="custom-control-label" for="limited_dns">
                                                <input type="text"
                                                    class="form-control @error('dns_count') is-invalid @enderror"
                                                    name="dns_count" value="{{ old('dns_count',$plan->dns_count) }}"
                                                    autocomplete="dns_count" autofocus>
                                            </label>
                                            @error('dns_count')
                                            <div class="text-danger pt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Keyword Count') }}:*</label>
                                <div class="col-sm-12 col-md-7">
                                    <div class="row">
                                        <div class="custom-radio custom-control ml-3">
                                            <input class="custom-control-input" type="radio" name="keyword"
                                                id="unlimited_keyword" value="" {{ old('keyword', $plan->keyword_count) == null ? 'checked' : "" }}>
                                            <label class="custom-control-label" for="unlimited_keyword">
                                                {{ __('Unlimited') }}
                                            </label>
                                        </div>
                                        <div class="custom-radio custom-control ml-3">
                                            <input class="custom-control-input" type="radio" name="keyword"
                                                id="limited_keyword" value="1" {{ old('keyword', $plan->keyword_count) != null ? 'checked' : "" }}>
                                            <label class="custom-control-label" for="limited_keyword">
                                                <input type="text"
                                                    class="form-control @error('keyword_count') is-invalid @enderror"
                                                    name="keyword_count" value="{{ old('keyword_count',$plan->keyword_count) }}"
                                                    autocomplete="keyword_count" autofocus>
                                            </label>
                                            @error('keyword_count')
                                            <div class="text-danger pt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="form-group row mb-4">
                                    <label for="address"
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Display Order') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input id="display_order" type="text"
                                            class="form-control @error('display_order') is-invalid @enderror"
                                            name="display_order"
                                            value="{{ old('display_order', $plan->display_order) }}"
                                            autocomplete="display_order" autofocus>
                                        @error('display_order')
                                        <div class="text-danger pt-1">{{ $message }}</div>
                                        @enderror
                                        <small class="text-secondary"><i class="fa fa-exclamation-circle"
                                                aria-hidden="true"></i>
                                            {{ __('You can enter a number that represents the display order of the plan to domains') }}
                                        </small>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Status') }}:*</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="custom-radio custom-control">
                                            <input class="custom-control-input" type="radio" name="status"
                                                id="planEnable" value=1
                                                {{ old('status', $plan->status) == 1 ? 'checked' : "" }}>
                                            <label class="custom-control-label" for="planEnable">
                                                {{ __('Enable') }}
                                            </label>
                                        </div>
                                        <div class="custom-radio custom-control">
                                            <input class="custom-control-input" type="radio" name="status"
                                                id="planDisable" value=0
                                                {{ old('status', $plan->status) == 0 ? 'checked' : "" }}>
                                            <label class="custom-control-label" for="planDisable">
                                                {{ __('Disable') }}
                                            </label>
                                        </div>
                                        <small class="form-text text-muted"><i class="fa fa-exclamation-circle"
                                                aria-hidden="true"></i>
                                            {{ __('Enable to activate this plan') }}.
                                            <br>
                                        </small>
                                    </div>
                                </div>


                                @if (config('app.env') != 'demo')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-custom"> {{ __('Update') }}</button>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection