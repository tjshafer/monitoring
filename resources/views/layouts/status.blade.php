<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="{{ asset('css/uptime.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components.css') }}">
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.9.1/dist/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.9.1/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.9.1/dist/js/uikit-icons.min.js"></script>
</head>

<body class="status index status-none">

    <div class="layout-content status status-index premium">
        <div class="navbar-bg height-75"></div>
        <nav class="navbar navbar-expand-lg main-navbar user-navbar bg-custom">
            <div class="form-inline mr-auto d-none d-sm-inline">
                <a class="user-brand sidebar-brand" href="{{ route('home') }}">
                    @if ($logo != null)
                    <img src="/system_logo/{{ $logo }}" alt="{{ config('app.name') }}" class="logo">
                    @else
                    {{ config('app.name') }}
                    @endif
                </a>
            </div>

            <div class="form-inline mr-auto d-inline d-sm-none">
                <a class="user-brand sidebar-brand" href="{{ route('home') }}">
                    @if ($logo != null)
                    <img src="/system_logo/{{ $logo }}" alt="{{ config('app.name') }}" class="logo-small">
                    @else
                    {{ config('app.name') }}
                    @endif
                </a>
            </div>

            @guest
            <ul class="navbar-nav navbar-right float-right ml-auto mt-none">
                <li><a href="{{ route('home.index') }}" class="nav-link nav-link-lg nav-link-user">
                        <i class="fas fa-home mr-1"></i>
                        <div class="d-sm-none d-lg-inline-block">{{ __('Home') }}</div>
                    </a>
                </li>

                <li class="dropdown"><a href="#" data-toggle="dropdown"
                        class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                        <i class="fas fa-globe-asia mr-1"></i>
                        <div class="d-sm-none d-lg-inline-block">{{ __('Language.') }}</div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        @foreach ($languages as $language)
                        <a href="/lang/{{ $language->code }}" class="dropdown-item has-icon">
                            {{ $language->language }}
                        </a>
                        @endforeach
                    </div>
                </li>

                <li class="dropdown"><a href="#" data-toggle="dropdown"
                        class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                        <i class="fa fa-user-circle mr-1"></i>
                        <div class="d-sm-none d-lg-inline-block">{{ __('User') }}</div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        {{-- <div class="dropdown-title">Logged in 5 min ago</div> --}}

                        <a href="{{ route('login') }}" class="dropdown-item has-icon">
                            <i class="far fa-user"></i> {{ __('Login') }}
                        </a>

                        <div class="dropdown-divider"></div>
                        <a href="{{ route('register') }}" class="dropdown-item has-icon">
                            <i class="far fa-user"></i> {{ __('Register') }}
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item has-icon" target="_blank"
                            href="https://monitoring.zone/saas_docs/1.0.0/user-doc.html"><i
                                class="far fa-question-circle"></i>
                            {{ __('Help') }}</a>

                    </div>
                </li>
                @else
                <ul class="navbar-nav navbar-right float-right ml-auto mt-none">
                    <li class="dropdown"><a href="#" data-toggle="dropdown"
                            class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            <i class="fas fa-globe-asia mr-1"></i>
                            <div class="d-sm-none d-lg-inline-block">{{ __('Language.') }}</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @foreach ($languages as $language)
                            <a href="/lang/{{ $language->code }}" class="dropdown-item has-icon">
                                {{ $language->language }}
                            </a>
                            @endforeach
                        </div>
                    </li>

                    <li class="dropdown">

                        @auth('admin')
                        <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            <i class="fa fa-user-circle mr-1"></i>
                            <div class="d-sm-none d-lg-inline-block">{{ __('Hi') }}, {{ Auth::user()->name }}</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            {{-- <div class="dropdown-title">Logged in 5 min ago</div> --}}

                            <a href="{{ route('adminProfile') }}" class="dropdown-item has-icon">
                                <i class="far fa-user"></i> {{ __('Profile') }}
                            </a>

                            @else
                            <a href="#" data-toggle="dropdown"
                                class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                                <i class="fa fa-user-circle mr-1"></i>
                                <div class="d-sm-none d-lg-inline-block">{{ __('Hi') }}, {{ Auth::user()->first_name }}
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                {{-- <div class="dropdown-title">Logged in 5 min ago</div> --}}
                                <a href="{{ route('userProfile') }}" class="dropdown-item has-icon">
                                    <i class="far fa-user"></i> {{ __('Profile') }}
                                </a>
                                @endauth

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item has-icon" target="_blank"
                                    href="https://ticketing.expert/saas_docs/1.0.0/"><i
                                        class="far fa-question-circle"></i>
                                    {{ __('Help') }}</a>
                                <div class="dropdown-divider"></div>
                                <a href{{ route('logout') }} class="dropdown-item has-icon text-danger pointer" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                    </li>
                    @endguest
                </ul>
        </nav>
        <div class="main-content main-sm-user main-user pl-md-5 pr-md-5">
            <section class="section mt-30">
                @yield('content')
            </section>
        </div>
        <footer class="main-footer public-footer">
            <div class="footer-left">
                {{ $footer_text }}
            </div>
            <div class="footer-right">
                <a href="{{ route('privacyPolicy') }}" class="pr-3 text-secondary">{{ __('Privacy Policy') }}</a><a
                    href="{{ route('terms') }}" class="pr-3 text-secondary">{{ __('Terms of Use') }}</a><span>T.E
                    1.1.0</span>
            </div>
        </footer>
    </div>

    <script src="{{ asset('js/responsive_uptime.js') }}"></script> <!-- General JS Scripts -->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/stisla.js') }}"></script>

    <!-- JS Libraies -->
    <script src="{{ asset('js/library.js') }}"></script>

    <!-- Template JS File -->
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <!-- Page Specific JS File -->
</body>

</html>