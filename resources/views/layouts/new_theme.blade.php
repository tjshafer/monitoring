<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Monitoring Zone</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/fontawesome.css') }}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('css/selectric.css') }}">
  <link rel="stylesheet" href="{{ asset('css/summernote-bs.min.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('css/components.css') }}">
  <link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
  <link href="{{ asset('css/vue-select.css') }}" rel="stylesheet">
  <script src="{{ asset('js/vue.min.js') }}"></script>
  <script src="{{ asset('js/vue-select.js') }}"></script>
  <script src="{{ asset('js/axios.min.js') }}"></script>
</head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <div class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                  class="fas fa-search"></i></a></li>
          </ul>
        </div>
        <ul class="navbar-nav navbar-right">
          @auth('web')
          <li><a class="nav-link nav-link-lg nav-link-user" href="{{ route('available_plans.index') }}"><i
                class="fas fa-columns mr-1"></i>
              <div class="d-sm-none d-lg-inline-block">{{ __('Plans') }}</div>
            </a></li>
            <li><a class="nav-link nav-link-lg nav-link-user" href="{{ route('alert_manager.index') }}"><i
                class="far fa-bell mr-1"></i>
              <div class="d-sm-none d-lg-inline-block">{{ __('Alert Manager') }}</div>
            </a></li>
           
          @endauth
          <li class="dropdown"><a href="#" data-toggle="dropdown"
              class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              <i class="fas fa-globe-asia mr-1"></i>
              <div class="d-sm-none d-lg-inline-block">{{ __('Language.') }}</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              @foreach ($languages as $language)
              <a href="/lang/{{ $language->code }}" class="dropdown-item has-icon">
                {{ $language->language }}
              </a>
              @endforeach
            </div>
          </li>
          <li class="dropdown"><a href="#" data-toggle="dropdown"
              class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              <i class="fa fa-user-circle mr-1"></i>
              <div class="d-sm-none d-lg-inline-block">{{ __('Hi') }}, {{ Auth::user()->first_name }}</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              {{-- <div class="dropdown-title">Logged in 5 min ago</div> --}}

              @auth('admin')
              <a href="{{ route('adminProfile') }}" class="dropdown-item has-icon">
                <i class="far fa-user"></i> {{ __('Profile') }}
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item has-icon" target="_blank"
                href="https://monitoring.zone/saas_docs/1.0.0/admin-doc.html"><i class="far fa-question-circle"></i>
                {{ __('Help') }}</a>
              @else
              <a href="{{ route('userProfile') }}" class="dropdown-item has-icon">
                <i class="far fa-user"></i> {{ __('Profile') }}
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item has-icon" target="_blank"
                href="https://monitoring.zone/saas_docs/1.0.0/user-doc.html"><i class="fas fa-question-circle"></i>
                {{ __('Help') }}</a>
              @endauth
              <div class="dropdown-divider"></div>
              <a href{{ route('logout') }} class="dropdown-item has-icon text-danger pointer" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
              </form>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="">
              @if ($logo != null)
              <img src="/system_logo/{{ $logo }}" alt="{{ config('app.name') }}" class="logo">
              @else
              {{ config('app.name') }}
              @endif
            </a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="">{{ $app_name_short }}</a>
          </div>
          <ul class="sidebar-menu">

            @auth('admin')

            <li class="menu-header">{{ __('Dashboard') }}</li>
            <li class="{{ Request::is('admin') ? 'active' : '' }}"> <a class="nav-link"
                href="{{ route('dashboard.index') }}"><i class="fas fa-fire"></i><span>{{ __('Dashboard') }}</span></a>
            </li>

            <li class="menu-header">{{ __('Admin Area') }}</li>
            <li class="{{ Request::is('admin/orders*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('orders.index') }}"><i class="fas fa-shopping-cart"
                  aria-hidden="true"></i><span>{{ __('Orders') }}</span></a></li>
            <li class="{{ Request::is('admin/invoices*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('invoices.index') }}"><i
                  class="fas fa-file-invoice"></i><span>{{ __('Invoices') }}</span></a></li>
            <li class="{{ Request::is('admin/services*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('services.index') }}"><i
                  class="fas fa-list"></i><span>{{ __('Subscriptions') }}</span></a></li>
            <li class="{{ Request::is('admin/users*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('users.index') }}"><i class="fas fa-users"
                  aria-hidden="true"></i><span>{{ __('Users') }}</span></a></li>
            <li class="{{ Request::is('admin/plans*') ? 'active' : '' }}"> <a class="nav-link"
                href="{{ route('plans.index') }}"><i class="fas fa-columns"></i><span>{{ __('Plans') }}</span></a></li>
            <li class="{{ Request::is('admin/gateways*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('gateways.index') }}"><i class="fas fa-credit-card"
                  aria-hidden="true"></i><span>{{ __('Gateways') }}</span></a></li>
            <li class="{{ Request::is('admin/email_template*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('email_template.index') }}"><i
                  class="fas fa-envelope-open-text"></i><span>{{ __('Email Templates') }}</span></a></li>

            <li class="menu-header">{{ __('Settings') }}</li>
            <li class="{{ Request::is('currency*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('currency.index') }}"><i class="fas fa-language"
                  aria-hidden="true"></i><span>{{ __('Currencies') }}</span></a></li>
            <li class="{{ Request::is('admin/language*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('language.index') }}"><i class="fas fa-language"
                  aria-hidden="true"></i><span>{{ __('Languages') }}</span></a></li>
            <li class="{{ Request::is('admin/billing_address*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('billing_address.index') }}"><i
                  class="fas fa-address-card"></i><span>{{ __('Billing Address') }}</span></a></li>
            <li class="{{ Request::is('admin/interval*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('interval.index') }}"><i
                  class="fas fa-clock"></i><span>{{ __('Ping Interval') }}</span></a></li>
            <li class="{{ Request::is('admin/settings*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('settings.index') }}"><i
                  class="fas fa-cogs"></i><span>{{ __('System Settings') }}</span></a>

              @else

            <li class="menu-header">{{ __('Dashboard') }}</li>
            <li class="{{ Request::is('dashboard*') ? 'active' : '' }}"> <a class="nav-link"
                href="{{ route('dashboard.index') }}"><i class="fas fa-fire"></i><span>{{ __('Dashboard') }}</span></a>
            </li>

            <li class="menu-header">{{ __('Monitor') }}</li>
            <li class="{{ Request::is('server_monitor*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('server_monitor.index') }}"><i class="fas fa-server"
                  aria-hidden="true"></i><span>{{ __('Servers') }}</span></a></li>
            <li class="{{ Request::is('webpage_monitor*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('webpage_monitor.index') }}"><i class="fas fa-globe"
                  aria-hidden="true"></i><span>{{ __('Webpages') }}</span></a></li>
            <li class="{{ Request::is('api_monitor*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('api_monitor.index') }}"><i class="fas fa-dot-circle"></i><span
                  class="ml-1">{{ __('APIs') }}</span></a></li>
            {{-- <li class="{{ Request::is('website_monitor*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('website_monitor.index') }}"><i class="fas fa-globe"
                  aria-hidden="true"></i><span>{{ __('Websites') }}</span></a></li> --}}
            <li class="{{ Request::is('ssl_monitor*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('ssl_monitor.index') }}"><i class="fas fa-globe"
                  aria-hidden="true"></i><span>{{ __('SSL') }}</span></a></li>
            <li class="{{ Request::is('domain_monitor*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('domain_monitor.index') }}"><i class="fas fa-globe"
                  aria-hidden="true"></i><span>{{ __('Domains') }}</span></a></li>
            <li class="{{ Request::is('dns_server_monitor*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('dns_server_monitor.index') }}"><i class="fas fa-globe"
                  aria-hidden="true"></i><span>{{ __('DNS') }}</span></a></li>
            <li class="{{ Request::is('keyword_monitor*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('keyword_monitor.index') }}"><i class="fas fa-globe"
                  aria-hidden="true"></i><span>{{ __('Keyword') }}</span></a></li>

            <li class="menu-header">{{ __('Status Pages') }}</li>
            <li class="{{ Request::is('status_page*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('status_page.index') }}"><i
                  class="fas fa-eye"></i><span>{{ __('Status Pages') }}</span></a></li>

            <li class="menu-header">{{ __('Incidents') }}</li>
            <li class="{{ Request::is('server_incident*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('server_incident.index') }}"><i class="fas fa-server"
                  aria-hidden="true"></i><span>{{ __('Server') }}</span></a></li>
            <li class="{{ Request::is('webpage_incident*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('webpage_incident.index') }}"><i class="fas fa-globe"
                  aria-hidden="true"></i><span>{{ __('Webpage') }}</span></a></li>
            <li class="{{ Request::is('api_incident*') ? 'active' : '' }}"><a class="nav-link"
                href="{{ route('api_incident.index') }}"><i
                  class="fas fa-dot-circle"></i><span>{{ __('API') }}</span></a></li>
            @endauth
          </ul>
        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          @yield('content')
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          {{ $footer_text }}
        </div>
        <div class="footer-right">
          <a href="{{ route('privacyPolicy') }}" class="pr-3 text-secondary">{{ __('Privacy Policy') }}</a><a
            href="{{ route('terms') }}" class="pr-3 text-secondary">{{ __('Terms of Use') }}</a>
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('js/main.js') }}"></script>
  <script src="{{ asset('js/stisla.js') }}"></script>

  <!-- JS Libraies -->
  <script src="{{ asset('js/library.js') }}"></script>
  <script src="{{ asset('js/datepicker.js') }}"></script>

  <!-- Template JS File -->
  <script src="{{ asset('js/script.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  <!-- Page Specific JS File -->
</body>

</html>