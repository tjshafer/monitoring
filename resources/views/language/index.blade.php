@extends('layouts.new_theme')

@section('content')

<div class="section-header">
    <h1>{{ __('Languages') }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard.index') }}">{{ __('Dashboard') }}</a></div>
        <div class="breadcrumb-item">{{ __('Languages') }}</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">{{ __('Languages') }}</h2>
    <p class="section-lead">
        {{__('Total number of languages')}}: {{ $languages->total() }}
    </p>

    <div class="row">
        <div class="col-12">
            @include('common.demo')
            @include('common.admin')
            @include('common.errors')
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('Add Language') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('language.store') }}">
                        @csrf

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Language') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="language" type="text"
                                    class="form-control @error('language') is-invalid @enderror" name="language"
                                    value="{{ old('language') }}" autocomplete="language" autofocus>
                                @error('language')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror

                                <small class="text-secondary"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('The name of the language must be entered in the same language. Example: English, Deutsche') }}.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label
                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Code') }}:*</label>
                            <div class="col-sm-12 col-md-7">
                                <input id="code" type="text" class="form-control @error('code') is-invalid @enderror"
                                    name="code" value="{{ old('code') }}" autocomplete="code" autofocus>
                                @error('code')
                                <div class="text-danger pt-1">{{ $message }}</div>
                                @enderror
                                <small class="text-secondary"><i class="fa fa-exclamation-circle"
                                        aria-hidden="true"></i>
                                    {{ __('The language code must be a two letter keyword based on ISO 2 letter (Alpha-2 code, ISO 639-1) standerd. Eg: en for English') }}.<br>
                                    {{ __('Reference: ') }} <a href="https://www.science.co.il/language/Codes.php"
                                        target="_blank" rel="noopener noreferrer">
                                        {{ __('Language codes') }} </a>
                                </small>
                            </div>
                        </div>

                        @if (config('app.env') != 'demo')
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-custom">{{ __('Add') }}</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="inline-block">{{ __('Available Languages') }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('Language') }}</th>
                                    <th>{{ __('Code') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($languages as $language)
                                <tr class="text-center">

                                    <td>{{ __($language->language) }}</td>
                                    <td>{{ __($language->code) }}</td>
                                    <td>
                                        <form action="{{ route('language.destroy', [$language->id]) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn bg-transparent"
                                                onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash text-danger" aria-hidden="true"
                                                    title="{{ __('Delete') }}"></i>
                                            </button>
                                        </form>
                                    </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        {{ $languages->appends($request->all())->links("pagination::bootstrap-4") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection