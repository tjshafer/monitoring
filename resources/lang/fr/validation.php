<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'mimes' => 'L :attribute doit être un fichier de type: :values.',
    'min' => [
        'numeric' => 'Le :attribute doit être au moins :min.',
        'file' => 'Le :attribute doit être au moins :min kilo-octets.',
        'string' => 'Le :attribute doit être au moins :min personnages.',
        'array' => 'Le :attribute doit avoir au moins :min éléments.',
    ],
    'password' => 'Le mot de passe est incorrect.',
    'required' => 'Le champ :attribute est obligatoire.',
    'same' => 'Les :attribute et :other doivent correspondre.',
    'unique' => 'Le :attribute a déjà été pris.',
    'email' => 'Le :attribute doit être une adresse e-mail valide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'title' => 'Titre',
        'name' => 'Nom',
        'email' => 'e-mail',
        'port' => 'Port',
        'subject' => 'matière',
        'language' => 'Langue',
        'code' => 'code',
        'c_password' => 'Confirmez le mot de passe',
        'message' => 'un message',
        'password' => 'le mot de passe',
        'first_name' => 'Prénom',
        'last_name' => 'nom de famille',
        'url' => 'URL',
        'response_code' => 'Code de réponse',
        'interval' => 'intervalle',
        'server_address' => 'adresse du serveur',
        'server_email' => 'E-mail du serveur',
        'price' => 'le prix',
        'properties[]' => 'Propriétés',
        'display_name' => 'Afficher un nom',
        'phone' => 'téléphoner',
        'address_1' => 'adresse',
        'postal_code' => 'code postal / zip',
        'state' => 'Etat',
        'country' => 'pays',
        'subject' => 'matière',
        'description' => 'la description',
        'updated_description' => 'la description',
        'status' => 'statut',
        'rel_id' => 'dessus',

        'properties.*' => 'dessus',
        'details.*' => 'dessus',

        'currency' => 'devise',
        'prefix' => 'préfixe',
        'display_order' => 'ordre daffichage',
        'server_count' => 'nombre de serveurs',
        'webpage_count' => 'nombre de pages Web',
        'api_count' => 'nombre dAPI',
        'user_count' => 'nombre dutilisateurs',
        'term' => 'terme',
        'price' => 'le prix',
        'currency_id' => 'devise'
    ],
    'value' => [

    ],
    'other' => [
        'password' => 'le mot de passe',

    ],
    'values' => [

    ],

];