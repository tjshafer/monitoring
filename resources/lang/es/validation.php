<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */


    'email' => 'El :attribute  debe ser una dirección de correo electrónico válida.',
    'image' => 'El :attribute debe ser una imagen.',
    'max' => [
        'numeric' => 'El :attribute no puede ser mayor que :max.',
        'file' => 'El :attribute no puede ser mayor que :max kilobytes.',
        'string' => 'El :attribute no puede ser mayor que :max caracteres.',
        'array' => 'El :attribute no puede tener más de :max artículos.',
    ],
    'mimes' => 'El :attribute debe ser un archivo de tipo: :values.',
    'min' => [
        'numeric' => 'El :attribute debe ser al menos :min.',
        'file' => 'El :attribute debe ser al menos :min kilobytes.',
        'string' => 'El :attribute debe ser al menos :min caracteres.',
        'array' => 'El :attribute debe tener al menos :min artículos.',
    ],
    'password' => 'La contraseña es incorrecta.',
    'required' => 'El campo de :attribute es obligatorio',
    'same' => 'El :attribute y :other debe coincidir con.',
    'unique' => 'El :attribute ya se ha tomado.',
    'unique_custom' => 'Ya me suscribí con este correo',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'title' => 'título',
        'name' => 'nombre',
        'email' => 'email',
        'port' => 'Puerto',
        'subject' => 'someter',
        'language' => 'idioma',
        'code' => 'código',
        'c_password' => 'confirmar Contraseña',
        'message' => 'mensaje',
        'password' => 'contraseña',
        'first_name' => 'primer nombre',
        'last_name' => 'apellido',
        'url' => 'URL',
        'response_code' => 'código de respuesta',
        'interval' => 'intervalo',
        'server_address' => 'dirección del servidor',
        'server_email' => 'servidor de correo electrónico',
        'price' => 'precio',
        'properties[]' => 'propiedades',
        'display_name' => 'nombre para mostrar',
        'phone' => 'teléfono',
        'address_1' => 'habla a',
        'postal_code' => 'código postal',
        'state' => 'Expresar',
        'country' => 'país',
        'subject' => 'Sujeta',
        'description' => 'descripción',
        'updated_description' => 'descripción',
        'status' => 'estado',
        'rel_id' => 'sobre',
        'properties.*' => 'encima',
        'details.*' => 'encima',

        'currency' => 'divisa',
        'prefix' => 'prefijo',
        'display_order' => 'Orden de visualización',
        'server_count' => 'recuento de servidores',
        'webpage_count' => 'recuento de páginas web',
        'api_count' => 'recuento de api',
        'user_count' => 'recuento de usuarios',
        'term' => 'término',
        'price' => 'precio',
        'currency_id' => 'divisa'
    ],
    'value' => [

    ],
    'other' => [
        'password' => 'contraseña',

    ],
    'values' => [

    ],

];