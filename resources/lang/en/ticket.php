<?php

return [

    'add_status' => 'Add status',
    'status_title' => 'Status title',
    'status_color' => 'Status color',
    'add' => 'Add',
    'ticket_statuses' => 'Ticket statuses',
    'action' => 'Action'

];
