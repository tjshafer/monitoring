<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */


    'email' => 'El :attribute  debe ser una dirección de correo electrónico válida.',
    'image' => 'El :attribute debe ser una imagen.',
    'max' => [
        'numeric' => 'El :attribute no puede ser mayor que :max.',
        'file' => 'El :attribute no puede ser mayor que :max kilobytes.',
        'string' => 'El :attribute no puede ser mayor que :max caracteres.',
        'array' => 'El :attribute no puede tener más de :max artículos.',
    ],
    'mimes' => 'El :attribute debe ser un archivo de tipo: :values.',
    'min' => [
        'numeric' => 'El :attribute debe ser al menos :min.',
        'file' => 'El :attribute debe ser al menos :min kilobytes.',
        'string' => 'El :attribute debe ser al menos :min caracteres.',
        'array' => 'El :attribute debe tener al menos :min artículos.',
    ],
    'password' => 'La contraseña es incorrecta.',
    'required' => 'El campo de :attribute es obligatorio',
    'same' => 'El :attribute y :other debe coincidir con.',
    'unique' => 'El :attribute ya se ha tomado.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'title' => 'Titel',
        'name' => 'name',
        'email' => 'email',
        'port' => 'Hafen',
        'subject' => 'Gegenstand',
        'language' => 'Sprache',
        'code' => 'code',
        'c_password' => 'Kennwort bestätigen',
        'message' => 'Botschaft',
        'password' => 'Passwort',
        'first_name' => 'Vorname',
        'last_name' => 'Nachname',
        'url' => 'URL',
        'response_code' => 'Antwortcode',
        'interval' => 'Intervall',
        'server_address' => 'Serveradresse',
        'server_email' => 'Server-E-Mail',
        'price' => 'Preis',
        'properties[]' => 'Eigenschaften',
        'display_name' => 'Anzeigename',
        'phone' => 'Telefon',
        'address_1' => 'Adresse',
        'postal_code' => 'Postleitzahl',
        'state' => 'Zustand',
        'country' => 'Land',
        'subject' => 'Gegenstand',
        'description' => 'Beschreibung',
        'updated_description' => 'Beschreibung',
        'status' => 'Status',
        'rel_id' => 'über',

        'properties.*' => 'Oben',
        'details.*' => 'Oben',

        'currency' => 'Währung',
        'prefix' => 'Präfix',
        'display_order' => 'Bestellung anzeigen',
        'server_count' => 'Serveranzahl',
        'webpage_count' => 'Webseitenanzahl',
        'api_count' => 'API-Zählung',
        'user_count' => 'Benutzeranzahl',
        'term' => 'Begriff',
        'price' => 'Preis',
        'currency_id' => 'Währung'
    ],
    'value' => [

    ],
    'other' => [
        'password' => 'passwort',

    ],
    'values' => [

    ],

];