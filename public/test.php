<?php

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Http\Request;

define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Check If The Application Is Under Maintenance
|--------------------------------------------------------------------------
|
| If the application is in maintenance / demo mode via the "down" command
| we will load this file so that any pre-rendered content can be shown
| instead of starting the framework, which could cause an exception.
|
*/

//echo "ji";exit;
require '../vendor/autoload.php';

//print_r(getAllLinks("http://whmcstools.com"));exit;

$allLinks = getAllLinks("https://www.w3schools.com");
foreach($allLinks as $url) {
    $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', $url);
    $httpcode = $response->getStatusCode();
    $dom = new DOMDocument();
    $html = (string) $response->getBody();
    $domHtml = @$dom->loadHTML($html);
    $dom->preserveWhiteSpace = false;
    $titleElement = $dom->getElementsByTagName('title');
    if ($titleElement->length > 0) {
	    $title = $titleElement->item(0)->textContent;
    } else {
	    $title = null;
    }
    echo "page title = $title\n";

    //$tags = get_meta_tags($content);
    //print_r($tags);
    echo "\n httpd code of   $url: $httpcode\n";	


    // Parse DOM to get meta data
    $metas = $dom->getElementsByTagName('meta');
    //print_r($metas);exit;
    $description = $keywords = '';
    for($i=0; $i<$metas->length; $i++){
	    $meta = $metas->item($i);
	    $attribute = strtolower($meta->getAttribute('name'));
	    echo "attribute =  $attribute \n";
    	if($attribute == 'description'){
        	$description = $meta->getAttribute('content');
    	}

    	if($attribute == 'keywords'){
        	$keywords = $meta->getAttribute('content');
    	}
	
    }

    echo "description = $description and \nkeywords =  $keywords\n";

}

 function getAllLinks($main_url) {
    // $uList=array();
    $urlList=array();
    $urlData = file_get_contents($main_url);
    $dom = new DOMDocument();
    @$dom->loadHTML($urlData);
    $xpath = new DOMXPath($dom);
    $hrefs = $xpath->evaluate("/html/body//a");
     for($i = 0; $i < $hrefs->length; $i++){
        $href = $hrefs->item($i);
        $url = $href->getAttribute('href');
        $url = filter_var($url, FILTER_SANITIZE_URL);

	if(filter_var($url, FILTER_VALIDATE_URL)){
		echo "$url\n";
            if(str_starts_with($url,$main_url)){
                $urlList[] = $url;
	    }
	} elseif(str_starts_with($url, "/")) {
		 $urlList[] = $main_url . $url;
	}	
     }

    return ($urlList);    
 
 }


echo $httpcode;exit;
/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request using
| the application's HTTP kernel. Then, we will send the response back
| to this client's browser, allowing them to enjoy our application.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

$kernel = $app->make(Kernel::class);

$response = tap($kernel->handle(
    $request = Request::capture()
))->send();

$kernel->terminate($request, $response);
