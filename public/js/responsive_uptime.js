function calculateViewbox(dayCount, rectWidth, rectPadding) {
    var viewBox = [];
    if (dayCount === 90) {
        viewBox.push(0);
    } else {
        var offset = 90 - dayCount;
        viewBox.push((offset * rectWidth) + (rectPadding * (offset))); // x origin
    }
    viewBox.push(0); // y origin
    viewBox.push((rectWidth * dayCount) + (rectPadding * (dayCount - 1))); // svg width
    viewBox.push(34); // svg height
    return viewBox.join(' ');
}

document.addEventListener('DOMContentLoaded', function () {

    var MAX_WIDTH_30_DAYS = 600,
        MAX_WIDTH_60_DAYS = 1024,
        svgs = document.getElementsByClassName('availability-time-line-graphic'),
        rects = svgs[0].getElementsByTagName('rect'),
        rectWidth = parseInt(rects[0].getAttribute('width')),
        rectPadding = parseInt(rects[1].getAttribute('x')) - parseInt(rects[0].getAttribute(
            'x')) - rectWidth,
        throttled = false,
        delay = 150,
        timeoutId;

    function getKeyAndCount(width) {
        if (width <= MAX_WIDTH_30_DAYS) {
            return {
                dayCount: 30,
                uptimeKey: 'thirty'
            }
        } else if (width <= MAX_WIDTH_60_DAYS) {
            return {
                dayCount: 60,
                uptimeKey: 'sixty'
            }
        } else {
            return {
                dayCount: 90,
                uptimeKey: 'ninety'
            }
        }
    }

    function setUptimeValue(values, uptimeKey) {
        var queryID = '.legend-item-' + values.component;
        var currentUptime = document.querySelector(queryID);
        if (currentUptime) {
            // Faster than setting innerHTML to "" then adding nodes
            var clone = currentUptime.cloneNode(false);
            var uptimeSpan = document.createElement('span');
            uptimeSpan.id = 'uptime-percent-' + values.component
            uptimeSpan.innerText = values[uptimeKey]
            clone.appendChild(uptimeSpan);
            var appendText = document.createTextNode(' % uptime');
            clone.appendChild(appendText);
            currentUptime.parentNode.replaceChild(clone, currentUptime);
        }
    }

    function setDayCount(el, dayCount) {
        // Faster than setting innerHTML to "" then adding nodes
        var clone = el.cloneNode(false);
        var dateSpan = document.createElement('span')
        dateSpan.className = "availability-time-line-legend-day-count"
        dateSpan.innerText = dayCount;
        clone.appendChild(dateSpan);
        var appendText = document.createTextNode(' days ago');
        clone.appendChild(appendText);
        el.parentNode.replaceChild(clone, el);
    }

    function resizeSvgViewBoxes() {
        var width = window.innerWidth;
        var columnInfo = getKeyAndCount(width);
        var dayCount = columnInfo.dayCount,
            uptimeKey = columnInfo.uptimeKey;
        var newViewboxValue = calculateViewbox(dayCount, rectWidth, rectPadding);

        // If a user quickly resizes from < 450 to > 900 without stopping,
        // it will retain the same 30 day info as it wont have changed, but this only
        // impacts 30 day display as it is the only one with shortened text
        if (newViewboxValue !== svgs[0].getAttribute('viewBox')) {
            for (var i = 0; i < svgs.length; i++) {
                var el = svgs[i];
                if (el.getAttribute('viewBox') !== newViewboxValue) {
                    el.setAttribute('viewBox', newViewboxValue);
                }
            }

            var dayCountElements = document.querySelectorAll(
                '.legend-item-date-range:first-of-type');

            for (var i = 0; i < dayCountElements.length; i++) {
                setDayCount(dayCountElements[i], dayCount);
            }

            uptimeValues = [{
                "component": "hchd2wyk50h7",
                "ninety": 100.0,
                "sixty": 100.0,
                "thirty": 100.0
            }, {
                "component": "vn9j228dp8c1",
                "ninety": 100.0,
                "sixty": 100.0,
                "thirty": 100.0
            }, {
                "component": "3bg3nq9mm436",
                "ninety": 100.0,
                "sixty": 100.0,
                "thirty": 100.0
            }];

            for (var i = 0; i < uptimeValues.length; i++) {
                setUptimeValue(uptimeValues[i], uptimeKey)
            }

            const uptimeLinkVar = document.querySelector('.components-uptime-link > var')
            if (uptimeLinkVar) {
                uptimeLinkVar.innerHTML = dayCount;
            }
        }
    }

    window.addEventListener('resize', function () {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function () {
            resizeSvgViewBoxes();
        }, delay);
    });

    resizeSvgViewBoxes();
});