<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/lang/{locale}', function ($locale) {
    session()->put('locale', $locale);

    return redirect()->back();
});

Auth::routes(['verify' => true]);
Route::get('/admin/login', [App\Http\Controllers\Auth\LoginController::class, 'showAdminLoginForm']);
Route::post('/admin/login', [App\Http\Controllers\Auth\LoginController::class, 'adminLogin']);

Route::middleware('auth:admin')->group(function () {
    Route::get('/admin', [App\Http\Controllers\Admin\DashboardController::class, 'index']);
});

Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);
Route::get('/', [App\Http\Controllers\User\DashboardController::class, 'index'])->name('home');

Route::resource('home', App\Http\Controllers\HomeController::class);
Route::get('/home/{plan_id}/{id}', [App\Http\Controllers\HomeController::class, 'showCurrencyWise'])->name('showCurrencyWise');

Route::resource('dashboard', App\Http\Controllers\User\DashboardController::class);
Route::resource('server_monitor', App\Http\Controllers\User\MonitorServerController::class);
Route::resource('dns_server_monitor', App\Http\Controllers\DnsMonitoringController::class);
Route::resource('keyword_monitor', App\Http\Controllers\KeywordMonitoringController::class);
Route::resource('webpage_monitor', App\Http\Controllers\User\MonitorWebpageController::class);
Route::resource('api_monitor', App\Http\Controllers\User\MonitorApiController::class);
Route::resource('website_monitor', App\Http\Controllers\User\MonitorWebsiteController::class);
Route::resource('available_plans', App\Http\Controllers\User\AddPlanController::class);
Route::resource('server_incident', App\Http\Controllers\User\ServerIncidentController::class);
Route::resource('webpage_incident', App\Http\Controllers\User\WebpageIncidentController::class);
Route::resource('website_incident', App\Http\Controllers\User\WebsiteIncidentController::class);
Route::resource('status_page', App\Http\Controllers\User\StatusPageController::class);
Route::resource('api_incident', App\Http\Controllers\User\ApiIncidentController::class);
Route::resource('ssl_monitor', App\Http\Controllers\User\SslMonitorController::class);
Route::resource('domain_monitor', App\Http\Controllers\User\DomainMonitorController::class);

Route::resource('alert_manager', App\Http\Controllers\User\AlertManagerController::class);

Route::get('/server_monitor/bulk_create/add', [App\Http\Controllers\User\MonitorServerController::class, 'bulk_create'])->name('server_bulk_create');
Route::post('/server_monitor/bulk_create', [App\Http\Controllers\User\MonitorServerController::class, 'bulk_store'])->name('server_bulk_store');
Route::get('/webpage_monitor/bulk_create/add', [App\Http\Controllers\User\MonitorWebpageController::class, 'bulk_create'])->name('webpage_bulk_create');
Route::post('/webpage_monitor/bulk_create', [App\Http\Controllers\User\MonitorWebpageController::class, 'bulk_store'])->name('webpage_bulk_store');
Route::get('/api_monitor/bulk_create/add', [App\Http\Controllers\User\MonitorApiController::class, 'bulk_create'])->name('api_bulk_create');
Route::post('/api_monitor/bulk_create', [App\Http\Controllers\User\MonitorApiController::class, 'bulk_store'])->name('api_bulk_store');
Route::get('/website_monitor/bulk_create/add', [App\Http\Controllers\User\MonitorWebsiteController::class, 'bulk_create'])->name('website_bulk_create');
Route::post('/website_monitor/bulk_create', [App\Http\Controllers\User\MonitorWebsiteController::class, 'bulk_store'])->name('website_bulk_store');
Route::get('/ssl_monitor/bulk_create/add', [App\Http\Controllers\User\SslMonitorController::class, 'bulk_create'])->name('ssl_bulk_create');
Route::post('/ssl_monitor/bulk_create', [App\Http\Controllers\User\SslMonitorController::class, 'bulk_store'])->name('ssl_bulk_store');
Route::get('/domain_monitor/bulk_create/add', [App\Http\Controllers\User\DomainMonitorController::class, 'bulk_create'])->name('domain_bulk_create');
Route::post('/domain_monitor/bulk_create', [App\Http\Controllers\User\DomainMonitorController::class, 'bulk_store'])->name('domain_bulk_store');

Route::POST('/update_server_details/{uuid}', [App\Http\Controllers\User\ServerIncidentController::class, 'updateIncidentDetails'])->name('updateServerDetails');
Route::POST('/update_page_details/{uuid}', [App\Http\Controllers\User\WebpageIncidentController::class, 'updateIncidentDetails'])->name('updateWebpageDetails');
Route::POST('/update_api_details/{uuid}', [App\Http\Controllers\User\ApiIncidentController::class, 'updateIncidentDetails'])->name('updateApiDetails');
Route::delete('/delete_server_details/{uuid}', [App\Http\Controllers\User\ServerIncidentController::class, 'deleteDetails'])->name('deleteServerDetails');
Route::delete('/delete_page_details/{uuid}', [App\Http\Controllers\User\WebpageIncidentController::class, 'deleteDetails'])->name('deleteWebpageDetails');
Route::delete('/delete_api_details/{uuid}', [App\Http\Controllers\User\ApiIncidentController::class, 'deleteDetails'])->name('deleteApiDetails');

Route::post('/{id}/place_order', [App\Http\Controllers\User\AddPlanController::class, 'placeOrder'])->name('place_order');
Route::get('/profile', [App\Http\Controllers\User\ProfileController::class, 'index'])->name('userProfile');
Route::post('/profile/update', [App\Http\Controllers\User\ProfileController::class, 'update'])->name('userProfileUpdate');

Route::get('/public_page/{name}', [App\Http\Controllers\PublicPageController::class, 'monitorPublicPage'])->name('monitorPublicPage');

Route::get('status_pages/{name}', [App\Http\Controllers\ShowStatusPageController::class, 'showStatusPage'])->name('showStatusPage');

Route::resource('/admin/plans', App\Http\Controllers\Admin\PlanController::class);
Route::resource('/admin/users', App\Http\Controllers\Admin\UserController::class);
Route::resource('/admin/orders', App\Http\Controllers\Admin\OrderController::class);
Route::resource('/admin/services', App\Http\Controllers\Admin\ServiceController::class);
Route::resource('/admin/invoices', App\Http\Controllers\Admin\InvoiceController::class);
Route::resource('/admin/language', App\Http\Controllers\Admin\LanguageController::class);
Route::resource('/admin/interval', App\Http\Controllers\Admin\IntervalController::class);
Route::resource('/admin/addon', App\Http\Controllers\Admin\AddonController::class);
Route::resource('/admin/billing_address', App\Http\Controllers\Admin\AddressController::class);
Route::get('/admin/profile', [App\Http\Controllers\Admin\ProfileController::class, 'index'])->name('adminProfile');
Route::post('/admin/profile/update', [App\Http\Controllers\Admin\ProfileController::class, 'update'])->name('adminProfileUpdate');
Route::resource('/admin/gateways', App\Http\Controllers\Admin\GatewayDetailsController::class);
Route::resource('/admin/pricing', App\Http\Controllers\Admin\PricingController::class);
Route::resource('/admin/currency', App\Http\Controllers\Admin\CurrencyController::class);
Route::resource('/admin/settings', App\Http\Controllers\Admin\SettingController::class);
Route::resource('email_template', App\Http\Controllers\Admin\EmailTemplateController::class);

Route::get('/pricing/{id}/pricing_index', [App\Http\Controllers\Admin\PricingController::class, 'pricing_index'])->name('pricing_index');
Route::get('/pricing/{id}/pricing_create', [App\Http\Controllers\Admin\PricingController::class, 'pricing_create'])->name('pricing_create');
Route::get('/pricing/{plan_id}/pricing_edit/{id}', [App\Http\Controllers\Admin\PricingController::class, 'pricing_edit'])->name('pricing_edit');

Route::get('available_plans/invoice/{id}', [App\Http\Controllers\User\AddPlanController::class, 'invoice'])->name('invoice');
Route::get('/plan_details/{plan_id}/{id}', [App\Http\Controllers\User\HomeController::class, 'showCurrencyWise'])->name('showCurrencyWise');

Route::post('{id}/process_order', [App\Http\Controllers\GatewayController::class, 'processOrder'])->name('processOrder');
Route::get('success', [App\Http\Controllers\GatewayController::class, 'success'])->name('success');
Route::get('cancel', [App\Http\Controllers\GatewayController::class, 'cancel'])->name('cancel');

Route::post('{id}/stripe-payment-confirm', [App\Http\Controllers\GatewayController::class, 'stripePayment'])->name('stripe.payment');
Route::post('paypal_response', [App\Http\Controllers\GatewayController::class, 'paypalResponse'])->name('paypalResponse');

Route::post('{id}/mollie-payment', [App\Http\Controllers\GatewayController::class, 'molliePayment'])->name('mollie.payment');
Route::post('{id}/bank-transfer', [App\Http\Controllers\GatewayController::class, 'bankTransfer'])->name('bank_transfer');
Route::post('WebhookNotification', [App\Http\Controllers\GatewayController::class, 'handleMollieWebhookNotification'])->name('webhooks.mollie');

Route::get('{uuid}/refund', [App\Http\Controllers\Admin\RefundController::class, 'refund'])->name('refund');
Route::get('paypalRefund', [App\Http\Controllers\Admin\RefundController::class, 'paypalRefund'])->name('paypalRefund');

Route::post('{id}/paypal_payment', [App\Http\Controllers\GatewayController::class, 'payPalPayment'])->name('paypal.payment');
Route::get('/paypal_done', [App\Http\Controllers\GatewayController::class, 'payPalResponse'])->name('getDone');

Route::get('/get_servers_api', [App\Http\Controllers\User\StatusPageController::class, 'getServersApi'])->name('getServersApi');
Route::get('/get_webpages_api', [App\Http\Controllers\User\StatusPageController::class, 'getWebpagesApi'])->name('getWebpagesApi');
Route::get('/get_apis', [App\Http\Controllers\User\StatusPageController::class, 'getApis'])->name('getApis');

Route::get('/privacy_policy', [App\Http\Controllers\FooterController::class, 'privacyPolicy'])->name('privacyPolicy');
Route::get('/terms_of_use', [App\Http\Controllers\FooterController::class, 'terms'])->name('terms');

Route::get('/dns_server_monitor/{id}/manage', [App\Http\Controllers\DnsMonitoringController::class, 'manage'])->name('manage');
Route::get('/dns_server_monitor/{id}/add_a_record', [App\Http\Controllers\DnsAaController::class, 'add'])->name('add_a_record');
Route::post('/dns_server_monitor/{id}/add_a_record', [App\Http\Controllers\DnsAaController::class, 'store'])->name('store_a_record');
Route::get('/dns_server_monitor/{id}/add_aaaa_record', [App\Http\Controllers\DnsAAAAController::class, 'add'])->name('add_aaaa_record');
Route::post('/dns_server_monitor/{id}/add_aaaa_record', [App\Http\Controllers\DnsAAAAController::class, 'store'])->name('store_aaaa_record');
Route::get('/dns_server_monitor/{id}/add_cname_record', [App\Http\Controllers\DnsCNameController::class, 'add'])->name('add_cname_record');
Route::post('/dns_server_monitor/{id}/add_cname_record', [App\Http\Controllers\DnsCNameController::class, 'store'])->name('store_cname_record');
Route::get('/dns_server_monitor/{id}/add_mx_record', [App\Http\Controllers\DnsMXController::class, 'add'])->name('add_mx_record');
Route::post('/dns_server_monitor/{id}/add_mx_record', [App\Http\Controllers\DnsMXController::class, 'store'])->name('store_mx_record');
Route::get('/dns_server_monitor/{id}/add_ns_record', [App\Http\Controllers\DnsNSController::class, 'add'])->name('add_ns_record');
Route::post('/dns_server_monitor/{id}/add_ns_record', [App\Http\Controllers\DnsNSController::class, 'store'])->name('store_ns_record');
Route::get('/dns_server_monitor/{id}/add_txt_record', [App\Http\Controllers\DnsTXTController::class, 'add'])->name('add_txt_record');
Route::post('/dns_server_monitor/{id}/add_txt_record', [App\Http\Controllers\DnsTXTController::class, 'store'])->name('store_txt_record');
Route::get('/dns_server_monitor/{id}/edit_a_record', [App\Http\Controllers\DnsAaController::class, 'edit'])->name('edit_a_record');
Route::post('/dns_server_monitor/{id}/edit_a_record', [App\Http\Controllers\DnsAaController::class, 'update'])->name('update_a_record');
Route::delete('/dns_server_monitor/{id}/delete_a_record', [App\Http\Controllers\DnsAaController::class, 'delete'])->name('delete_a_record');
Route::get('/dns_server_monitor/{id}/edit_aaaa_record', [App\Http\Controllers\DnsAAAAController::class, 'edit'])->name('edit_aaaa_record');
Route::post('/dns_server_monitor/{id}/edit_aaaa_record', [App\Http\Controllers\DnsAAAAController::class, 'update'])->name('update_aaaa_record');
Route::delete('/dns_server_monitor/{id}/delete_aaaa_record', [App\Http\Controllers\DnsAAAAController::class, 'delete'])->name('delete_aaaa_record');
Route::get('/dns_server_monitor/{id}/edit_cname_record', [App\Http\Controllers\DnsCNameController::class, 'edit'])->name('edit_cname_record');
Route::post('/dns_server_monitor/{id}/edit_cname_record', [App\Http\Controllers\DnsCNameController::class, 'update'])->name('update_cname_record');
Route::delete('/dns_server_monitor/{id}/delete_cname_record', [App\Http\Controllers\DnsCNameController::class, 'delete'])->name('delete_cname_record');
Route::get('/dns_server_monitor/{id}/edit_mx_record', [App\Http\Controllers\DnsMXController::class, 'edit'])->name('edit_mx_record');
Route::post('/dns_server_monitor/{id}/edit_mx_record', [App\Http\Controllers\DnsMXController::class, 'update'])->name('update_mx_record');
Route::delete('/dns_server_monitor/{id}/delete_mx_record', [App\Http\Controllers\DnsMXController::class, 'delete'])->name('delete_mx_record');
Route::get('/dns_server_monitor/{id}/edit_ns_record', [App\Http\Controllers\DnsNSController::class, 'edit'])->name('edit_ns_record');
Route::post('/dns_server_monitor/{id}/edit_ns_record', [App\Http\Controllers\DnsNSController::class, 'update'])->name('update_ns_record');
Route::delete('/dns_server_monitor/{id}/delete_ns_record', [App\Http\Controllers\DnsNSController::class, 'delete'])->name('delete_ns_record');
Route::get('/dns_server_monitor/{id}/edit_txt_record', [App\Http\Controllers\DnsTXTController::class, 'edit'])->name('edit_txt_record');
Route::post('/dns_server_monitor/{id}/edit_txt_record', [App\Http\Controllers\DnsTXTController::class, 'update'])->name('update_txt_record');
Route::delete('/dns_server_monitor/{id}/delete_txt_record', [App\Http\Controllers\DnsTXTController::class, 'delete'])->name('delete_txt_record');
Route::get('/dns_server_monitor/{id}/view_a_record', [App\Http\Controllers\DnsAaController::class, 'show'])->name('show_a_record');
Route::get('/dns_server_monitor/{id}/view_aaaa_record', [App\Http\Controllers\DnsAAAAController::class, 'show'])->name('show_aaaa_record');
Route::get('/dns_server_monitor/{id}/view_cname_record', [App\Http\Controllers\DnsCNameController::class, 'show'])->name('show_cname_record');
Route::get('/dns_server_monitor/{id}/view_mx_record', [App\Http\Controllers\DnsMXController::class, 'show'])->name('show_mx_record');
Route::get('/dns_server_monitor/{id}/view_ns_record', [App\Http\Controllers\DnsNSController::class, 'show'])->name('show_ns_record');
Route::get('/dns_server_monitor/{id}/view_txt_record', [App\Http\Controllers\DnsTXTController::class, 'show'])->name('show_txt_record');
