<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class Logger
{
    public static function emergency($message): void
    {
        Log::emergency($message);
    }

    public static function info($message): void
    {
        Log::info($message);
    }

    public static function error($message): void
    {
        Log::error($message);
    }
}
