<?php

namespace App\Helpers;

use App\Models\Incident;
use App\Models\Order;
use App\Models\Service;

class Random
{
    public static function getOrderNumber()
    {
        while (1) {
            $order_id = rand(1000000, 9999999);
            Logger::info('Generating the order id');
            Logger::info('order id generated: '.$order_id);
            $count = Order::where('order_id', $order_id)->count();

            if ($count > 0) {
                Logger::info("Order id $order_id already exist.");

                continue;
            }

            return $order_id;
        }
    }

    public static function getServiceNumber()
    {
        while (1) {
            $service_id = rand(1000000, 9999999);
            Logger::info('Generating the service id');
            Logger::info('service id generated: '.$service_id);
            $count = Service::where('service_id', $service_id)->count();

            if ($count > 0) {
                Logger::info("service id $service_id already exist.");

                continue;
            }

            return $service_id;
        }
    }

    public static function getIncidentNumber()
    {
        while (1) {
            $incident_id = rand(1000000, 9999999);
            Logger::info('Generating the incident id');
            Logger::info('incident id generated: '.$incident_id);
            $count = Incident::where('incident_id', $incident_id)->count();

            if ($count > 0) {
                Logger::info("incident id $incident_id already exist.");

                continue;
            }

            return $incident_id;
        }
    }
}
