<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('server_monitor:cron')->everyMinute();
        $schedule->command('webpage_monitor:cron')->everyMinute();
        $schedule->command('api_monitor:cron')->everyMinute();
        $schedule->command('ssl_monitor:cron')->daily();
        $schedule->command('domain_monitor:cron')->daily();
        $schedule->command('plan_validity:cron')->daily();
        $schedule->command('dns_monitor:cron')->everyMinute();
        $schedule->command('keyword_monitor:cron')->everyMinute();

        if (config('app.env') == 'demo') {
            $schedule->command('demo:cron')
                ->daily();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
