<?php

namespace App\Console\Commands;

use App\Helpers\Logger;
use App\Models\Hooks;
use App\Models\MonitorWebsite;
use App\Models\Service;
use App\Models\Services\MonitorWebsiteService;
use App\Models\User;
use App\Services\Email;
use App\Services\WebsiteStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class WebsiteMonitorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website_monitor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $websites = MonitorWebsite::all();

        $websiteStatus = new WebsiteStatus();

        $websiteService = new MonitorWebsiteService();

        foreach ($websites as $website) {
            //Get the user
            $user = User::where('id', $website->user_id)->first();
            //Check whether the user is active or innactive
            if ($user->status_id == 1 || $user->status_id == 2) {
                $service = Service::where('user_id', $user->id)->latest()->first();
                if ($service != null && $service->status_id == 1) {
                    Logger::info('checking domain expiry for '.$website->url);
                    $domain_info = $websiteStatus->domainExpiryStatus($website->url);
                    if ($domain_info) {
                        Logger::info('checking ssl expiry for '.$website->url);
                        $ssl_expiry = $websiteStatus->sslExpiryStatus($website->url, $website->ssl_port);
                        $domain_expiry = date('Y-m-d H:i:s', $domain_info->expirationDate);

                        $website->ssl_expiry = $ssl_expiry;
                        $website->domain_expiry = Carbon::parse($domain_expiry);
                        $website->save();

                        Logger::info('creating log for '.$website->url);
                        $websiteService->monitorLog($website);

                        // Send validity reminder mail

                        $currentDate = Carbon::now()->format('Y-m-d');
                        $domainFirstInvoiceDate = Carbon::parse($domain_expiry)->subDays(7)->format('Y-m-d');
                        $domainSecondInvoiceDate = Carbon::parse($domain_expiry)->subDays(2)->format('Y-m-d');
                        $emailService = new Email();
                        /**
                         * Sending Domain validity reminder mail.
                         */
                        if ($domainFirstInvoiceDate == $currentDate) {
                            Logger::info('Domain validity will expire in 7 days');
                            $expireIn = 7;
                            $emailService->domainValidityReminder($website, $expireIn);
                        } elseif ($domainSecondInvoiceDate == $currentDate) {
                            Logger::info('Domain validity will expire in 2 days');
                            $expireIn = 2;
                            $emailService->domainValidityReminder($website, $expireIn);
                        }

                        //sms part
                        $hooks = Hooks::where('status', 1)->get();
                        foreach ($hooks as $hook) {
                            //Logger::info($hook);
                            $classname = $hook->class;
                            if (class_exists($classname)) {
                                $instance = (new \ReflectionClass($classname))->newInstance();
                                if (method_exists($instance, 'onDomainExpiry')) {
                                    $instance->onDomainExpiry($website, $expireIn);
                                }
                            }
                        }
                        /**
                         * Sending SSL validity reminder mail.
                         */
                        if ($ssl_expiry) {
                            $sslFirstInvoiceDate = Carbon::parse($ssl_expiry)->subDays(7)->format('Y-m-d');
                            $sslSecondInvoiceDate = Carbon::parse($ssl_expiry)->subDays(2)->format('Y-m-d');

                            if ($sslFirstInvoiceDate == $currentDate) {
                                Logger::info('SSL validity will expire in 7 days');
                                $expireIn = 7;
                                $emailService->sslValidityReminder($website, $expireIn);
                            } elseif ($sslSecondInvoiceDate == $currentDate) {
                                Logger::info('SSL validity will expire in 2 days');
                                $expireIn = 2;
                                $emailService->sslValidityReminder($website, $expireIn);
                            }

                            //sms part
                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onSslExpiry')) {
                                        $instance->onSslExpiry($website, $expireIn);
                                    }
                                }
                            }
                        }
                    } else {
                        Logger::info('Domain info not available for '.$website->url);
                    }
                } else {
                    Logger::info('No service active for '.$user->first_name);
                }
            }
        }
    }
}
