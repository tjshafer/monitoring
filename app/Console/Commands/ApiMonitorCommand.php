<?php

namespace App\Console\Commands;

use App\Helpers\Logger;
use App\Helpers\Uuid;
use App\Models\Hooks;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\Service;
use App\Models\Services\IncidentService;
use App\Models\Services\MonitorService;
use App\Models\Setting;
use App\Models\User;
use App\Services\Email;
use App\Services\MonitorStatus;
use App\Services\SubscriptionEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class ApiMonitorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api_monitor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        //Get all available Apis
        $apis = Monitor::where('type', 'api')->get();
        
        $apiStatus = new MonitorStatus();

        $apiService = new MonitorService();

        foreach ($apis as $api) {

            $user = User::where('id', $api->user_id)->first();

            //Check whether the user is active or innactive
            if ($user->status_id == 1 || $user->status_id == 2) {
                /**
                 * Check the api monitoring interval by previousUpdate() function in MonitorStatus
                 * Check current status only if the time interval is over in addition with previous update
                 */
                if ($apiStatus->previousUpdate($api)) {
                    Logger::info('Checking status of API');
                    //Create an object for MonitorLog
                    $log = new MonitorLog();
                    $log->uuid = Uuid::getUuid();
                    $log->rel_id = $api->uuid;
                    $log->type = 'api';
                    Logger::info("id: $api->uuid");
                    $startTime = intval(microtime(true) * 1000);
                    /**
                     * The case where API is up
                     * Status check using isUpApi() function in MonitorStatus service
                     */
                    if ($apiStatus->isUpApi($api->url, $api->expected_response_code, $api->json, $api->methode)) {
                        $endTime = intval(microtime(true) * 1000);
                        Logger::info("Previous status: $api->previous_status");
                        Logger::info('Status check done');
                        Logger::info($api->name.' is up and running');
                        //Let's notify that the API is back online with E-mail
                        if ($api->previous_status == 'down') {
                            $autoIncident = Setting::where('name', 'auto_incident')
                            ->value('value');
                            if ($autoIncident) {
                                Logger::info('Updating auto incident');
                                $incidentService = new IncidentService();
                                $incidentService->updateAutoIncident($api->uuid);
                            }

                            Logger::error('Sending API up mail for '.$api->name);
                            $log->down_time = $api->interval;

                            // Sent api up mail to user's Email
                            Logger::error('Sending api up mail');
                            $emailService = new Email();
                            $emailService->apiMonitorUp($api);

                            // Sent api up mail to email subscription enabled statuspage users
                            $emailSubscriptionService = new SubscriptionEmail();
                            $emailSubscriptionService->apiMonitorUp($api);

                            //sent twilio sms
                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onApiIsUp')) {
                                        $instance->onApiIsUp($api);
                                    }
                                }
                            }
                        }

                        // make the latest API status as up
                        $api->previous_status = 'up';
                        $log->response_time = ($endTime - $startTime);
                        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                        $log->previous_time = Carbon::now()->subMinute($api->interval)->format('Y-m-d H:i:s');
                        /**
                         * Api is up, just add the row in to a table
                         */
                        $log->is_up = 1;
                        $log->save();

                        $avgResponseTime = $apiService->getAverageResponseTime($api->uuid);
                        $api->avg_response_time = $avgResponseTime;
                        $api->save();
                    } else {
                        /**
                         * Api is down, add a row to the table,
                         * if should be notified send an email/sms.
                         */
                        $endTime = intval(microtime(true) * 1000);
                        $log->is_up = 0;
                        Logger::error($api->name.' is down');

                        //Let's notify that the API is down with E-mail
                        if ($api->previous_status == 'up') {
                            $autoIncident = Setting::where('name', 'auto_incident')
                                ->value('value');
                            if ($autoIncident) {
                                Logger::info('Adding auto incident');
                                $incidentService = new IncidentService();
                                $incidentService->addAutoIncident($api->uuid);
                            }

                            // Sent api down mail to user's Email
                            Logger::error('Sending api down mail');
                            $emailService = new Email();
                            $emailService->apiMonitorDown($api);

                            // Sent api down mail to email subscription enabled statuspage users
                            $emailSubscriptionService = new SubscriptionEmail();
                            $emailSubscriptionService->apiMonitorDown($api);
                            //sent sms
                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onApiIsDown')) {
                                        $instance->onApiIsDown($api);
                                    }
                                }
                            }
                        }
                        // make the latest API status as down
                        $api->previous_status = 'down';
                        $log->down_time = $api->interval;
                        $log->response_time = 0;
                        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                        $log->previous_time = Carbon::now()->subMinute($api->interval)->format('Y-m-d H:i:s');
                        $log->save();

                        //Calculating avg. response time with getAverageResponseTime() function in MonitorService
                        $api->avg_response_time = $apiService->getAverageResponseTime($api->uuid);
                        $api->save();
                    }
                }
            }
        }
    }
}
