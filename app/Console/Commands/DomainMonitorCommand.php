<?php

namespace App\Console\Commands;

use App\Helpers\Logger;
use App\Models\DomainMonitor;
use App\Models\Hooks;
use App\Models\Service;
use App\Models\Services\DomainMonitorService;
use App\Models\User;
use App\Services\Email;
use App\Services\WebsiteStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class DomainMonitorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domain_monitor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $domains = DomainMonitor::all();

        $domainStatus = new WebsiteStatus();

        $domainService = new DomainMonitorService();

        foreach ($domains as $domain) {
            //Get the user
            $user = User::where('id', $domain->user_id)->first();
            //Check whether the user is active or innactive
            if ($user->status_id == 1 || $user->status_id == 2) {
                $service = Service::where('user_id', $user->id)->latest()->first();
                if ($service != null && $service->status_id == 1) {
                    Logger::info('checking domain expiry for '.$domain->url);
                    $domain_info = $domainStatus->domainExpiryStatus($domain->url);
                    if ($domain_info) {
                        $domain_expiry = date('Y-m-d H:i:s', $domain_info->expirationDate);
                        $domain->domain_expiry = Carbon::parse($domain_expiry);
                        $domain->save();

                        Logger::info('creating log for '.$domain->url);
                        $domainService->monitorLog($domain);

                        // Send validity reminder mail

                        $currentDate = Carbon::now()->format('Y-m-d');
                        $domainFirstInvoiceDate = Carbon::parse($domain_expiry)->subDays(7)->format('Y-m-d');
                        $domainSecondInvoiceDate = Carbon::parse($domain_expiry)->subDays(2)->format('Y-m-d');
                        $emailService = new Email();
                        /**
                         * Sending Domain validity reminder mail.
                         */
                        if ($domainFirstInvoiceDate == $currentDate) {
                            Logger::info('Domain validity will expire in 7 days');
                            $expireIn = 7;
                            $emailService->domainValidityReminder($domain, $expireIn);
                        } elseif ($domainSecondInvoiceDate == $currentDate) {
                            Logger::info('Domain validity will expire in 2 days');
                            $expireIn = 2;
                            $emailService->domainValidityReminder($domain, $expireIn);
                        }
                        //sms part
                        $hooks = Hooks::where('status', 1)->get();
                        foreach ($hooks as $hook) {
                            //Logger::info($hook);
                            $classname = $hook->class;
                            if (class_exists($classname)) {
                                $instance = (new \ReflectionClass($classname))->newInstance();
                                if (method_exists($instance, 'onDomainExpiry')) {
                                    $instance->onDomainExpiry($domain, $expireIn);
                                }
                            }
                        }
                    } else {
                        Logger::info('Domain info not available for '.$domain->url);
                    }
                } else {
                    Logger::info('No service active for '.$user->first_name);
                }
            }
        }
    }
}
