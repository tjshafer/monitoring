<?php

namespace App\Console\Commands;

use App\Helpers\Logger;
use App\Helpers\Uuid;
use App\Models\Hooks;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\Service;
use App\Models\Services\IncidentService;
use App\Models\Services\MonitorService;
use App\Models\Setting;
use App\Models\User;
use App\Services\Email;
use App\Services\MonitorStatus;
use App\Services\SubscriptionEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class WebpageMonitorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webpage_monitor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        //Get all available webpages
        $pages = Monitor::where('type', 'webpage')
            ->get();

        /**
         * Service class that associated with webpage monitoring status
         * Refer app/Service/MonitorStatus.php
         */
        $webpageStatus = new MonitorStatus();
        /**
         * Service class that interact with Monitor model
         * Refer app/Service/Models/MonitorService.php
         */
        $webpageService = new MonitorService();
        foreach ($pages as $page) {
            //Get the user
            $user = User::where('id', $page->user_id)->first();
            //Check whether the user is active or innactive
            if ($user->status_id == 1 || $user->status_id == 2) {
                /**
                 * Check the webpage monitoring interval by previousUpdate() function in MonitorStatus
                 * Check current status only if the time interval is over in addition with previous update
                 */
                if ($webpageStatus->previousUpdate($page)) {
                    Logger::info('Checking status of Webpage');
                    //Create an object for MonitorLog
                    $log = new MonitorLog();
                    $log->uuid = Uuid::getUuid();
                    $log->rel_id = $page->uuid;
                    $log->type = 'webpage';
                    Logger::info("id: $page->uuid");
                    $url = $page->url;
                    $expected_response_code = $page->expected_response_code;
                    $startTime = intval(microtime(true) * 1000);

                    /**
                     * The case where webpage is up
                     * Status check using isUpWebpage() function in MonitorStatus service
                     */
                    if ($webpageStatus->isUpWebpage($url, $expected_response_code)) {
                        $endTime = intval(microtime(true) * 1000);
                        Logger::info("Previous status: $page->previous_status");
                        Logger::info('Status check done');
                        Logger::info($page->name.' is up and running');
                        //Let's notify that the webpage is back online with E-mail
                        if ($page->previous_status == 'down') {
                            $autoIncident = Setting::where('name', 'auto_incident')
                            ->value('value');
                            if ($autoIncident) {
                                Logger::info('Updating auto incident');
                                $incidentService = new IncidentService();
                                $incidentService->updateAutoIncident($page->uuid);
                            }

                            $log->down_time = $page->interval;
                            // Sent webpage up mail to user's Email
                            Logger::error('Sending webpage up mail');
                            $emailService = new Email();
                            $emailService->webpageMonitorUp($page);

                            // Sent webpage up mail to email subscription enabled statuspage users
                            $emailSubscriptionService = new SubscriptionEmail();
                            $emailSubscriptionService->webpageMonitorUp($page);

                            //sent twilio sms
                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onWebpageIsUp')) {
                                        $instance->onWebpageIsUp($page);
                                    }
                                }
                            }
                        }

                        // make the latest webpage status as up
                        $page->previous_status = 'up';
                        $log->response_time = ($endTime - $startTime);
                        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                        $log->previous_time = Carbon::now()->subMinute($page->interval)->format('Y-m-d H:i:s');
                        /**
                         * Webpage is up, just add the row in to a table
                         */
                        $log->is_up = 1;
                        $log->save();

                        $avgResponseTime = $webpageService->getAverageResponseTime($page->uuid);
                        $page->avg_response_time = $avgResponseTime;
                        $page->save();
                    } else {
                        /**
                         * Webpage is down, add a row to the table,
                         * if should be notified send an email/sms.
                         */
                        $endTime = intval(microtime(true) * 1000);
                        $log->is_up = 0;
                        Logger::error($page->name.' is down');

                        //Let's notify that the webpage is down with E-mail
                        if ($page->previous_status == 'up') {
                            $autoIncident = Setting::where('name', 'auto_incident')
                                ->value('value');
                            if ($autoIncident) {
                                Logger::info('Adding auto incident');
                                $incidentService = new IncidentService();
                                $incidentService->addAutoIncident($page->uuid);
                            }

                            // Sent webpage down mail to user's Email
                            Logger::error('Sending webpage down mail');
                            $emailService = new Email();
                            $emailService->webpageMonitorDown($page);

                            // Sent webpage down mail to email subscription enabled statuspage users
                            $emailSubscriptionService = new SubscriptionEmail();
                            $emailSubscriptionService->webpageMonitorDown($page);

                            //sent sms
                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onWebpageIsDown')) {
                                        $instance->onWebpageIsDown($page);
                                    }
                                }
                            }
                        }
                        // make the latest webpage status as down
                        $page->previous_status = 'down';
                        $log->down_time = $page->interval;
                        $log->response_time = 0;
                        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                        $log->previous_time = Carbon::now()->subMinute($page->interval)->format('Y-m-d H:i:s');
                        $log->save();

                        //Calculating avg. response time with getAverageResponseTime() function in MonitorService
                        $page->avg_response_time = $webpageService->getAverageResponseTime($page->uuid);
                        $page->save();
                    }
                }
            }
        }
    }
}
