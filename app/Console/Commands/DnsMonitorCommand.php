<?php

namespace App\Console\Commands;

use App\Helpers\Logger;
use App\Helpers\Uuid;
use App\Models\DnsMonitoring;
use App\Models\DnsMonitorLogs;
use App\Models\Hooks;
use App\Models\Service;
use App\Models\Services\DnsMonitorService;
use App\Models\Services\IncidentService;
use App\Models\Setting;
use App\Models\User;
use App\Services\DnsStatus;
use App\Services\Email;
use App\Services\SubscriptionEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class DnsMonitorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dns_monitor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $dnsServers = DnsMonitoring::with('user')->get();

        $dnsStatus = new DnsStatus();

        $DnsService = new DnsMonitorService();

        foreach ($dnsServers as $dnsServer) {
            $dnsARecords = DnsMonitoring::find($dnsServer->uuid)->aRecord;
            $dnsAaaaRecords = DnsMonitoring::find($dnsServer->uuid)->aaaaRecord;
            $dnsCnameRecords = DnsMonitoring::find($dnsServer->uuid)->cnameRecord;
            $dnsMxRecords = DnsMonitoring::find($dnsServer->uuid)->mxRecord;
            $dnsNsRecords = DnsMonitoring::find($dnsServer->uuid)->nsRecord;
            $dnsTxtRecords = DnsMonitoring::find($dnsServer->uuid)->txtRecord;

            $serverUp = true;

            Logger::info($dnsServer->website_domain);
            //DnsARecord check
            foreach ($dnsARecords as $dnsARecord) {
                Logger::info($dnsARecord->ip_address);
                //Get the user
                $user = User::where('id', $dnsServer->user_id)->first();
                //Check whether the user is active or innactive
                if ($user->status_id == 1 || $user->status_id == 2) {
                    /**
                     * Check the server monitoring interval by previousUpdate() function in DnsStatus
                     * Check current status only if the time interval is over in addition with previous update
                     */
                    if ($dnsStatus->previousUpdate($dnsServer, $dnsARecord)) {
                        Logger::info('Checking status of A Record');
                        //Create an object for DnsMonitorLog
                        $log = new DnsMonitorLogs();
                        $url = $dnsServer->website_domain;
                        $log->uuid = Uuid::getUuid();
                        $log->rel_id = $dnsARecord->uuid;
                        $log->website_id = $dnsServer->uuid;
                        $log->type = 'A';
                        $startTime = intval(microtime(true) * 1000);
                        $ip_array = explode(',', $dnsARecord->ip_address);

                        /**
                         * The case where server is up
                         * Status check using checkArecord() function in DnsStatus service
                         */
                        $resolved = $dnsStatus->checkArecord($url, $ip_array);
                        $dnsARecord->last_resolved_ip = $resolved;
                        if ($resolved != null) {
                            //if ($dnsStatus->checkArecord($url, array($dnsARecord->ip_address))) {
                            $endTime = intval(microtime(true) * 1000);

                            Logger::info('Previous status');
                            Logger::info($dnsARecord->previous_status);
                            Logger::info('Status check done');

                            //Let's notify that the API is back online with E-mail
                            if ($dnsARecord->previous_status == 'down') {
                                // $autoIncident = Setting::where('name', 'auto_incident')
                                    //     ->value('value');
                                // if ($autoIncident) {
                                    //     Logger::info("Updating auto incident");
                                    //     $incidentService = new IncidentService();
                                    //     $incidentService->updateAutoIncident($dnsARecord->uuid);
                                // }
                                $log->down_time = $dnsServer->interval;
                                // Sent server up mail to user's Email
                                Logger::error('Sending server up mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorUp($dnsServer, 'A Record ', $dnsARecord->ip_address);

                                // Sent server up mail to email subscription enabled statuspage users
                                // $emailSubscriptionService = new SubscriptionEmail();
                                // $emailSubscriptionService->serverMonitorUp($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsUp')) {
                                            $instance->onDnsServerIsUp($dnsServer);
                                        }
                                    }
                                }
                            }
                            // make the latest server status as up
                            $dnsARecord->previous_status = 'up';

                            $log->response_time = ($endTime - $startTime);
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            /**
                             * Server is up, just add the row in to a table
                             */
                            $log->is_up = 1;
                            $log->save();

                            $avgResponseTime = $DnsService->getAverageResponseTime($dnsARecord->uuid);
                            $dnsARecord->avg_response_time = $avgResponseTime;
                            $dnsARecord->save();
                        } else {
                            /*
                            Server is down, add a row to the table,
                            if should be notified send an email/sms.
                            */
                            $endTime = intval(microtime(true) * 1000);

                            $log->is_up = 0;
                            Logger::error($dnsServer->website_domain.' is down');

                            //Let's notify that the server is down with E-mail
                            if ($dnsARecord->previous_status == 'up') {
                                //     $autoIncident = Setting::where('name', 'auto_incident')
                                //         ->value('value');
                                //     if ($autoIncident) {
                                //         Logger::info("Adding auto incident");
                                //         $incidentService = new IncidentService();
                                //         $incidentService->addAutoIncident($server->uuid);
                                //     }

                                // Sent server down mail to user's Email
                                Logger::error('Sending server down mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorDown($dnsServer, 'A Record ', $dnsARecord->ip_address);

                                //     // Sent server down mail to email subscription enabled statuspage users
                                //     $emailSubscriptionService = new SubscriptionEmail();
                                //     $emailSubscriptionService->serverMonitorDown($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsDown')) {
                                            $instance->onDnsServerIsDown($dnsServer);
                                        }
                                    }
                                }
                            }
                            // make the latest server status as down
                            $dnsARecord->previous_status = 'down';
                            $log->response_time = 0;
                            $log->down_time = $dnsServer->interval;
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            $log->save();

                            //Calculating avg. response time with getAverageResponseTime() function in DnsMonitorService
                            $dnsARecord->avg_response_time = $DnsService->getAverageResponseTime($dnsARecord->uuid);
                            $dnsARecord->save();

                            $serverUp = false;
                        }
                    }
                }
            }
            // DnsAAAARecord check
            foreach ($dnsAaaaRecords as $dnsAaaaRecord) {
                //Get the user
                $user = User::where('id', $dnsServer->user_id)->first();
                //Check whether the user is active or innactive
                if ($user->status_id == 1 || $user->status_id == 2) {
                    /**
                     * Check the server monitoring interval by previousUpdate() function in DnsStatus
                     * Check current status only if the time interval is over in addition with previous update
                     */
                    if ($dnsStatus->previousUpdate($dnsServer, $dnsAaaaRecord)) {
                        Logger::info('Checking status of AAAA Record');
                        //Create an object for DnsMonitorLog
                        $log = new DnsMonitorLogs();
                        $url = $dnsServer->website_domain;
                        $log->uuid = Uuid::getUuid();
                        $log->rel_id = $dnsAaaaRecord->uuid;
                        $log->website_id = $dnsServer->uuid;
                        $log->type = 'AAAA';
                        $startTime = intval(microtime(true) * 1000);
                        $ip_array = explode(',', $dnsAaaaRecord->ipv6_address);

                        /**
                         * The case where server is up
                         * Status check using checkAaaarecord() function in DnsStatus service
                         */
                        $resolved = $dnsStatus->checkAaaaRecord($url, $ip_array);
                        $dnsAaaaRecord->last_resolved_ip = $resolved;
                        if ($resolved != null) {
                            $endTime = intval(microtime(true) * 1000);

                            Logger::info('Previous status');
                            Logger::info($dnsAaaaRecord->previous_status);
                            Logger::info('Status check done');
                            //Let's notify that the API is back online with E-mail
                            if ($dnsAaaaRecord->previous_status == 'down') {
                                $log->down_time = $dnsServer->interval;

                                Logger::error('Sending server up mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorUp($dnsServer, 'AAAA Record ', $dnsAaaaRecord->ipv6_address);

                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsUp')) {
                                            $instance->onDnsServerIsUp($dnsServer);
                                        }
                                    }
                                }
                            }
                            // make the latest server status as up
                            $dnsAaaaRecord->previous_status = 'up';

                            $log->response_time = ($endTime - $startTime);
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            /**
                             * Server is up, just add the row in to a table
                             */
                            $log->is_up = 1;
                            $log->save();

                            $avgResponseTime = $DnsService->getAverageResponseTime($dnsAaaaRecord->uuid);
                            $dnsAaaaRecord->avg_response_time = $avgResponseTime;
                            $dnsAaaaRecord->save();
                        } else {
                            /*
                            Server is down, add a row to the table,
                            if should be notified send an email/sms.
                            */
                            $endTime = intval(microtime(true) * 1000);

                            $log->is_up = 0;
                            Logger::error($dnsServer->website_domain.' is down');

                            //Let's notify that the server is down with E-mail
                            if ($dnsAaaaRecord->previous_status == 'up') {
                                // Sent server down mail to user's Email
                                Logger::error('Sending server down mail');

                                $emailService = new Email();
                                $emailService->dnsMonitorDown($dnsServer, 'AAAA Record', $dnsAaaaRecord->ipv6_address);

                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsDown')) {
                                            $instance->onDnsServerIsDown($dnsServer);
                                        }
                                    }
                                }
                            }
                            // make the latest server status as down
                            $dnsAaaaRecord->previous_status = 'down';
                            $log->response_time = 0;
                            $log->down_time = $dnsServer->interval;
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            $log->save();

                            //Calculating avg. response time with getAverageResponseTime() function in DnsMonitorService
                            $dnsAaaaRecord->avg_response_time = $DnsService->getAverageResponseTime($dnsAaaaRecord->uuid);
                            $dnsAaaaRecord->save();
                            $serverUp = false;
                        }
                    }
                }
            }
            // DnsCnameRecord check
            foreach ($dnsCnameRecords as $dnsCnameRecord) {
                //Get the user
                $user = User::where('id', $dnsServer->user_id)->first();
                //Check whether the user is active or innactive
                if ($user->status_id == 1 || $user->status_id == 2) {
                    /**
                     * Check the server monitoring interval by previousUpdate() function in DnsStatus
                     * Check current status only if the time interval is over in addition with previous update
                     */
                    if ($dnsStatus->previousUpdate($dnsServer, $dnsCnameRecord)) {
                        Logger::info('Checking status of Cname Record');
                        //Create an object for DnsMonitorLog
                        $log = new DnsMonitorLogs();
                        $url = $dnsServer->website_domain;
                        $log->uuid = Uuid::getUuid();
                        $log->rel_id = $dnsCnameRecord->uuid;
                        $log->website_id = $dnsServer->uuid;
                        $log->type = 'CNAME';
                        $startTime = intval(microtime(true) * 1000);

                        /**
                         * The case where server is up
                         * Status check using checkCnameRecord() function in DnsStatus service
                         */
                        if ($dnsStatus->checkCnameRecord($url, $dnsCnameRecord->value)) {
                            $endTime = intval(microtime(true) * 1000);

                            Logger::info('Previous status');
                            Logger::info($dnsCnameRecord->previous_status);
                            Logger::info('Status check done');

                            //Let's notify that the API is back online with E-mail
                            if ($dnsCnameRecord->previous_status == 'down') {
                                // $autoIncident = Setting::where('name', 'auto_incident')
                                    //     ->value('value');
                                // if ($autoIncident) {
                                    //     Logger::info("Updating auto incident");
                                    //     $incidentService = new IncidentService();
                                    //     $incidentService->updateAutoIncident($dnsCnameRecord->uuid);
                                // }
                                $log->down_time = $dnsServer->interval;
                                // Sent server up mail to user's Email
                                Logger::error('Sending server up mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorUp($dnsServer, 'Cname Record ', $dnsCnameRecord->value);

                                // Sent server up mail to email subscription enabled statuspage users
                                // $emailSubscriptionService = new SubscriptionEmail();
                                // $emailSubscriptionService->serverMonitorUp($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsUp')) {
                                            $instance->onDnsServerIsUp($dnsServer);
                                        }
                                    }
                                }
                            }
                            // make the latest server status as up
                            $dnsCnameRecord->previous_status = 'up';
                            $log->response_time = ($endTime - $startTime);
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            /**
                             * Server is up, just add the row in to a table
                             */
                            $log->is_up = 1;
                            $log->save();

                            $avgResponseTime = $DnsService->getAverageResponseTime($dnsCnameRecord->uuid);
                            $dnsCnameRecord->avg_response_time = $avgResponseTime;
                            $dnsCnameRecord->save();
                        } else {
                            /*
                            Server is down, add a row to the table,
                            if should be notified send an email/sms.
                            */
                            $endTime = intval(microtime(true) * 1000);

                            $log->is_up = 0;
                            Logger::error($dnsServer->website_domain.' is down');

                            //Let's notify that the server is down with E-mail
                            if ($dnsCnameRecord->previous_status == 'up') {
                                    //     $autoIncident = Setting::where('name', 'auto_incident')
                                    //         ->value('value');
                                    //     if ($autoIncident) {
                                    //         Logger::info("Adding auto incident");
                                    //         $incidentService = new IncidentService();
                                    //         $incidentService->addAutoIncident($server->uuid);
                                    //     }

                                // Sent server down mail to user's Email
                                Logger::error('Sending server down mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorDown($dnsServer, 'Cname Record ', $dnsCnameRecord->value);

                                //     // Sent server down mail to email subscription enabled statuspage users
                                //     $emailSubscriptionService = new SubscriptionEmail();
                                //     $emailSubscriptionService->serverMonitorDown($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsDown')) {
                                            $instance->onDnsServerIsDown($dnsServer);
                                        }
                                    }
                                }
                            }
                            // make the latest server status as down
                            $dnsCnameRecord->previous_status = 'down';
                            $log->response_time = 0;
                            $log->down_time = $dnsServer->interval;
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            $log->save();

                            //Calculating avg. response time with getAverageResponseTime() function in DnsMonitorService
                            $dnsCnameRecord->avg_response_time = $DnsService->getAverageResponseTime($dnsCnameRecord->uuid);
                            $dnsCnameRecord->save();
                            $serverUp = false;
                        }
                    }
                }
            }
            //DnsMxRecord check
            foreach ($dnsMxRecords as $dnsMxRecord) {
                //Get the user
                $user = User::where('id', $dnsServer->user_id)->first();
                //Check whether the user is active or innactive
                if ($user->status_id == 1 || $user->status_id == 2) {
                    /**
                     * Check the server monitoring interval by previousUpdate() function in DnsStatus
                     * Check current status only if the time interval is over in addition with previous update
                     */
                    if ($dnsStatus->previousUpdate($dnsServer, $dnsMxRecord)) {
                        Logger::info('Checking status of Mx Record');
                        //Create an object for DnsMonitorLog
                        $log = new DnsMonitorLogs();
                        $url = $dnsServer->website_domain;
                        $log->uuid = Uuid::getUuid();
                        $log->rel_id = $dnsMxRecord->uuid;
                        $log->website_id = $dnsServer->uuid;
                        $log->type = 'MX';
                        $startTime = intval(microtime(true) * 1000);

                        /**
                         * The case where server is up
                         * Status check using checkMxRecord() function in DnsStatus service
                         */
                        if ($dnsStatus->checkMxRecord($url, $dnsMxRecord->priority, $dnsMxRecord->exchange)) {
                            $endTime = intval(microtime(true) * 1000);

                            Logger::info('Previous status');
                            Logger::info($dnsMxRecord->previous_status);
                            Logger::info('Status check done');

                            //Let's notify that the API is back online with E-mail
                            if ($dnsMxRecord->previous_status == 'down') {
                                // $autoIncident = Setting::where('name', 'auto_incident')
                                     //     ->value('value');
                                // if ($autoIncident) {
                                     //     Logger::info("Updating auto incident");
                                     //     $incidentService = new IncidentService();
                                     //     $incidentService->updateAutoIncident($dnsMxRecord->uuid);
                                // }
                                $log->down_time = $dnsServer->interval;
                                // Sent server up mail to user's Email
                                Logger::error('Sending server up mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorUp($dnsServer, 'Mx Record ', $dnsMxRecord->exchange);

                                // Sent server up mail to email subscription enabled statuspage users
                                // $emailSubscriptionService = new SubscriptionEmail();
                                // $emailSubscriptionService->serverMonitorUp($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsUp')) {
                                            $instance->onDnsServerIsUp($dnsServer);
                                        }
                                    }
                                }
                            }
                            // make the latest server status as up
                            $dnsMxRecord->previous_status = 'up';
                            $log->response_time = ($endTime - $startTime);
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            /**
                             * Server is up, just add the row in to a table
                             */
                            $log->is_up = 1;
                            $log->save();

                            $avgResponseTime = $DnsService->getAverageResponseTime($dnsMxRecord->uuid);
                            $dnsMxRecord->avg_response_time = $avgResponseTime;
                            $dnsMxRecord->save();
                        } else {
                            /*
                            Server is down, add a row to the table,
                            if should be notified send an email/sms.
                            */
                            $endTime = intval(microtime(true) * 1000);
                            $log->is_up = 0;
                            Logger::error($dnsServer->website_domain.' is down');

                            //Let's notify that the server is down with E-mail
                            if ($dnsMxRecord->previous_status == 'up') {
                                    //     $autoIncident = Setting::where('name', 'auto_incident')
                                    //         ->value('value');
                                    //     if ($autoIncident) {
                                    //         Logger::info("Adding auto incident");
                                    //         $incidentService = new IncidentService();
                                    //         $incidentService->addAutoIncident($server->uuid);
                                    //     }

                                // Sent server down mail to user's Email
                                Logger::error('Sending server down mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorDown($dnsServer, 'Mx Record ', $dnsMxRecord->exchange);

                                //     // Sent server down mail to email subscription enabled statuspage users
                                //     $emailSubscriptionService = new SubscriptionEmail();
                                //     $emailSubscriptionService->serverMonitorDown($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsDown')) {
                                            $instance->onDnsServerIsDown($dnsServer);
                                        }
                                    }
                                }
                            }

                            // make the latest server status as down
                            $dnsMxRecord->previous_status = 'down';
                            $log->response_time = 0;
                            $log->down_time = $dnsServer->interval;
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            $log->save();

                            //Calculating avg. response time with getAverageResponseTime() function in DnsMonitorService
                            $dnsMxRecord->avg_response_time = $DnsService->getAverageResponseTime($dnsMxRecord->uuid);
                            $dnsMxRecord->save();
                            $serverUp = false;
                        }
                    }
                }
            }
            //DnsNsRecord check
            foreach ($dnsNsRecords as $dnsNsRecord) {
                //Get the user
                $user = User::where('id', $dnsServer->user_id)->first();
                //Check whether the user is active or innactive
                if ($user->status_id == 1 || $user->status_id == 2) {
                    /**
                     * Check the server monitoring interval by previousUpdate() function in DnsStatus
                     * Check current status only if the time interval is over in addition with previous update
                     */
                    if ($dnsStatus->previousUpdate($dnsServer, $dnsNsRecord)) {
                        Logger::info('Checking status of Ns Record');
                        //Create an object for DnsMonitorLog
                        $log = new DnsMonitorLogs();
                        $url = $dnsServer->website_domain;
                        $log->uuid = Uuid::getUuid();
                        $log->rel_id = $dnsNsRecord->uuid;
                        $log->website_id = $dnsServer->uuid;
                        $log->type = 'NS';
                        $startTime = intval(microtime(true) * 1000);

                        /**
                         * The case where server is up
                         * Status check using checkNsRecord() function in DnsStatus service
                         */
                        if ($dnsStatus->checkNsRecord($url, $dnsNsRecord->value)) {
                            $endTime = intval(microtime(true) * 1000);

                            Logger::info('Previous status');
                            Logger::info($dnsNsRecord->previous_status);
                            Logger::info('Status check done');

                            //Let's notify that the API is back online with E-mail
                            if ($dnsNsRecord->previous_status == 'down') {
                                // $autoIncident = Setting::where('name', 'auto_incident')
                                     //     ->value('value');
                                // if ($autoIncident) {
                                     //     Logger::info("Updating auto incident");
                                     //     $incidentService = new IncidentService();
                                     //     $incidentService->updateAutoIncident($dnsNsRecord->uuid);
                                // }
                                $log->down_time = $dnsServer->interval;
                                // Sent server up mail to user's Email
                                Logger::error('Sending server up mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorUp($dnsServer, 'Ns Record ', $dnsNsRecord->value);

                                // Sent server up mail to email subscription enabled statuspage users
                                // $emailSubscriptionService = new SubscriptionEmail();
                                // $emailSubscriptionService->serverMonitorUp($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsUp')) {
                                            $instance->onDnsServerIsUp($dnsServer);
                                        }
                                    }
                                }
                            }

                            // make the latest server status as up
                            $dnsNsRecord->previous_status = 'up';
                            $log->response_time = ($endTime - $startTime);
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            /**
                             * Server is up, just add the row in to a table
                             */
                            $log->is_up = 1;
                            $log->save();

                            $avgResponseTime = $DnsService->getAverageResponseTime($dnsNsRecord->uuid);
                            $dnsNsRecord->avg_response_time = $avgResponseTime;
                            $dnsNsRecord->save();
                        } else {
                            /*
                            Server is down, add a row to the table,
                            if should be notified send an email/sms.
                            */
                            $endTime = intval(microtime(true) * 1000);
                            $log->is_up = 0;
                            Logger::error($dnsServer->website_domain.' is down');

                            //Let's notify that the server is down with E-mail
                            if ($dnsNsRecord->previous_status == 'up') {
                                    //     $autoIncident = Setting::where('name', 'auto_incident')
                                    //         ->value('value');
                                    //     if ($autoIncident) {
                                    //         Logger::info("Adding auto incident");
                                    //         $incidentService = new IncidentService();
                                    //         $incidentService->addAutoIncident($server->uuid);
                                    //     }

                                // Sent server down mail to user's Email
                                Logger::error('Sending server down mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorDown($dnsServer, 'Ns Record ', $dnsNsRecord->value);

                                //     // Sent server down mail to email subscription enabled statuspage users
                                //     $emailSubscriptionService = new SubscriptionEmail();
                                //     $emailSubscriptionService->serverMonitorDown($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsDown')) {
                                            $instance->onDnsServerIsDown($dnsServer);
                                        }
                                    }
                                }
                            }

                            // make the latest server status as down
                            $dnsNsRecord->previous_status = 'down';
                            $log->response_time = 0;
                            $log->down_time = $dnsServer->interval;
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            $log->save();

                            //Calculating avg. response time with getAverageResponseTime() function in DnsMonitorService
                            $dnsNsRecord->avg_response_time = $DnsService->getAverageResponseTime($dnsNsRecord->uuid);
                            $dnsNsRecord->save();
                            $serverUp = false;
                        }
                    }
                }
            }
            //DnsTxtRecord check
            foreach ($dnsTxtRecords as $dnsTxtRecord) {
                //Get the user
                $user = User::where('id', $dnsServer->user_id)->first();
                //Check whether the user is active or innactive
                if ($user->status_id == 1 || $user->status_id == 2) {
                    /**
                     * Check the server monitoring interval by previousUpdate() function in DnsStatus
                     * Check current status only if the time interval is over in addition with previous update
                     */
                    if ($dnsStatus->previousUpdate($dnsServer, $dnsTxtRecord)) {
                        Logger::info('Checking status of Txt Record');
                        //Create an object for DnsMonitorLog
                        $log = new DnsMonitorLogs();
                        $url = $dnsServer->website_domain;
                        $log->uuid = Uuid::getUuid();
                        $log->rel_id = $dnsTxtRecord->uuid;
                        $log->website_id = $dnsServer->uuid;
                        $log->type = 'A';
                        $startTime = intval(microtime(true) * 1000);

                        /**
                         * The case where server is up
                         * Status check using checkTxtRecord() function in DnsStatus service
                         */
                        if ($dnsStatus->checkTxtRecord($url, $dnsTxtRecord->value)) {
                            $endTime = intval(microtime(true) * 1000);
                            Logger::info('Previous status');
                            Logger::info($dnsTxtRecord->previous_status);
                            Logger::info('Status check done');

                            //Let's notify that the API is back online with E-mail
                            if ($dnsTxtRecord->previous_status == 'down') {
                                // $autoIncident = Setting::where('name', 'auto_incident')
                                     //     ->value('value');
                                // if ($autoIncident) {
                                     //     Logger::info("Updating auto incident");
                                     //     $incidentService = new IncidentService();
                                     //     $incidentService->updateAutoIncident($dnsARecord->uuid);
                                // }
                                $log->down_time = $dnsServer->interval;
                                // Sent server up mail to user's Email
                                Logger::error('Sending server up mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorUp($dnsServer, 'Txt Record ', $dnsTxtRecord->value);

                                // Sent server up mail to email subscription enabled statuspage users
                                // $emailSubscriptionService = new SubscriptionEmail();
                                // $emailSubscriptionService->serverMonitorUp($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsUp')) {
                                            $instance->onDnsServerIsUp($dnsServer);
                                        }
                                    }
                                }
                            }

                            // make the latest server status as up
                            $dnsTxtRecord->previous_status = 'up';
                            $log->response_time = ($endTime - $startTime);
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            /**
                             * Server is up, just add the row in to a table
                             */
                            $log->is_up = 1;
                            $log->save();

                            $avgResponseTime = $DnsService->getAverageResponseTime($dnsTxtRecord->uuid);
                            $dnsTxtRecord->avg_response_time = $avgResponseTime;
                            $dnsTxtRecord->save();
                        } else {
                            /*
                            Server is down, add a row to the table,
                            if should be notified send an email/sms.
                            */
                            $endTime = intval(microtime(true) * 1000);
                            $log->is_up = 0;
                            Logger::error($dnsServer->website_domain.' is down');

                            //Let's notify that the server is down with E-mail
                            if ($dnsTxtRecord->previous_status == 'up') {
                                    //     $autoIncident = Setting::where('name', 'auto_incident')
                                    //         ->value('value');
                                    //     if ($autoIncident) {
                                    //         Logger::info("Adding auto incident");
                                    //         $incidentService = new IncidentService();
                                    //         $incidentService->addAutoIncident($server->uuid);
                                    //     }

                                // Sent server down mail to user's Email
                                Logger::error('Sending server down mail');
                                $emailService = new Email();
                                $emailService->dnsMonitorDown($dnsServer, 'Txt Record ', $dnsTxtRecord->value);

                                //     // Sent server down mail to email subscription enabled statuspage users
                                //     $emailSubscriptionService = new SubscriptionEmail();
                                //     $emailSubscriptionService->serverMonitorDown($server);
                                $hooks = Hooks::where('status', 1)->get();
                                foreach ($hooks as $hook) {
                                    //Logger::info($hook);
                                    $classname = $hook->class;
                                    if (class_exists($classname)) {
                                        $instance = (new \ReflectionClass($classname))->newInstance();
                                        if (method_exists($instance, 'onDnsServerIsDown')) {
                                            $instance->onDnsServerIsDown($dnsServer);
                                        }
                                    }
                                }
                            }
                            // make the latest server status as down
                            $dnsTxtRecord->previous_status = 'down';
                            $log->response_time = 0;
                            $log->down_time = $dnsServer->interval;
                            $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                            $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                            $log->save();

                            //Calculating avg. response time with getAverageResponseTime() function in DnsMonitorService
                            $dnsTxtRecord->avg_response_time = $DnsService->getAverageResponseTime($dnsTxtRecord->uuid);
                            $dnsTxtRecord->save();
                            $serverUp = false;
                        }
                    }
                }
            }

            if ($serverUp) {
                $dnsServer->previous_status = 'up';
                $dnsServer->updated_at = Carbon::now()->format('Y-m-d H:i:s');
                $dnsServer->save();
            } else {
                $dnsServer->previous_status = 'down';
                $dnsServer->updated_at = Carbon::now()->format('Y-m-d H:i:s');
                $dnsServer->save();
            }
            if (count($dnsARecords) + count($dnsAaaaRecords) + count($dnsCnameRecords) + count($dnsMxRecords) + count($dnsNsRecords) + count($dnsTxtRecords) == 0) {
                $dnsServer->previous_status = 'NA';
                $dnsServer->updated_at = Carbon::now()->format('Y-m-d H:i:s');
                $dnsServer->save();
            }
        }
    }
}
