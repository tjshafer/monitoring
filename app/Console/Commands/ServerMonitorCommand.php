<?php

namespace App\Console\Commands;

use App\Helpers\Logger;
use App\Helpers\Uuid;
use App\Models\Hooks;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\Service;
use App\Models\Services\IncidentService;
use App\Models\Services\MonitorService;
use App\Models\Setting;
use App\Models\User;
use App\Services\Email;
use App\Services\MonitorStatus;
use App\Services\SubscriptionEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class ServerMonitorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server_monitor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        //Get all available servers
        $servers = Monitor::where('type', 'server')
            ->get();

        /**
         * Service class that associated with server monitoring status
         * Refer app/Service/MonitorStatus.php
         */
        $serverStatus = new MonitorStatus();
        /**
         * Service class that interact with Monitor model
         * Refer app/Service/Models/MonitorService.php
         */
        $monitorService = new MonitorService();
        foreach ($servers as $server) {
            //Get the user
            $user = User::where('id', $server->user_id)->first();
            //Check whether the user is active or innactive
            if ($user->status_id == 1 || $user->status_id == 2) {
                /**
                 * Check the server monitoring interval by previousUpdate() function in MonitorStatus
                 * Check current status only if the time interval is over in addition with previous update
                 */
                if ($serverStatus->previousUpdate($server)) {
                    Logger::info('Checking status of Server');
                    //Create an object for MonitorLog
                    $log = new MonitorLog();
                    $url = $server->url;
                    $port = $server->port;
                    $log->uuid = Uuid::getUuid();
                    $log->rel_id = $server->uuid;
                    $log->type = 'server';
                    $startTime = intval(microtime(true) * 1000);

                    /**
                     * The case where server is up
                     * Status check using isUpServer() function in MonitorStatus service
                     */
                    if ($serverStatus->isUpServer($url, $port)) {
                        $endTime = intval(microtime(true) * 1000);
                        Logger::info('Previous status');
                        Logger::info($server->previous_status);
                        Logger::info('Status check done');

                        //Let's notify that the API is back online with E-mail
                        if ($server->previous_status == 'down') {
                            $autoIncident = Setting::where('name', 'auto_incident')
                                ->value('value');
                            if ($autoIncident) {
                                Logger::info('Updating auto incident');
                                $incidentService = new IncidentService();
                                $incidentService->updateAutoIncident($server->uuid);
                            }
                            $log->down_time = $server->interval;
                            // Sent server up mail to user's Email
                            Logger::error('Sending server up mail');
                            $emailService = new Email();
                            $emailService->serverMonitorUp($server);

                            // Sent server up mail to email subscription enabled statuspage users
                            $emailSubscriptionService = new SubscriptionEmail();
                            $emailSubscriptionService->serverMonitorUp($server);
                            //sent twilio sms
                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onServerIsUp')) {
                                        $instance->onServerIsUp($server);
                                    }
                                }
                            }
                        }

                        // make the latest server status as up
                        $server->previous_status = 'up';
                        $log->response_time = ($endTime - $startTime);
                        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                        $log->previous_time = Carbon::now()->subMinute($server->interval)->format('Y-m-d H:i:s');
                        /**
                         * Server is up, just add the row in to a table
                         */
                        $log->is_up = 1;
                        $log->save();

                        $avgResponseTime = $monitorService->getAverageResponseTime($server->uuid);
                        $server->avg_response_time = $avgResponseTime;
                        $server->save();
                        Logger::info($server->port);
                    } else {
                        /*
                        Server is down, add a row to the table,
                        if should be notified send an email/sms.
                        */
                        $endTime = intval(microtime(true) * 1000);
                        $log->is_up = 0;
                        Logger::error($server->url.' is down');

                        //Let's notify that the server is down with E-mail
                        if ($server->previous_status == 'up') {
                            $autoIncident = Setting::where('name', 'auto_incident')
                                ->value('value');
                            if ($autoIncident) {
                                Logger::info('Adding auto incident');
                                $incidentService = new IncidentService();
                                $incidentService->addAutoIncident($server->uuid);
                            }

                            // Sent server down mail to user's Email
                            Logger::error('Sending server down mail');
                            $emailService = new Email();
                            $emailService->serverMonitorDown($server);

                            // Sent server down mail to email subscription enabled statuspage users
                            $emailSubscriptionService = new SubscriptionEmail();
                            $emailSubscriptionService->serverMonitorDown($server);

                            //sent sms
                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onServerIsDown')) {
                                        $instance->onServerIsDown($server);
                                    }
                                }
                            }
                        }
                        // make the latest server status as down
                        $server->previous_status = 'down';
                        $log->down_time = $server->interval;
                        $log->response_time = 0;
                        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                        $log->previous_time = Carbon::now()->subMinute($server->interval)->format('Y-m-d H:i:s');
                        $log->save();

                        //Calculating avg. response time with getAverageResponseTime() function in MonitorService
                        $server->avg_response_time = $monitorService->getAverageResponseTime($server->uuid);
                        $server->save();
                    }
                }
            }
        }
    }
}
