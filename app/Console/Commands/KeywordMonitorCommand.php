<?php

namespace App\Console\Commands;

use App\Helpers\Logger;
use App\Helpers\Uuid;
use App\Models\Hooks;
use App\Models\KeywordMonitoring;
use App\Models\KeywordMonitorLogs;
use App\Models\Service;
use App\Models\Services\IncidentService;
use App\Models\Services\KeywordMonitorService;
use App\Models\Setting;
use App\Models\User;
use App\Services\Email;
use App\Services\KeywordStatus;
use App\Services\SubscriptionEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class KeywordMonitorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'keyword_monitor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $keywords = KeywordMonitoring::with('user')->get();

        $keywordStatus = new KeywordStatus();

        $keywordService = new KeywordMonitorService();

        foreach ($keywords as $keyword) {
            //Get the user
            $user = User::where('id', $keyword->user_id)->first();
            //Check whether the user is active or innactive
            if ($user->status_id == 1 || $user->status_id == 2) {
                /**
                 * Check the keyword monitoring interval by previousUpdate() function in KeywordStatus
                 * Check current status only if the time interval is over in addition with previous update
                 */
                if ($keywordStatus->previousUpdate($keyword)) {
                    Logger::info('Checking status of keyword monitor');
                    //Create an object for KeywordMonitorLog
                    $log = new KeywordMonitorLogs();
                    $url = $keyword->url;
                    $log->uuid = Uuid::getUuid();
                    $log->rel_id = $keyword->uuid;
                    $startTime = intval(microtime(true) * 1000);

                    /**
                     * The case where monitor is up
                     * Status check using isKeywordUp() function in KeywordStatus service
                     */
                    if ($keywordStatus->isKeywordUp($url, $keyword->keyword, $keyword->is_case_sensitive, $keyword->alert_when)) {
                        $endTime = intval(microtime(true) * 1000);

                        Logger::info('Previous status');
                        Logger::info($keyword->previous_status);
                        Logger::info('Status check done');

                        //Let's notify that the API is back online with E-mail
                        if ($keyword->previous_status == 'down') {
                            // $autoIncident = Setting::where('name', 'auto_incident')
                                //     ->value('value');
                            // if ($autoIncident) {
                                //     Logger::info("Updating auto incident");
                                //     $incidentService = new IncidentService();
                                //     $incidentService->updateAutoIncident($dnsARecord->uuid);
                            // }
                            $log->down_time = $keyword->interval;
                            // Sent server up mail to user's Email
                            Logger::error('Sending server up mail');
                            $emailService = new Email();
                            $emailService->keywordMonitorUp($keyword);

                            // Sent server up mail to email subscription enabled statuspage users
                            // $emailSubscriptionService = new SubscriptionEmail();
                            // $emailSubscriptionService->serverMonitorUp($server);

                            //send twilio sms
                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onKeywordIsUp')) {
                                        $instance->onKeywordIsUp($keyword);
                                    }
                                }
                            }
                        }
                        // make the latest server status as up
                        $keyword->previous_status = 'up';

                        $log->response_time = ($endTime - $startTime);
                        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                        $log->previous_time = Carbon::now()->subMinute($keyword->interval)->format('Y-m-d H:i:s');
                        /**
                         * Server is up, just add the row in to a table
                         */
                        $log->is_up = 1;
                        $log->save();

                        $avgResponseTime = $keywordService->getAverageResponseTime($keyword->uuid);
                        $keyword->avg_response_time = $avgResponseTime;
                        $keyword->save();
                    } else {
                        /*
                        keyword is down, add a row to the table,
                        if should be notified send an email/sms.
                        */
                        $endTime = intval(microtime(true) * 1000);

                        $log->is_up = 0;
                        Logger::error($keyword->keyword.' is down');

                        //Let's notify that the server is down with E-mail
                        if ($keyword->previous_status == 'up') {
                            //     $autoIncident = Setting::where('name', 'auto_incident')
                            //         ->value('value');
                            //     if ($autoIncident) {
                            //         Logger::info("Adding auto incident");
                            //         $incidentService = new IncidentService();
                            //         $incidentService->addAutoIncident($server->uuid);
                            //     }

                            // Sent server down mail to user's Email
                            Logger::error('Sending server down mail');
                            $emailService = new Email();
                            $emailService->keywordMonitorDown($keyword);

                            //     // Sent server down mail to email subscription enabled statuspage users
                            //     $emailSubscriptionService = new SubscriptionEmail();
                            //     $emailSubscriptionService->serverMonitorDown($server);

                            $hooks = Hooks::where('status', 1)->get();
                            foreach ($hooks as $hook) {
                                //Logger::info($hook);
                                $classname = $hook->class;
                                if (class_exists($classname)) {
                                    $instance = (new \ReflectionClass($classname))->newInstance();
                                    if (method_exists($instance, 'onKeywordIsDown')) {
                                        $instance->onKeywordIsDown($keyword);
                                    }
                                }
                            }
                        }
                        // make the latest server status as down
                        $keyword->previous_status = 'down';
                        $log->response_time = 0;
                        $log->down_time = $keyword->interval;
                        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                        $log->previous_time = Carbon::now()->subMinute($keyword->interval)->format('Y-m-d H:i:s');
                        $log->save();

                        //Calculating avg. response time with getAverageResponseTime() function in DnsMonitorService
                        $keyword->avg_response_time = $keywordService->getAverageResponseTime($keyword->uuid);
                        $keyword->save();
                    }
                }
            }
        }
    }
}
