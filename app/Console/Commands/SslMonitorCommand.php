<?php

namespace App\Console\Commands;

use App\Helpers\Logger;
use App\Models\Hooks;
use App\Models\Service;
use App\Models\Services\SslMonitorService;
use App\Models\SslMonitor;
use App\Models\User;
use App\Services\Email;
use App\Services\WebsiteStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class SslMonitorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ssl_monitor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $monitors = SslMonitor::all();

        $sslStatus = new WebsiteStatus();

        $sslService = new SslMonitorService();

        foreach ($monitors as $ssl) {
            //Get the user
            $user = User::where('id', $ssl->user_id)->first();
            //Check whether the user is active or innactive
            if ($user->status_id == 1 || $user->status_id == 2) {
                $service = Service::where('user_id', $user->id)->latest()->first();
                if ($service != null && $service->status_id == 1) {
                    // Logger::info("checking domain expiry for ".$ssl->url);
                    // $domain_info = $sslStatus->domainExpiryStatus($ssl->url);
                    // if ($domain_info) {
                    Logger::info('checking ssl expiry for '.$ssl->url);
                    $ssl_expiry = $sslStatus->sslExpiryStatus($ssl->url, $ssl->ssl_port);

                    $ssl->ssl_expiry = $ssl_expiry;
                    $ssl->save();

                    Logger::info('creating log for '.$ssl->url);
                    $sslService->monitorLog($ssl);

                    // Send validity reminder mail

                    $currentDate = Carbon::now()->format('Y-m-d');
                    $emailService = new Email();

                    /**
                     * Sending SSL validity reminder mail and twilio sms.
                     */
                    if ($ssl_expiry) {
                        $sslFirstInvoiceDate = Carbon::parse($ssl_expiry)->subDays(7)->format('Y-m-d');
                        $sslSecondInvoiceDate = Carbon::parse($ssl_expiry)->subDays(2)->format('Y-m-d');

                        if ($sslFirstInvoiceDate == $currentDate) {
                            Logger::info('SSL validity will expire in 7 days');
                            $expireIn = 7;
                            $emailService->sslValidityReminder($ssl, $expireIn);
                        } elseif ($sslSecondInvoiceDate == $currentDate) {
                            Logger::info('SSL validity will expire in 2 days');
                            $expireIn = 2;
                            $emailService->sslValidityReminder($ssl, $expireIn);
                        }

                        //sms part
                        $hooks = Hooks::where('status', 1)->get();
                        foreach ($hooks as $hook) {
                            //Logger::info($hook);
                            $classname = $hook->class;
                            if (class_exists($classname)) {
                                $instance = (new \ReflectionClass($classname))->newInstance();
                                if (method_exists($instance, 'onSslExpiry')) {
                                    $instance->onSslExpiry($ssl, $expireIn);
                                }
                            }
                        }
                    }
                // } else {
                    //     Logger::info("Domain ifo not available for ".$ssl->url);
                // }
                } else {
                    Logger::info('No service active for '.$user->first_name);
                }
            }
        }
    }
}
