<?php

namespace App\Console\Commands;

use App\Models\Monitor;
use App\Services\Demo;
use Illuminate\Console\Command;

class DemoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * Will run only in Demo version
     */
    public function handle(): void
    {
        /**
         * Service associated with demo version
         * Refer app/Services/Demo.php
         */
        $demo = new Demo();

        /**
         * Deletes 5 days old data
         */
        $monitors = Monitor::all();
        foreach ($monitors as $item) {
            $demo->deleteData($item);
        }
    }
}
