<?php

namespace App\Models\Services;

use App\Models\Plan;

class PlanService
{
    /**
     * Get dashboard contents
     */
    public function addPlan($request): void
    {
        $plan = new Plan();
        $plan->name = $request->name;
        $plan->description = $request->description;
        if ($request->ssl) {
            $plan->ssl_count = $request->ssl_count;
        }
        if ($request->domains) {
            $plan->domain_count = $request->domain_count;
        }
        if ($request->servers) {
            $plan->server_count = $request->server_count;
        }
        if ($request->webpages) {
            $plan->webpage_count = $request->webpage_count;
        }
        if ($request->apis) {
            $plan->api_count = $request->api_count;
        }
        if ($request->subdomains) {
            $plan->subdomain_count = $request->subdomain_count;
        }
        if ($request->dns) {
            $plan->dns_count = $request->dns_count;
        }
        if ($request->keyword) {
            $plan->keyword_count = $request->keyword_count;
        }
        $plan->display_order = $request->display_order;
        $plan->status = $request->status;
        $plan->save();
    }

    public function updatePlan($request, $plan): void
    {
        $updateArray = [];
        $updateArray['name'] = $request->name;
        $updateArray['description'] = $request->description;
        if ($request->servers) {
            $updateArray['server_count'] = $request->server_count;
        } else {
            $updateArray['server_count'] = null;
        }
        if ($request->webpages) {
            $updateArray['webpage_count'] = $request->webpage_count;
        } else {
            $updateArray['webpage_count'] = null;
        }
        if ($request->apis) {
            $updateArray['api_count'] = $request->api_count;
        } else {
            $updateArray['api_count'] = null;
        }
        if ($request->ssl) {
            $updateArray['ssl_count'] = $request->ssl_count;
        } else {
            $updateArray['ssl_count'] = null;
        }
        if ($request->domains) {
            $updateArray['domain_count'] = $request->domain_count;
        } else {
            $updateArray['domain_count'] = null;
        }
        if ($request->subdomains) {
            $updateArray['subdomain_count'] = $request->subdomain_count;
        } else {
            $updateArray['subdomain_count'] = null;
        }
        if ($request->dns) {
            $updateArray['dns_count'] = $request->dns_count;
        } else {
            $updateArray['dns_count'] = null;
        }
        if ($request->keyword) {
            $updateArray['keyword_count'] = $request->keyword_count;
        } else {
            $updateArray['keyword_count'] = null;
        }
        $updateArray['status'] = $request->status;
        $updateArray['display_order'] = $request->display_order;
        $plan->update($updateArray);
    }

    public function validity($pricing)
    {
        if ($pricing->period == 'Day(s)') {
            $validity = $pricing->term;
        } elseif ($pricing->period == 'Week(s)') {
            $validity = $pricing->term * 7;
        } elseif ($pricing->period == 'Month(s)') {
            $validity = $pricing->term * 30;
        } elseif ($pricing->period == 'Year(s)') {
            $validity = $pricing->term * 365;
        }

        return $validity;
    }

    public function displayValidity($pricing): string
    {
        // if ($pricing->period == "Daily") {
        // $validity = "$pricing->term Day(s)";
        // } elseif ($pricing->period == "Weekly"){
        //     $validity = "$pricing->term Week(s)";
        // } elseif ($pricing->period == "Monthly"){
        //     $validity = "$pricing->term Month(s)";
        // } elseif ($pricing->period == "Yearly"){
        //     $validity = "$pricing->term Years";
        // }
        $validity = $pricing->term.' '.$pricing->period;

        return $validity;
    }
}
