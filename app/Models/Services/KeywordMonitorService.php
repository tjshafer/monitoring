<?php

namespace App\Models\Services;

use App\Helpers\Uuid;
use App\Models\KeywordMonitoring;
use App\Models\KeywordMonitorLogs;
use App\Services\KeywordStatus;
use Illuminate\Support\Carbon;

class KeywordMonitorService
{
    /*
     * Getting filtered monitors
     */
    public function getFilteredElements($user, $request): \Illuminate\Database\Eloquent\Builder
    {
        $query = KeywordMonitoring::query()->where('user_id', $user->id);
        //Filter monitors by by search keywords
        if ($request->search) {
            $query = $query->where('name', 'like', '%'.$request->search.'%')
                           ->orWhere('url', 'like', '%'.$request->search.'%');
        }

        $monitors = $query;

        return $monitors;
    }

    public function addKeyword($request, $user): bool
    {
        $monitor = new KeywordMonitoring();

        //create object for keyword monitor log.
        $log = new KeywordMonitorLogs();

        //service class that interact with keyword monitor.
        $keyService = new KeywordStatus();
        $monitor->uuid = Uuid::getUuid();
        $monitor->user_id = $user->id;
        $monitor->url = $request->url;
        $monitor->name = $request->name;
        $monitor->keyword = $request->keyword;
        $monitor->is_case_sensitive = $request->case;
        $monitor->alert_when = $request->alert;
        $monitor->interval = $request->interval;
        $monitor->email = $user->email;

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $startTime = intval(microtime(true) * 1000);

        if ($keyService->isKeywordUp($request->url, $request->keyword, $request->case, $request->alert)) {
            $monitor->previous_status = 'up';
            $log->is_up = 1;
        } else {
            $monitor->previous_status = 'down';
            $log->is_up = 0;
        }
        $endTime = intval(microtime(true) * 1000);
        $monitor->avg_response_time = ($endTime - $startTime);
        $log->response_time = $monitor->avg_response_time;

        $monitor->save();
        $log->save();

        return true;
    }

    public function updateKeyword($request, $monitor): bool
    {
        //create object for keyword monitor log.
        $log = new KeywordMonitorLogs();
        //service class that interact with keyword monitor.
        $keyService = new KeywordStatus();

        $updateArray = [];
        $updateArray['url'] = $request->url;
        $updateArray['name'] = $request->name;
        $updateArray['keyword'] = $request->keyword;
        $updateArray['is_case_sensitive'] = intval($request->case);
        $updateArray['alert_when'] = $request->alert;
        $updateArray['interval'] = $request->interval;

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $startTime = intval(microtime(true) * 1000);

        if ($keyService->isKeywordUp($request->url, $request->keyword, $request->case, $request->alert)) {
            $log->is_up = 1;
            $updateArray['previous_status'] = 'up';
        } else {
            $log->is_up = 0;
            $updateArray['previous_status'] = 'down';
        }

        $endTime = intval(microtime(true) * 1000);
        $updateArray['avg_response_time'] = ($endTime - $startTime);
        $log->response_time = ($endTime - $startTime);
        $monitor->update($updateArray);
        $log->save();

        return true;
    }

    /*
     * Calculation of average response time of a keyword
     */
    public function getAverageResponseTime($relId)
    {
        //Get logs of corresponding monitor.
        $value = KeywordMonitorLogs::where('rel_id', $relId)
            ->where('is_up', 1)
            ->avg('response_time');

        if (empty($value)) {
            return 0;
        }

        return $value;
    }

    public function getCount($user, $request, $alert): int
    {
        $keyword = KeywordMonitoring::query()->where('user_id', $user->id)
                    ->where('alert_when', $alert)->count();

        return $keyword;
    }
}
