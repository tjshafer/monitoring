<?php

namespace App\Models\Services;

use App\Helpers\Logger;
use App\Helpers\Uuid;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\PublicPage;
use App\Models\Setting;
use App\Models\User;
use App\Services\MonitorStatus;
use Illuminate\Support\Carbon;

class MonitorService
{
    /*
     * Getting filtered monitors
     */
    public function getFilteredElements($user, $request, $type): \Illuminate\Database\Eloquent\Builder
    {
        $query = Monitor::query()->where('user_id', $user->id)
            ->where('type', $type);
        $query->latest();
        //Filter monitors by by search keywords
        if ($request->search) {
            $query = $query->where('name', 'like', '%'.$request->search.'%')
                ->orWhere('url', 'like', '%'.$request->search.'%');
        }

        $monitors = $query;

        return $monitors;
    }

    /*
     * Adding new monitor
     */
    public function addMonitor($request, $user, $type): bool
    {
        //create an object for MonitorLog
        $log = new MonitorLog();
        //create an object for Monitor.
        $monitor = new Monitor();
        $monitor->uuid = Uuid::getUuid();
        $monitor->user_id = $user->id;
        $monitor->name = $request->name;
        $monitor->type = $type;
        $monitor->url = $request->url;
        if ($request->port) {
            $monitor->port = $request->port;
        }
        if ($request->methode) {
            $monitor->methode = $request->methode;
        }
        if ($request->json) {
            $monitor->json = $request->json;
        }
        if ($request->expected_response_code) {
            $monitor->expected_response_code = $request->expected_response_code;
        }

        $monitor->interval = $request->interval;
        $monitor->email = $request->email;
        //case were the monitor is down
        if ($request->skip_test_connection) {
            $monitor->test_result = 0;
            $monitor->previous_status = 'down';
            $monitor->avg_response_time = 0;
            $log->is_up = 0;
        } else {
            //case were the monitor is up
            $monitor->test_result = 1;
            $monitor->previous_status = 'up';
            $log->is_up = 1;
            $startTime = intval(microtime(true) * 1000);
            /*
            * service class that interact with the Monitor.
            * refer app/Services/MonitorStatus.php
            */
            $monitorStatus = new MonitorStatus();
            if ($type == 'server') {
                //Checking server status using the isUpServer() function in MonitorStatus

                if (! $monitorStatus->isUpServer($request->url, $request->port)) {
                    return false;
                }
            } elseif ($type == 'webpage') {
                //Checking webpage status using the isUpWebpage() function in MonitorStatus
                if (! $monitorStatus->isUpWebpage($request->url, $request->expected_response_code)) {
                    return false;
                }
            } elseif ($type == 'api') {
                //Checking Api status using the isUpApi() function in MonitorStatus
                if (! $monitorStatus->isUpApi($request->url, $request->expected_response_code, $request->json, $request->methode)) {
                    return false;
                }
            }

            $endTime = intval(microtime(true) * 1000);
            $monitor->avg_response_time = ($endTime - $startTime);
        }
        $monitor->save();
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->type = $type;
        $log->response_time = $monitor->avg_response_time;
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->save();

        if ($request->skip_test_connection) {
            $autoIncident = Setting::where('name', 'auto_incident')
                ->value('value');
            if ($autoIncident) {
                Logger::info('Adding auto incident');
                $incidentService = new IncidentService();
                $incidentService->addAutoIncident($monitor->uuid);
            }
        }

        // create object for PublicPage
        $publicPage = new PublicPage();

        /*
        * service class that interact with the public pages.
        * refer app/Models/Services/PublicPageService.php
        */
        $publicPageService = new PublicPageService();
        //Add public page using addPage() function in Public
        $publicPageService->addPage($monitor, $publicPage);

        return true;
    }

    /*
     * Updating a server
     */
    public function updateMonitor($request, $monitor, $type): bool
    {
        //create an object for MonitorLog
        $log = new MonitorLog();
        $updateArray = [];
        $updateArray['url'] = $request->url;
        if ($request->port) {
            $updateArray['port'] = $request->port;
        }
        if ($request->methode) {
            $updateArray['methode'] = $request->methode;
        }
        if ($request->json) {
            $updateArray['json'] = $request->json;
        }
        if ($request->expected_response_code) {
            $updateArray['expected_response_code'] = $request->expected_response_code;
        }

        $updateArray['name'] = $request->name;
        $updateArray['interval'] = $request->interval;
        $updateArray['email'] = $request->email;
        //case were the monitor is down
        if ($request->skip_test_connection) {
            $updateArray['test_result'] = 0;
            $updateArray['previous_status'] = 'down';
            $updateArray['avg_response_time'] = 0;
            $log->is_up = 0;

            if ($monitor->previous_status == 'up') {
                $autoIncident = Setting::where('name', 'auto_incident')
                    ->value('value');
                if ($autoIncident) {
                    Logger::info('Updating auto incident');
                    $incidentService = new IncidentService();
                    $incidentService->addAutoIncident($monitor->uuid);
                }
            }
        } else {
            //case were the monitor is up
            $updateArray['test_result'] = 1;
            $updateArray['previous_status'] = 'up';
            $log->is_up = 1;
            $startTime = intval(microtime(true) * 1000);
            /*
            * service class that interact with the Monitor.
            * refer app/Services/MonitorStatus.php
            */
            $monitorStatus = new MonitorStatus();
            if ($type == 'server') {
                //Checking server status using the isUpServer() function in MonitorStatus

                if (! $monitorStatus->isUpServer($request->url, $request->port)) {
                    return false;
                }
            } elseif ($type == 'webpage') {
                //Checking webpage status using the isUpWebpage() function in MonitorStatus
                if (! $monitorStatus->isUpWebpage($request->url, $request->expected_response_code)) {
                    return false;
                }
            } elseif ($type == 'api') {
                //Checking Api status using the isUpApi() function in MonitorStatus
                if (! $monitorStatus->isUpApi($request->url, $request->expected_response_code, $request->json, $request->methode)) {
                    return false;
                }
            }
            $endTime = intval(microtime(true) * 1000);
            $updateArray['avg_response_time'] = ($endTime - $startTime);

            if ($monitor->previous_status == 'down') {
                $autoIncident = Setting::where('name', 'auto_incident')
                    ->value('value');
                if ($autoIncident) {
                    Logger::info('Updating auto incident');
                    $incidentService = new IncidentService();
                    $incidentService->updateAutoIncident($monitor->uuid);
                }
            }
        }

        $monitor->update($updateArray);
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->type = $type;
        $log->response_time = $monitor->avg_response_time;
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->save();

        return true;
    }

    public function addBulkMonitor($monitors, $type): bool
    {
        foreach ($monitors as $data) {
            // echo "<pre>";
            // print_r($monitor[0]);exit;
            $user = auth()->user();
            $monitorNames = Monitor::where('name', $data[0])
                ->where('type', $type)->pluck('name');
            if (count($monitorNames)) {
                continue;
            }

            $monitorsCount = Monitor::where('user_id', $user->id)
                ->where('type', $type)
                ->whereNull('deleted_at')
                ->count();
            //Check whether the user's adding limit exceeded or not
            if (! $user->can('createServer', [Monitor::class, $monitorsCount])) {
                return false;
            }

            $monitor = new Monitor();
            $monitor->uuid = Uuid::getUuid();
            $monitor->user_id = $user->id;
            $monitor->name = $data[0];
            $monitor->type = $type;
            $monitor->url = $data[0];
            $monitor->previous_status = 'NA';

            if ($type == 'server') {
                $monitor->port = $data[1];
                $monitor->interval = $data[2];
            } elseif ($type == 'webpage') {
                $monitor->expected_response_code = $data[1];
                $monitor->interval = $data[2];
            } else {
                $monitor->methode = $data[1];
                $monitor->expected_response_code = $data[2];
                $monitor->interval = $data[3];
            }
            $monitor->save();

            // create object for PublicPage
            $publicPage = new PublicPage();

            /*
            * service class that interact with the public pages.
            * refer app/Models/Services/PublicPageService.php
            */
            $publicPageService = new PublicPageService();
            //Add public page using addPage() function in Public
            $publicPageService->addPage($monitor, $publicPage);
        }

        return true;
    }

    /*
     * Calculation of average response time of a Server
     */
    public function getAverageResponseTime($relId)
    {
        //Get logs of corresponding monitoe.
        $value = MonitorLog::where('rel_id', $relId)
            ->where('is_up', 1)
            ->avg('response_time');

        if (empty($value)) {
            return 0;
        }

        return $value;
    }

    public function csvConvert($request): array
    {
        $file = $request->file('csv_file');

        $monitors = [];

        if (($open = fopen($file, 'r')) !== false) {
            while (($data = fgetcsv($open, 1000, ',')) !== false) {
                $monitors[] = $data;
            }

            fclose($open);
        }
        array_shift($monitors);

        return $monitors;
    }
}
