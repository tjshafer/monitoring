<?php

namespace App\Models\Services;

use App\Models\User;
use Illuminate\Support\Carbon;

class PublicPageService
{
    /*
     * Adding public page
     */
    public function addPage($monitor, $publicPage): void
    {
        $publicPage->rel_id = $monitor->uuid;
        $publicPage->name = $monitor->name;
        $publicPage->status = true;
        $publicPage->save();
    }

    /*
     * Getting details to be shown in public pages
     */
    public function showPage($logs): array
    {
        //Get the user
        $user = User::find(auth()->id());

        //Get count of logs
        $totalLogs = $logs->count();
        //Get count of up logs
        $upCount = $logs->where('is_up', true)
            ->count();
        //Get count of down logs
        $downCount = $logs->where('is_up', false)
            ->count();
        //Get avg response time
        $avgResponse = $logs->where('is_up', 1)
            ->avg('response_time');
        $avgResponseTime = round($avgResponse, 2);

        //Calculate upTime and downTime percentage
        if ($totalLogs !== 0) {
            $upTime = round((100 * $upCount / $totalLogs), 2);
            $downTime = round((100 * $downCount / $totalLogs), 2);
        } else {
            $upTime = 0;
            $downTime = 0;
        }

        /*
         * Getting Last week detals
         */

        //Get date of 7 days ego from now
        $date = Carbon::now()->subDays(7)->startOfDay()->format('Y-m-d H:i:s');
        //Get last 7 days logs
        $weekLogs = $logs->where('created_at', '>=', $date)->count();
        //Get last 7 days up logs
        $weekUpLogs = $logs->where('created_at', '>=', $date)
            ->where('is_up', true)
            ->count();
        //Calculate last 7 day up percentage
        if ($weekLogs !== 0) {
            $weekStatus = (100 * $weekUpLogs) / $weekLogs;
        } else {
            $weekStatus = 0;
        }
        $weekStatus = round($weekStatus, 2);
        //Get each day status of last 7 days
        $reports = [];
        for ($i = 0; $i < 7; $i++) {
            //Get the date in the format of dd-Month (eg: 02-Jun)
            $dateList = Carbon::now()->subDays($i)->startOfDay()->format('Y-m-d');
            //Get the start of the day
            $startDate = Carbon::now()->subDays($i)->startOfDay()->format('Y-m-d H:i:s');
            //Get the end of the day
            $endDate = Carbon::now()->subDays($i)->endOfDay()->format('Y-m-d H:i:s');
            //Get that day logs
            $weekDayLogs = $logs->whereBetween('created_at', [$startDate, $endDate])->count();
            //get that day up logs
            $weekDayUpCount = $logs->whereBetween('created_at', [$startDate, $endDate])
                ->where('is_up', true)
                ->count();

            //Get that day status
            if ($weekDayLogs !== 0) {
                $dayStatus = (100 * $weekDayUpCount) / $weekDayLogs;
            } else {
                $dayStatus = 0;
            }
            $dayStatus = round($dayStatus, 2);
            $reports[$i]['date'] = $dateList;
            $reports[$i]['status'] = $dayStatus;
        }
        $dd = Carbon::now()->subDays(7)->startOfDay()->format('d-M');
        // dd($dd);exit;
        $response = [];
        $response['weekStatus'] = $weekStatus;
        $response['reports'] = $reports;
        $response['avgResponseTime'] = $avgResponseTime;
        $response['upTime'] = $upTime;
        $response['downTime'] = $downTime;

        return $response;
    }

    public function viewStatusPage($monitors, $monitor_ids): array
    {
        /*$totalLogs = MonitorLog::whereIn('rel_id', $monitor_ids)
            ->get();
        $todayLogs = $totalLogs->where('created_at', '>=', Carbon::today())->count();
        $lastWeekLogs = $totalLogs->where('created_at', '>=', Carbon::now()->subDays(7)->startOfDay()->format('Y-m-d H:i:s'))->count();
        $lastMonthLogs = $totalLogs->where('created_at', '>=', Carbon::now()->subDays(30)->startOfDay()->format('Y-m-d H:i:s'))->count();
        $todayUpLogs = $totalLogs->where('created_at', '>=',Carbon::today())
            ->where('is_up', true)
            ->count();
        $lastWeekUpLogs = $totalLogs->where('created_at', '>=', Carbon::now()->subDays(7)->startOfDay()->format('Y-m-d H:i:s'))
            ->where('is_up', true)
            ->count();
        $lastMonthUpLogs = $totalLogs->where('created_at', '>=', Carbon::now()->subDays(30)->startOfDay()->format('Y-m-d H:i:s'))
            ->where('is_up', true)
            ->count();
        //Get that day status
        if ($todayLogs !== 0){
            $todayStatus = (100*$todayUpLogs)/$todayLogs;
        }else{
            $todayStatus = 0;
        }
        //Get last 7 day status
        if ($lastWeekLogs !== 0){
            $lastWeekStatus = (100*$lastWeekUpLogs)/$lastWeekLogs;
        }else{
            $lastWeekStatus = 0;
        }
        //Get last 30 day status
        if ($lastMonthLogs !== 0){
            $lastMonthStatus = (100*$lastMonthUpLogs)/$lastMonthLogs;
        }else{
            $lastMonthStatus = 0;
    }*/
        $todayUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
                   ->where('created_at', '>=', Carbon::today())
                   ->whereIn('rel_id', $monitor_ids)
                   ->groupBy(\DB::raw('rel_id '))
               ->oldest()
           ->first();
        $oneWeekUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
                    ->where('created_at', '>=', Carbon::now()->subDays(7)->startOfDay()->format('Y-m-d H:i:s'))
                    ->whereIn('rel_id', $monitor_ids)
                    ->groupBy(\DB::raw('rel_id '))
                ->oldest()
            ->first();
        $oneMonthUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
                    ->where('created_at', '>=', Carbon::now()->subDays(30)->startOfDay()->format('Y-m-d H:i:s'))
                    ->whereIn('rel_id', $monitor_ids)
                    ->groupBy(\DB::raw('rel_id '))
                ->oldest()
                ->first();
        $todayStatus = $todayUp->perc;
        $lastWeekStatus = $oneWeekUp->perc;
        $lastMonthStatus = $oneMonthUp->perc;
        $startDate = Carbon::now()->subDays(89)->startOfDay()->format('Y-m-d');
        $startDateString = "'".$startDate."'";
        $endDate = Carbon::now()->format('Y-m-d H:i:s');
        $startDateTwoMonth = Carbon::now()->subDays(59)->startOfDay()->format('Y-m-d');
        $startDateOneMonth = Carbon::now()->subDays(29)->startOfDay()->format('Y-m-d');
        $threeMonthUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->get();
        $twoMonthUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', $startDateTwoMonth)
            ->where('created_at', '<=', $endDate)
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->get();
        $oneMonthUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', $startDateOneMonth)
            ->where('created_at', '<=', $endDate)
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->get();
        $threeMonthStatus = [];
        $twoMonthStatus = [];
        $oneMonthStatus = [];
        foreach ($threeMonthUp as $row90) {
            $threeMonthStatus[$row90->rel_id] = $row90->perc;
        }
        foreach ($twoMonthUp as $row60) {
            $twoMonthStatus[$row60->rel_id] = $row60->perc;
        }
        foreach ($oneMonthUp as $row30) {
            $oneMonthStatus[$row30->rel_id] = $row30->perc;
        }
        // echo "<pre>"; print_r($oneMonthUp);exit;
        $rows = MonitorLog::select(\DB::raw("count(*) as count,
        rel_id,  sum(is_up) as up_count,
            DATE_FORMAT(created_at, '%Y-%m-%d') as
            log_date,
            DATEDIFF(DATE_FORMAT(created_at, '%Y-%m-%d'), $startDateString) as i,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc"))
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw("rel_id,
            DATEDIFF(DATE_FORMAT(created_at, '%Y-%m-%d'), $startDateString),
            DATE_FORMAT(created_at, '%Y-%m-%d') "))
            ->oldest()
            ->get();
        $reports = [];
        foreach ($rows as $row) {
            $reports[$row->i][$row->rel_id]['date'] = $row->log_date;
            $reports[$row->i][$row->rel_id]['perc'] = $row->perc;
        }
        // echo "<pre>"; print_r($reports);exit;
        $response = [];
        $response['reports'] = $reports;
        $response['todayStatus'] = round($todayStatus, 2);
        $response['lastWeekStatus'] = round($lastWeekStatus, 2);
        $response['lastMonthStatus'] = round($lastMonthStatus, 2);
        $response['threeMonthStatus'] = $threeMonthStatus;
        $response['twoMonthStatus'] = $twoMonthStatus;
        $response['oneMonthStatus'] = $oneMonthStatus;

        return $response;
    }
}
