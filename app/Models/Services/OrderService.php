<?php

namespace App\Models\Services;

use App\Models\Invoice;
use App\Models\Order;
use App\Models\Plan;
use App\Models\Pricing;
use App\Models\Service;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Carbon;

class OrderService
{
    /*
     * Calculation of price to be paid by a user for a plan
     */
    public function price($user, $pricing): float
    {
        $planService = new PlanService();
        //Get the existing service or subscription of the current user
        $existService = Service::where('user_id', $user->id)->first();
        /*
         * The case were there is an active service exist for the current user
         */
        if ($existService && $existService->pricing_id !== $pricing->id) {
            //Get the existing plan pricing
            $existPricing = Pricing::find($existService->pricing_id);
            //Get existing plan validity
            $validity = $planService->validity($existPricing);
            //Get existing plan expiry date
            $expiryDate = Carbon::parse($existService->expiry_date);
            $now = Carbon::now();

            /*
             * case were plan not expired
             */
            if ($expiryDate > $now && $existService->status_id == 1) {
                //Calculation of remaining validity
                $validityLeft = $expiryDate->diffInDays($now) + 1;
                //calculation of remaining balance
                $balance = (($existService->pricing->price) / ($validity)) * ($validityLeft);
                //Calculation of payable amount
                $actualPrice = ($pricing->price) - $balance;
            } else {
                $actualPrice = $pricing->price;
            }
        } else {
            /*
             * The case were there is no active service exist for the current user
             */
            $actualPrice = $pricing->price;
        }
        //Rounding the price to two decimal parts
        $price = round($actualPrice, 2);

        return $price;
    }

    /*
     * Order confirmation
     */
    public function confirmOrder($user, $plan, $pricing, $price, $txId, $paymentStatus, $gateway): void
    {
        /*
         * service class that interact with the AddPlan model.
         * refer app/Services/Models/AddPlanService.php
         */
        $addPlanService = new AddPlanService();
        //Get the previous plan
        $existService = Service::where('user_id', $user->id)->latest()->first();
        //Get the latest orderof the user
        $order = Order::where('user_id', $user->id)->latest()->first();

        $orderAcceptSettings = Setting::where('name', 'order_accept')->first();
        $order->status = 'pending';
        $order->save();
        /*
         * The case were there is no service exist for the user
         */
        if (! $existService) {
            $isRenew = false;
            $description = "Plan name: $plan->name, Plan price: $plan->price";

            //Adding Service details using addService() function in AddPlanService
            $service = $addPlanService->addService($plan, $pricing, $order, $user);

            //Adding invoice using addInvoice() function in AddPlanService
            $invoice = $addPlanService->addInvoice($user, $pricing, $price, $order, $paymentStatus, $txId, $gateway, $isRenew);
        } else {
            /*
             * The case were there is service exist for the user
             */
            $isRenew = true;
            $service = $existService;

            $description = "Plan name: $plan->name, Plan price: $pricing->price";
            //Adding invoice using addInvoice() function in AddPlanService
            $invoice = $addPlanService->addInvoice($user, $pricing, $price, $order, $paymentStatus, $txId, $gateway, $isRenew);
            /*
             * The case were existing plan is same to new plan (Update case)
             */
            if ($existService->pricing_id == $pricing->id) {
                //Updating Service details using updateService() function in AddPlanService
                if ($orderAcceptSettings->value == false) {
                    $order->status = 'active';
                    $order->save();
                    $addPlanService->updateService($service, $plan, $pricing, $user, $order);
                }
            } else {
                /*
                 * The case were existing plan is not same to new plan (Upgrade case)
                 */
                $amount = ($price) - ($pricing->price);
                $description1 = "Plan name: $plan->name, plan upgraded from: $existService->plan_id";

                //Adding upgrade service details using upgradeService() function in AddPlanService
                if ($orderAcceptSettings->value == false) {
                    $order->status = 'active';
                    $order->save();
                    $addPlanService->upgradeService($service, $plan, $pricing, $user, $order, $price, $paymentStatus);
                }

                //Adding invoice details using addInvoiceDetails() function in AddPlanService (There will be two invoices in theis case)
                $invoiceDetails = $addPlanService->addInvoiceDetails($invoice, $service, $description1, $amount, $pricing);
            }
        }

        //Adding invoice details using addInvoiceDetails() function in AddPlanService
        $invoiceDetails = $addPlanService->addInvoiceDetails($invoice, $service, $description, $pricing->price, $pricing);
    }

    /*
     * Changing subscription details during refund
     */
    public function refundOrder($invoice): void
    {
        //Get the latest service activated for the user and update the status
        $service = Service::where('user_id', $invoice->user_id)
          ->latest()->first();

        $service->status_id = 5;
        $invoice->payment_status = 'refunded';
        $invoice->refund_date = Carbon::now()->format('Y-m-d H:i:s');
        $invoice->save();
        $service->save();
    }

    /*
     * Adding order,invoice and service details in the case of trial order
     */
    public function trialOrderDetails($user, $plan, $pricing, $gateway, $paymentStatus, $txId, $isRenew, $description, $price)
    {
        $addPlanService = new AddPlanService();
        $order = $addPlanService->addOrder($gateway, $user, $price, $pricing);
        $service = $addPlanService->addService($plan, $pricing, $order, $user);
        $invoice = $addPlanService->addInvoice($user, $pricing, $price, $order, $paymentStatus, $txId, $gateway, $isRenew);
        $invoiceDetails = $addPlanService->addInvoiceDetails($invoice, $service, $description, $price, $pricing);

        return $order;
    }

    public function acceptOrder($order)
    {
        $user = User::find($order->user_id);
        $service = Service::where('user_id', $user->id)->latest()->first();
        $pricing = Pricing::find($order->product_id);
        $plan = Plan::find($pricing->plan_id);
        $invoice = Invoice::where('order_id', $order->uuid)->first();

        $order->status = 'active';
        $order->save();

        /*
         * service class that interact with the AddPlan model.
         * refer app/Services/Models/AddPlanService.php
         */
        $addPlanService = new AddPlanService();

        if ($order->parent_order_id) {
            if ($service->pricing_id == $pricing->id) {
                //Updating Service details using updateService() function in AddPlanService
                $addPlanService->updateService($service, $plan, $pricing, $user, $order);
            } else {
                //Adding upgrade service details using upgradeService() function in AddPlanService
                $addPlanService->upgradeService($service, $plan, $pricing, $user, $order, $order->amount, $invoice->payment_status);
            }
        }

        return $order;
    }
}
