<?php

namespace App\Models\Services;

use App\Helpers\Uuid;
use App\Models\SslMonitor;
use App\Models\SslMonitorLog;
use App\Models\User;
use App\Services\WebsiteStatus;
use Illuminate\Support\Carbon;

class SslMonitorService
{
    /*
    * Getting filtered websites
    */
    public function getFilteredElements($user, $request): \Illuminate\Database\Eloquent\Builder
    {
        $query = SslMonitor::query()->where('user_id', $user->id);
        $query->latest();
        //Filter websites by by search keywords
        if ($request->search) {
            $query = $query->where('name', 'like', '%'.$request->search.'%')
                ->orWhere('url', 'like', '%'.$request->search.'%');
        }

        $monitors = $query;

        return $monitors;
    }

    public function addSsl($request, $user): SslMonitor
    {
        $monitor = new SslMonitor();
        $monitor->uuid = Uuid::getUuid();
        $monitor->user_id = $user->id;
        $monitor->name = $request->name;

        $monitor_url = str_replace('https://', '', $request->url);
        $monitor_url = str_replace('http://', '', $monitor_url);
        $monitor_url = strtok($monitor_url, '/');
        $monitor->url = $monitor_url;

        $monitor->ssl_port = $request->ssl_port;
        $monitor->email = $request->email;

        $websiteStatus = new WebsiteStatus();
        // $domain_info = $websiteStatus->domainExpiryStatus($monitor_url);
        // if ($domain_info) {
        $ssl_expiry = $websiteStatus->sslExpiryStatus($monitor_url, $request->ssl_port);
        $monitor->ssl_expiry = $ssl_expiry;
        // } else {
        //     return false;
        // }

        $monitor->save();

        $this->monitorLog($monitor);

        return $monitor;
    }

    public function updateSsl($request, $monitor)
    {
        $updateArray = [];
        $updateArray['name'] = $request->name;

        $monitor_url = str_replace('https://', '', $request->url);
        $monitor_url = str_replace('http://', '', $monitor_url);
        $monitor_url = strtok($monitor_url, '/');
        $updateArray['url'] = $monitor_url;

        $updateArray['ssl_port'] = $request->ssl_port;
        $updateArray['email'] = $request->email;

        $websiteStatus = new WebsiteStatus();
        // $domain_info = $websiteStatus->domainExpiryStatus($monitor_url);
        // if ($domain_info) {
        $ssl_expiry = $websiteStatus->sslExpiryStatus($monitor_url, $request->ssl_port);
        $updateArray['ssl_expiry'] = $ssl_expiry;
        // } else {
        //     return false;
        // }

        $monitor->update($updateArray);

        $this->monitorLog($monitor);

        return $monitor;
    }

    public function addBulkSsl($ssl_list): ?bool
    {
        foreach ($ssl_list as $data) {
            $user = auth()->user();
            $monitorNames = SslMonitor::where('name', $data[0])
                ->pluck('name');
            if (count($monitorNames)) {
                continue;
            }

            $monitorsCount = SslMonitor::where('user_id', $user->id)
                ->whereNull('deleted_at')
                ->count();
            //Check whether the user's adding limit exceeded or not
            if (! $user->can('createSsl', [Monitor::class, $monitorsCount])) {
                return null;
            }

            $monitor = new SslMonitor();
            $monitor->uuid = Uuid::getUuid();
            $monitor->user_id = $user->id;
            $monitor->name = $data[0];
            $monitor->url = $data[0];
            $monitor->ssl_port = $data[1] ? $data[1] : 443;
            $monitor->save();
        }

        return true;
    }

    public function monitorLog($monitor): void
    {
        $log = new SslMonitorLog();
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->ssl_expiry = $monitor->ssl_expiry;

        $current_date = Carbon::now()->format('Y-m-d H:i:s');
        if ($monitor->ssl_expiry >= $current_date) {
            $log->ssl_status = true;
        } else {
            $log->ssl_status = false;
        }
        $log->save();
    }

    public function csvConvert($request): array
    {
        $file = $request->file('csv_file');

        $ssl_list = [];

        if (($open = fopen($file, 'r')) !== false) {
            while (($data = fgetcsv($open, 1000, ',')) !== false) {
                $ssl_list[] = $data;
            }

            fclose($open);
        }
        array_shift($ssl_list);

        return $ssl_list;
    }
}
