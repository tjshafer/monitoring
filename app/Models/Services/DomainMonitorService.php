<?php

namespace App\Models\Services;

use App\Helpers\Uuid;
use App\Models\DomainMonitor;
use App\Models\DomainMonitorLog;
use App\Models\User;
use App\Services\WebsiteStatus;
use Illuminate\Support\Carbon;

class DomainMonitorService
{
    /*
    * Getting filtered domains
    */
    public function getFilteredElements($user, $request): \Illuminate\Database\Eloquent\Builder
    {
        $query = DomainMonitor::query()->where('user_id', $user->id);
        $query->latest();
        //Filter domains by by search keywords
        if ($request->search) {
            $query = $query->where('name', 'like', '%'.$request->search.'%')
                ->orWhere('url', 'like', '%'.$request->search.'%');
        }

        $domains = $query;

        return $domains;
    }

    public function addDomain($request, $user)
    {
        $domain = new DomainMonitor();
        $domain->uuid = Uuid::getUuid();
        $domain->user_id = $user->id;
        $domain->name = $request->name;

        $domain_url = str_replace('https://', '', $request->url);
        $domain_url = str_replace('http://', '', $domain_url);
        $domain_url = strtok($domain_url, '/');

        $domain->url = $domain_url;
        $domain->email = $request->email;

        $domainStatus = new WebsiteStatus();
        $domain_info = $domainStatus->domainExpiryStatus($domain_url);
        if ($domain_info) {
            $domain_expiry = date('Y-m-d H:i:s', $domain_info->expirationDate);
            $domain->domain_expiry = Carbon::parse($domain_expiry);
        } else {
            return false;
        }

        $domain->save();

        $this->monitorLog($domain);

        return $domain;
    }

    public function updateDomain($request, $domain)
    {
        $updateArray = [];
        $updateArray['name'] = $request->name;

        $domain_url = str_replace('https://', '', $request->url);
        $domain_url = str_replace('http://', '', $domain_url);
        $domain_url = strtok($domain_url, '/');

        $updateArray['url'] = $domain_url;
        $updateArray['email'] = $request->email;

        $domainStatus = new WebsiteStatus();
        $domain_info = $domainStatus->domainExpiryStatus($domain_url);
        if ($domain_info) {
            $domain_expiry = date('Y-m-d H:i:s', $domain_info->expirationDate);
            $updateArray['domain_expiry'] = Carbon::parse($domain_expiry);
        } else {
            return false;
        }

        $domain->update($updateArray);

        $this->monitorLog($domain);

        return $domain;
    }

    public function addBulkDomains($domains): ?bool
    {
        foreach ($domains as $data) {
            $user = auth()->user();
            $monitorNames = DomainMonitor::where('name', $data[0])
                ->pluck('name');
            if (count($monitorNames)) {
                continue;
            }

            $monitorsCount = DomainMonitor::where('user_id', $user->id)
                ->whereNull('deleted_at')
                ->count();
            //Check whether the user's adding limit exceeded or not
            if (! $user->can('createDomain', [Monitor::class, $monitorsCount])) {
                return null;
            }

            $domain = new DomainMonitor();
            $domain->uuid = Uuid::getUuid();
            $domain->user_id = $user->id;
            $domain->name = $data[0];
            $domain->url = $data[0];
            $domain->save();
        }

        return true;
    }

    public function monitorLog($domain): void
    {
        $log = new DomainMonitorLog();
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $domain->uuid;
        $log->domain_expiry = $domain->domain_expiry;

        $current_date = Carbon::now()->format('Y-m-d H:i:s');

        if ($domain->domain_expiry >= $current_date) {
            $log->domain_status = true;
        } else {
            $log->domain_status = false;
        }
        $log->save();
    }

    public function csvConvert($request): array
    {
        $file = $request->file('csv_file');

        $domains = [];

        if (($open = fopen($file, 'r')) !== false) {
            while (($data = fgetcsv($open, 1000, ',')) !== false) {
                $domains[] = $data;
            }

            fclose($open);
        }
        array_shift($domains);

        return $domains;
    }
}
