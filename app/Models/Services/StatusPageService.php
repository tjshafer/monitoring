<?php

namespace App\Models\Services;

use App\Helpers\Uuid;
use App\Models\MonitorLog;
use App\Models\MonitorStatusPage;
use App\Models\StatusPage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class StatusPageService
{
    /*
     * Adding public page
     */
    public function addPage($user, $request, $fileName): StatusPage
    {
        // $domain = env("DOMAIN");
        // $domain = str_replace("https://", "", $domain);
        // $domain = str_replace("http://", "", $domain);

        $status_page = new StatusPage;
        $status_page->uuid = Uuid::getUuid();
        $status_page->name = $request->name;
        $status_page->sub_domain = $request->sub_domain ? $request->sub_domain : null;
        $status_page->user_id = $user->id;
        $status_page->logo = $fileName;
        $status_page->description = $request->description;
        $status_page->slug = Str::of($request->post('name'))->slug('-');
        $status_page->save();

        return $status_page;
    }

    public function updatePage($user, $request, $uuid, $fileName)
    {
        // $domain = env("DOMAIN");
        // $domain = str_replace("https://", "", $domain);
        // $domain = str_replace("http://", "", $domain);

        $status_page = StatusPage::find($uuid);
        $updateArray = [];
        $updateArray['name'] = $request->name;

        $updateArray['sub_domain'] = $request->sub_domain ? $request->sub_domain : null;
        $updateArray['logo'] = $fileName;
        $updateArray['description'] = $request->description;
        $updateArray['slug'] = Str::of($request->post('name'))->slug('-');
        $status_page->update($updateArray);

        return $status_page;
    }

    public function addData($monitor_id, $type, $status_page): void
    {
        $monitor = new MonitorStatusPage;
        $monitor->uuid = Uuid::getUuid();
        $monitor->monitor_id = $monitor_id;
        $monitor->rel_id = $status_page->uuid;
        $monitor->type = $type;
        $monitor->save();
    }

    public function viewStatusPage($monitors, $monitor_ids): array
    {
        // $totalLogs = MonitorLog::whereIn('rel_id', $monitor_ids)
        //     ->get();

        // $todayLogs = $totalLogs->where('created_at', '>=', Carbon::today())->count();
        // $lastWeekLogs = $totalLogs->where('created_at', '>=', Carbon::now()->subDays(7)->startOfDay()->format('Y-m-d H:i:s'))->count();
        // $lastMonthLogs = $totalLogs->where('created_at', '>=', Carbon::now()->subDays(30)->startOfDay()->format('Y-m-d H:i:s'))->count();
        // $todayUpLogs = $totalLogs->where('created_at', '>=',Carbon::today())
        //     ->where('is_up', true)
        //     ->count();

        // $lastWeekUpLogs = $totalLogs->where('created_at', '>=', Carbon::now()->subDays(7)->startOfDay()->format('Y-m-d H:i:s'))
        //     ->where('is_up', true)
        //     ->count();

        // $lastMonthUpLogs = $totalLogs->where('created_at', '>=', Carbon::now()->subDays(30)->startOfDay()->format('Y-m-d H:i:s'))
        //     ->where('is_up', true)
        //     ->count();

        // //Get that day status
        // if ($todayLogs !== 0){
        //     $todayStatus = (100*$todayUpLogs)/$todayLogs;
        // }else{
        //     $todayStatus = 0;
        // }

        // //Get last 7 day status
        // if ($lastWeekLogs !== 0){
        //     $lastWeekStatus = (100*$lastWeekUpLogs)/$lastWeekLogs;
        // }else{
        //     $lastWeekStatus = 0;
        // }

        // //Get last 30 day status
        // if ($lastMonthLogs !== 0){
        //     $lastMonthStatus = (100*$lastMonthUpLogs)/$lastMonthLogs;
        // }else{
        //     $lastMonthStatus = 0;
        // }

        $todayUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', Carbon::today())
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->first();

        $oneWeekUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', Carbon::now()->subDays(7)->startOfDay()->format('Y-m-d H:i:s'))
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->first();

        $oneMonthUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', Carbon::now()->subDays(30)->startOfDay()->format('Y-m-d H:i:s'))
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->first();

        $todayStatus = $todayUp ? $todayUp->perc : 0;
        $lastWeekStatus = $oneWeekUp ? $oneWeekUp->perc : 0;
        $lastMonthStatus = $oneMonthUp ? $oneMonthUp->perc : 0;

        $startDate = Carbon::now()->subDays(89)->startOfDay()->format('Y-m-d');
        $startDateString = "'".$startDate."'";
        $endDate = Carbon::now()->format('Y-m-d H:i:s');
        $startDateTwoMonth = Carbon::now()->subDays(59)->startOfDay()->format('Y-m-d');
        $startDateOneMonth = Carbon::now()->subDays(29)->startOfDay()->format('Y-m-d');

        $threeMonthUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->get();

        $twoMonthUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', $startDateTwoMonth)
            ->where('created_at', '<=', $endDate)
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->get();

        $oneMonthUp = MonitorLog::select(\DB::raw('
            rel_id,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc'))
            ->where('created_at', '>=', $startDateOneMonth)
            ->where('created_at', '<=', $endDate)
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw('rel_id '))
        ->oldest()
        ->get();
        $threeMonthStatus = [];
        $twoMonthStatus = [];
        $oneMonthStatus = [];
        foreach ($threeMonthUp as $row90) {
            $threeMonthStatus[$row90->rel_id] = $row90->perc;
        }
        foreach ($twoMonthUp as $row60) {
            $twoMonthStatus[$row60->rel_id] = $row60->perc;
        }
        foreach ($oneMonthUp as $row30) {
            $oneMonthStatus[$row30->rel_id] = $row30->perc;
        }
        // echo "<pre>"; print_r($oneMonthUp);exit;

        $rows = MonitorLog::select(\DB::raw("count(*) as count,
        rel_id,  sum(is_up) as up_count,
            DATE_FORMAT(created_at, '%Y-%m-%d') as
            log_date,
            DATEDIFF(DATE_FORMAT(created_at, '%Y-%m-%d'), $startDateString) as i,
            ROUND((sum(is_up) /  count(*)) * 100, 2)  as perc"))
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->whereIn('rel_id', $monitor_ids)
            ->groupBy(\DB::raw("rel_id,
            DATEDIFF(DATE_FORMAT(created_at, '%Y-%m-%d'), $startDateString),
            DATE_FORMAT(created_at, '%Y-%m-%d') "))
            ->oldest()
            ->get();

        $reports = [];
        foreach ($rows as $row) {
            $reports[$row->i][$row->rel_id]['date'] = $row->log_date;
            $reports[$row->i][$row->rel_id]['perc'] = $row->perc;
        }
        // echo "<pre>"; print_r($reports);exit;

        $response = [];

        $response['reports'] = $reports;
        $response['todayStatus'] = round($todayStatus, 2);
        $response['lastWeekStatus'] = round($lastWeekStatus, 2);
        $response['lastMonthStatus'] = round($lastMonthStatus, 2);
        $response['threeMonthStatus'] = $threeMonthStatus;
        $response['twoMonthStatus'] = $twoMonthStatus;
        $response['oneMonthStatus'] = $oneMonthStatus;

        return $response;
    }
}
