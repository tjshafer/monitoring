<?php

namespace App\Models\Services;

use App\Helpers\Uuid;
use App\Models\DnsAAAAMonitor;
use App\Models\DnsAMonitor;
use App\Models\DnsCNameMonitor;
use App\Models\DnsMonitoring;
use App\Models\DnsMonitorLogs;
use App\Models\DnsMXMonitor;
use App\Models\DnsNSMonitor;
use App\Models\DnsTXTMonitor;
use App\Services\DnsStatus;
use Illuminate\Support\Carbon;

class DnsMonitorService
{
    /*
     * Getting filtered monitors
     */
    public function getFilteredElements($user, $request): \Illuminate\Database\Eloquent\Builder
    {
        $query = DnsMonitoring::query()->where('user_id', $user->id);
        //Filter monitors by by search keywords
        if ($request->search) {
            $query = $query->where('website_domain', 'like', '%'.$request->search.'%');
        }

        $monitors = $query;

        return $monitors;
    }

    /*
     *Add Dns monitor
     */
    public function addDnsMonitor($request, $user): bool
    {
        $monitor = new DnsMonitoring();
        $monitor->uuid = Uuid::getUuid();
        $monitor->user_id = $user->id;
        $monitor->website_domain = $request->url;
        $monitor->interval = $request->interval;
        $monitor->email = $user->email;
        $monitor->save();
        // $this->CheckAllRecords($monitor);
        return true;
    }

    /*
     *Update Dns
     */
    public function updateMonitor($request, $monitor): bool
    {
        $updateArray = [];
        $updateArray['website_domain'] = $request->url;
        $updateArray['interval'] = $request->interval;
        $monitor->update($updateArray);
        $this->checkRecordsOnUpdateDns($monitor);

        return true;
    }

    /*
     * Calculation of average response time of a Server
     */
    public function getAverageResponseTime($relId)
    {
        //Get logs of corresponding monitor.
        $value = DnsMonitorLogs::where('rel_id', $relId)
            ->where('is_up', 1)
            ->avg('response_time');

        if (empty($value)) {
            return 0;
        }

        return $value;
    }

    /*
     *Add A Record to Dns
     */
    public function addARecord($request, $uuid, $domain): bool
    {
        $monitor = new DnsAMonitor();
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $startTime = intval(microtime(true) * 1000);
        $monitor->uuid = Uuid::getUuid();
        $monitor->website_id = $uuid;
        $monitor->ip_address = $request->ip_address;

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->website_id = $uuid;
        $log->type = 'A';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';
        $ip_array = explode(',', $request->ip_address);

        //checking dns status
        $resolved = $dns_status->checkArecord($domain->website_domain, $ip_array);
        $monitor->last_resolved_ip = $resolved;
        if ($resolved != null) {
            $log->is_up = 1;
            $monitor->previous_status = 'up';
        } else {
            $log->is_up = 0;
            $monitor->previous_status = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $monitor->avg_response_time = ($endTime - $startTime);
        $log->response_time = $monitor->avg_response_time;

        $monitor->save();
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    /*
     *Add AAAA Record to Dns
     */
    public function addAAAARecord($request, $uuid, $domain): bool
    {
        $monitor = new DnsAAAAMonitor();
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $startTime = intval(microtime(true) * 1000);
        $monitor->uuid = Uuid::getUuid();
        $monitor->website_id = $uuid;
        $monitor->ipv6_address = $request->ip_address;

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->website_id = $uuid;
        $log->type = 'AAAA';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';
        $ip_array = explode(',', $request->ip_address);

        $resolved = $dns_status->checkAaaaRecord($domain->website_domain, $ip_array);
        $monitor->last_resolved_ip = $resolved;
        if ($resolved != null) {
            $log->is_up = 1;
            $monitor->previous_status = 'up';
        } else {
            $log->is_up = 0;
            $monitor->previous_status = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $monitor->avg_response_time = ($endTime - $startTime);
        $log->response_time = $monitor->avg_response_time;
        $monitor->save();
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    /*
     *Add CName Record to Dns
     */
    public function addCNameRecord($request, $uuid, $domain): bool
    {
        $monitor = new DnsCNameMonitor();
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $startTime = intval(microtime(true) * 1000);
        $monitor->uuid = Uuid::getUuid();
        $monitor->website_id = $uuid;
        $monitor->value = $request->value;

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->website_id = $uuid;
        $log->type = 'CNAME';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';

        if ($dns_status->checkCnameRecord($domain->website_domain, $request->value)) {
            $log->is_up = 1;
            $monitor->previous_status = 'up';
        } else {
            $log->is_up = 0;
            $monitor->previous_status = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $monitor->avg_response_time = ($endTime - $startTime);
        $log->response_time = $monitor->avg_response_time;
        $monitor->save();
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    /*
     *Add NS Record to Dns
     */
    public function addNSRecord($request, $uuid, $domain): bool
    {
        $monitor = new DnsNSMonitor();
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $startTime = intval(microtime(true) * 1000);
        $monitor->uuid = Uuid::getUuid();
        $monitor->website_id = $uuid;
        $monitor->value = $request->value;

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->website_id = $uuid;
        $log->type = 'NS';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';

        if ($dns_status->checkNsRecord($domain->website_domain, $request->value)) {
            $log->is_up = 1;
            $monitor->previous_status = 'up';
        } else {
            $log->is_up = 0;
            $monitor->previous_status = 'down';
        }

        $endTime = intval(microtime(true) * 1000);
        $monitor->avg_response_time = ($endTime - $startTime);
        $log->response_time = $monitor->avg_response_time;
        $monitor->save();
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    /*
     *Add TXT Record to Dns
     */
    public function addTXTRecord($request, $uuid, $domain): bool
    {
        $monitor = new DnsTXTMonitor();
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $startTime = intval(microtime(true) * 1000);
        $monitor->uuid = Uuid::getUuid();
        $monitor->website_id = $uuid;
        $monitor->value = $request->value;

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->website_id = $uuid;
        $log->type = 'TXT';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';

        if ($dns_status->checkTxtRecord($domain->website_domain, $request->value)) {
            $log->is_up = 1;
            $monitor->previous_status = 'up';
        } else {
            $log->is_up = 0;
            $monitor->previous_status = 'down';
        }

        $endTime = intval(microtime(true) * 1000);
        $monitor->avg_response_time = ($endTime - $startTime);
        $log->response_time = $monitor->avg_response_time;
        $monitor->save();
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    /*
     *Add MX Record to Dns
     */
    public function addMXRecord($request, $uuid, $domain): bool
    {
        $monitor = new DnsMXMonitor();
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $startTime = intval(microtime(true) * 1000);
        $monitor->uuid = Uuid::getUuid();
        $monitor->website_id = $uuid;
        $monitor->priority = $request->priority;
        $monitor->exchange = $request->exchange;

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $monitor->uuid;
        $log->website_id = $uuid;
        $log->type = 'MX';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';

        if ($dns_status->checkMxRecord($domain->website_domain, $request->priority, $request->exchange)) {
            $log->is_up = 1;
            $monitor->previous_status = 'up';
        } else {
            $log->is_up = 0;
            $monitor->previous_status = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $monitor->avg_response_time = ($endTime - $startTime);
        $log->response_time = $monitor->avg_response_time;
        $monitor->save();
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    public function updateARecord($request, $record, $domain): bool
    {
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $updateArray = [];
        $updateArray['ip_address'] = $request->ip_address;

        $startTime = intval(microtime(true) * 1000);
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $record->uuid;
        $log->website_id = $record->website_id;
        $log->type = 'A';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';
        $ip_array = explode(',', $request->ip_address);

        //checking dns status
        $resolved = $dns_status->checkArecord($domain->website_domain, $ip_array);
        $updateArray['last_resolved_ip'] = $resolved;
        if ($resolved != null) {
            $log->is_up = 1;
            $updateArray['previous_status'] = 'up';
        } else {
            $log->is_up = 0;
            $updateArray['previous_status'] = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $updateArray['avg_response_time'] = ($endTime - $startTime);
        $log->response_time = ($endTime - $startTime);
        $record->update($updateArray);
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    public function updateAAAARecord($request, $record, $domain): bool
    {
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $updateArray = [];
        $updateArray['ipv6_address'] = $request->ip_address;

        $startTime = intval(microtime(true) * 1000);
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $record->uuid;
        $log->website_id = $record->website_id;
        $log->type = 'AAAA';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';

        $ip_array = explode(',', $request->ip_address);
        //checking dns status
        $resolved = $dns_status->checkAaaaRecord($domain->website_domain, $ip_array);
        $updateArray['last_resolved_ip'] = $resolved;
        if ($resolved != null) {
            $log->is_up = 1;
            $updateArray['previous_status'] = 'up';
        } else {
            $log->is_up = 0;
            $updateArray['previous_status'] = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $updateArray['avg_response_time'] = ($endTime - $startTime);
        $log->response_time = ($endTime - $startTime);

        $record->update($updateArray);
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    public function updateCnameRecord($request, $record, $domain): bool
    {
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $updateArray = [];
        $updateArray['value'] = $request->value;

        $startTime = intval(microtime(true) * 1000);

        $log->uuid = Uuid::getUuid();
        $log->rel_id = $record->uuid;
        $log->website_id = $record->website_id;
        $log->type = 'CNAME';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';
        if ($dns_status->checkCnameRecord($domain->website_domain, $request->value)) {
            $log->is_up = 1;
            $updateArray['previous_status'] = 'up';
        } else {
            $log->is_up = 0;
            $updateArray['previous_status'] = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $updateArray['avg_response_time'] = ($endTime - $startTime);
        $log->response_time = ($endTime - $startTime);

        $record->update($updateArray);
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    public function updateNsRecord($request, $record, $domain): bool
    {
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $updateArray = [];
        $updateArray['value'] = $request->value;

        $startTime = intval(microtime(true) * 1000);
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $record->uuid;
        $log->website_id = $record->website_id;
        $log->type = 'NS';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';

        if ($dns_status->checkNsRecord($domain->website_domain, $request->value)) {
            $log->is_up = 1;
            $updateArray['previous_status'] = 'up';
        } else {
            $log->is_up = 0;
            $updateArray['previous_status'] = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $updateArray['avg_response_time'] = ($endTime - $startTime);
        $log->response_time = ($endTime - $startTime);

        $record->update($updateArray);
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    public function updateTxtRecord($request, $record, $domain): bool
    {
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $updateArray = [];
        $updateArray['value'] = $request->value;

        $startTime = intval(microtime(true) * 1000);
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $record->uuid;
        $log->website_id = $record->website_id;
        $log->type = 'TXT';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';

        if ($dns_status->checkTxtRecord($domain->website_domain, $request->value)) {
            $log->is_up = 1;
            $updateArray['previous_status'] = 'up';
        } else {
            $log->is_up = 0;
            $updateArray['previous_status'] = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $updateArray['avg_response_time'] = ($endTime - $startTime);
        $log->response_time = ($endTime - $startTime);

        $record->update($updateArray);
        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    public function updateMXRecord($request, $record, $domain): bool
    {
        //create object for dns monitor log.
        $log = new DnsMonitorLogs();
        //service class that interact with dns monitor.
        $dns_status = new DnsStatus();

        $updateArray = [];
        $updateArray['priority'] = $request->priority;
        $updateArray['exchange'] = $request->exchange;

        $startTime = intval(microtime(true) * 1000);
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $record->uuid;
        $log->website_id = $record->website_id;
        $log->type = 'MX';
        $log->previous_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
        $log->down_time = 0;
        $log->response_json = '';

        if ($dns_status->checkMxRecord($domain->website_domain, $request->priority, $request->exchange)) {
            $log->is_up = 1;
            $updateArray['previous_status'] = 'up';
        } else {
            $log->is_up = 0;
            $updateArray['previous_status'] = 'down';
        }
        $endTime = intval(microtime(true) * 1000);
        $updateArray['avg_response_time'] = ($endTime - $startTime);
        $log->response_time = ($endTime - $startTime);

        $record->update($updateArray);

        $log->save();
        $this->CheckAllRecords($domain);

        return true;
    }

    /*
     * This function sets the dns status up if all records are up.
     */
    public function CheckAllRecords($server): void
    {
        $Server_status = 'up';
        $dnsARecords = DnsMonitoring::find($server->uuid)->aRecord;
        $dnsAaaaRecords = DnsMonitoring::find($server->uuid)->aaaaRecord;
        $dnsCnameRecords = DnsMonitoring::find($server->uuid)->cnameRecord;
        $dnsMxRecords = DnsMonitoring::find($server->uuid)->mxRecord;
        $dnsNsRecords = DnsMonitoring::find($server->uuid)->nsRecord;
        $dnsTxtRecords = DnsMonitoring::find($server->uuid)->txtRecord;

        if (count($dnsARecords) + count($dnsAaaaRecords) + count($dnsCnameRecords) + count($dnsMxRecords) + count($dnsNsRecords) + count($dnsTxtRecords) == 0) {
            $Server_status = 'NA';
        }

        foreach ($dnsARecords as $record) {
            if ($record->previous_status == 'down') {
                $Server_status = 'down';
            }
        }
        foreach ($dnsAaaaRecords as $record) {
            if ($record->previous_status == 'down') {
                $Server_status = 'down';
            }
        }
        foreach ($dnsCnameRecords as $record) {
            if ($record->previous_status == 'down') {
                $Server_status = 'down';
            }
        }
        foreach ($dnsMxRecords as $record) {
            if ($record->previous_status == 'down') {
                $Server_status = 'down';
            }
        }
        foreach ($dnsNsRecords as $record) {
            if ($record->previous_status == 'down') {
                $Server_status = 'down';
            }
        }
        foreach ($dnsTxtRecords as $record) {
            if ($record->previous_status == 'down') {
                $Server_status = 'down';
            }
        }

        if ($Server_status == 'down') {
            $this->setStatus($server, $Server_status);
        } elseif ($Server_status == 'up') {
            $this->setStatus($server, $Server_status);
        } else {
            $this->setStatus($server, 'NA');
        }
    }

    public function setStatus($server, $status): void
    {
        $server->previous_status = $status;
        $server->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $server->save();
    }

    /**
     * This function checks all the records under a dns on update of url.
     */
    public function checkRecordsOnUpdateDns($dnsServer): void
    {
        $dnsStatus = new DnsStatus();

        $dnsARecords = DnsMonitoring::find($dnsServer->uuid)->aRecord;
        $dnsAaaaRecords = DnsMonitoring::find($dnsServer->uuid)->aaaaRecord;
        $dnsCnameRecords = DnsMonitoring::find($dnsServer->uuid)->cnameRecord;
        $dnsMxRecords = DnsMonitoring::find($dnsServer->uuid)->mxRecord;
        $dnsNsRecords = DnsMonitoring::find($dnsServer->uuid)->nsRecord;
        $dnsTxtRecords = DnsMonitoring::find($dnsServer->uuid)->txtRecord;

        $serverUp = true;
        //DnsARecord check
        foreach ($dnsARecords as $dnsARecord) {
            //Create an object for DnsMonitorLog
            $log = new DnsMonitorLogs();
            $url = $dnsServer->website_domain;
            $log->uuid = Uuid::getUuid();
            $log->rel_id = $dnsARecord->uuid;
            $log->website_id = $dnsServer->uuid;
            $log->type = 'A';
            $startTime = intval(microtime(true) * 1000);
            /**
             * The case where server is up
             * Status check using checkArecord() function in DnsStatus service
             */
            $ip_array = explode(',', $dnsARecord->ip_address);

            //checking dns status
            $resolved = $dnsStatus->checkArecord($url, $ip_array);
            $dnsARecord->last_resolved_ip = $resolved;
            if ($resolved != null) {
                $endTime = intval(microtime(true) * 1000);
                $dnsARecord->previous_status = 'up';
                $log->response_time = ($endTime - $startTime);
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->is_up = 1;
                $log->save();
                $dnsARecord->avg_response_time = $log->response_time;
                $dnsARecord->save();
            } else {
                $endTime = intval(microtime(true) * 1000);
                $log->is_up = 0;
                $dnsARecord->previous_status = 'down';
                $log->response_time = 0;
                $log->down_time = $dnsServer->interval;
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->save();
                $dnsARecord->avg_response_time = ($endTime - $startTime);
                $dnsARecord->save();

                $serverUp = false;
            }
        }
        //DnsAaaaRecord check
        foreach ($dnsAaaaRecords as $dnsAaaaRecord) {
            //Create an object for DnsMonitorLog
            $log = new DnsMonitorLogs();
            $url = $dnsServer->website_domain;
            $log->uuid = Uuid::getUuid();
            $log->rel_id = $dnsAaaaRecord->uuid;
            $log->website_id = $dnsServer->uuid;
            $log->type = 'AAAA';
            $startTime = intval(microtime(true) * 1000);

            $ip_array = explode(',', $dnsAaaaRecord->ipv6_address);
            $resolved = $dnsStatus->checkAaaaRecord($url, $ip_array);
            $dnsAaaaRecord->last_resolved_ip = $resolved;
            if ($resolved != null) {
                $endTime = intval(microtime(true) * 1000);
                $dnsAaaaRecord->previous_status = 'up';
                $log->response_time = ($endTime - $startTime);
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->is_up = 1;
                $log->save();
                $dnsAaaaRecord->avg_response_time = $log->response_time;
                $dnsAaaaRecord->save();
            } else {
                $endTime = intval(microtime(true) * 1000);
                $log->is_up = 0;
                $dnsAaaaRecord->previous_status = 'down';
                $log->response_time = 0;
                $log->down_time = $dnsServer->interval;
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->save();
                $dnsAaaaRecord->avg_response_time = ($endTime - $startTime);
                $dnsAaaaRecord->save();

                $serverUp = false;
            }
        }
        //DnsCnameRecord check
        foreach ($dnsCnameRecords as $dnsCnameRecord) {
            //Create an object for DnsMonitorLog
            $log = new DnsMonitorLogs();
            $url = $dnsServer->website_domain;
            $log->uuid = Uuid::getUuid();
            $log->rel_id = $dnsCnameRecord->uuid;
            $log->website_id = $dnsServer->uuid;
            $log->type = 'CNAME';
            $startTime = intval(microtime(true) * 1000);
            /**
             * The case where server is up
             * Status check using checkCnameRecord() function in DnsStatus service
             */
            if ($dnsStatus->checkCnameRecord($url, $dnsCnameRecord->value)) {
                $endTime = intval(microtime(true) * 1000);
                $dnsCnameRecord->previous_status = 'up';
                $log->response_time = ($endTime - $startTime);
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->is_up = 1;
                $log->save();
                $dnsCnameRecord->avg_response_time = $log->response_time;
                $dnsCnameRecord->save();
            } else {
                $endTime = intval(microtime(true) * 1000);
                $log->is_up = 0;
                $dnsCnameRecord->previous_status = 'down';
                $log->response_time = 0;
                $log->down_time = $dnsServer->interval;
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->save();
                $dnsCnameRecord->avg_response_time = ($endTime - $startTime);
                $dnsCnameRecord->save();

                $serverUp = false;
            }
        }
        //DnsMxRecord check
        foreach ($dnsMxRecords as $dnsMxRecord) {
            //Create an object for DnsMonitorLog
            $log = new DnsMonitorLogs();
            $url = $dnsServer->website_domain;
            $log->uuid = Uuid::getUuid();
            $log->rel_id = $dnsMxRecord->uuid;
            $log->website_id = $dnsServer->uuid;
            $log->type = 'MX';
            $startTime = intval(microtime(true) * 1000);
            /**
             * The case where server is up
             * Status check using checkMxRecord() function in DnsStatus service
             */
            if ($dnsStatus->checkMxRecord($url, $dnsMxRecord->priority, $dnsMxRecord->exchange)) {
                $endTime = intval(microtime(true) * 1000);
                $dnsMxRecord->previous_status = 'up';
                $log->response_time = ($endTime - $startTime);
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->is_up = 1;
                $log->save();
                $dnsMxRecord->avg_response_time = $log->response_time;
                $dnsMxRecord->save();
            } else {
                $endTime = intval(microtime(true) * 1000);
                $log->is_up = 0;
                $dnsMxRecord->previous_status = 'down';
                $log->response_time = 0;
                $log->down_time = $dnsServer->interval;
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->save();
                $dnsMxRecord->avg_response_time = ($endTime - $startTime);
                $dnsMxRecord->save();

                $serverUp = false;
            }
        }
        //DnsNsRecord check
        foreach ($dnsNsRecords as $dnsNsRecord) {
            //Create an object for DnsMonitorLog
            $log = new DnsMonitorLogs();
            $url = $dnsServer->website_domain;
            $log->uuid = Uuid::getUuid();
            $log->rel_id = $dnsNsRecord->uuid;
            $log->website_id = $dnsServer->uuid;
            $log->type = 'NS';
            $startTime = intval(microtime(true) * 1000);
            /**
             * The case where server is up
             * Status check using checkNsRecord() function in DnsStatus service
             */
            if ($dnsStatus->checkNsRecord($url, $dnsNsRecord->value)) {
                $endTime = intval(microtime(true) * 1000);
                $dnsNsRecord->previous_status = 'up';
                $log->response_time = ($endTime - $startTime);
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->is_up = 1;
                $log->save();
                $dnsNsRecord->avg_response_time = $log->response_time;
                $dnsNsRecord->save();
            } else {
                $endTime = intval(microtime(true) * 1000);
                $log->is_up = 0;
                $dnsNsRecord->previous_status = 'down';
                $log->response_time = 0;
                $log->down_time = $dnsServer->interval;
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->save();
                $dnsNsRecord->avg_response_time = ($endTime - $startTime);
                $dnsNsRecord->save();

                $serverUp = false;
            }
        }
        //DnsTxtRecord check
        foreach ($dnsTxtRecords as $dnsTxtRecord) {
            //Create an object for DnsMonitorLog
            $log = new DnsMonitorLogs();
            $url = $dnsServer->website_domain;
            $log->uuid = Uuid::getUuid();
            $log->rel_id = $dnsTxtRecord->uuid;
            $log->website_id = $dnsServer->uuid;
            $log->type = 'TXT';
            $startTime = intval(microtime(true) * 1000);
            /**
             * The case where server is up
             * Status check using checkTxtRecord() function in DnsStatus service
             */
            if ($dnsStatus->checkTxtRecord($url, $dnsTxtRecord->value)) {
                $endTime = intval(microtime(true) * 1000);
                $dnsTxtRecord->previous_status = 'up';
                $log->response_time = ($endTime - $startTime);
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->is_up = 1;
                $log->save();
                $dnsTxtRecord->avg_response_time = $log->response_time;
                $dnsTxtRecord->save();
            } else {
                $endTime = intval(microtime(true) * 1000);
                $log->is_up = 0;
                $dnsTxtRecord->previous_status = 'down';
                $log->response_time = 0;
                $log->down_time = $dnsServer->interval;
                $log->current_time = Carbon::now()->format('Y-m-d H:i:s');
                $log->previous_time = Carbon::now()->subMinute($dnsServer->interval)->format('Y-m-d H:i:s');
                $log->save();
                $dnsTxtRecord->avg_response_time = ($endTime - $startTime);
                $dnsTxtRecord->save();

                $serverUp = false;
            }
        }

        if ($serverUp) {
            $dnsServer->previous_status = 'up';
            $dnsServer->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $dnsServer->save();
        } else {
            $dnsServer->previous_status = 'down';
            $dnsServer->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $dnsServer->save();
        }
        if (count($dnsARecords) + count($dnsAaaaRecords) + count($dnsCnameRecords) + count($dnsMxRecords) + count($dnsNsRecords) + count($dnsTxtRecords) == 0) {
            $dnsServer->previous_status = 'NA';
            $dnsServer->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $dnsServer->save();
        }
    }
}
