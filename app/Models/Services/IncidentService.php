<?php

namespace App\Models\Services;

use App\Helpers\Random;
use App\Helpers\Uuid;
use App\Models\Incident;
use App\Models\IncidentDetails;
use App\Services\SubscriptionEmail;

class IncidentService
{
    /*
     * Getting filtered incidents
     */
    public function getFilteredIncidents($user, $request, $query)
    {
        $query->latest('updated_at');
        //Filter incidents by server/webpage/Api
        if ($request->rel_id) {
            $query = $query->where('rel_id', $request->rel_id);
        }
        //Filter incidents by status
        if ($request->status == 'false') {
            $query = $query->where('status', false);
        } elseif ($request->status == 'true') {
            $query = $query->where('status', true);
        }
        $incidents = $query;

        return $incidents;
    }

    /*
     * Add incidet to incidents table
     */
    public function addIncident($request, $incident)
    {
        $incident->uuid = Uuid::getUuid();
        $incident->rel_id = $request->rel_id;
        $incident->incident_id = Random::getIncidentNumber();
        $incident->subject = $request->subject;
        $incident->description = $request->description;
        $incident->save();

        return $incident;
    }

    /*
     * updation of incident
     */
    public function updateIncident($request, $incident)
    {
        $updateArray = [];
        $updateArray['subject'] = $request->subject;
        $updateArray['description'] = $request->description;
        $incident->update($updateArray);

        return $incident;
    }

    /*
     * Adding incident follow up details
     */
    public function updateIncidentDetails($request, $incident, $incidentDetails)
    {
        $incidentDetails->uuid = Uuid::getUuid();
        $incidentDetails->incident_id = $incident->uuid;
        $incidentDetails->status = $request->status;
        $incidentDetails->description = $request->updated_description;

        if ($request->status == 'resolved') {
            $incident->status = true;
        } else {
            $incident->status = false;
        }
        $incident->save();
        $incidentDetails->save();

        return $incidentDetails;
    }

    public function addAutoIncident($rel_id): Incident
    {
        $incident = new Incident();
        $incident->uuid = Uuid::getUuid();
        $incident->incident_id = Random::getIncidentNumber();
        $incident->rel_id = $rel_id;
        $incident->is_auto = true;
        $incident->subject = 'The system is down';
        $incident->description = 'Something went wrong. Kindly wait for sometime';
        $incident->status = false;
        $incident->save();

        // Sent incident added mail to email subscription enabled statuspage users
        $emailSubscriptionService = new SubscriptionEmail();
        $emailSubscriptionService->addIncident($incident->monitors, $incident);

        return $incident;
    }

    public function updateAutoIncident($rel_id): IncidentDetails
    {
        $incident = Incident::where('rel_id', $rel_id)
            ->where('is_auto', true)
            ->latest()->first();

        $incident->status = true;
        $incident->save();

        $incidentDetails = new IncidentDetails();
        $incidentDetails->uuid = Uuid::getUuid();
        $incidentDetails->incident_id = $incident->uuid;
        $incidentDetails->status = 'Resolved';
        $incidentDetails->description = 'The problem resolved successfully';
        $incidentDetails->save();

        // Sent incident updated or resolved mail to email subscription enabled statuspage users
        $emailSubscriptionService = new SubscriptionEmail();
        $monitor = $incident->monitors;
        $emailSubscriptionService->resolveIncident($monitor, $incidentDetails);

        return $incidentDetails;
    }
}
