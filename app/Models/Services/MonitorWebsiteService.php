<?php

namespace App\Models\Services;

use App\Helpers\Uuid;
use App\Models\MonitorWebsite;
use App\Models\User;
use App\Models\WebsiteMonitorLog;
use App\Services\WebsiteStatus;
use Illuminate\Support\Carbon;

class MonitorWebsiteService
{
    /*
    * Getting filtered websites
    */
    public function getFilteredElements($user, $request): \Illuminate\Database\Eloquent\Builder
    {
        $query = MonitorWebsite::query()->where('user_id', $user->id);
        $query->latest();
        //Filter websites by by search keywords
        if ($request->search) {
            $query = $query->where('name', 'like', '%'.$request->search.'%')
                ->orWhere('url', 'like', '%'.$request->search.'%');
        }

        $websites = $query;

        return $websites;
    }

    public function addWebsite($request, $user)
    {
        $website = new MonitorWebsite();
        $website->uuid = Uuid::getUuid();
        $website->user_id = $user->id;
        $website->name = $request->name;

        $website_url = str_replace('https://', '', $request->url);
        $website_url = str_replace('http://', '', $website_url);
        $website_url = strtok($website_url, '/');
        $website->url = $website_url;

        $website->ssl_port = $request->ssl_port;
        $website->email = $request->email;

        $websiteStatus = new WebsiteStatus();
        $domain_info = $websiteStatus->domainExpiryStatus($website_url);
        if ($domain_info) {
            $ssl_expiry = $websiteStatus->sslExpiryStatus($website_url, $request->ssl_port);
            $domain_expiry = date('Y-m-d H:i:s', $domain_info->expirationDate);
            $website->ssl_expiry = $ssl_expiry;
            $website->domain_expiry = Carbon::parse($domain_expiry);
        } else {
            return false;
        }

        $website->save();

        $this->monitorLog($website);

        return $website;
    }

    public function updateWebsite($request, $website)
    {
        $updateArray = [];
        $updateArray['name'] = $request->name;

        $website_url = str_replace('https://', '', $request->url);
        $website_url = str_replace('http://', '', $website_url);
        $website_url = strtok($website_url, '/');
        $updateArray['url'] = $website_url;

        $updateArray['ssl_port'] = $request->ssl_port;
        $updateArray['email'] = $request->email;

        $websiteStatus = new WebsiteStatus();
        $domain_info = $websiteStatus->domainExpiryStatus($website_url);
        if ($domain_info) {
            $ssl_expiry = $websiteStatus->sslExpiryStatus($website_url, $request->ssl_port);
            $domain_expiry = date('Y-m-d H:i:s', $domain_info->expirationDate);
            $updateArray['ssl_expiry'] = $ssl_expiry;
            $updateArray['domain_expiry'] = Carbon::parse($domain_expiry);
        } else {
            return false;
        }

        $website->update($updateArray);

        $this->monitorLog($website);

        return $website;
    }

    public function addBulkWebsites($websites)
    {
        foreach ($websites as $data) {
            $user = auth()->user();
            $monitorNames = MonitorWebsite::where('name', $data[0])
                ->pluck('name');
            if (count($monitorNames)) {
                continue;
            }

            $monitorsCount = MonitorWebsite::where('user_id', $user->id)
                ->whereNull('deleted_at')
                ->count();
            //Check whether the user's adding limit exceeded or not
            if (! $user->can('createWebsite', [Monitor::class, $monitorsCount])) {
                return null;
            }

            $website = new MonitorWebsite();
            $website->uuid = Uuid::getUuid();
            $website->user_id = $user->id;
            $website->name = $data[0];
            $website->url = $data[0];
            $website->ssl_port = $data[1] ? $data[1] : 443;
            $website->save();
        }
    }

    public function monitorLog($website): void
    {
        $log = new WebsiteMonitorLog();
        $log->uuid = Uuid::getUuid();
        $log->rel_id = $website->uuid;
        $log->ssl_expiry = $website->ssl_expiry;
        $log->domain_expiry = $website->domain_expiry;

        $current_date = Carbon::now()->format('Y-m-d H:i:s');
        if ($website->ssl_expiry >= $current_date) {
            $log->ssl_status = true;
        } else {
            $log->ssl_status = false;
        }

        if ($website->domain_expiry >= $current_date) {
            $log->domain_status = true;
        } else {
            $log->domain_status = false;
        }
        $log->save();
    }

    public function csvConvert($request): array
    {
        $file = $request->file('csv_file');

        $websites = [];

        if (($open = fopen($file, 'r')) !== false) {
            while (($data = fgetcsv($open, 1000, ',')) !== false) {
                $websites[] = $data;
            }

            fclose($open);
        }
        array_shift($websites);

        return $websites;
    }
}
