<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonitorStatusPage extends Model
{
    protected $primaryKey = 'uuid';

    public $incrementing = false;

    use HasFactory;

    public function statusPages(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(StatusPage::class, 'rel_id');
    }

    public function monitors(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Monitor::class, 'monitor_id');
    }
}
