<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DnsMonitorLogs extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $fillable = ['rel_id', 'website_id', 'type', 'is_up', 'previous_time', 'current_time', 'down_time', 'response_json'];

    protected $casts = [
        'previous_time' => 'datetime',
        'current_time' => 'datetime',
    ];
}
