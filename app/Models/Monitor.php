<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Monitor extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $fillable = [
        'name', 'url', 'type', 'port', 'expected_response_code', 'email', 'previous_status',
        'interval', 'avg_response_time', 'methode', 'json', 'test_result',
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function publicPages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PublicPage::class, 'rel_id');
    }

    public function incidents(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Incident::class, 'rel_id');
    }

    public function logs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(MonitorLog::class, 'rel_id');
    }

    public function statusPages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(MonitorStatusPage::class, 'monitor_id');
    }

    protected static function boot(): void
    {
        parent::boot();

        static::deleting(function ($monitor) {
            $monitor->publicPages()->delete();
            $monitor->incidents()->delete();
            $monitor->logs()->delete();
            $monitor->statusPages()->delete();
        });
    }
}
