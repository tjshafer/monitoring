<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DnsMXMonitor extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $fillable = ['website_id', 'priority', 'exchange', 'previous_status', 'current_status', 'response_time'];

    protected $table = 'dns_mx_records';
}
