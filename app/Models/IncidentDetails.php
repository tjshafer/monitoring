<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IncidentDetails extends Model
{
    protected $primaryKey = 'uuid';

    public $incrementing = false;

    use HasFactory;

    public function incidents(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Incident::class, 'incident_id');
    }
}
