<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KeywordMonitoring extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $fillable = ['name', 'url', 'keyword', 'is_case_sensitive', 'alert_when', 'interval', 'email', 'previous_status', 'current_status'];

    protected $table = 'keyword_monitoring';

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
