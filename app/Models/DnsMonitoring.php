<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DnsMonitoring extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $fillable = ['website_domain', 'interval', 'previous_status', 'current_status', 'email'];

    protected $table = 'dns_monitoring';

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function aRecord(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DnsAMonitor::class, 'website_id');
    }

    public function aaaaRecord(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DnsAAAAMonitor::class, 'website_id');
    }

    public function cnameRecord(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DnsCNameMonitor::class, 'website_id');
    }

    public function mxRecord(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DnsMXMonitor::class, 'website_id');
    }

    public function nsRecord(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DnsNSMonitor::class, 'website_id');
    }

    public function txtRecord(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DnsTXTMonitor::class, 'website_id');
    }
}
