<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incident extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $fillable = ['incident_id', 'rel_id', 'subject', 'description', 'status'];

    public function monitors(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Monitor::class, 'rel_id');
    }
}
