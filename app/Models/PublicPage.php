<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PublicPage extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name', 'rel_id', 'status',
    ];

    public function monitors(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Monitor::class, 'rel_id');
    }
}
