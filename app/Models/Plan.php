<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'description', 'status', 'server_count',
        'webpage_count', 'api_count', 'ssl_count', 'domain_count', 'subdomain_count', 'dns_count', 'keyword_count', 'display_order',
    ];
}
