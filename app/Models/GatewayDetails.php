<?php

namespace App\Models;

use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GatewayDetails extends Model
{
    use HasFactory;
    use Encryptable;

    protected $encryptable = [
        'value',
    ];

    protected $fillable = [
        'name', 'display_name', 'status', 'description', 'value',
    ];

    public function gateways(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Gateway::class, 'gateway_id');
    }
}
