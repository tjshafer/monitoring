<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class ApiMonitorUp extends Mailable
{
    use Queueable, SerializesModels;

    protected $api;

    protected $content;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($api, $content, $user)
    {
        $this->api = $api;
        $this->content = $content;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $content = $this->content;
        $user_name = $this->user;
        $api_name = $this->api->name;
        $api_url = $this->api->url;
        $expected_response_code = $this->api->expected_response_code;
        $time = Carbon::now()->toDateTimeString();
        $app_url = config('app.url');
        $app_name = config('app.name');
        $value = ['{$api_name}', '{$user_name}', '{$api_url}', '{$expected_response_code}', '{$time}', '{$app_name}', '{$app_url}'];
        $actual_value = [$api_name, $user_name, $api_url, $expected_response_code, $time, $app_name, $app_url];
        $body = $content->message;

        $message = str_replace($value, $actual_value, $body);

        return $this->subject($content->subject)
            ->html($message);
    }
}
