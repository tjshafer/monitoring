<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class KeywordMonitorUp extends Mailable
{
    use Queueable, SerializesModels;

    protected $keyword;

    protected $content;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($keyword, $content, $user)
    {
        $this->keyword = $keyword;
        $this->content = $content;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $content = $this->content;
        $user_name = $this->user;
        $keyword_name = $this->keyword->name;
        $url = $this->keyword->url;
        $time = Carbon::now()->toDateTimeString();
        $app_url = config('app.url');
        $app_name = config('app.name');
        $value = ['{$keyword_name}', '{$user_name}', '{$url}', '{$time}', '{$app_name}', '{$app_url}'];
        $actual_value = [$keyword_name, $user_name, $url, $time, $app_name, $app_url];
        $body = $content->message;

        $message = str_replace($value, $actual_value, $body);

        return $this->subject($content->subject)
            ->html($message);
    }
}
