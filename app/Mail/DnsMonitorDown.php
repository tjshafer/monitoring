<?php

namespace App\Mail;

use App\Helpers\Logger;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class DnsMonitorDown extends Mailable
{
    use Queueable, SerializesModels;

    protected $server;

    protected $record;

    protected $value;

    protected $content;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($server, $record, $value, $content, $user)
    {
        $this->server = $server;
        $this->record = $record;
        $this->value = $value;
        $this->content = $content;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $content = $this->content;
        $user_name = $this->user;
        $website_domain = $this->server->website_domain;
        $record = $this->record;
        $rec_value = $this->value;
        $time = Carbon::now()->toDateTimeString();
        $app_url = config('app.url');
        $app_name = config('app.name');
        $value = ['{$user_name}', '{$website_domain}', '{$record}', '{$value}', '{$time}', '{$app_name}', '{$app_url}'];
        $actual_value = [$user_name, $website_domain, $record, $rec_value, $time, $app_name, $app_url];
        $body = $content->message;

        $message = str_replace($value, $actual_value, $body);
        //Logger::info($message);
        return $this->subject($content->subject)
            ->html($message);
    }
}
