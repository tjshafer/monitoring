<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class IncidentResolveMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $status_page;

    protected $content;

    protected $incident_details;

    protected $user;

    protected $monitor;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($incident_details, $content, $monitor, $status_page, $user)
    {
        $this->status_page = $status_page;
        $this->content = $content;
        $this->incident_details = $incident_details;
        $this->user = $user;
        $this->monitor = $monitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $content = $this->content;
        $user_name = $this->user;
        $monitor_name = $this->monitor->name;
        $status_page_name = $this->status_page->name;
        $incident_description = $this->incident_details->incidents->description;
        $incident_subject = $this->incident_details->incidents->subject;
        $incident_update_description = $this->incident_details->description;
        $incident_status = 'Resolved';

        $app_url = config('app.url');
        $app_url = str_replace('https://', '', $app_url);
        $app_url = str_replace('http://', '', $app_url);
        $app_url = $this->status_page->sub_domain.'.'.$app_url;

        $app_name = config('app.name');
        $value = ['{$monitor_name}', '{$user_name}', '{$status_page_name}', '{$incident_description}', '{$incident_subject}', '{$incident_update_description}', '{$incident_status}', '{$app_name}', '{$app_url}'];
        $actual_value = [$monitor_name, $user_name, $status_page_name, $incident_description, $incident_subject, $incident_update_description, $incident_status, $app_name, $app_url];
        $body = $content->message;

        $message = str_replace($value, $actual_value, $body);

        return $this->subject($content->subject)
            ->html($message);
    }
}
