<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SslValidityReminder extends Mailable
{
    use Queueable, SerializesModels;

    protected $website;

    protected $content;

    protected $expireIn;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($website, $expireIn, $content)
    {
        $this->website = $website;
        $this->content = $content;
        $this->expireIn = $expireIn;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $content = $this->content;
        $user_name = $this->website->user->first_name;
        $website_name = $this->website->name;
        $website_url = $this->website->url;
        $expire_in_days = $this->expireIn;
        $domain_expiry_date = $this->website->domain_expiry;
        $app_url = config('app.url');
        $app_name = config('app.name');
        $value = ['{$website_name}', '{$user_name}', '{$website_url}', '{$expire_in_days}', '{$domain_expiry_date}', '{$app_name}', '{$app_url}'];
        $actual_value = [$website_name, $user_name, $website_url, $expire_in_days, $domain_expiry_date, $app_name, $app_url];
        $body = $content->message;

        $message = str_replace($value, $actual_value, $body);

        return $this->subject($content->subject)
            ->html($message);
    }
}
