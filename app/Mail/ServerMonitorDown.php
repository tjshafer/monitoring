<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class ServerMonitorDown extends Mailable
{
    use Queueable, SerializesModels;

    protected $server;

    protected $content;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($server, $content, $user)
    {
        $this->server = $server;
        $this->content = $content;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $content = $this->content;
        $user_name = $this->user;
        $server_name = $this->server->name;
        $server_url = $this->server->url;
        $server_port = $this->server->port;
        $time = Carbon::now()->toDateTimeString();
        $app_url = config('app.url');
        $app_name = config('app.name');
        $value = ['{$server_name}', '{$user_name}', '{$server_url}', '{$server_port}', '{$time}', '{$app_name}', '{$app_url}'];
        $actual_value = [$server_name, $user_name, $server_url, $server_port, $time, $app_name, $app_url];
        $body = $content->message;

        $message = str_replace($value, $actual_value, $body);

        return $this->subject($content->subject)
            ->html($message);
    }
}
