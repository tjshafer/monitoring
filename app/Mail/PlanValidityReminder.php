<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PlanValidityReminder extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    protected $expireIn;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $expireIn)
    {
        $this->user = $user;
        $this->expireIn = $expireIn;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('add_plan.mail.plan_validity_reminder')
                ->with([
                    'user' => $this->user,
                    'expireIn' => $this->expireIn,
                ]);
    }
}
