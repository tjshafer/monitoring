<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class WebpageMonitorDown extends Mailable
{
    use Queueable, SerializesModels;

    protected $webpage;

    protected $content;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($webpage, $content, $user)
    {
        $this->webpage = $webpage;
        $this->content = $content;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $content = $this->content;
        $user_name = $this->user;
        $webpage_name = $this->webpage->name;
        $webpage_url = $this->webpage->url;
        $expected_response_code = $this->webpage->expected_response_code;
        $time = Carbon::now()->toDateTimeString();
        $app_url = config('app.url');
        $app_name = config('app.name');
        $value = ['{$webpage_name}', '{$user_name}', '{$webpage_url}', '{$expected_response_code}', '{$time}', '{$app_name}', '{$app_url}'];
        $actual_value = [$webpage_name, $user_name, $webpage_url, $expected_response_code, $time, $app_name, $app_url];
        $body = $content->message;

        $message = str_replace($value, $actual_value, $body);

        return $this->subject($content->subject)
            ->html($message);
    }
}
