<?php

namespace App\Policies;

use App\Models\Incident;
use App\Models\Monitor;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class IncidentPolicy
{
    use HandlesAuthorization;

    /**
     * Let's check whether the user added atleast one API.
     * otherwise incident can't be added
     */
    public function create(User $user, $type)
    {
        $monitors = Monitor::where('user_id', $user->id)
            ->where('type', $type)
            ->get();
        if (count($monitors)) {
            return true;
        }

        return false;
    }
}
