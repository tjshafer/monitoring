<?php

namespace App\Policies;

use App\Models\Invoice;
use App\Models\Order;
use App\Models\Plan;
use App\Models\Service;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MonitorPolicy
{
    use HandlesAuthorization;

    /**
     * Check whether the user is active or not
     */
    public function isActive(User $user)
    {
        if ($user->status_id == 1 || $user->status_id == 2) {
            //status active or innactive case is true
            return true;
        }
        //status suspended or terminated case is false
        return false;
    }

    /**
     * Check whether the user have an active subscription
     * Imp:- Note that ***
     *                 Here in true case user have to add a plan first
     *                 ***
     * False case indicates that user have an active subscription
     */
    public function view(User $user)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        if ($service == null) {
            return true;
        }
        if ($service->status_id != 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Let's check whether the user's recent order is confirmed by the admin'
     * If not user can't do further things
     */
    public function orderConfirm(User $user)
    {
        $order = Order::where('user_id', $user->id)->latest()->first();
        if ($order) {
            $invoice = Invoice::where('order_id', $order->uuid)->latest()->first();
            if ($invoice) {
                if ($order->status == 'active' && $invoice->payment_status == 'paid') {
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        } else {
            return false;
        }
    }

    /**
     * Let's check whether the user's adding limit exceeded according to the plan
     */
    public function createApi(User $user, $apis)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        //Checks only if there is a subscription
        if ($service != null) {
            //Checks only if the service is not expired
            if ($service->status_id == 1) {
                $plan = Plan::where('id', $service->plan_id)->first();

                //Get the maximum no.of apis that can be added
                if ($plan->api_count != null) {
                    if ($plan->api_count > $apis) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            }
        }
    }

    public function createServer(User $user, $servers)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        //Checks only if there is a subscription
        if ($service != null) {
            //Checks only if the service is not expired
            if ($service->status_id == 1) {
                $plan = Plan::where('id', $service->plan_id)->first();

                //Get the maximum no.of servers that can be added
                if ($plan->server_count != null) {
                    if ($plan->server_count > $servers) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            }
        }
    }

    public function createWebpage(User $user, $webpages)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        //Checks only if there is a subscription
        if ($service != null) {
            //Checks only if the service is not expired
            if ($service->status_id == 1) {
                $plan = Plan::where('id', $service->plan_id)->first();

                //Get the maximum no.of webpages that can be added
                if ($plan->webpage_count != null) {
                    if ($plan->webpage_count > $webpages) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            }
        }
    }

    public function createSsl(User $user, $ssl_count)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        //Checks only if there is a subscription
        if ($service != null) {
            //Checks only if the service is not expired
            if ($service->status_id == 1) {
                $plan = Plan::where('id', $service->plan_id)->first();

                //Get the maximum no.of webpages that can be added
                if ($plan->ssl_count != null) {
                    if ($plan->ssl_count > $ssl_count) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            }
        }
    }

    public function createDomain(User $user, $domain_count)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        //Checks only if there is a subscription
        if ($service != null) {
            //Checks only if the service is not expired
            if ($service->status_id == 1) {
                $plan = Plan::where('id', $service->plan_id)->first();
                //Get the maximum no.of webpages that can be added
                if ($plan->domain_count != null) {
                    if ($plan->domain_count > $domain_count) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            }
        }
    }

    public function createDns(User $user, $dns_count)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        //Checks only if there is a subscription
        if ($service != null) {
            //Checks only if the service is not expired
            if ($service->status_id == 1) {
                $plan = Plan::where('id', $service->plan_id)->first();
                //Get the maximum no.of dns that can be added
                if ($plan->dns_count != null) {
                    if ($plan->dns_count > $dns_count) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            }
        }
    }

    public function createKeyword(User $user, $keyword_count)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        //Checks only if there is a subscription
        if ($service != null) {
            //Checks only if the service is not expired
            if ($service->status_id == 1) {
                $plan = Plan::where('id', $service->plan_id)->first();
                //Get the maximum no.of keywords that can be added
                if ($plan->keyword_count != null) {
                    if ($plan->keyword_count > $keyword_count) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
