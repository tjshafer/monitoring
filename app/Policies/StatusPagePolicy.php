<?php

namespace App\Policies;

use App\Models\Invoice;
use App\Models\Order;
use App\Models\Plan;
use App\Models\Service;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StatusPagePolicy
{
    use HandlesAuthorization;

    /**
     * Check whether the user is active or not
     */
    public function isActive(User $user)
    {
        if ($user->status_id == 1 || $user->status_id == 2) {
            //status active or innactive case is true
            return true;
        }
        //status suspended or terminated case is false
        return false;
    }

    /**
     * Check whether the user have an active subscription
     * Imp:- Note that ***
     *                 Here in true case user have to add a plan first
     *                 ***
     * False case indicates that user have an active subscription
     */
    public function view(User $user)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        if ($service == null) {
            return true;
        }
        if ($service->status_id != 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Let's check whether the user's recent order is confirmed by the admin'
     * If not user can't do further things
     */
    public function orderConfirm(User $user)
    {
        $order = Order::where('user_id', $user->id)->latest()->first();
        if ($order) {
            $invoice = Invoice::where('order_id', $order->uuid)->latest()->first();
            if ($invoice) {
                if ($order->status == 'active' && $invoice->payment_status == 'paid') {
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        } else {
            return false;
        }
    }

    /**
     * Let's check whether the user's adding limit exceeded according to the plan
     */
    public function createStatuspage(User $user, $statusPages)
    {
        $service = Service::where('user_id', $user->id)->latest()->first();
        //Checks only if there is a subscription
        if ($service != null) {
            //Checks only if the service is not expired
            if ($service->status_id == 1) {
                $plan = Plan::where('id', $service->plan_id)->first();

                //Get the maximum no.of apis that can be added
                if ($plan->subdomain_count != null) {
                    if ($plan->subdomain_count > $statusPages) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
