<?php

namespace App\Services;

use App\Helpers\Logger;
use App\Mail\ApiMonitorDown;
use App\Mail\ApiMonitorUp;
use App\Mail\DnsMonitorDown;
use App\Mail\DnsMonitorUp;
use App\Mail\DomainValidityReminder;
use App\Mail\KeywordMonitorDown;
use App\Mail\KeywordMonitorUp;
use App\Mail\ServerMonitorDown;
use App\Mail\ServerMonitorUp;
use App\Mail\SslValidityReminder;
use App\Mail\WebpageMonitorDown;
use App\Mail\WebpageMonitorUp;
use App\Models\EmailTemplate;
use Illuminate\Support\Facades\Mail;

class Email
{
    public function serverMonitorDown($server): void
    {
        $content = EmailTemplate::where('name', 'server_monitor_down')->first();
        if ($server->email) {
            $toEmail = $server->email;
        } else {
            $toEmail = $server->user->email;
        }
        $user = $server->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending server monitpr down mail to '.$toEmail);
            $mail = new ServerMonitorDown($server, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function serverMonitorUp($server): void
    {
        $content = EmailTemplate::where('name', 'server_monitor_up')->first();
        if ($server->email) {
            $toEmail = $server->email;
        } else {
            $toEmail = $server->user->email;
        }
        $user = $server->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending server monitor up mail to '.$toEmail);
            $mail = new ServerMonitorUp($server, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function webpageMonitorDown($webpage): void
    {
        $content = EmailTemplate::where('name', 'webpage_monitor_down')->first();
        if ($webpage->email) {
            $toEmail = $webpage->email;
        } else {
            $toEmail = $webpage->user->email;
        }
        $user = $webpage->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending webpage monitpr down mail to '.$toEmail);
            $mail = new WebpageMonitorDown($webpage, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function webpageMonitorUp($webpage): void
    {
        $content = EmailTemplate::where('name', 'webpage_monitor_up')->first();
        if ($webpage->email) {
            $toEmail = $webpage->email;
        } else {
            $toEmail = $webpage->user->email;
        }
        $user = $webpage->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending webpage monitor up mail to '.$toEmail);
            $mail = new WebpageMonitorUp($webpage, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function apiMonitorDown($api): void
    {
        $content = EmailTemplate::where('name', 'api_monitor_down')->first();
        if ($api->email) {
            $toEmail = $api->email;
        } else {
            $toEmail = $api->user->email;
        }
        $user = $api->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending api monitpr down mail to '.$toEmail);
            $mail = new ApiMonitorDown($api, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function apiMonitorUp($api): void
    {
        $content = EmailTemplate::where('name', 'api_monitor_up')->first();
        if ($api->email) {
            $toEmail = $api->email;
        } else {
            $toEmail = $api->user->email;
        }
        $user = $api->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending api monitor up mail to '.$toEmail);
            $mail = new ApiMonitorUp($api, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function domainValidityReminder($website, $expireIn): void
    {
        $content = EmailTemplate::where('name', 'domain_validity_reminder')->first();
        if ($website->email) {
            $toEmail = $website->email;
        } else {
            $toEmail = $website->user->email;
        }
        if ($content->status == true) {
            Logger::info("Sending domain validity reminder email to: $toEmail");
            $mail = new DomainValidityReminder($website, $expireIn, $content);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function sslValidityReminder($website, $expireIn): void
    {
        $content = EmailTemplate::where('name', 'ssl_validity_reminder')->first();
        if ($website->email) {
            $toEmail = $website->email;
        } else {
            $toEmail = $website->user->email;
        }
        if ($content->status == true) {
            Logger::info("Sending SSL validity reminder email to: $toEmail");
            $mail = new SslValidityReminder($website, $expireIn, $content);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    private function getCredentials($monitor): void
    {
        if ($monitor->email) {
            $toEmail = $monitor->email;
        } else {
            $toEmail = $monitor->user->email;
        }
        $user = $monitor->user->first_name;
    }

    public function dnsMonitorUp($server, $record, $value): void
    {
        $content = EmailTemplate::where('name', 'dns_monitor_up')->first();
        if ($server->email) {
            $toEmail = $server->email;
        } else {
            $toEmail = $server->user->email;
        }
        $user = $server->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending DNS monitor up mail to '.$toEmail);
            $mail = new DnsMonitorUp($server, $record, $value, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function dnsMonitorDown($server, $record, $value): void
    {
        $content = EmailTemplate::where('name', 'dns_monitor_down')->first();
        if ($server->email) {
            $toEmail = $server->email;
        } else {
            $toEmail = $server->user->email;
        }
        $user = $server->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending DNS monitor down mail to '.$toEmail);
            $mail = new DnsMonitorDown($server, $record, $value, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function keywordMonitorDown($keyword): void
    {
        $content = EmailTemplate::where('name', 'keyword_monitor_down')->first();
        if ($keyword->email) {
            $toEmail = $keyword->email;
        } else {
            $toEmail = $keyword->user->email;
        }
        $user = $keyword->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending keyword monitor down mail to '.$toEmail);
            $mail = new KeywordMonitorDown($keyword, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }

    public function keywordMonitorUp($keyword): void
    {
        $content = EmailTemplate::where('name', 'keyword_monitor_up')->first();
        if ($keyword->email) {
            $toEmail = $keyword->email;
        } else {
            $toEmail = $keyword->user->email;
        }
        $user = $keyword->user->first_name;
        if ($content->status == true) {
            Logger::info('Sending keyword monitor up mail to '.$toEmail);
            $mail = new KeywordMonitorUp($keyword, $content, $user);
            Mail::to($toEmail)
                ->queue($mail);
        }
    }
}
