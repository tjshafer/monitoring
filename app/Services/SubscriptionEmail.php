<?php

namespace App\Services;

use App\Helpers\Logger;
use App\Mail\ApiMonitorDown;
use App\Mail\ApiMonitorUp;
use App\Mail\IncidentCreateMail;
use App\Mail\IncidentResolveMail;
use App\Mail\IncidentUpdateMail;
use App\Mail\ServerMonitorDown;
use App\Mail\ServerMonitorUp;
use App\Mail\WebpageMonitorDown;
use App\Mail\WebpageMonitorUp;
use App\Models\EmailSubscription;
use App\Models\EmailTemplate;
use Illuminate\Support\Facades\Mail;

class SubscriptionEmail
{
    public function serverMonitorDown($server): void
    {
        $content = EmailTemplate::where('name', 'server_monitor_down')->first();
        $emailSubscriptions = $this->getEmails($server);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending server monitpr down mail to '.$email);
                $mail = new ServerMonitorDown($server, $content, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    public function serverMonitorUp($server): void
    {
        $content = EmailTemplate::where('name', 'server_monitor_up')->first();
        $emailSubscriptions = $this->getEmails($server);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending server monitor up mail to '.$email);
                $mail = new ServerMonitorUp($server, $content, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    public function webpageMonitorDown($webpage): void
    {
        $content = EmailTemplate::where('name', 'webpage_monitor_down')->first();
        $emailSubscriptions = $this->getEmails($webpage);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending webpage monitpr down mail to '.$email);
                $mail = new WebpageMonitorDown($webpage, $content, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    public function webpageMonitorUp($webpage): void
    {
        $content = EmailTemplate::where('name', 'webpage_monitor_up')->first();
        $emailSubscriptions = $this->getEmails($webpage);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending webpage monitor up mail to '.$email);
                $mail = new WebpageMonitorUp($webpage, $content, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    public function apiMonitorDown($api): void
    {
        $content = EmailTemplate::where('name', 'api_monitor_down')->first();
        $emailSubscriptions = $this->getEmails($api);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending api monitpr down mail to '.$email);
                $mail = new ApiMonitorDown($api, $content, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    public function apiMonitorUp($api): void
    {
        $content = EmailTemplate::where('name', 'api_monitor_up')->first();
        $emailSubscriptions = $this->getEmails($api);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending api monitor up mail to '.$email);
                $mail = new ApiMonitorUp($api, $content, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    public function addIncident($monitor, $incident): void
    {
        $content = EmailTemplate::where('name', 'incident_create_mail')->first();
        $emailSubscriptions = $this->getEmails($monitor);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending incident added mail to '.$email);
                $mail = new IncidentCreateMail($incident, $content, $monitor, $data->statusPages, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    public function updateIncident($monitor, $incident_details): void
    {
        $content = EmailTemplate::where('name', 'incident_update_mail')->first();
        $emailSubscriptions = $this->getEmails($monitor);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending incident updated mail to '.$email);
                $mail = new IncidentUpdateMail($incident_details, $content, $monitor, $data->statusPages, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    public function resolveIncident($monitor, $incident_details): void
    {
        $content = EmailTemplate::where('name', 'incident_resolve_mail')->first();
        $emailSubscriptions = $this->getEmails($monitor);
        if ($content->status == true && $emailSubscriptions) {
            foreach ($emailSubscriptions as $data) {
                $email = $data->email;
                $user = substr($email, 0, strrpos($email, '@'));
                Logger::info('Sending incident resolved mail to '.$email);
                $mail = new IncidentResolveMail($incident_details, $content, $monitor, $data->statusPages, $user);
                Mail::to($email)
                    ->queue($mail);
            }
        }
    }

    private function getEmails($monitor)
    {
        $status_pages = $monitor->statusPages->pluck('rel_id');
        if ($status_pages) {
            return EmailSubscription::whereIn('status_page_id', $status_pages)->get();
        }

        return [];
    }
}
