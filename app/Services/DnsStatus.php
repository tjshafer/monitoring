<?php

namespace App\Services;

use App\Models\DnsMonitorLogs;
use App\Models\Service;
use Illuminate\Support\Carbon;

class DnsStatus
{
    public function checkArecord($domain, $ipList)
    {
        $ipList = array_map('trim', $ipList);
        $records = dns_get_record($domain, DNS_A);
        if (empty($records)) {
            return;
        }
        $maxKey = max(array_keys($records));
        $record = $records[$maxKey];
        $resolvedIp = $record['ip'];

        //$aRecordStatus = false;
        if (in_array($resolvedIp, $ipList)) {
            return $resolvedIp;
        }

        // if($aRecordStatus) {
        //     //TODO: Log the below data
        //     // echo " Resolved to $resolvedIp and it\'s good";
        // } else {
        //    //TODO: Log the below data
        //    // echo " Resolved to $resolvedIp and it\'s bad";
        // }
    }

    public function checkAaaaRecord($domain, $ipv6list)
    {
        $ipv6list = array_map('trim', $ipv6list);
        $records = dns_get_record($domain, DNS_AAAA);
        if (empty($records)) {
            return;
        }
        //$recordFound = false;
        foreach ($records as $record) {
            $recordIpv6 = $record['ipv6'];
            if (in_array($recordIpv6, $ipv6list)) {
                return $recordIpv6;
            }
        }
    }

    public function checkCnameRecord($domain, $value): bool
    {
        $records = dns_get_record($domain, DNS_CNAME);
        if (empty($records)) {
            return false;
        }
        $maxKey = max(array_keys($records));
        $record = $records[$maxKey];
        $resolvedValue = $record['target'];

        $cnameRecordStatus = false;
        if ($resolvedValue == $value) {
            $cnameRecordStatus = true;
        }

        if ($cnameRecordStatus) {
            //TODO: Log the below data
            // echo " Resolved to $resolvedIp and it\'s good";
        } else {
            //TODO: Log the below data
            // echo " Resolved to $resolvedIp and it\'s bad";
        }

        return $cnameRecordStatus;
    }

    public function checkMxRecord($domain, $pri, $target): bool
    {
        $records = dns_get_record($domain, DNS_MX);
        foreach ($records as $k => $value) {
            $recordPri = $value['pri'];
            $recordTarget = $value['target'];
            if ($pri == $recordPri && $target == $recordTarget) {
                return true;
            }
        }

        return false;
    }

    public function checkNsRecord($domain, $target): bool
    {
        $records = dns_get_record($domain, DNS_NS);
        foreach ($records as $k => $value) {
            $recordTarget = $value['target'];
            if ($target == $recordTarget) {
                return true;
            }
        }

        return false;
    }

    public function checkTxtRecord($domain, $txt): bool
    {
        $records = dns_get_record($domain, DNS_TXT);
        foreach ($records as $k => $value) {
            $recordTxt = $value['txt'];
            if ($recordTxt == $txt) {
                return true;
            }
        }

        return false;
    }

    public function previousUpdate($dnsServer, $monitorRecord)
    {
        //Get the latest subscription
        $service = Service::where('user_id', $dnsServer->user_id)->latest()->first();
        if ($service != null && $service->status_id == 1) {
            //Get last checked time from logs
            $lastRun = DnsMonitorLogs::where('rel_id', $monitorRecord->uuid)->latest()->first();
            if ($lastRun) {
                $lastRunTime = Carbon::parse($lastRun->current_time);
            } else {
                return true;
            }
            //Get the next ping time using ping interval
            $nextPingTime = $lastRunTime->addMinutes($dnsServer->interval);
            $currentPingTime = Carbon::now()->format('Y-m-d H:i:s');
            if ($currentPingTime >= $nextPingTime) {
                //recheck the status
                return true;
            }
            //don't recheck the status
            return false;
        } else {
            return false;
        }
    }
}
