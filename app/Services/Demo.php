<?php

namespace App\Services;

use App\Helpers\Logger;
use Illuminate\Support\Carbon;

class Demo
{
    /**
     * Let's delete 5 days old data in the case of demo environement
     */
    public function deleteData($item): void
    {
        $today = Carbon::now()->format('Y-m-d H:i:s');
        $itemDate = $item->created_at;
        Logger::info("item date: $itemDate");
        $deleteDate = $itemDate->addDays(5);
        Logger::info("delete date: $deleteDate");

        if ($deleteDate < $today) {
            $item->delete();
            Logger::info('Item deleted deleted');
        }
    }
}
