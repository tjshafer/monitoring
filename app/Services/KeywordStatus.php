<?php

namespace App\Services;

use App\Models\KeywordMonitorLogs;
use App\Models\Service;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;

class KeywordStatus
{
    public function isKeywordUp($url, $keyword, $isCaseSensitive, $key_exist)
    {
        $response = Http::get($url);
        $body = $response->body();
        if ($key_exist == 'keyword_exist') {
            if ($isCaseSensitive) {
                if (strpos($body, $keyword) !== false) {
                    return true;
                }

                return false;
            } else {
                if (stripos($body, $keyword) !== false) {
                    return true;
                }

                return false;
            }
        } elseif ($key_exist == 'keyword_not_exist') {
            if ($isCaseSensitive) {
                if (strpos($body, $keyword) !== false) {
                    return false;
                }

                return true;
            } else {
                if (stripos($body, $keyword) !== false) {
                    return false;
                }

                return true;
            }
        }
    }

    public function previousUpdate($monitor)
    {
        //Get the latest subscription
        $service = Service::where('user_id', $monitor->user_id)->latest()->first();
        if ($service != null && $service->status_id == 1) {
            //Get last checked time from logs
            $lastRun = KeywordMonitorLogs::where('rel_id', $monitor->uuid)->latest()->first();
            if ($lastRun) {
                $lastRunTime = Carbon::parse($lastRun->current_time);
            } else {
                return true;
            }
            //Get the next ping time using ping interval
            $nextPingTime = $lastRunTime->addMinutes(5);
            $currentPingTime = Carbon::now()->format('Y-m-d H:i:s');
            if ($currentPingTime >= $nextPingTime) {
                //recheck the status
                return true;
            }
            //don't recheck the status
            return false;
        } else {
            return false;
        }
    }
}
