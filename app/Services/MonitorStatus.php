<?php

namespace App\Services;

use App\Helpers\Logger;
use App\Models\MonitorLog;
use App\Models\Service;
use Illuminate\Support\Carbon;

class MonitorStatus
{
    /**
     * Let's Check the current status of the server
     */
    public function isUpServer($server, $port)
    {
        // If websites are added with https or http,
        // remove them.
        $server = str_replace('https://', '', $server);
        $server = str_replace('http://', '', $server);

        $fp = @fsockopen($server, $port, $errno, $errstr, 10);
        if (! $fp) {
            Logger::emergency("$server Down, ".'Class: '.__CLASS__.' Function: '.__FUNCTION__);
            Logger::emergency('Error number: '.$errno);
            Logger::emergency('Error string: '.$errstr);

            return false;
        }
        fclose($fp);
        Logger::info('Message from '.'Class: '.__CLASS__.' Function: '.__FUNCTION__);
        Logger::info("$server is up and running ");

        return true;
    }

    /**
     * Let's Check the current status of the webpage
     */
    public function isUpWebpage($url, $response_code)
    {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url);
            $httpcode = $response->getStatusCode();
        } catch(\Exception $e) {
            $httpcode = -1;
        }

        if ($httpcode == $response_code) {
            return true;
        }

        return false;
    }

    /**
     * Let's Check the current status of API
     */
    public function isUpApi($url, $response_code, $json, $methode)
    {
        $curl = curl_init();
        $payload = $json;
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload); // onlu if $payload is not null
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $methode,
            CURLOPT_HTTPHEADER => [
                // "x-rapidapi-host: skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
                // "x-rapidapi-key: SIGN-UP-FOR-KEY"
            ],
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($err) {
            return false;
        }
        if ($status == $response_code) {
            return true;
        }
        curl_close($curl);
    }

    /**
     * Let's see if it's time to recheck the monitor status
     * On the basis of ping interval provided
     */
    public function previousUpdate($monitor)
    {
        //Get the latest subscription
        $service = Service::where('user_id', $monitor->user_id)->latest()->first();
        if ($service != null && $service->status_id == 1) {
            //Get last checked time from logs
            $lastRun = MonitorLog::where('rel_id', $monitor->uuid)->latest()->first();
            if ($lastRun) {
                $lastRunTime = Carbon::parse($lastRun->current_time);
            } else {
                return true;
            }
            //Get the next ping time using ping interval
            $nextPingTime = $lastRunTime->addMinutes($monitor->interval);
            $currentPingTime = Carbon::now()->format('Y-m-d H:i:s');
            if ($currentPingTime >= $nextPingTime) {
                //recheck the status
                return true;
            }
            //don't recheck the status
            return false;
        } else {
            return false;
        }
    }
}
