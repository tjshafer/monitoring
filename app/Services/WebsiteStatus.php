<?php

namespace App\Services;

use App\Helpers\Logger;
use Illuminate\Support\Carbon;
use Iodev\Whois\Factory;

class WebsiteStatus
{
    public function domainExpiryStatus($website_url)
    {
        // Creating default configured client
        $whois = Factory::get()->createWhois();

        // Checking if the domain is available or not
        if ($whois->isDomainAvailable($website_url)) {
            return false;
        }

        // Getting parsed domain info
        $info = $whois->loadDomainInfo($website_url);
        Logger::info('Expiry Date = '.date('Y-m-d', $info->expirationDate));

        return $info;

        /**
         * Additional informations that can get
         */

        // // Supports Unicode (converts to punycode)
        // if ($whois->isDomainAvailable("почта.рф")) {
        //     print "Bingo! Domain is available! :)";
        // }

        // // Getting raw-text lookup
        // $response = $whois->lookupDomain('google.com');
        // print $response->text;
    }

    /**
     * Let's Check the current status of the server
     */
    public function sslExpiryStatus($website_url, $port)
    {
        $url = $website_url;
        $serverStatus = new MonitorStatus();
        if ($serverStatus->isUpServer($url, $port)) {
            // Check http:// exist in the url
            // or not if not exist the add it
            if (strpos($url, 'http') !== 0) {
                $url = 'https://'.$url;
            }

            $orignal_parse = parse_url($url, PHP_URL_HOST);
            $get = stream_context_create(['ssl' => ['capture_peer_cert' => true]]);
            $read = stream_socket_client('ssl://'.$orignal_parse.":$port", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
            $cert = stream_context_get_params($read);
            $certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);

            $expiry_date = gmdate('Y-m-d H:i:s', $certinfo['validTo_time_t']);
            Logger::info('Expiry Date = '.$expiry_date);

            return Carbon::parse($expiry_date);
        }
        $expiry_date = null;

        return $expiry_date;
    }
}
