<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountryStateController extends Controller
{
    /*
     * Get the list of countries
     */
    public function country()
    {
        $countries = DB::table('countries')->get()->toJson(JSON_PRETTY_PRINT);

        return response($countries);
    }

    /*
     * Get the list of states corresponds to a country
     */
    public function state(Request $request)
    {
        $id = $request->input('country_id');
        $states = DB::table('states')
        ->where('country_id', $id)->get()->toJson(JSON_PRETTY_PRINT);

        return response($states);
    }
}
