<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Incident;
use App\Models\IncidentDetails;
use App\Models\Monitor;
use App\Models\Services\IncidentService;
use App\Models\User;
use App\Services\SubscriptionEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServerIncidentController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the icidents of servers
     */
    public function index(Request $request)
    {
        //Get logged in user
        $user = User::find(auth()->id());
        //Get all servers of the user
        $servers = Monitor::where('user_id', $user->id)
            ->where('type', 'server')
            ->get();
        /*
         * Query all server incidents corresponding to the user.
         * use hasMany ralation bcz user details only available in servers table
         */
        $query = Incident::query()->whereHas('monitors', function ($list) use ($user) {
            $list->where('user_id', $user->id)
                ->where('type', 'server');
        });

        /*
         * service class that interact with the Incident model.
         * refer getFilteredIncidents() function in app/Models/Services/IncidentService.php
         */
        $incidentService = new IncidentService();
        //Get filtered incidents using getFilteredIncidents function in IncidentService
        $incidents = $incidentService->getFilteredIncidents($user, $request, $query)
            ->paginate(10);
        $params = [
            'incidents' => $incidents,
            'servers' => $servers,
            'request' => $request,
        ];

        return view('incidents.server.index', $params);
    }

    /*
     * View create page of incidents
     */
    public function create()
    {
        //Get logged in user
        $user = User::find(auth()->id());
        /*
         * Check whether there is a server or not
         * refer app/Policies/IncidentPolicy.php
        */
        if (! $user->can('create', [Incident::class, 'server'])) {
            return to_route('server_monitor.create')
            ->with('error', __('Add a server first'));
        }

        //Get all available servers of the user
        $servers = Monitor::where('user_id', $user->id)
            ->where('type', 'server')
            ->get();
        $params = [
            'servers' => $servers,
        ];

        return view('incidents.server.create', $params);
    }

    /*
     * create server incidents.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'description' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Create an object for ServerIncident
            $incident = new Incident();

            /*
             * service class that interact with the Incident model.
             * refer addIncident() function in app/Models/Services/IncidentService.php
            */
            $incidentService = new IncidentService();
            //Add an incident using addIncident() function
            $incident = $incidentService->addIncident($request, $incident);

            // Sent incident added mail to email subscription enabled statuspage users
            $emailSubscriptionService = new SubscriptionEmail();
            $emailSubscriptionService->addIncident($incident->monitors, $incident);

            return to_route('server_incident.index')
                ->with('success', __('Incident added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Show the incident update page.
     */
    public function edit($uuid)
    {
        //Get the incident
        $incident = Incident::find($uuid);
        $params = [
            'incident' => $incident,
        ];

        return view('incidents.server.edit', $params);
    }

    /*
     * Update server incidents.
     */
    public function update(Request $request, $uuid): \Illuminate\Http\RedirectResponse
    {
        //Get the incident
        $incident = Incident::find($uuid);
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the Incident model.
             * refer updateIncident() function in app/Models/Services/IncidentService.php
             */
            $incidentService = new IncidentService();
            //update incident using updateIncident() function
            $incident = $incidentService->updateIncident($request, $incident);

            return to_route('server_incident.index')
                ->with('success', __('Incident updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Show the Incident follow up page.
     */
    public function show(Request $request, $uuid)
    {
        //Get the incident
        $incident = Incident::find($uuid);
        //Get the incident details
        $incidentDetails = IncidentDetails::where('incident_id', $uuid)
            ->latest('updated_at')
            ->paginate(10);
        $params = [
            'incidentDetails' => $incidentDetails,
            'incident' => $incident,
            'request' => $request,
        ];

        return view('incidents.server.show', $params);
    }

    /*
     * Update the incident details/follow ups.
     */
    public function updateIncidentDetails(Request $request, $uuid): \Illuminate\Http\RedirectResponse
    {
        //Get the incident
        $incident = Incident::find($uuid);
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'updated_description' => 'required',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Create an object for IncidentDetails
            $incidentDetails = new IncidentDetails();

            /*
             * service class that interact with the Incident model.
             * refer updateIncidentDetails() function in app/Models/Services/IncidentService.php
             */
            $incidentService = new IncidentService();
            //Update incident details using updateIncidentDetails() function
            $incidentDetails = $incidentService->updateIncidentDetails($request, $incident, $incidentDetails);

            // Sent incident updated or resolved mail to email subscription enabled statuspage users
            $emailSubscriptionService = new SubscriptionEmail();
            $monitor = $incident->monitors;
            if ($request->input('status') == 'resolved') {
                $emailSubscriptionService->resolveIncident($monitor, $incidentDetails);
            } else {
                $emailSubscriptionService->updateIncident($monitor, $incidentDetails);
            }

            return redirect()->back()
                ->with('success', __('Incident Details updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', $e->getMessage());
        }
    }

    /*
     * Delete an incident
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $incident = Incident::find($uuid);
        $incident->delete();

        return to_route('server_incident.index')
            ->with('success', __('Incident deleted'));
    }

    /*
     * Delete an incident follow up details
     */
    public function deleteDetails($uuid): \Illuminate\Http\RedirectResponse
    {
        $incidentDetails = IncidentDetails::find($uuid);
        $incidentId = $incidentDetails->incident_id;
        $incidentDetails->delete();

        return to_route('server_incident.show', $incidentId)
            ->with('success', __('Incident details deleted'));
    }
}
