<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Currency;
use App\Models\Plan;
use App\Models\Pricing;
use App\Models\Service;
use App\Models\Services\OrderService;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;

class AddPlanController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

        /*
         * Show the pricing page
         */
    public function index()
    {
        $plans = Plan::where('status', true)
            ->oldest('display_order')->get();
        $params = [
            'plans' => $plans,
        ];

        return view('add_plan.index', $params);
    }

    public function show($id)
    {
        $user = User::find(auth()->id());
        $plan = Plan::find($id);
        $currency = Currency::where('currency', $user->currency)->first();
        if ($currency && $id != 1) {
            $pricing = Pricing::where('plan_id', $id)
                 ->where('currency_id', $currency->id)
                 ->oldest('price')->get();
        } else {
            $pricing = Pricing::where('plan_id', $id)->get();
        }

        $currencies = Currency::all();
        $params = [
            'plan' => $plan,
            'pricing' => $pricing,
            'currencies' => $currencies,
        ];

        return view('add_plan.show', $params);
    }

    public function showCurrencyWise($plan_id, $id)
    {
        $plan = Plan::find($plan_id);
        $pricing = Pricing::where('plan_id', $plan_id)
            ->where('currency_id', $id)
            ->oldest('price')->get();
        $currencies = Currency::all();
        $selected_currency = Currency::find($id);
        $params = [
            'plan' => $plan,
            'pricing' => $pricing,
            'currencies' => $currencies,
            'selected_currency' => $selected_currency,
        ];

        return view('add_plan.currency_plans', $params);
    }

    public function invoice($id)
    {
        //Get logged in user
        $user = User::find(auth()->id());
        $service = Service::where('user_id', $user->id)
            ->latest()->first();

        //Get the plan
        $pricing = Pricing::find($id);
        $plan = Plan::find($pricing->plan_id);

        /*
         * Check whether the user already availed the trial offer
         * refer app/Policies/AddPlanPolicy.php
        */
        if ($service && $pricing->price == 0.00) {
            return to_route('available_plans.index')
                ->with('error', __('You cant use trial service anymore'));
        }
        //Get plan and corresponding properties

        /*
         * Get the payable price using the function price() in OrderService
         * refer app/Models/Central/Services/OrderService.php
        */
        $orderService = new OrderService();
        $price = $orderService->price($user, $pricing);

        /*
         * Check whether the user can downgrade the plan on the basis of price
         * refer app/Policies/AddPlanPolicy.php
        */
        if (! $user->can('downGrade', [AddPlan::class, $price])) {
            return to_route('available_plans.index')
            ->with('error', __('Your cant downgrade your plan at this moment. Please contact support team'));
        }

        /*
         * Check whether the currency selected is same to registered currency
        */
        if (! $user->can('selectCurrency', [AddPlan::class, $pricing])) {
            return to_route('available_plans.index')
            ->with('error', __('Your cant use the selected currency'));
        }

        //Check whether there is a billing address
        $billingAddress = Address::find($id = 1);
        if ($billingAddress) {
            $address = $billingAddress;
        } else {
            $address = null;
        }
        //Change the format of order_date
        $order_date = Carbon::now()->format('F d, Y');
        $params = [
            'plan' => $plan,
            'price' => $price,
            'pricing' => $pricing,
            'user' => $user,
            'address' => $address,
            'order_date' => $order_date,
        ];

        //Put the price on session to use from other controllers
        Session::put('price', $price);

        /*
         * Check whether there is an active plan to show a messsage
         * refer app/Policies/AddPlanPolicy.php
        */
        if ($user->can('planExist', AddPlan::class)) {
            Session::flash('error', 'You have an active plan. Continue only if you want to renew your plan. 
            Note that if you renewed your plan, existing plan will be replaced.');

            return view('add_plan.invoice', $params);
        }

        return view('add_plan.invoice', $params);
    }
}
