<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Services\SslMonitorService;
use App\Models\SslMonitor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class SslMonitorController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         * to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the available monitors
     */
    public function index(Request $request)
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        $ssl_expiry_remaining = [];
        /*
         * service class that interact with the Monitor model.
         * refer app/Models/Services/SslMonitorService.php
         */
        $sslMonitorService = new SslMonitorService();
        $current_date = Carbon::now()->format('Y-m-d H:i:s');
        //Get filtered monitors using getFilteredElements() function in  SslMonitorService
        $monitors = $sslMonitorService->getFilteredElements($user, $request)
            ->paginate(10);
        $ssl_expiry_remaining = [];
        $domain_expiry_remaining = [];
        foreach ($monitors as $monitor) {
            if ($monitor->ssl_expiry) {
                $ssl_expiry = Carbon::parse($monitor->ssl_expiry);
                $ssl_expiry_remaining[$monitor->uuid] = Carbon::parse($current_date)->diffInDays($ssl_expiry);
            } else {
                $ssl_expiry_remaining[$monitor->uuid] = null;
            }
        }
        $param = [
            'monitors' => $monitors,
            'current_date' => $current_date,
            'request' => $request,
            'ssl_expiry_remaining' => $ssl_expiry_remaining,
        ];

        return view('ssl-monitor.index', $param);
    }

    /*
     * Show the create monitor page
     */
    public function create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the available monitors of the current user
        $monitors = SslMonitor::where('user_id', $user->id)
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('ssl_monitor.index')
            ->with('error', __('You are not allowed to add monitor, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('ssl_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createSsl', [Monitor::class, $monitors])) {
            return to_route('ssl_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('ssl-monitor.create');
    }

    public function bulk_create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the available monitors of the current user
        $monitors = SslMonitor::where('user_id', $user->id)
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('ssl_monitor.index')
            ->with('error', __('You are not allowed to add monitor, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('ssl_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createSsl', [Monitor::class, $monitors])) {
            return to_route('ssl_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('ssl-monitor.bulk_create');
    }

    /*
     * create a monitor
     */
    public function store(Request $request)
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => 'required | unique:App\Models\SslMonitor,name,NULL,uuid,deleted_at,NULL',
                'url' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Ssl add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Get logged in user
            $user = auth()->user();

            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/SslMonitorService.php
             */
            $sslMonitorService = new SslMonitorService();
            //Add monitor using addMonitor() function in the SslMonitorService
            //case of successfull addition of monitor
            if ($sslMonitorService->addSsl($request, $user)) {
                return to_route('ssl_monitor.index')
                    ->with('success', __('Ssl added'));
            }

            return redirect()
            ->back()
            ->with('error', __('This ssl info is not available'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function bulk_store(Request $request)
    {
        try {
            if ($request->hasFile('csv_file')) {
                //Form validation
                $validator = Validator::make($request->all(), [
                    'csv_file' => 'required|mimes:csv,',
                ]);

                if ($validator->fails()) {
                    Logger::error('Ssl add form is invalid: '.json_encode($request->all()));

                    return redirect()
                        ->back()
                        ->withInput()
                        ->withErrors($validator);
                }

                $sslMonitorService = new SslMonitorService();

                $monitors = $sslMonitorService->csvConvert($request);
                if ($sslMonitorService->addBulkSsl($monitors)) {
                    return to_route('ssl_monitor.index')
                        ->with('success', __('Ssl list added'));
                }

                return to_route('ssl_monitor.index')
                    ->with('error', __('Limit Exceeded'));
            }
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __($e->getMessage()));
        }
    }

    /*
     * Show the monitor edit page
     */
    public function edit($uuid)
    {
        //Get the monitor
        $monitor = SslMonitor::find($uuid);
        $params = [
            'monitor' => $monitor,
        ];

        return view('ssl-monitor.edit', $params);
    }

    /*
     * Update a monitor
     */
    public function update(Request $request, $uuid)
    {
        try {
            $monitor = SslMonitor::find($uuid);
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => "required | unique:App\Models\SslMonitor,name,{$monitor->uuid},uuid,deleted_at,NULL",
                'url' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Ssl add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/SslMonitorService.php
             */
            $monitorService = new SslMonitorService();
            //Update monitor using updateMonitor() function in SslMonitorService
            //case of successfull updation of monitor

            if ($monitorService->updateSsl($request, $monitor)) {
                return to_route('ssl_monitor.index')
                    ->with('success', __('Ssl added'));
            }

            return redirect()
            ->back()
            ->with('error', __('This ssl info is not available'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', $e->getMessage());
        }
    }

    /*
     * Delete the monitor
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = SslMonitor::find($uuid);
        $monitor->delete();

        return to_route('ssl_monitor.index')
            ->with('success', __('Ssl deleted'));
    }
}
