<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Incident;
use App\Models\IncidentDetails;
use App\Models\Monitor;
use App\Models\Services\IncidentService;
use App\Models\User;
use App\Services\SubscriptionEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiIncidentController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the icidents of APIs
     */
    public function index(Request $request)
    {
        //Get logged in user
        $user = User::find(auth()->id());
        //Get all APIs of the user
        $apis = Monitor::where('user_id', $user->id)
            ->where('type', 'api')
            ->get();
        /*
         * Query all Api incidents corresponding to the user.
         * use hasMany ralation bcz user details only available in apis table
         */
        $query = Incident::query()->whereHas('monitors', function ($list) use ($user) {
            $list->where('user_id', $user->id)
            ->where('type', 'api');
        });

        /*
         * service class that interact with the Incident model.
         * refer getFilteredIncidents() function in app/Models/Services/IncidentService.php
         */
        $incidentService = new IncidentService();
        //Get filtered incidents using getFilteredIncidents function in IncidentService
        $incidents = $incidentService->getFilteredIncidents($user, $request, $query)
            ->paginate(10);
        $params = [
            'incidents' => $incidents,
            'apis' => $apis,
            'request' => $request,
        ];

        return view('incidents.api.index', $params);
    }

    /*
     * View create page of incidents
     */
    public function create()
    {
        //Get logged in user
        $user = User::find(auth()->id());
        /*
         * Check whether there is an API
         * refer app/Policies/IncidentPolicy.php
        */
        if (! $user->can('create', [Incident::class, 'api'])) {
            return to_route('api_monitor.create')
            ->with('error', __('Add an API first'));
        }

        //Get all available APIs of the user
        $apis = Monitor::where('user_id', $user->id)
            ->where('type', 'api')
            ->get();
        $params = [
            'apis' => $apis,
        ];

        return view('incidents.api.create', $params);
    }

    /*
     * create Api incidents.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'description' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Create an object for Incident
            $incident = new Incident();
            /*
             * service class that interact with the Incident model.
             * refer addIncident() function in app/Models/Services/IncidentService.php
            */
            $incidentService = new IncidentService();
            //Add an incident using addIncident() function
            $incident = $incidentService->addIncident($request, $incident);

            // Sent incident added mail to email subscription enabled statuspage users
            $emailSubscriptionService = new SubscriptionEmail();
            $emailSubscriptionService->addIncident($incident->monitors, $incident);

            return to_route('api_incident.index')
                ->with('success', __('Incident added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Show the incident update page.
     */
    public function edit($uuid)
    {
        //Get the incident
        $incident = Incident::find($uuid);
        $params = [
            'incident' => $incident,
        ];

        return view('incidents.api.edit', $params);
    }

    /*
     * Update Api incidents.
     */
    public function update(Request $request, $uuid): \Illuminate\Http\RedirectResponse
    {
        //Get the incident
        $incident = Incident::find($uuid);
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the Incident model.
             * refer updateIncident() function in app/Models/Services/IncidentService.php
             */
            $incidentService = new IncidentService();
            //update incident using updateIncident() function
            $incidentService->updateIncident($request, $incident);

            return to_route('api_incident.index')
                ->with('success', __('Incident updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Show the Incident follow up page.
     */
    public function show(Request $request, $uuid)
    {
        //Get the incident
        $incident = Incident::find($uuid);
        //Get the incident details
        $incidentDetails = IncidentDetails::where('incident_id', $uuid)
            ->latest('updated_at')
            ->paginate(10);
        $params = [
            'incidentDetails' => $incidentDetails,
            'incident' => $incident,
            'request' => $request,
        ];

        return view('incidents.api.show', $params);
    }

    /*
     * Update the incident details/follow ups.
     */
    public function updateIncidentDetails(Request $request, $uuid): \Illuminate\Http\RedirectResponse
    {
        //Get the incident
        $incident = Incident::find($uuid);
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'updated_description' => 'required',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Create an object for IncidentDetails
            $incidentDetails = new IncidentDetails();
            /*
             * service class that interact with the Incident model.
             * refer updateIncidentDetails() function in app/Models/Services/IncidentService.php
             */
            $incidentService = new IncidentService();
            //Update incident details using updateIncidentDetails() function
            $incidentDetails = $incidentService->updateIncidentDetails($request, $incident, $incidentDetails);

            // Sent incident updated or resolved mail to email subscription enabled statuspage users
            $emailSubscriptionService = new SubscriptionEmail();
            $monitor = $incident->monitors;
            if ($request->input('status') == 'resolved') {
                $emailSubscriptionService->resolveIncident($monitor, $incidentDetails);
            } else {
                $emailSubscriptionService->updateIncident($monitor, $incidentDetails);
            }

            return redirect()->back()
                ->with('success', __('Incident Details updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete an incident
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $incident = Incident::find($uuid);
        $incident->delete();

        return to_route('api_incident.index')
            ->with('success', __('Incident deleted'));
    }

    /*
     * Delete an incident follow up details
     */
    public function deleteDetails($uuid): \Illuminate\Http\RedirectResponse
    {
        $incidentDetails = IncidentDetails::find($uuid);
        $incidentId = $incidentDetails->incident_id;
        $incidentDetails->delete();

        return to_route('api_incident.show', $incidentId)
            ->with('success', __('Incident details deleted'));
    }
}
