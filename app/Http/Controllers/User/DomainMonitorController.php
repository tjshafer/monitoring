<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\DomainMonitor;
use App\Models\Services\DomainMonitorService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class DomainMonitorController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         * to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the available domains
     */
    public function index(Request $request)
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        $ssl_expiry_remaining = [];
        $domain_expiry_remaining = null;
        /*
         * service class that interact with the Monitor model.
         * refer app/Models/Services/DomainMonitorService.php
         */
        $domainMonitorService = new DomainMonitorService();
        $current_date = Carbon::now()->format('Y-m-d H:i:s');
        //Get filtered domains using getFilteredElements() function in  DomainMonitorService
        $domains = $domainMonitorService->getFilteredElements($user, $request)
            ->paginate(10);
        $domain_expiry_remaining = [];
        foreach ($domains as $domain) {
            $domain_expiry = Carbon::parse($domain->domain_expiry);
            $domain_expiry_remaining[$domain->uuid] = Carbon::parse($current_date)->diffInDays($domain_expiry);
        }
        $param = [
            'domains' => $domains,
            'current_date' => $current_date,
            'request' => $request,
            'domain_expiry_remaining' => $domain_expiry_remaining,
        ];

        return view('domain-monitor.index', $param);
    }

    /*
     * Show the create domain page
     */
    public function create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the available domains of the current user
        $domains = DomainMonitor::where('user_id', $user->id)
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('domain_monitor.index')
            ->with('error', __('You are not allowed to add domain, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('domain_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createDomain', [Monitor::class, $domains])) {
            return to_route('domain_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('domain-monitor.create');
    }

    public function bulk_create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the available domains of the current user
        $domains = DomainMonitor::where('user_id', $user->id)
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('domain_monitor.index')
            ->with('error', __('You are not allowed to add domain, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('domain_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createDomain', [Monitor::class, $domains])) {
            return to_route('domain_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('domain-monitor.bulk_create');
    }

    /*
     * create a domain
     */
    public function store(Request $request)
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => 'required | unique:App\Models\DomainMonitor,name,NULL,uuid,deleted_at,NULL',
                'url' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Domain add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Get logged in user
            $user = auth()->user();

            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/DomainMonitorService.php
             */
            $domainMonitorService = new DomainMonitorService();
            //Add domain using addMonitor() function in the DomainMonitorService
            //case of successfull addition of domain
            if ($domainMonitorService->addDomain($request, $user)) {
                return to_route('domain_monitor.index')
                    ->with('success', __('Domain added'));
            }

            return redirect()
            ->back()
            ->with('error', __('This domain info is not available'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function bulk_store(Request $request)
    {
        try {
            if ($request->hasFile('csv_file')) {
                //Form validation
                $validator = Validator::make($request->all(), [
                    'csv_file' => 'required|mimes:csv,',
                ]);

                if ($validator->fails()) {
                    Logger::error('Domain add form is invalid: '.json_encode($request->all()));

                    return redirect()
                        ->back()
                        ->withInput()
                        ->withErrors($validator);
                }

                $domainMonitorService = new DomainMonitorService();

                $domains = $domainMonitorService->csvConvert($request);
                if ($domainMonitorService->addBulkDomains($domains)) {
                    return to_route('domain_monitor.index')
                        ->with('success', __('Domains added'));
                }

                return to_route('domain_monitor.index')
                    ->with('error', __('Limit Exceeded'));
            }
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __($e->getMessage()));
        }
    }

    /*
     * Show the domain edit page
     */
    public function edit($uuid)
    {
        //Get the domain
        $domain = DomainMonitor::find($uuid);
        $params = [
            'domain' => $domain,
        ];

        return view('domain-monitor.edit', $params);
    }

    /*
     * Update a domain
     */
    public function update(Request $request, $uuid)
    {
        try {
            $domain = DomainMonitor::find($uuid);
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => "required | unique:App\Models\DomainMonitor,name,{$domain->uuid},uuid,deleted_at,NULL",
                'url' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Domain add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/DomainMonitorService.php
             */
            $domainService = new DomainMonitorService();
            //Update domain using updateMonitor() function in DomainMonitorService
            //case of successfull updation of domain

            if ($domainService->updateDomain($request, $domain)) {
                return to_route('domain_monitor.index')
                    ->with('success', __('Domain updated'));
            }

            return redirect()
            ->back()
            ->with('error', __('This domain info is not available'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete the domain
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = DomainMonitor::find($uuid);
        $monitor->delete();

        return to_route('domain_monitor.index')
            ->with('success', __('Domain deleted'));
    }
}
