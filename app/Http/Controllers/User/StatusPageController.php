<?php

namespace App\Http\Controllers\User;

use App\Helpers\AttachmentHelper;
use App\Helpers\Logger;
use App\Helpers\Uuid;
use App\Http\Controllers\Controller;
use App\Models\Monitor;
use App\Models\MonitorStatusPage;
use App\Models\Services\StatusPageService;
use App\Models\Setting;
use App\Models\StatusPage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StatusPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::find(auth()->id());
        $enableSubdomain = Setting::where('name', 'statuspage_with_subdomain')
            ->first();
        $statusPages = StatusPage::where('user_id', $user->id)
            ->paginate(10);

        $params = [
            'user' => $user,
            'statusPages' => $statusPages,
            'request' => $request,
            'enableSubdomain' => $enableSubdomain->value,
        ];

        return view('status_page.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::find(auth()->id());
        $statusPages = StatusPage::where('user_id', $user->id)
            ->count();
        $enableSubdomain = Setting::where('name', 'statuspage_with_subdomain')
            ->first();

        /*
         * refer app/Policies/StatuspagePolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', StatusPage::class)) {
            return to_route('status_page.index')
            ->with('error', __('You are not allowed to add statuspages, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', StatusPage::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', StatusPage::class)) {
            return to_route('status_page.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createStatuspage', [StatusPage::class, $statusPages])) {
            return to_route('status_page.index')
            ->with('error', __('Limit exceeded'));
        }

        $servers = Monitor::where('user_id', $user->id)
            ->where('type', 'server')->get();
        $webpages = Monitor::where('user_id', $user->id)
            ->where('type', 'webpage')->get();
        $apis = Monitor::where('user_id', $user->id)
            ->where('type', 'api')->get();
        // $selected_servers = $ticket->tags()->pluck('uuid')->toArray();
        $domain = config('settings.domain');
        $domain = str_replace('https://', '', $domain);
        $domain = str_replace('http://', '', $domain);

        $params = [
            'user' => $user,
            'servers' => $servers,
            'webpages' => $webpages,
            'apis' => $apis,
            'domain' => $domain,
            'enableSubdomain' => $enableSubdomain->value,
        ];

        return view('status_page.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            $enableSubdomain = Setting::where('name', 'statuspage_with_subdomain')
                ->first();
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => 'required | unique:App\Models\StatusPage,name',
                'description' => 'required',
            ]);

            if ($enableSubdomain->value == true) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required | unique:App\Models\StatusPage,name',
                    'description' => 'required',
                    'sub_domain' => 'required | unique:App\Models\StatusPage,sub_domain',
                ]);
            }

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            if (! $request->input('server_ids') && ! $request->input('webpage_ids') && ! $request->input('api_ids')) {
                return redirect()->back()
                    ->withInput()
                    ->with('error', __('Atleast one system should be selected'));
            }

            $fileName = null;
            if ($request->hasFile('logo')) {
                $file = $request->file('logo');
                $validator = Validator::make($request->all(), [
                    'logo' => 'required|mimes:png,svg,',
                ]);

                if ($validator->fails()) {
                    return redirect()
                    ->back()
                    ->withInput()
                    ->with('error', __('Invalid file format'));
                }

                // Set attachment name store to database
                $attachmentHelper = new AttachmentHelper();
                $fileName = $attachmentHelper->privateIconStore($file, 'status_page_logo');
            }

            $user = User::find(auth()->id());
            $servers = explode(',', $request->input('server_ids'));
            $webpages = explode(',', $request->input('webpage_ids'));
            $apis = explode(',', $request->input('api_ids'));

            $statusPageService = new StatusPageService;
            $status_page = $statusPageService->addPage($user, $request, $fileName);

            foreach ($servers as $server_id) {
                if ($server_id) {
                    $type = 'server';
                    $statusPageService->addData($server_id, $type, $status_page);
                }
            }

            foreach ($webpages as $webpage_id) {
                if ($webpage_id) {
                    $type = 'webpage';
                    $statusPageService->addData($webpage_id, $type, $status_page);
                }
            }

            foreach ($apis as $api_id) {
                if ($api_id) {
                    $type = 'api';
                    $statusPageService->addData($api_id, $type, $status_page);
                }
            }

            return to_route('status_page.index')
                ->with('success', __('Status Page added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __($e->getMessage()));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StatusPage  $statusPage
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $user = User::find(auth()->id());
        $status_page = StatusPage::find($uuid);
        $servers = Monitor::where('user_id', $user->id)
            ->where('type', 'server')->get();
        $webpages = Monitor::where('user_id', $user->id)
            ->where('type', 'webpage')->get();
        $apis = Monitor::where('user_id', $user->id)
            ->where('type', 'api')->get();
        $selected_servers = MonitorStatusPage::where('rel_id', $uuid)
            ->where('type', 'server')->pluck('monitor_id')->toArray();
        $selected_webpages = MonitorStatusPage::where('rel_id', $uuid)
            ->where('type', 'webpage')->pluck('monitor_id')->toArray();
        $selected_apis = MonitorStatusPage::where('rel_id', $uuid)
            ->where('type', 'api')->pluck('monitor_id')->toArray();

        $enableSubdomain = Setting::where('name', 'statuspage_with_subdomain')
            ->first();

        $domain = config('settings.domain');
        $domain = str_replace('https://', '', $domain);
        $domain = str_replace('http://', '', $domain);

        $params = [
            'servers' => $servers,
            'webpages' => $webpages,
            'apis' => $apis,
            'selected_servers' => $selected_servers,
            'selected_webpages' => $selected_webpages,
            'selected_apis' => $selected_apis,
            'domain' => $domain,
            'enableSubdomain' => $enableSubdomain->value,
        ];

        return view('status_page.edit', $status_page, $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\StatusPage  $statusPage
     */
    public function update(Request $request, $uuid): \Illuminate\Http\RedirectResponse
    {
        try {
            $enableSubdomain = Setting::where('name', 'statuspage_with_subdomain')
                ->first();
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => "required | unique:App\Models\StatusPage,name,{$uuid},uuid",
                'description' => 'required',
            ]);

            if ($enableSubdomain->value == true) {
                $validator = Validator::make($request->all(), [
                    'name' => "required | unique:App\Models\StatusPage,name,{$uuid},uuid",
                    'description' => 'required',
                    'sub_domain' => 'required | unique:App\Models\StatusPage,sub_domain',
                ]);
            }

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            if (! $request->input('server_ids') && ! $request->input('webpage_ids') && ! $request->input('api_ids')) {
                return redirect()->back()
                    ->withInput()
                    ->with('error', __('Atleast one system should be selected'));
            }

            $fileName = null;
            if ($request->hasFile('logo')) {
                $file = $request->file('logo');
                $validator = Validator::make($request->all(), [
                    'logo' => 'required|mimes:png,svg,',
                ]);

                if ($validator->fails()) {
                    return redirect()
                    ->back()
                    ->withInput()
                    ->with('error', __('Invalid file format'));
                }

                // Set attachment name store to database
                $attachmentHelper = new AttachmentHelper();
                $fileName = $attachmentHelper->privateIconStore($file, 'status_page_logo');
            }

            $user = User::find(auth()->id());

            $servers = explode(',', $request->input('server_ids'));
            $webpages = explode(',', $request->input('webpage_ids'));
            $apis = explode(',', $request->input('api_ids'));

            $statusPageService = new StatusPageService;
            $status_page = $statusPageService->updatePage($user, $request, $uuid, $fileName);

            $monitorStatusPages = MonitorStatusPage::where('rel_id', $uuid)
                ->get();
            foreach ($monitorStatusPages as $data) {
                $data->delete();
            }

            foreach ($servers as $server_id) {
                if ($server_id) {
                    $type = 'server';
                    $statusPageService->addData($server_id, $type, $status_page);
                }
            }

            foreach ($webpages as $webpage_id) {
                if ($webpage_id) {
                    $type = 'webpage';
                    $statusPageService->addData($webpage_id, $type, $status_page);
                }
            }

            foreach ($apis as $api_id) {
                if ($api_id) {
                    $type = 'api';
                    $statusPageService->addData($api_id, $type, $status_page);
                }
            }

            return to_route('status_page.index')
                ->with('success', __('Status page updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __($e->getMessage()));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StatusPage  $statusPage
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $status_page = StatusPage::find($uuid);
        $status_page->delete();

        return redirect()->back()
            ->with('success', __('Status page deleted'));
    }

    public function getServersApi(Request $request)
    {
        $user = User::find(auth()->id());
        $servers = Monitor::where('user_id', $user->id)
            ->where('type', 'server')->get();

        return response($servers);
    }

    public function getWebpagesApi(Request $request)
    {
        $user = User::find(auth()->id());
        $webpages = Monitor::where('user_id', $user->id)
            ->where('type', 'webpage')->get();

        return response($webpages);
    }

    public function getApis(Request $request)
    {
        $user = User::find(auth()->id());
        $apis = Monitor::where('user_id', $user->id)
            ->where('type', 'api')->get();

        return response($apis);
    }
}
