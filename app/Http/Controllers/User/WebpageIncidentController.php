<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Incident;
use App\Models\IncidentDetails;
use App\Models\Monitor;
use App\Models\Services\IncidentService;
use App\Models\User;
use App\Services\SubscriptionEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WebpageIncidentController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the icidents of webpages
     */
    public function index(Request $request)
    {
        //Get logged in user
        $user = User::find(auth()->id());
        //Get all webpages of the user
        $pages = Monitor::where('user_id', $user->id)
            ->where('type', 'webpage')
            ->get();

        /*
         * Query all webpage incidents corresponding to the user.
         * use hasMany ralation bcz user details only available in webpages table
         */
        $query = Incident::query()->whereHas('monitors', function ($list) use ($user) {
            $list->where('user_id', $user->id)
                ->where('type', 'webpage');
        });

        /*
         * service class that interact with the Incident model.
         * refer getFilteredIncidents() function in app/Models/Services/IncidentService.php
         */
        $incidentService = new IncidentService();
        //Get filtered incidents using getFilteredIncidents function in IncidentService
        $incidents = $incidentService->getFilteredIncidents($user, $request, $query)
            ->paginate(10);
        $params = [
            'incidents' => $incidents,
            'pages' => $pages,
            'request' => $request,
        ];

        return view('incidents.webpage.index', $params);
    }

    /*
     * View create page of incidents
     */
    public function create()
    {
        //Get logged in user
        $user = User::find(auth()->id());
        /*
         * Check whether there is a webpage or not
         * refer app/Policies/IncidentPolicy.php
        */
        if (! $user->can('create', [Incident::class, 'webpage'])) {
            return to_route('webpage_monitor.create')
            ->with('error', __('Add a webpage first'));
        }

        //Get all available webpages of the user
        $pages = Monitor::where('user_id', $user->id)
            ->where('type', 'webpage')
            ->get();
        $params = [
            'pages' => $pages,
        ];

        return view('incidents.webpage.create', $params);
    }

    /*
     * create webpage incidents.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'description' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Create an object for Incident
            $incident = new Incident();

            /*
             * service class that interact with the Incident model.
             * refer addIncident() function in app/Models/Services/IncidentService.php
            */
            $incidentService = new IncidentService();
            //Add an incident using addIncident() function
            $incident = $incidentService->addIncident($request, $incident);

            // Sent incident added mail to email subscription enabled statuspage users
            $emailSubscriptionService = new SubscriptionEmail();
            $emailSubscriptionService->addIncident($incident->monitors, $incident);

            return to_route('webpage_incident.index')
                ->with('success', __('Incident added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Show the incident update page.
     */
    public function edit($uuid)
    {
        //Get the incident
        $incident = Incident::find($uuid);
        $params = [
            'incident' => $incident,
        ];

        return view('incidents.webpage.edit', $params);
    }

    /*
     * Update webpage incidents.
     */
    public function update(Request $request, $uuid): \Illuminate\Http\RedirectResponse
    {
        //Get the incident
        $incident = Incident::find($uuid);
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the Incident model.
             * refer updateIncident() function in app/Models/Services/IncidentService.php
             */
            $incidentService = new IncidentService();
            //update incident using updateIncident() function
            $incidentService->updateIncident($request, $incident);

            return to_route('webpage_incident.index')
                ->with('success', __('Incident updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Show the Incident follow up page.
     */
    public function show(Request $request, $uuid)
    {
        //Get the incident
        $incident = Incident::find($uuid);
        //Get the incident details
        $incidentDetails = IncidentDetails::where('incident_id', $uuid)
            ->latest('updated_at')
            ->paginate(10);
        $params = [
            'incidentDetails' => $incidentDetails,
            'incident' => $incident,
            'request' => $request,
        ];

        return view('incidents.webpage.show', $params);
    }

    /*
     * Update the incident details/follow ups.
     */
    public function updateIncidentDetails(Request $request, $uuid): \Illuminate\Http\RedirectResponse
    {
        //Get the incident
        $incident = Incident::find($uuid);
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'updated_description' => 'required',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Create an object for IncidentDetails
            $incidentDetails = new IncidentDetails();

            /*
             * service class that interact with the Incident model.
             * refer updateIncidentDetails() function in app/Models/Services/IncidentService.php
             */
            $incidentService = new IncidentService();
            //Update incident details using updateIncidentDetails() function
            $incidentDetails = $incidentService->updateIncidentDetails($request, $incident, $incidentDetails);

            // Sent incident updated or resolved mail to email subscription enabled statuspage users
            $emailSubscriptionService = new SubscriptionEmail();
            $monitor = $incident->monitors;
            if ($request->input('status') == 'resolved') {
                $emailSubscriptionService->resolveIncident($monitor, $incidentDetails);
            } else {
                $emailSubscriptionService->updateIncident($monitor, $incidentDetails);
            }

            return redirect()->back()
                ->with('success', __('Incident Details updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete an incident
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $incident = Incident::find($uuid);
        $incident->delete();

        return to_route('webpage_incident.index')
            ->with('success', __('Incident deleted'));
    }

    /*
     * Delete an incident follow up details
     */
    public function deleteDetails($uuid): \Illuminate\Http\RedirectResponse
    {
        $incidentDetails = IncidentDetails::find($uuid);
        $incidentId = $incidentDetails->incident_id;
        $incidentDetails->delete();

        return to_route('webpage_incident.show', $incidentId)
            ->with('success', __('Incident details deleted'));
    }
}
