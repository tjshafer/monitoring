<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Incident;
use App\Models\Monitor;
use App\Models\MonitorWebsite;
use App\Models\Order;
use App\Models\Service;
use App\Models\User;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the count of servers of current user
        $servers = Monitor::where('user_id', $user->id)
            ->where('type', 'server')->count();
        //Get the count of up servers of current user
        $upServer = Monitor::where('user_id', $user->id)
            ->where('previous_status', 'up')
            ->where('type', 'server')->count();
        //Get the count of down servers of current user
        $downServer = Monitor::where('user_id', $user->id)
            ->where('previous_status', 'down')
            ->where('type', 'server')->count();

        //Get the count of webpages of current user
        $pages = Monitor::where('user_id', $user->id)
            ->where('type', 'webpage')->count();
        //Get the count of up webpages of current user
        $upPage = Monitor::where('user_id', $user->id)
            ->where('previous_status', 'up')
            ->where('type', 'webpage')->count();
        //Get the count of down webpages of current user
        $downPage = Monitor::where('user_id', $user->id)
            ->where('previous_status', 'down')
            ->where('type', 'webpage')->count();

        //Get the count of APIs of current user
        $apis = Monitor::where('user_id', $user->id)
            ->where('type', 'api')->count();
        //Get the count of up APIs of current user
        $upApi = Monitor::where('user_id', $user->id)
            ->where('previous_status', 'up')
            ->where('type', 'api')->count();
        //Get the count of down APIs of current user
        $downApi = Monitor::where('user_id', $user->id)
            ->where('previous_status', 'down')
            ->where('type', 'api')->count();

        //Get the count of Websites of current user
        $websites = MonitorWebsite::where('user_id', $user->id)
            ->count();
        $currentDate = Carbon::now()->format('Y-m-d H:i:s');
        //Get the count of domain expired websites of current user
        $domainExpired = MonitorWebsite::where('user_id', $user->id)
            ->where('domain_expiry', '<', $currentDate)
            ->count();
        //Get the count of ssl expired websites of current user
        $sslExpired = MonitorWebsite::where('user_id', $user->id)
            ->where('ssl_expiry', '<', $currentDate)
            ->count();

        /*
         * Get the subscribed plan details
        */
        $service = Service::where('user_id', $user->id)->latest()->first();
        /*
         * Get all server incidents corresponding to the user.
         * use hasMany ralation bcz user details only available in servers table
        */
        $serverIncidents = Incident::whereHas('monitors', function ($query) use ($user) {
            $query->where('user_id', $user->id)
            ->where('type', 'server');
        })->get();
        //Get the count of server incidents of current user
        $serverIncidentsCount = $serverIncidents->count();
        //Get the count of pending server incidents of current user
        $pendingServerIncidents = $serverIncidents->where('status', false)->count();
        //Get the count of resolved server incidents of current user
        $resolvedServerIncidents = $serverIncidents->where('status', true)->count();

        /*
         * Get all webpage incidents corresponding to the user.
         * use hasMany ralation bcz user details only available in webpages table
         */
        $webpageIncidents = Incident::whereHas('monitors', function ($query) use ($user) {
            $query->where('user_id', $user->id)
            ->where('type', 'webpage');
        })->get();
        //Get the count of webpage incidents of current user
        $webpageIncidentsCount = $webpageIncidents->count();
        //Get the count of pending webpage incidents of current user
        $pendingWebpageIncidents = $webpageIncidents->where('status', false)->count();
        //Get the count of resolved webpage incidents of current user
        $resolvedWebpageIncidents = $webpageIncidents->where('status', true)->count();

        /*
         * Get all Api incidents corresponding to the user.
         * use hasMany ralation bcz user details only available in apis table
         */
        $apiIncidents = Incident::whereHas('monitors', function ($query) use ($user) {
            $query->where('user_id', $user->id)
            ->where('type', 'api');
        })->get();
        //Get the count of api incidents of current user
        $apiIncidentsCount = $apiIncidents->count();
        //Get the count of pending webpage incidents of current user
        $pendingApiIncidents = $apiIncidents->where('status', false)->count();
        //Get the count of resolved webpage incidents of current user
        $resolvedApiIncidents = $apiIncidents->where('status', true)->count();

        $latestOrder = Order::where('user_id', $user->id)->latest()->first();

        $params = [
            'servers' => $servers,
            'pages' => $pages,
            'apis' => $apis,
            'websites' => $websites,
            'upServer' => $upServer,
            'downServer' => $downServer,
            'upPage' => $upPage,
            'downPage' => $downPage,
            'upApi' => $upApi,
            'downApi' => $downApi,
            'domainExpired' => $domainExpired,
            'sslExpired' => $sslExpired,
            'serverIncidentsCount' => $serverIncidentsCount,
            'resolvedServerIncidents' => $resolvedServerIncidents,
            'pendingServerIncidents' => $pendingServerIncidents,
            'webpageIncidentsCount' => $webpageIncidentsCount,
            'pendingWebpageIncidents' => $pendingWebpageIncidents,
            'resolvedWebpageIncidents' => $resolvedWebpageIncidents,
            'apiIncidentsCount' => $apiIncidentsCount,
            'pendingApiIncidents' => $pendingApiIncidents,
            'resolvedApiIncidents' => $resolvedApiIncidents,
            'service' => $service,
            'latestOrder' => $latestOrder,
        ];

        return view('dashboard.user', $params);
    }
}
