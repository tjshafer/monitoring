<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\MonitorWebsite;
use App\Models\Services\MonitorWebsiteService;
use App\Models\Services\PublicPageService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class MonitorWebsiteController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         * to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the available websites
     */
    public function index(Request $request)
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        $ssl_expiry_remaining = [];
        $domain_expiry_remaining = null;
        /*
         * service class that interact with the Monitor model.
         * refer app/Models/Services/MonitorWebsiteService.php
         */
        $monitorWebsiteService = new MonitorWebsiteService();
        $current_date = Carbon::now()->format('Y-m-d H:i:s');
        //Get filtered websites using getFilteredElements() function in  MonitorWebsiteService
        $websites = $monitorWebsiteService->getFilteredElements($user, $request)
            ->paginate(10);
        $ssl_expiry_remaining = [];
        $domain_expiry_remaining = [];
        foreach ($websites as $website) {
            if ($website->ssl_expiry) {
                $ssl_expiry = Carbon::parse($website->ssl_expiry);
                $ssl_expiry_remaining[$website->uuid] = Carbon::parse($current_date)->diffInDays($ssl_expiry);
            } else {
                $ssl_expiry_remaining[$website->uuid] = null;
            }

            $domain_expiry = Carbon::parse($website->domain_expiry);
            $domain_expiry_remaining[$website->uuid] = Carbon::parse($current_date)->diffInDays($domain_expiry);
        }
        $param = [
            'websites' => $websites,
            'current_date' => $current_date,
            'request' => $request,
            'ssl_expiry_remaining' => $ssl_expiry_remaining,
            'domain_expiry_remaining' => $domain_expiry_remaining,
        ];

        return view('website-monitor.index', $param);
    }

    /*
     * Show the create website page
     */
    public function create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the available websites of the current user
        $websites = MonitorWebsite::where('user_id', $user->id)
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('website_monitor.index')
            ->with('error', __('You are not allowed to add website, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('website_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createWebsite', [Monitor::class, $websites])) {
            return to_route('website_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('website-monitor.create');
    }

    public function bulk_create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the available websites of the current user
        $websites = MonitorWebsite::where('user_id', $user->id)
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('website_monitor.index')
            ->with('error', __('You are not allowed to add website, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('website_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createWebsite', [Monitor::class, $websites])) {
            return to_route('website_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('website-monitor.bulk_create');
    }

    /*
     * create a website
     */
    public function store(Request $request)
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => 'required | unique:App\Models\MonitorWebsite,name,NULL,uuid,deleted_at,NULL',
                'url' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Website add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Get logged in user
            $user = auth()->user();

            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/MonitorWebsiteService.php
             */
            $monitorWebsiteService = new MonitorWebsiteService();
            //Add website using addMonitor() function in the MonitorWebsiteService
            //case of successfull addition of website
            if ($monitorWebsiteService->addWebsite($request, $user)) {
                return to_route('website_monitor.index')
                    ->with('success', __('Website added'));
            }

            return redirect()
            ->back()
            ->with('error', __('This domain info is not available'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function bulk_store(Request $request)
    {
        try {
            if ($request->hasFile('csv_file')) {
                //Form validation
                $validator = Validator::make($request->all(), [
                    'csv_file' => 'required|mimes:csv,',
                ]);

                if ($validator->fails()) {
                    Logger::error('Server add form is invalid: '.json_encode($request->all()));

                    return redirect()
                        ->back()
                        ->withInput()
                        ->withErrors($validator);
                }

                $monitorWebsiteService = new MonitorWebsiteService();

                $websites = $monitorWebsiteService->csvConvert($request);
                if ($monitorWebsiteService->addBulkWebsites($websites)) {
                    return to_route('website_monitor.index')
                        ->with('success', __('Websites added'));
                }

                return to_route('website_monitor.index')
                    ->with('error', __('Limit Exceeded'));
            }
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __($e->getMessage()));
        }
    }

    /**
     * Display the details
     */
    public function show($uuid)
    {
        //Get the website
        $website = MonitorWebsite::find($uuid);

        /*
         * service class that interact with the public pages.
         * refer app/Models/Services/PublicPageService.php
         */
        $websiteDetails = new PublicPageService();
        //Get the website details details using showPage() function in PublicPageService
        $response = $websiteDetails->showPage($logs);

        $params = [
            'upTime' => $response['upTime'],
            'downTime' => $response['downTime'],
            'avgResponseTime' => $response['avgResponseTime'],
            'page' => $website,
            'lastChecked' => $lastChecked,
            'lastLogs' => $lastLogs,
        ];

        return view('website-monitor.view', $params);
    }

    /*
     * Show the website edit page
     */
    public function edit($uuid)
    {
        //Get the website
        $website = MonitorWebsite::find($uuid);
        $params = [
            'website' => $website,
        ];

        return view('website-monitor.edit', $params);
    }

    /*
     * Update a website
     */
    public function update(Request $request, $uuid)
    {
        try {
            $website = MonitorWebsite::find($uuid);
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => "required | unique:App\Models\MonitorWebsite,name,{$website->uuid},uuid,deleted_at,NULL",
                'url' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Website add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/MonitorWebsiteService.php
             */
            $websiteService = new MonitorWebsiteService();
            //Update website using updateMonitor() function in MonitorWebsiteService
            //case of successfull updation of website

            if ($websiteService->updateWebsite($request, $website)) {
                return to_route('website_monitor.index')
                    ->with('success', __('Website added'));
            }

            return redirect()
            ->back()
            ->with('error', __('This domain info is not available'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete the website
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = MonitorWebsite::find($uuid);
        $monitor->delete();

        return to_route('website_monitor.index')
            ->with('success', __('Website deleted'));
    }
}
