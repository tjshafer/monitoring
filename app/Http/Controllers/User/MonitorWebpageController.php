<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Interval;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\Services\MonitorService;
use App\Models\Services\PublicPageService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MonitorWebpageController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         * to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the available webpages
     */
    public function index(Request $request)
    {
        //Get the logged in user
        $user = User::find(auth()->id());

        /*
         * service class that interact with the Monitor model.
         * refer app/Models/Services/MonitorService.php
         */
        $monitorService = new MonitorService();
        $type = 'webpage';
        //Get filtered webpages using getFilteredElements() function in  MonitorService
        $pages = $monitorService->getFilteredElements($user, $request, $type)
            ->paginate(10);

        $param = [
            'pages' => $pages,
            'request' => $request,
        ];

        return view('webpage-monitor.index', $param);
    }

    /*
     * Show the create webpage page
     */
    public function create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the available webpages of the current user
        $pages = Monitor::where('user_id', $user->id)
            ->where('type', 'webpage')
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('webpage_monitor.index')
            ->with('error', __('You are not allowed to add webpage, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('webpage_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createWebpage', [Monitor::class, $pages])) {
            return to_route('webpage_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        //Get all avilable Intervals
        $intervals = Interval::all();
        $params = [
            'intervals' => $intervals,
        ];

        return view('webpage-monitor.create', $params);
    }

    public function bulk_create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the available webpages of the current user
        $pages = Monitor::where('user_id', $user->id)
            ->where('type', 'webpage')
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('webpage_monitor.index')
            ->with('error', __('You are not allowed to add webpage, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('webpage_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createWebpage', [Monitor::class, $pages])) {
            return to_route('webpage_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('webpage-monitor.bulk_create');
    }

    /*
     * create a webpage
     */
    public function store(Request $request)
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => 'required | unique:App\Models\Monitor,name,NULL,uuid,deleted_at,NULL',
                'url' => 'required',
                'expected_response_code' => 'required|numeric',
                'interval' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Webpage add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Get logged in user
            $user = auth()->user();
            $type = 'webpage';

            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/MonitorService.php
             */
            $monitorService = new MonitorService();
            //Add webpage using addMonitor() function in the MonitorService
            //case of successfull addition of webpage
            if ($monitorService->addMonitor($request, $user, $type)) {
                return to_route('webpage_monitor.index')
                ->with('success', __('Webpage added'));
            }
            //case were the webpage is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Webpage is not up', [
                            'page' => $request->input('name'),
                            'response_code' => $request->input('expected_response_code'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function bulk_store(Request $request)
    {
        try {
            if ($request->hasFile('csv_file')) {
                //Form validation
                $validator = Validator::make($request->all(), [
                    'csv_file' => 'required|mimes:csv,',
                ]);

                if ($validator->fails()) {
                    Logger::error('Webpage add form is invalid: '.json_encode($request->all()));

                    return redirect()
                        ->back()
                        ->withInput()
                        ->withErrors($validator);
                }

                $monitorService = new MonitorService();

                $monitors = $monitorService->csvConvert($request);

                $type = 'webpage';
                if ($monitorService->addBulkMonitor($monitors, $type)) {
                    return to_route('webpage_monitor.index')
                        ->with('success', __('Webpagess added'));
                }

                return to_route('webpage_monitor.index')
                    ->with('error', __('Limit Exceeded'));
            }
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /**
     * Display the details
     */
    public function show($uuid)
    {
        //Get the webpage
        $page = Monitor::find($uuid);
        //Get the logs of the current webpage
        $logs = $logs = MonitorLog::where('rel_id', $uuid)->get();
        //Get the last 5 logs of the webpage
        $lastLogs = MonitorLog::where('rel_id', $uuid)->latest()->take(5)->get();
        //Get the latest log
        $lastLog = MonitorLog::where('rel_id', $uuid)->latest()->first();
        //Get last checked time
        if ($lastLog) {
            $lastChecked = $lastLog->created_at;
        } else {
            $lastChecked = null;
        }

        /*
         * service class that interact with the public pages.
         * refer app/Models/Services/PublicPageService.php
         */
        $webpageDetails = new PublicPageService();
        //Get the webpage details details using showPage() function in PublicPageService
        $response = $webpageDetails->showPage($logs);

        $params = [
            'upTime' => $response['upTime'],
            'downTime' => $response['downTime'],
            'avgResponseTime' => $response['avgResponseTime'],
            'page' => $page,
            'lastChecked' => $lastChecked,
            'lastLogs' => $lastLogs,
        ];

        return view('webpage-monitor.view', $params);
    }

    /*
     * Show the webpage edit page
     */
    public function edit($uuid)
    {
        //Get the webpage
        $webpage = Monitor::find($uuid);
        //GEt all available intervals
        $intervals = Interval::all();
        $params = [
            'webpage' => $webpage,
            'intervals' => $intervals,
        ];

        return view('webpage-monitor.edit', $params);
    }

    /*
     * Update a webpage
     */
    public function update(Request $request, $uuid)
    {
        try {
            $webpage = Monitor::find($uuid);
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => "required | unique:App\Models\Monitor,name,{$webpage->uuid},uuid,deleted_at,NULL",
                'url' => 'required',
                'expected_response_code' => 'required|numeric',
                'interval' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Webpage add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $type = 'webpage';
            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/MonitorService.php
             */
            $webpageService = new MonitorService();
            //Update webpage using updateMonitor() function in MonitorService
            //case of successfull updation of webpage
            if ($webpageService->updateMonitor($request, $webpage, $type)) {
                return to_route('webpage_monitor.index')
                ->with('success', __('Webpage updated'));
            }
            //case were the webpage is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Webpage is not up', [
                            'page' => $request->input('name'),
                            'response_code' => $request->input('expected_response_code'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete the webpage
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = Monitor::find($uuid);
        $monitor->delete();

        return to_route('webpage_monitor.index')
            ->with('success', __('Webpage deleted'));
    }
}
