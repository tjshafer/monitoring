<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Helpers\Uuid;
use App\Http\Controllers\Controller;
use App\Models\Addon;
use App\Models\ClientSettings;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AlertManagerController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access

         */
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        $emails = ClientSettings::where('user_id', $user->id)->where('key', 'alert_emails')->first();
        $numbers = ClientSettings::where('user_id', $user->id)->where('key', 'alert_sms_numbers')->first();
        $twilio = Addon::where('name', 'Twilio SMS')
                        ->where('status', 1)->get();

        $params = [
            'emails' => $emails,
            'numbers' => $numbers,
            'twilio' => $twilio,
        ];

        return view('alert.index', $params);
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            // Form validation
            $validator = Validator::make($request->all(), [
                'emails' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }
            //Get the logged in user
            $user = User::find(auth()->id());
            $updateEmailSettings = ClientSettings::where('key', 'alert_emails')
                                ->where('user_id', $user->id)->first();
            $updateSmsSettings = ClientSettings::where('key', 'alert_sms_numbers')
                                ->where('user_id', $user->id)->first();
            if ($updateEmailSettings) {
                $emailSettings = $updateEmailSettings;
            } else {
                $emailSettings = new ClientSettings();
            }
            $emailSettings->uuid = Uuid::getUuid();
            $emailSettings->key = 'alert_emails';
            $emailSettings->value = $request->input('emails');
            $emailSettings->user_id = $user->id;
            $emailSettings->save();

            if ($request->input('numbers')) {
                if ($updateSmsSettings) {
                    $smsSettings = $updateSmsSettings;
                } else {
                    $smsSettings = new ClientSettings();
                }

                $smsSettings->uuid = Uuid::getUuid();
                $smsSettings->key = 'alert_sms_numbers';
                $smsSettings->value = $request->input('numbers');
                $smsSettings->user_id = $user->id;
                $smsSettings->save();
            }

            return to_route('alert_manager.index')
                ->with('success', __('Settings Updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
            ->back()
            ->withInput()
            ->with('error', __($e->getMessage()));
        }
    }
}
