<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Interval;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\Services\MonitorService;
use App\Models\Services\PublicPageService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MonitorServerController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         * to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the available servers
     */
    public function index(Request $request)
    {
        //Get the logged in user
        $user = User::find(auth()->id());

        /*
         * service class that interact with the Monitor model.
         * refer app/Models/Services/MonitorService.php
         */
        $monitorService = new MonitorService();
        $type = 'server';
        //Get filtered servers using getFilteredElements() function in  MonitorService
        $servers = $monitorService->getFilteredElements($user, $request, $type)
            ->paginate(10);

        $param = [
            'servers' => $servers,
            'request' => $request,
        ];

        return view('server-monitor.index', $param);
    }

    /*
     * Shows create server page
     */
    public function create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the count of servers of the current user
        $servers = Monitor::where('user_id', $user->id)
            ->where('type', 'server')
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorServerPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('server_monitor.index')
            ->with('error', __('You are not allowed to add server, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('server_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createServer', [Monitor::class, $servers])) {
            return to_route('server_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        //Get all avilable Intervals
        $intervals = Interval::all();
        $params = [
            'intervals' => $intervals,
        ];

        return view('server-monitor.create', $params);
    }

    public function bulk_create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the count of servers of the current user
        $servers = Monitor::where('user_id', $user->id)
            ->where('type', 'server')
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorServerPolicy.php
         */

        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('server_monitor.index')
            ->with('error', __('You are not allowed to add server, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('server_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createServer', [Monitor::class, $servers])) {
            return to_route('server_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('server-monitor.bulk_create');
    }

    /*
     * List the available servers
     */
    public function store(Request $request)
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'url' => 'required',
                'name' => 'required | unique:App\Models\Monitor,name,NULL,uuid,deleted_at,NULL',
                'port' => 'required|numeric',
                'interval' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Server add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Get logged in user
            $user = auth()->user();
            $type = 'server';
            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/MonitorService.php
             */
            $monitorService = new MonitorService();
            //Add Server using addServer() function in the MonitorService
            //case of successfull addition of server
            if ($monitorService->addMonitor($request, $user, $type)) {
                return to_route('server_monitor.index')
                ->with('success', __('Server added'));
            }
            //case were the server is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Server is not up', [
                            'server' => $request->input('url'),
                            'port' => $request->input('port'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function bulk_store(Request $request)
    {
        try {
            if ($request->hasFile('csv_file')) {
                //Form validation
                $validator = Validator::make($request->all(), [
                    'csv_file' => 'required|mimes:csv,',
                ]);

                if ($validator->fails()) {
                    Logger::error('Server add form is invalid: '.json_encode($request->all()));

                    return redirect()
                        ->back()
                        ->withInput()
                        ->withErrors($validator);
                }

                $monitorService = new MonitorService();

                $monitors = $monitorService->csvConvert($request);

                $type = 'server';
                if ($monitorService->addBulkMonitor($monitors, $type)) {
                    return to_route('server_monitor.index')
                        ->with('success', __('Servers added'));
                }

                return to_route('server_monitor.index')
                    ->with('error', __('Limit Exceeded'));
            }
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __($e->getMessage()));
        }
    }

    /**
     * Display the details
     */
    public function show(Request $request, $uuid)
    {
        //Get the server
        $server = Monitor::find($uuid);
        //Get the logs of the current server
        $logs = MonitorLog::where('rel_id', $uuid)->get();
        //Get the last 5 logs of the server
        $lastLogs = MonitorLog::where('rel_id', $uuid)->latest()->take(5)->get();
        //Get the latest log
        $lastLog = MonitorLog::where('rel_id', $uuid)->latest()->first();
        //Get last checked time
        if ($lastLog) {
            $lastChecked = $lastLog->created_at;
        } else {
            $lastChecked = null;
        }
        /*
         * service class that interact with the public pages.
         * refer app/Models/Services/PublicPageService.php
         */
        $serverDetails = new PublicPageService();
        //Get the server details details using showPage() function in PublicPageService
        $response = $serverDetails->showPage($logs);

        $params = [
            'upTime' => $response['upTime'],
            'downTime' => $response['downTime'],
            'avgResponseTime' => $response['avgResponseTime'],
            'server' => $server,
            'lastChecked' => $lastChecked,
            'lastLogs' => $lastLogs,
        ];

        return view('server-monitor.view', $params);
    }

    /*
     * Show the server edit page
     */
    public function edit(Request $request, $uuid)
    {
        //Get the server
        $server = Monitor::find($uuid);
        //GEt all available intervals
        $intervals = Interval::all();
        $params = [
            'server' => $server,
            'intervals' => $intervals,
        ];

        return view('server-monitor.edit', $params);
    }

    /*
     * Update a server
     */
    public function update(Request $request, $uuid)
    {
        try {
            $server = Monitor::find($uuid);
            //Form validation
            $validator = Validator::make($request->all(), [
                'url' => 'required',
                'name' => "required | unique:App\Models\Monitor,name,{$server->uuid},uuid,deleted_at,NULL",
                'port' => 'required|numeric',
                'interval' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Server update form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $type = 'server';
            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/MonitorService.php
             */
            $monitorService = new MonitorService();
            //Update server using updateServer() function in MonitorService
            //case of successfull updation of server
            if ($monitorService->updateMonitor($request, $server, $type)) {
                return to_route('server_monitor.index')
                ->with('success', __('Server updated'));
            }
            //case were the server is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Server is not up', [
                            'server' => $request->input('url'),
                            'port' => $request->input('port'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete the server
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = Monitor::find($uuid);
        $monitor->delete();

        return to_route('server_monitor.index')
            ->with('success', __('Server deleted'));
    }
}
