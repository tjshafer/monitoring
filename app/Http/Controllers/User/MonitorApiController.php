<?php

namespace App\Http\Controllers\User;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Interval;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\Services\MonitorService;
use App\Models\Services\PublicPageService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MonitorApiController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         * to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    /*
     * List the available APIs
     */
    public function index(Request $request)
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        $type = 'api';

        /*
         * service class that interact with the MonitorApi model.
         * refer app/Models/Services/MonitorService.php
         */
        $apiService = new MonitorService();
        //Get filtered apis using getFilteredElements() function in  MonitorService
        $apis = $apiService->getFilteredElements($user, $request, $type)
            ->paginate(10);

        $param = [
            'apis' => $apis,
            'request' => $request,
        ];

        return view('api-monitor.index', $param);
    }

    /*
     * Shows create API page
     */
    public function create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the count of APIs of the current user
        $apis = Monitor::where('user_id', $user->id)
        ->where('type', 'api')
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorApiPolicy.php
         */
        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('api_monitor.index')
            ->with('error', __('You are not allowed to add API, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('api_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait for sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createApi', [Monitor::class, $apis])) {
            return to_route('api_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        //Get all avilable Intervals
        $intervals = Interval::all();
        $params = [
            'intervals' => $intervals,
        ];

        return view('api-monitor.create', $params);
    }

    public function bulk_create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the count of APIs of the current user
        $apis = Monitor::where('user_id', $user->id)
        ->where('type', 'api')
            ->whereNull('deleted_at')
            ->count();

        /*
         * refer app/Policies/MonitorApiPolicy.php
         */
        //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('api_monitor.index')
            ->with('error', __('You are not allowed to add API, please contact support'));
        }

        //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('api_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait for sometime or Contact support'));
        }

        //Check whether the user's adding limit exceeded or not
        if (! $user->can('createApi', [Monitor::class, $apis])) {
            return to_route('api_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        return view('api-monitor.bulk_create');
    }

    /*
     * List the available APIs
     */
    public function store(Request $request)
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => 'required | unique:App\Models\Monitor,name,NULL,uuid,deleted_at,NULL',
                'url' => 'required',
                'expected_response_code' => 'required|numeric',
                'interval' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('Api add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Get logged in user
            $user = auth()->user();
            $type = 'api';

            /*
             * service class that interact with the MonitorApi model.
             * refer app/Models/Services/MonitorService.php
             */
            $apiService = new MonitorService();
            //Add API using addMonitor() function in the MonitorService
            //case of successfull addition of API
            if ($apiService->addMonitor($request, $user, $type)) {
                return to_route('api_monitor.index')
                ->with('success', __('Api added'));
            }
            //case were the API is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('API is not up', [
                            'api' => $request->input('name'),
                            'expected_response_code' => $request->input('expected_response_code'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', $e->getMessage());
        }
    }

    public function bulk_store(Request $request)
    {
        try {
            if ($request->hasFile('csv_file')) {
                //Form validation
                $validator = Validator::make($request->all(), [
                    'csv_file' => 'required|mimes:csv,',
                ]);

                if ($validator->fails()) {
                    Logger::error('API add form is invalid: '.json_encode($request->all()));

                    return redirect()
                        ->back()
                        ->withInput()
                        ->withErrors($validator);
                }

                $monitorService = new MonitorService();

                $monitors = $monitorService->csvConvert($request);

                $type = 'api';
                if ($monitorService->addBulkMonitor($monitors, $type)) {
                    return to_route('api_monitor.index')
                        ->with('success', __('APIs added'));
                }

                return to_route('api_monitor.index')
                    ->with('error', __('Limit Exceeded'));
            }
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /**
     * Display the details
     */
    public function show(Request $request, $uuid)
    {
        //Get the API
        $api = Monitor::find($uuid);
        //Get the logs of the current API
        $logs = $logs = MonitorLog::where('rel_id', $uuid)->get();
        //Get the last 5 logs of the API
        $lastLogs = MonitorLog::where('rel_id', $uuid)->latest()->take(5)->get();
        //Get the latest log
        $lastLog = MonitorLog::where('rel_id', $uuid)->latest()->first();
        //Get last checked time
        if ($lastLog) {
            $lastChecked = $lastLog->created_at;
        } else {
            $lastChecked = null;
        }

        /*
         * service class that interact with the public pages.
         * refer app/Models/Services/PublicPageService.php
         */
        $apiDetails = new PublicPageService();
        //Get the api details details using showPage() function in PublicPageService
        $response = $apiDetails->showPage($logs);

        $params = [
            'upTime' => $response['upTime'],
            'downTime' => $response['downTime'],
            'avgResponseTime' => $response['avgResponseTime'],
            'api' => $api,
            'lastChecked' => $lastChecked,
            'lastLogs' => $lastLogs,
        ];

        return view('api-monitor.view', $params);
    }

    /*
     * Show the API edit page
     */
    public function edit($uuid)
    {
        //Get the API
        $api = Monitor::find($uuid);
        //GEt all available intervals
        $intervals = Interval::all();
        $params = [
            'api' => $api,
            'intervals' => $intervals,
        ];

        return view('api-monitor.edit', $params);
    }

    /*
     * Update an API
     */
    public function update(Request $request, $uuid)
    {
        try {
            //Get the API
            $api = Monitor::find($uuid);
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => "required | unique:App\Models\Monitor,name,{$api->uuid},uuid,deleted_at,NULL",
                'url' => 'required',
                'expected_response_code' => 'required|numeric',
                'interval' => 'required',
                'email' => 'nullable|email',
            ]);
            if ($validator->fails()) {
                Logger::error('API add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }
            $type = 'api';

            /*
             * service class that interact with the MonitorApi model.
             * refer app/Models/Services/MonitorService.php
             */
            $apiService = new MonitorService();
            //Update API using updateMonitor() function in MonitorService
            //case of successfull updation of API
            if ($apiService->updateMonitor($request, $api, $type)) {
                return to_route('api_monitor.index')
                ->with('success', __('API updated'));
            }
            //case were the API is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('API is not up', [
                            'page' => $request->input('name'),
                            'expected_response_code' => $request->input('expected_response_code'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete the API
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = Monitor::find($uuid);
        $monitor->delete();

        return to_route('api_monitor.index')
            ->with('success', __('API deleted'));
    }
}
