<?php

namespace App\Http\Controllers;

use App\Models\Incident;
use App\Models\IncidentDetails;
use App\Models\MonitorStatusPage;
use App\Models\Services\StatusPageService;
use App\Models\StatusPage;

class ShowStatusPageController extends Controller
{
    public function showStatusPage($name)
    {
        $monitorsList = MonitorStatusPage::whereHas('statusPages', function ($list) use ($name) {
            $list->where('slug', $name);
        })->get();

        $statusPage = StatusPage::where('slug', $name)->first();
        if (! count($monitorsList)) {
            return to_route('status_page.edit', [$statusPage->uuid])
                ->with('error', __('Atleast one system should be selected'));
        }

        $totalMonitors = $monitorsList->count();
        $downMonitors = 0;
        foreach ($monitorsList as $data) {
            $status = $data->monitors->previous_status;
            if ($status == 'down') {
                $downMonitors = $downMonitors + 1;
            }
        }
        $monitor_ids = $monitorsList->pluck('monitor_id')->toArray();
        //Get the incidents of the current monitor
        $incidents = Incident::whereIn('rel_id', $monitor_ids)
            ->latest()
            ->get();
        //Get the incident details of the each incident of the monitor
        $incidentDetails = [];
        foreach ($incidents as $incident) {
            $incident_id = $incident->uuid;
            $details = IncidentDetails::where('incident_id', $incident_id)
                ->latest()
                ->get();

            $incidentDetails[$incident_id] = $details;
        }

        $statusPageService = new StatusPageService;
        $response = $statusPageService->viewStatusPage($monitorsList, $monitor_ids);

        $params = [
            'reports' => $response['reports'],
            'monitors' => $monitorsList,
            'incidents' => $incidents,
            'incidentDetails' => $incidentDetails,
            'totalMonitors' => $totalMonitors,
            'downMonitors' => $downMonitors,
            'todayStatus' => $response['todayStatus'],
            'lastWeekStatus' => $response['lastWeekStatus'],
            'lastMonthStatus' => $response['lastMonthStatus'],
            'oneMonthStatus' => $response['oneMonthStatus'],
            'twoMonthStatus' => $response['twoMonthStatus'],
            'threeMonthStatus' => $response['threeMonthStatus'],
        ];

        return view('status_page.show', $params);
    }
}
