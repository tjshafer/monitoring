<?php

namespace App\Http\Controllers;

use App\Helpers\Logger;
use App\Models\DnsAAAAMonitor;
use App\Models\DnsAMonitor;
use App\Models\DnsCNameMonitor;
use App\Models\DnsMonitoring;
use App\Models\DnsMonitorLogs;
use App\Models\DnsMXMonitor;
use App\Models\DnsNSMonitor;
use App\Models\DnsTXTMonitor;
use App\Models\Interval;
use App\Models\Services\DnsMonitorService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DnsMonitoringController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         * to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    //
    public function index(Request $request)
    {
        //Get the logged in user
        $user = User::find(auth()->id());

        /*
         * service class that interact with the Dns Monitor model.
         * refer app/Models/Services/DnsMonitorService.php
         */
        $monitorService = new DnsMonitorService();

        //Get filtered servers using getFilteredElements() function in  MonitorService
        $servers = $monitorService->getFilteredElements($user, $request)
            ->paginate(10);
        $param = [
            'servers' => $servers,
            'request' => $request,
        ];

        return view('dns_monitoring.index', $param);
    }

    /*
     * Shows create server page
     */
    public function create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the count of servers of the current user
        $servers = DnsMonitoring::where('user_id', $user->id)
                    ->whereNull('deleted_at')
                    ->count();

        /*
         * refer app/Policies/MonitorServerPolicy.php
         */

        // //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('dns_server_monitor.index')
            ->with('error', __('You are not allowed to add server, please contact support'));
        }

        // //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        // //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('dns_server_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        // //Check whether the user's adding limit exceeded or not
        if (! $user->can('createDns', [Monitor::class, $servers])) {
            return to_route('dns_server_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        //Get all avilable Intervals
        $intervals = Interval::all();
        $params = [
            'intervals' => $intervals,
        ];

        return view('dns_monitoring.create', $params);
    }

    public function store(Request $request)
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'url' => 'required',
                'interval' => 'required',

            ]);
            if ($validator->fails()) {
                Logger::error('Server add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Get logged in user
            $user = auth()->user();
            /*
             * service class that interact with the DNS Monitor model.
             * refer app/Models/Services/MonitorService.php
             */
            $monitorService = new DnsMonitorService();
            //Add Server using addServer() function in the MonitorService
            //case of successfull addition of server
            if ($monitorService->addDnsMonitor($request, $user)) {
                $server = DnsMonitoring::where('website_domain', $request->input('url'))
                    ->whereNull('deleted_at')->first();

                return to_route('manage', $server->uuid)
                ->with('success', __('Server added'));
            }
            //case were the server is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Server is not up', [
                            'server' => $request->input('url'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /**
     * Display the details
     */
    public function manage($uuid)
    {
        $server = DnsMonitoring::find($uuid);
        $dns_a_records = DnsAMonitor::where('website_id', $uuid)->get();
        $dns_aaaa_records = DnsAAAAMonitor::where('website_id', $uuid)->get();
        $dns_cname_records = DnsCNameMonitor::where('website_id', $uuid)->get();
        $dns_mx_records = DnsMXMonitor::where('website_id', $uuid)->get();
        $dns_ns_records = DnsNSMonitor::where('website_id', $uuid)->get();
        $dns_txt_records = DnsTXTMonitor::where('website_id', $uuid)->get();
        $dns_monitor_logs = DnsMonitorLogs::where('website_id', $uuid)->get();
        // echo $dns_a_records;exit;
        $params = [
            'server' => $server,
            'dns_a_records' => $dns_a_records,
            'dns_aaaa_records' => $dns_aaaa_records,
            'dns_cname_records' => $dns_cname_records,
            'dns_mx_records' => $dns_mx_records,
            'dns_ns_records' => $dns_ns_records,
            'dns_txt_records' => $dns_txt_records,
            'dns_monitor_logs' => $dns_monitor_logs,
        ];

        return view('dns_monitoring.manage', $params);
    }

    /*
     * Show the server edit page
     */
    public function edit(Request $request, $uuid)
    {
        $server = DnsMonitoring::find($uuid);

        //GEt all available intervals
        $intervals = Interval::all();
        $params = [
            'server' => $server,
            'intervals' => $intervals,
        ];

        return view('dns_monitoring.edit', $params);
    }

    /*
     * Update a server
     */
    public function update(Request $request, $uuid)
    {
        try {
            $server = DnsMonitoring::find($uuid);
            //Form validation
            $validator = Validator::make($request->all(), [
                'url' => 'required',
                'interval' => 'required',
            ]);
            if ($validator->fails()) {
                Logger::error('Server update form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/MonitorService.php
             */
            $monitorService = new DnsMonitorService();
            //Update server using updateServer() function in MonitorService
            //case of successfull updation of server
            if ($monitorService->updateMonitor($request, $server)) {
                return to_route('manage', $server->uuid)
                ->with('success', __('Server updated'));
            }
            //case were the server is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Server is not up', [
                            'server' => $request->input('url'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete the server
     */
    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = DnsMonitoring::find($uuid);
        $monitor->delete();

        return to_route('dns_server_monitor.index')
            ->with('success', __('Server deleted'));
    }
}
