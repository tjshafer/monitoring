<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AttachmentHelper;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
         to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index(Request $request)
    {
        // Display the settings page
        $settings = Setting::all();

        return view('settings.index', ['settings' => $settings]);
    }

    public function edit(Request $request, $id)
    {
        $setting = Setting::find($id);
        // Display imap enable page
        if ($setting->type == 'radio') {
            return view('settings.radio', $setting);
        // Display add extension page
        } elseif ($setting->type == 'text') {
            return view('settings.text', $setting);
        } elseif ($setting->type == 'attachment') {
            return view('settings.attachment', $setting);
        } elseif ($setting->type == 'text_area') {
            return view('settings.textarea', $setting);
        }
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        // Get the settings
        $setting = Setting::find($id);

        // Enable Imap
        if ($setting->type == 'radio') {
            $updateArray = [];
            if ($request->input('Enable') == 'enable') {
                $updateArray['value'] = 1;
            } else {
                $updateArray['value'] = 0;
            }
            $setting->update($updateArray);
        // Add extensions
        } elseif ($setting->type == 'text') {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'text' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
            }

            // update settings
            $updateArray = [];
            $updateArray['value'] = $request->input('text');
            $setting->update($updateArray);
        } elseif ($setting->type == 'text_area') {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'textarea' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
            }

            // update settings
            $updateArray = [];
            $updateArray['value'] = $request->input('textarea');
            $setting->update($updateArray);
        } elseif ($setting->type == 'attachment') {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
            }
            $updateArray = [];
            if ($request->hasFile('attachment')) {
                $file = $request->file('attachment');
                $validator = Validator::make($request->all(), [
                    'attachment' => 'required|mimes:png,svg,',
                ]);

                if ($validator->fails()) {
                    return redirect()
                    ->back()
                    ->withInput()
                    ->with('error', __('Invalid file format'));
                }

                // Set attachment name store to database
                $attachmentHelper = new AttachmentHelper();
                $fileName = $attachmentHelper->privateIconStore($file, 'system_logo');
                $updateArray['value'] = $fileName;
            }
            // Update settings
            // $updateArray['value'] = $request->attachment;
            $setting->update($updateArray);
        }

        return to_route('settings.index')
            ->with('success', __('Settings updated'));
    }
}
