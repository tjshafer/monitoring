<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
           to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index()
    {
        /*
         * Get the billing address
         * There will be only one billing address whose id is 1
         */
        $address = Address::find(1);
        $params = [
            'address' => $address,
        ];

        return view('address.index', $params);
    }

    public function create()
    {
        // Display the address update form
        $address = Address::find(1);
        $params = [
            'address' => $address,
        ];

        return view('address.create', $params);
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            // Form validation
            $validator = Validator::make($request->all(), [
                'phone' => 'numeric | nullable',
                'name' => 'required | string | max:255',
                'address_1' => 'required',
                'city' => 'required | string | max:255',
                'postal_code' => 'required',
                'state' => 'required',
                'country' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $updateAddress = Address::find(1);
            //Update the biling address if an address exist
            if ($updateAddress) {
                $address = $updateAddress;
            } else {
                // create a new address
                $address = new Address();
            }
            $address->id = 1;
            $address->name = $request->input('name');
            $address->phone = $request->input('phone');
            $address->address_1 = $request->input('address_1');
            $address->address_2 = $request->input('address_2');
            $address->city = $request->input('city');
            $address->postal_code = $request->input('postal_code');
            $address->state_id = $request->input('state');
            $address->country_id = $request->input('country');
            $address->save();

            return to_route('billing_address.index')
                ->with('success', __('Address added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __($e->getMessage()));
        }
    }
}
