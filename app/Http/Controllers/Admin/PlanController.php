<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Plan;
use App\Models\Services\PlanService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
         to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    /*
     * List available plans
     */
    public function index()
    {
        $plans = Plan::oldest('display_order')->get();
        $params = [
            'plans' => $plans,
        ];

        return view('plans.index', $params);
    }

    /*
     * View add new plan page
     */
    public function create()
    {
        return view('plans.create');
    }

    /*
     * Add a new plan
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'description' => 'required',
                'display_order' => 'required | numeric',
            ]);
            $validator->sometimes('server_count', 'required|numeric', function ($request) {
                return $request->input('servers') == 1;
            });
            // $validator->sometimes('webpage_count', 'required|numeric', function($request){
            //     return $request->webpages == 1;
            // });
            $validator->sometimes('api_count', 'required|numeric', function ($request) {
                return $request->input('apis') == 1;
            });
            $validator->sometimes('ssl_count', 'required|numeric', function ($request) {
                return $request->input('ssl') == 1;
            });
            $validator->sometimes('domain_count', 'required|numeric', function ($request) {
                return $request->input('domains') == 1;
            });
            $validator->sometimes('subdomain_count', 'required|numeric', function ($request) {
                return $request->input('subdomains') == 1;
            });
            $validator->sometimes('dns_count', 'required|numeric', function ($request) {
                return $request->input('dns') == 1;
            });
            $validator->sometimes('keyword_count', 'required|numeric', function ($request) {
                return $request->input('keyword') == 1;
            });
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $planService = new PlanService();
            $planService->addPlan($request);

            return to_route('plans.index')
                ->with('success', __('Plan added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Show plan edit page
     */
    public function edit($id)
    {
        $plan = Plan::find($id);
        $params = [
            'plan' => $plan,
        ];

        return view('plans.edit', $params);
    }

    /*
     * Update a plan and it'sproperties
     */
    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        // dd($request->properties);
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'description' => 'required',
                'display_order' => 'required | numeric',
            ]);
            $validator->sometimes('server_count', 'required|numeric', function ($request) {
                return $request->input('servers') == 1;
            });
            // $validator->sometimes('webpage_count', 'required|numeric', function($request){
            //     return $request->webpages == 1;
            // });
            $validator->sometimes('api_count', 'required|numeric', function ($request) {
                return $request->input('apis') == 1;
            });
            $validator->sometimes('ssl_count', 'required|numeric', function ($request) {
                return $request->input('ssl') == 1;
            });
            $validator->sometimes('domain_count', 'required|numeric', function ($request) {
                return $request->input('domains') == 1;
            });
            $validator->sometimes('subdomain_count', 'required|numeric', function ($request) {
                return $request->input('subdomains') == 1;
            });
            $validator->sometimes('dns_count', 'required|numeric', function ($request) {
                return $request->input('dns') == 1;
            });
            $validator->sometimes('keyword_count', 'required|numeric', function ($request) {
                return $request->input('keyword') == 1;
            });
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $plan = Plan::find($id);
            // dd($plan);
            $planService = new PlanService();
            $planService->updatePlan($request, $plan);

            return to_route('plans.index')
                ->with('success', __('Plan updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', $e->getMessage());
        }
    }

    /*
     * Delete a plan
     */
    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $plan = Plan::find($id);
        $plan->delete();

        return to_route('plans.index')
            ->with('success', __('Plan deleted'));
    }
}
