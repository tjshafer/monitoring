<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ServiceController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
         to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index(Request $request)
    {
        //Query all subscriptions in descending order.
        $query = Service::query()->latest('updated_at');
        //Filter services by by search keywords
        if ($request->input('search')) {
            $search = $request->input('search');
            //use hasMany ralation bcz user details only available in user table
            $query = $query->whereHas('user', function ($items) use ($search) {
                $items->where('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%');
            })->orWhereHas('orders', function ($items) use ($search) {
                $items->where('order_id', 'like', '%'.$search.'%');
            })->orWhere('service_id', 'like', '%'.$search.'%');
        }
        $services = $query->paginate(10);

        $params = [
            'services' => $services,
            'request' => $request,
        ];

        return view('order-service.index', $params);
    }

    /*
     * View the service status update page
    */
    public function edit($uuid)
    {
        //Get the service
        $service = Service::find($uuid);
        //Get all statuses
        $statuses = Status::all();

        return view('order-service.edit', $service, compact('statuses'));
    }

    /*
     * Update the status of the service
    */
    public function update(Request $request, $uuid): \Illuminate\Http\RedirectResponse
    {
        //Get the service
        $service = Service::find($uuid);
        //TODO:find a better way to find status id
        $status_id = Status::where('name', $request->input('status'))->pluck('id');
        $service->status_id = $status_id[0];

        $currentDay = Carbon::now()->format('Y-m-d');
        if (Carbon::parse($request->input('grace_period'))->format('Y-m-d') > $currentDay) {
            $service->grace_period = $request->input('grace_period');
        }
        $service->update();

        return to_route('services.index')
            ->with('success', __('Service updated'));
    }
}
