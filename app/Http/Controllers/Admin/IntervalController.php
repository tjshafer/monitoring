<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IntervalController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
           to this controller
        */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index(Request $request)
    {
        // List the available intervals
        $intervals = Interval::paginate(10);
        $params = [
            'intervals' => $intervals,
            'request' => $request,
        ];

        return view('interval.index', $params);
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'interval' => 'required|numeric|unique:App\Models\Interval,interval',
                'display_name' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Add a new interval
            $interval = new Interval();
            $interval->interval = $request->input('interval');
            $interval->display_name = $request->input('display_name');
            $interval->save();

            return to_route('interval.index')
                ->with('success', __('Interval added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        /*
         * Delete the interval
         */
        $interval = Interval::find($id);
        $interval->delete();

        return to_route('interval.index')
            ->with('success', __('Interval deleted'));
    }
}
