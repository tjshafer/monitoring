<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LanguageController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
           to this controller
        */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index(Request $request)
    {
        // List the available languages
        $languages = Language::paginate(10);
        $params = [
            'languages' => $languages,
            'request' => $request,
        ];

        return view('language.index', $params);
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            //form validation
            $validator = Validator::make($request->all(), [
                'language' => 'required',
                'code' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Add a new language
            $language = new Language();
            $language->language = $request->input('language');
            $language->code = $request->input('code');
            $language->save();

            return to_route('language.index')
                ->with('success', __('Language added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Delete a language
     */
    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $language = Language::find($id);
        $language->delete();

        return to_route('language.index')
            ->with('success', __('Language deleted'));
    }
}
