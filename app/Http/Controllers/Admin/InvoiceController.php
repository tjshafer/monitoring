<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\Plan;
use App\Models\Pricing;
use App\Models\Services\PlanService;
use App\Models\User;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
         to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index(Request $request)
    {
        //Query all invoices in descending order
        $query = Invoice::query()->latest('updated_at');
        //Filter invoices by by search keywords
        if ($request->input('search')) {
            $search = $request->input('search');
            /**
             * use hasMany ralation bcz user details and order details are only available in
             * user and order table
             */
            $query = $query->whereHas('user', function ($items) use ($search) {
                $items->where('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%');
            })->orWhereHas('orders', function ($items) use ($search) {
                $items->where('order_id', 'like', '%'.$search.'%');
            });
        }
        $invoices = $query->paginate(10);

        $params = [
            'invoices' => $invoices,
            'request' => $request,
        ];

        return view('invoices.index', $params);
    }

    /*
     * Shows the invoce details page
     */
    public function show($uuid)
    {
        //Get the invoice
        $invoice = Invoice::find($uuid);
        //Get the invoice details
        $invoiceDetails = InvoiceDetails::where('invoice_id', $invoice->uuid)->latest()->first();
        //Get the pricing ID
        $pricing_id = $invoiceDetails->services->pricing_id;
        //Get the pricing details
        $pricing = Pricing::find($pricing_id);
        //Get the plan details
        $plan = Plan::where('id', $pricing->plan_id)->first();
        //Get the plan validity from plan property pivot table
        $planService = new PlanService();
        $validity = $planService->displayValidity($pricing);
        //get the user
        $user = User::where('id', $invoice->user_id)->first();
        $params = [
            'invoice' => $invoice,
            'user' => $user,
            'plan' => $plan,
            'pricing' => $pricing,
            'validity' => $validity,
        ];

        return view('invoices.show', $params);
    }
}
