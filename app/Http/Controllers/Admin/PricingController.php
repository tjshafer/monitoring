<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Plan;
use App\Models\Pricing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PricingController extends Controller
{
    public function __construct()
    {
        /*
        make sure only logged in and verified user has access
        to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function pricing_index($plan_id)
    {
        // dd('hi');
        // $this->authorize('isAdmin', Authorize::class);
        $prices = Pricing::where('plan_id', $plan_id)->get();
        $params = [
            'prices' => $prices,
            'plan_id' => $plan_id,
        ];

        return view('pricing.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pricing_create($plan_id)
    {
        // $this->authorize('isAdmin', Authorize::class);
        $plans = Plan::first();
        /*
         * Check wheather there is a valid billing address
         * if not, redirect to add billing address page
         */
        if (! $plans) {
            return to_route('plans.create')
                ->with('error', __('Add a plan first'));
        }

        $currency = Currency::first();
        /*
         * Check wheather there is a valid billing address
         * if not, redirect to add billing address page
         */
        if (! $currency) {
            return to_route('currency.create')
                ->with('error', __('Add a currency first'));
        }
        $plans = Plan::all();
        $currencies = Currency::all();
        $params = [
            'plans' => $plans,
            'currencies' => $currencies,
            'plan_id' => $plan_id,
        ];

        return view('pricing.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        // $this->authorize('isAdmin', Authorize::class);
        try {
            $validator = Validator::make($request->all(), [
                'term' => 'required | integer',
                'plan_id' => 'required',
                'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                // 'price_renews' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
                'currency_id' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $price = new Pricing();
            $price->plan_id = $request->input('plan_id');
            $price->term = $request->input('term');
            $price->period = $request->input('period');
            $price->price = $request->price;
            // $price->price_renews = $request->input('price')_renews;
            $price->currency_id = $request->input('currency_id');
            $price->save();

            return to_route('pricing_index', $request->input('plan_id'))
                ->with('success', __('Price added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
             ->back()
            ->withInput()
            ->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function pricing_edit(Request $request, $plan_id, $id)
    {
        // $this->authorize('isAdmin', Authorize::class);
        $price = Pricing::find($id);
        $plans = Plan::all();
        $currencies = Currency::all();
        $params = [
            'price' => $price,
            'plans' => $plans,
            'currencies' => $currencies,
            'plan_id' => $plan_id,
        ];

        return view('pricing.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\pricing  $pricing
     */
    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        // $this->authorize('isAdmin', Authorize::class);
        try {
            $validator = Validator::make($request->all(), [
                'term' => 'required | integer',
                'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                // 'price_renews' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
                'currency_id' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $price = Pricing::find($id);
            $price->update($request->all());

            return to_route('pricing_index', $price->plan_id)
                ->with('success', __('Price Updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
             ->back()
            ->withInput()
            ->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pricing  $pricing
     */
    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        // $this->authorize('isAdmin', Authorize::class);
        $price = Pricing::find($id);
        $price->delete();

        return redirect()->back()
            ->with('success', __('Price deleted'));
    }
}
