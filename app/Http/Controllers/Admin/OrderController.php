<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Mail\OrderAccept;
use App\Models\Order;
use App\Models\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
         to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index(Request $request)
    {
        //Query all orders in descending order
        $query = Order::query()->latest('order_id');
        //Filter orders by by search keywords
        if ($request->input('search')) {
            $search = $request->input('search');
            //use hasMany ralation bcz user details only available in user table
            $query = $query->whereHas('user', function ($items) use ($search) {
                $items->where('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%');
            })->orWhere('order_id', 'like', '%'.$search.'%');
        }
        $orders = $query->paginate(10);

        $params = [
            'orders' => $orders,
            'request' => $request,
        ];

        return view('orders.index', $params);
    }

    /*
     * Order accept function.
     * Changes the status of the order.
     */
    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $orderService = new OrderService;
        $order = Order::find($id);
        $updated_order = $orderService->acceptOrder($order);
        // $order->status = 'active';
        // $order->save();

        /*
         * Send order accepted mail to user's Email address
         * refer app/Mail/OrderAccept.php
         */
        if (! empty($order->user_id) && ! empty($order->user->email)) {
            Logger::info('Order accept mail to '.$order->user->email);
            $data = [];
            $mail = new OrderAccept($updated_order);
            Mail::to($order->user->email)
                ->queue($mail);
        }

        return redirect()->back()
            ->with('success', __('Order accepted'));
    }
}
