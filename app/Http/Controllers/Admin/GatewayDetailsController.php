<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Gateway;
use App\Models\GatewayDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GatewayDetailsController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
           to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index()
    {
        //Take all available gateways
        $gateways = Gateway::all();

        return view('gateway.index', ['gateways' => $gateways]);
    }

    public function edit($id)
    {
        $billingAddress = Address::first();
        /*
         * Check wheather there is a valid billing address
         * if not, redirect to add billing address page
         */
        if (! $billingAddress) {
            return to_route('billing_address.create')
                ->with('error', __('Add a billing address first'));
        }

        // get the gateway
        $gateway = Gateway::find($id);
        // get the gateway details table
        $gatewayDetails = GatewayDetails::where('gateway_id', $gateway->id)->get();
        $params = [
            'gatewayDetails' => $gatewayDetails,
        ];
        // View the gateway edit page
        return view('gateway.edit', $gateway, $params);
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'details.*' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //get the gateway
            $gateway = Gateway::find($id);
            /*
             * Update the details of the above gateway in
             * gateway details table
             */
            if ($gateway->name != 'bank_transfer') {
                foreach ($request->input('details') as $key => $value) {
                    $gatewayDetails = GatewayDetails::find($key);
                    $gatewayDetails->value = $value;
                    $gatewayDetails->save();
                }
            }

            //update the gateway
            if ($request->input('status') == 'enable') {
                $gateway->status = true;
            } else {
                $gateway->status = false;
            }
            if ($request->input('test_mode') == 'enable') {
                $gateway->test_mode = true;
            } else {
                $gateway->test_mode = false;
            }
            $gateway->update();

            return to_route('gateways.index')
                ->with('success', __('Gateway details updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }
}
