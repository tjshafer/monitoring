<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Monitor;
use App\Models\MonitorWebsite;
use App\Models\Order;
use App\Models\Service;
use App\Models\User;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
           to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    public function index()
    {
        /*
         * Take the count of all Servers,wepages and APIs
         */
        $users = User::count();
        $servers = Monitor::where('type', 'server')->count();
        $pages = Monitor::where('type', 'webpage')->count();
        $apis = Monitor::where('type', 'api')->count();
        $websites = MonitorWebsite::count();

        //Count of total orders
        $orders = Order::count();
        //Count of total pending orders
        $pendingOrders = Order::where('status', 'pending')->count();
        //Count of total completed orders
        $completedOrders = Order::where('status', 'active')->count();
        //Count of total trial orders
        $trialOrders = Order::where('amount', 0.00)->count();
        //Count of total paid orders
        $paidOrders = Order::where('amount', '!=', 0.00)->count();
        //Count of total orders that are currently active
        $activeOrders = Service::where('status_id', 1)->count();
        //Count of total active trial orders

        $activeTrialOrders = Service::where('plan_id', 1)
            ->where('status_id', 1)->count();
        //Count of total expired trial orders
        $inactiveTrialOrders = Service::where('plan_id', 1)
            ->where('status_id', '!=', 1)->count();
        //Count of total active paid orders
        $activePaidOrders = Service::where('plan_id', '!=', 1)
            ->where('status_id', 1)->count();
        //Count of total expired or inactive paid orders orders
        $inactivePaidOrders = Service::where('plan_id', '!=', 1)
            ->where('status_id', '!=', 1)->count();

        //Take the total income of all time
        $totalIncome = Invoice::where('test_mode', false)->sum('amount');
        //Take the today's income
        $todayIncome = Invoice::where('test_mode', false)
            ->where('payment_status', 'paid')
            ->whereDate('created_at', Carbon::today())
            ->sum('amount');
        //Take last week's total income
        $thisWeekIncome = Invoice::where('test_mode', false)
            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->sum('amount');
        //Take last month's total income
        $thisMonthIncome = Invoice::where('test_mode', false)
            ->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
            ->sum('amount');
        //Take last year's total income
        $thisYearIncome = Invoice::where('test_mode', false)
            ->whereBetween('created_at', [Carbon::now()->startOfYear(), Carbon::now()->endOfYear()])
            ->sum('amount');

        $params = [
            'users' => $users,
            'servers' => $servers,
            'pages' => $pages,
            'apis' => $apis,
            'websites' => $websites,
            'orders' => $orders,
            'activeOrders' => $activeOrders,
            'pendingOrders' => $pendingOrders,
            'completedOrders' => $completedOrders,
            'trialOrders' => $trialOrders,
            'paidOrders' => $paidOrders,
            'activeOrders' => $activeOrders,
            'activeTrialOrders' => $activeTrialOrders,
            'inactiveTrialOrders' => $inactiveTrialOrders,
            'activePaidOrders' => $activePaidOrders,
            'inactivePaidOrders' => $inactivePaidOrders,
            'totalIncome' => $totalIncome,
            'todayIncome' => $todayIncome,
            'thisWeekIncome' => $thisWeekIncome,
            'thisMonthIncome' => $thisMonthIncome,
            'thisYearIncome' => $thisYearIncome,

        ];

        return view('dashboard.admin', $params);
    }
}
