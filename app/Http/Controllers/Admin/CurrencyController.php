<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Logger;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
         to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = Currency::all();
        $params = [
            'currencies' => $currencies,
        ];

        return view('currency.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('currency.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'currency' => 'required',
                'prefix' => 'required',
            ]);

            $currency = new Currency();
            $currency->currency = $request->input('currency');
            $currency->prefix = $request->input('prefix');
            $currency->save();

            return to_route('currency.index')
                ->with('success', __('Currency added'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
             ->back()
            ->withInput()
            ->with('error', __($e->getMessage()));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // dd($id);
        $currency = Currency::find($id);
        $params = [
            'currency' => $currency,
        ];

        return view('currency.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\currency  $currency
     */
    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'currency' => 'required',
                'prefix' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $currency = Currency::find($id);
            // dd($currency);
            $currency->update($request->all());

            return to_route('currency.index')
                ->with('success', __('Currency Updated'));
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
             ->back()
            ->withInput()
            ->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\currency  $currency
     */
    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $currency = Currency::find($id);
        $currency->delete();

        return to_route('currency.index')
            ->with('success', __('Currency deleted'));
    }
}
