<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified admin has access
         to this controller
         */
        $this->middleware(['auth:admin', 'verified']);
    }

    /*
     * View the admin profile
     */
    public function index(Request $request)
    {
        //Get the logged in admin
        $admin = Auth::guard('admin')->user();

        return view('profiles.admin', ['admin' => $admin]);
    }

    public function update(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {
            // get the logged in admin
            $admin = Auth::guard('admin')->user();
            //Form validation
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required | unique:App\Models\Admin,email,'.$admin->id,
            ]);
            /*
             * Update password
             */
            $check = $request->input('old_password');

            $updateArray = [];
            if (isset($check)) {
                // If old password is incorrect
                if (! Hash::check($check, $admin->password)) {
                    return redirect()
                        ->back()
                        ->withInput()
                        ->with('error', __('Please check your password'));

                // If old password is correct
                } elseif (Hash::check($check, $admin->password)) {
                    $validator = Validator::make($request->all(), [
                        'password' => 'required|min:6',
                        'c_password' => 'required|same:password',
                    ]);
                    $updateArray['password'] = Hash::make($request->input('password'));
                }
            }
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            // update profile
            $updateArray['first_name'] = $request->input('first_name');
            $updateArray['last_name'] = $request->input('last_name');
            $updateArray['email'] = $request->input('email');
            $admin->update($updateArray);

            return redirect()->back()
                ->with('success', __('Profile updated'));
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }
}
