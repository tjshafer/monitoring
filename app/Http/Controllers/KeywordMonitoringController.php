<?php

namespace App\Http\Controllers;

use App\Helpers\Logger;
use App\Models\Interval;
use App\Models\KeywordMonitoring;
use App\Models\KeywordMonitorLogs;
use App\Models\Services\KeywordMonitorService;
use App\Models\Services\PublicPageService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KeywordMonitoringController extends Controller
{
    public function __construct()
    {
        /*
         * make sure only logged in and verified user has access
         * to this controller
         */
        $this->middleware(['auth', 'verified']);
    }

    //
    public function index(Request $request)
    {
        //Get the logged in user
        $user = User::find(auth()->id());

        /*
        * service class that interact with the keyword Monitor model.
        * refer app/Models/Services/KeywordMonitorService.php
        */
        $monitorService = new KeywordMonitorService();

        $keywords = $monitorService->getFilteredElements($user, $request)
                    ->paginate(10);
        $countKeyExist = $monitorService->getCount($user, $request, 'keyword_exist');
        $countKeyNotExist = $monitorService->getCount($user, $request, 'keyword_not_exist');

        $param = [
            'keywords' => $keywords,
            'request' => $request,
            'keyExist' => $countKeyExist,
            'keyNotExist' => $countKeyNotExist,
        ];

        return view('keyword_monitoring.index', $param);
    }

    public function create()
    {
        //Get the logged in user
        $user = User::find(auth()->id());

        //Get the count of keywords of the current user
        $keywords = KeywordMonitoring::where('user_id', $user->id)
                    ->whereNull('deleted_at')
                    ->count();

        /*
         * refer app/Policies/MonitorServerPolicy.php
         */

        // //Check whether the user is active or not
        if (! $user->can('isActive', Monitor::class)) {
            return to_route('keyword_monitor.index')
            ->with('error', __('You are not allowed to add keyword, please contact support'));
        }

        // //Check whether the user have a plan or not
        if ($user->can('view', Monitor::class)) {
            return to_route('available_plans.index')
            ->with('error', __('Add a plan'));
        }

        // //Check whether the user's order is confirmed or not
        if (! $user->can('orderConfirm', Monitor::class)) {
            return to_route('keyword_monitor.index')
            ->with('error', __('Your order is not confirmed.  wait sometime or Contact support'));
        }

        // //Check whether the user's adding limit exceeded or not
        if (! $user->can('createKeyword', [Monitor::class, $keywords])) {
            return to_route('keyword_monitor.index')
            ->with('error', __('Limit exceeded'));
        }

        //Get all avilable Intervals
        $intervals = Interval::all();
        $params = [
            'intervals' => $intervals,
        ];

        return view('keyword_monitoring.create', $params);
    }

    public function store(Request $request)
    {
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'url' => 'required',
                'name' => 'required | unique:App\Models\KeywordMonitoring',
                'keyword' => 'required|max:255',
                'alert' => 'required',
                'case' => 'required',
                'interval' => 'required',

            ]);
            if ($validator->fails()) {
                Logger::error('Keyword add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            //Get logged in user
            $user = auth()->user();
            /*
             * service class that interact with the keyword Monitor model.
             * refer app/Models/Services/KeywordMonitorService.php
             */
            $monitorService = new KeywordMonitorService();
            //Add Server using addKeyword() function in the KeywordMonitorService
            //case of successfull addition of server
            if ($monitorService->addKeyword($request, $user)) {
                return to_route('keyword_monitor.index')
                ->with('success', __('Keyword added'));
            }
            //case were the server is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Keyword is not found', [
                            'keyword' => $request->input('url'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function edit(Request $request, $uuid)
    {
        $monitor = KeywordMonitoring::find($uuid);
        //GEt all available intervals
        $intervals = Interval::all();
        $params = [
            'monitor' => $monitor,
            'intervals' => $intervals,
        ];

        return view('keyword_monitoring.edit', $params);
    }

    public function update(Request $request, $uuid)
    {
        try {
            $monitor = KeywordMonitoring::find($uuid);
            //Form validation
            $validator = Validator::make($request->all(), [
                'url' => 'required',
                'name' => 'required',
                'keyword' => 'required|max:255',
                'alert' => 'required',
                'case' => 'required',
                'interval' => 'required',

            ]);
            if ($validator->fails()) {
                Logger::error('Keyword edit form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the keyword Monitor model.
             * refer app/Models/Services/KeywordMonitorService.php
             */
            $monitorService = new KeywordMonitorService();
            //Add Server using updateKeyword() function in the KeywordMonitorService
            //case of successfull addition of server
            if ($monitorService->updateKeyword($request, $monitor)) {
                return to_route('keyword_monitor.index')
                ->with('success', __('Keyword updated'));
            }
            //case were the server is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Keyword is not found', [
                            'keyword' => $request->input('url'),
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function show(Request $request, $uuid)
    {
        //Get the keyword monitor
        $keyword = KeywordMonitoring::find($uuid);
        //Get the logs of the current keyword
        $logs = KeywordMonitorLogs::where('rel_id', $uuid)->get();
        //Get the last 5 logs of the keyword
        $lastLogs = KeywordMonitorLogs::where('rel_id', $uuid)->latest()->take(5)->get();
        //Get the latest log
        $lastLog = KeywordMonitorLogs::where('rel_id', $uuid)->latest()->first();
        //Get last checked time
        if ($lastLog) {
            $lastChecked = $lastLog->created_at;
        } else {
            $lastChecked = null;
        }
        /*
         * service class that interact with the public pages.
         * refer app/Models/Services/PublicPageService.php
         */
        $keywordDetails = new PublicPageService();
        //Get the server details details using showPage() function in PublicPageService
        $response = $keywordDetails->showPage($logs);

        $params = [
            'upTime' => $response['upTime'],
            'downTime' => $response['downTime'],
            'avgResponseTime' => $response['avgResponseTime'],
            'keyword' => $keyword,
            'lastChecked' => $lastChecked,
            'lastLogs' => $lastLogs,
        ];

        return view('keyword_monitoring.view', $params);
    }

    public function destroy($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = KeywordMonitoring::find($uuid);
        $monitor->delete();

        return to_route('keyword_monitor.index')
            ->with('success', __('Keyword deleted'));
    }
}
