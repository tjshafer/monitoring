<?php

namespace App\Http\Controllers;

use App\Models\Incident;
use App\Models\IncidentDetails;
use App\Models\Monitor;
use App\Models\MonitorLog;
use App\Models\PublicPage;
use App\Models\Services\PublicPageService;
use App\Models\User;
use Illuminate\Support\Carbon;

class PublicPageController extends Controller
{
    /**
     * Public page view
     */
    public function monitorPublicPage($name)
    {
        //Get the logged in user
        $user = User::find(auth()->id());
        //Get the monitor
        $monitor = Monitor::where('name', $name)->first();
        //Get the public page
        $publicPage = PublicPage::where('rel_id', $monitor->uuid)->first();
        //Get the monitor created date
        $createdDate = Carbon::parse($monitor->created_at)->format('Y-m-d');

        //Get the logs of the current monitor
        $logs = MonitorLog::where('rel_id', $monitor->uuid)->get();
        //Get the incidents of the current monitor
        $incidents = Incident::where('rel_id', $monitor->uuid)
            ->latest()
            ->get();
        //Get the incident details of the each incident of the monitor
        $incidentDetails = [];
        foreach ($incidents as $incident) {
            $incident_id = $incident->uuid;
            $details = IncidentDetails::where('incident_id', $incident_id)
                ->latest()
                ->get();

            $incidentDetails[$incident_id] = $details;
        }

        /*
         * service class that interact with the public pages.
         * refer app/Models/Services/PublicPageService.php
         */
        $publicPageService = new PublicPageService();
        //Get the public page details using showPage() function in PublicPageService
        $response = $publicPageService->showPage($logs);
        $params = [
            'createdDate' => $createdDate,
            'weekStatus' => $response['weekStatus'],
            'avgResponseTime' => $response['avgResponseTime'],
            'upTime' => $response['upTime'],
            'publicPage' => $publicPage,
            'reports' => $response['reports'],
            'user' => $user,
            'incidents' => $incidents,
            'incidentDetails' => $incidentDetails,
        ];

        return view('public-pages.show', $params);
    }
}
