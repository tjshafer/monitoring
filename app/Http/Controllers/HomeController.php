<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Plan;
use App\Models\Pricing;

class HomeController extends Controller
{
    /**
     * Home page that displays pricing before login
     */
    public function index()
    {
        $plans = Plan::where('status', true)
            ->oldest('display_order')->get();
        $params = [
            'plans' => $plans,
        ];

        return view('home.index', $params);
    }

    public function show($id)
    {
        $plan = Plan::find($id);
        $pricing = Pricing::where('plan_id', $id)
            ->where('currency_id', 1)->get();

        $currencies = Currency::all();
        $params = [
            'plan' => $plan,
            'pricing' => $pricing,
            'currencies' => $currencies,
        ];

        return view('home.show', $params);
    }

    public function showCurrencyWise($plan_id, $id)
    {
        $plan = Plan::find($plan_id);
        $pricing = Pricing::where('plan_id', $plan_id)
            ->where('currency_id', $id)
            ->oldest('price')->get();
        $currencies = Currency::all();
        $selected_currency = Currency::find($id);
        $params = [
            'plan' => $plan,
            'pricing' => $pricing,
            'currencies' => $currencies,
            'selected_currency' => $selected_currency,
        ];

        return view('home.currency_plans', $params);
    }
}
