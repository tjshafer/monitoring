<?php

namespace App\Http\Controllers;

use App\Helpers\Logger;
use App\Models\Address;
use App\Models\Gateway;
use App\Models\GatewayDetails;
use App\Models\Order;
use App\Models\Plan;
use App\Models\Pricing;
use App\Models\Service;
use App\Models\Services\AddPlanService;
use App\Models\Services\OrderService;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Mollie\Laravel\Facades\Mollie;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use Stripe;

class GatewayController extends Controller
{
    /**
     * Process order page
     */
    public function processOrder(Request $request, $id, Gateway $gateway)
    {
        //Get the pricing details
        $pricing = Pricing::find($id);
        //get plan
        $plan = Plan::find($pricing->plan_id);
        //Get the logged in user
        $user = User::find(auth()->id());

        /*
         * service class that interact with the Order model.
         * refer app/Models/Services/OrderService.php
         */
        $orderService = new OrderService();
        //Get the price using price() function in OrderService
        $price = $orderService->price($user, $pricing);
        /*
         * Trial order case
         */
        if ($price == 0.00) {
            $gateway = null;
            $paymentStatus = 'paid';
            $txId = null;
            $isRenew = false;
            $description = "Plan name:$plan->name";
            //Add order details using trialOrderDetails() function in OrderService
            $order = $orderService->trialOrderDetails($user, $plan, $pricing, $gateway, $paymentStatus, $txId, $isRenew, $description, $price);
            $params = [
                'order' => $order,
            ];

            return view('gateway.success', $params);
        }
        /*
         * Paid order case
         */
        $gatewayName = $request->gateway;
        //!= 'bank_transfer' ? $request->input('gateway') : 'Bank Transfer';
        /*
         * Check whether the selected gateway is activated or not
         * refrer app/Policies/GatewayPolicy.php
         */
        if (! $user->can('view', [$gateway, $gatewayName])) {
            return redirect()->back()
            ->with('error', __('This gateway is temporarily unavailable'));
        }
        /*
         * service class that interact with the Order model.
         * refer app/Models/Services/AddPlanService.php
         */
        $addPlanService = new AddPlanService();
        $order = $addPlanService->addOrder($request->input('gateway'), $user, $price, $pricing);
        Session::put('order', $order);
        //Stripe gateway case
        if ($request->input('gateway') == 'stripe') {
            //Get the stripe key
            $stripeKey = GatewayDetails::where('name', 'stripe_key')->first();
            $params = [
                'plan' => $plan,
                'pricing' => $pricing,
                'price' => $price,
                'stripeKey' => $stripeKey,
            ];

            return view('gateway.stripe', $params);
        }
        if ($request->input('gateway') == 'paypal') {
            $gateway = Gateway::where('name', 'paypal')->first();
            $params = [
                'user' => $user,
                'plan' => $plan,
                'pricing' => $pricing,
                'gateway' => $gateway,
                'price' => $price,
                'order' => $order,
            ];

            return view('gateway.paypal', $params);
        } elseif ($request->input('gateway') == 'mollie') {
            //Mollie gateway case
            $params = [
                'plan' => $plan,
                'pricing' => $pricing,
                'price' => $price,
            ];

            return view('gateway.mollie', $params);
        } elseif ($request->input('gateway') == 'bank_transfer') {
            //Bank Transfer case
            $params = [
                'plan' => $plan,
                'pricing' => $pricing,
                'price' => $price,
            ];

            return view('gateway.banktransfer', $params);
        }
    }

    /**
     * Stripe payment API call
     */
    public function stripePayment(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        /*
         * service class that interact with the Order model.
         * refer app/Models/Services/OrderService.php
         */
        $orderService = new OrderService();
        //Get the user
        $user = User::find(auth()->id());
        //Get the pricing details
        $pricing = Pricing::find($id);
        //Get the requested plan
        $plan = Plan::find($pricing->plan_id);
        //Get the price using price() function in OrderService
        $price = $orderService->price($user, $pricing);
        //Get the billig address
        $billingAddress = Address::find($id = 1);
        //Get the stripe key
        $stripeKey = GatewayDetails::where('name', 'stripe_secret')->first();
        //Calling API
        Stripe\Stripe::setApiKey($stripeKey->value);
        $customer = \Stripe\Customer::create([
            'name' => "$user->first_name $user->last_name",
            'source' => $request->input('stripeToken'),
            'address[line1]' => $user->address_1,
            'address[postal_code]' => $user->postal_code,
            'address[city]' => $user->city,
            'address[state]' => $user->states->name,
            'address[country]' => $user->countries->name,
        ]);
        $response = Stripe\Charge::create([
            'customer' => $customer->id,
            'amount' => 100 * $price,
            'currency' => $pricing->currencies->currency,
            'description' => 'Making test payment.',
            'shipping[name]' => $billingAddress->name,
            'shipping[address][line1]' => $billingAddress->address_1,
            'shipping[address][postal_code]' => $billingAddress->postal_code,
            'shipping[address][city]' => $billingAddress->city,
            'shipping[address][state]' => $billingAddress->states->name,
            'shipping[address][country]' => $billingAddress->countries->name,
        ]);

        Session::flash('success', 'Payment has been successfully processed.');
        //Get the transaction id from response
        $txId = $response['id'];
        //Get the status from response
        if ($response['status'] == 'succeeded') {
            $paymentStatus = 'paid';
        } else {
            $paymentStatus = 'unpaid';
        }
        //Get the gateway name
        $gateway = Gateway::where('name', 'stripe')->first();
        //Add complete order details using confirmOrder() function in OrderService
        $orderService->confirmOrder($user, $plan, $pricing, $price, $txId, $paymentStatus, $gateway);

        return to_route('success');
    }

    /**
     * Mollie payment API call
     */
    public function molliePayment($id)
    {
        /*
         * service class that interact with the Order model.
         * refer app/Models/Central/Services/OrderService.php
         */
        $orderService = new OrderService();
        //Get user
        $user = User::find(auth()->id());
        //Get the price
        $pricing = Pricing::find($id);
        //Get the requested plan
        $plan = Plan::find($pricing->plan_id);
        //Get the price
        $price = $orderService->price($user, $plan, $pricing);
        //Get the mollie key
        $mollie_key = GatewayDetails::where('name', 'mollie_key')->first();
        // dd($mollie_key->value);
        //Mollie API call
        try {
            Mollie::api()->setApiKey("$mollie_key->value");
            $payment = Mollie::api()->payments()->create([
                'amount' => [
                    'currency' => $pricing->currencies->currency, // Type of currency you want to send
                    'value' => sprintf('%.2f', $price), // You must send the correct number of decimals, thus we enforce the use of strings
                ],
                'description' => 'Payment By codehunger',
                'redirectUrl' => route('success'),
                'metadata' => [
                    'user_id' => $user->id,
                    'pricing_id' => $pricing->id,
                ],
                // after the payment completion where you to redirect
                'webhookUrl' => route('webhooks.mollie'),
            ]);
            $payment = Mollie::api()->payments()->get($payment->id);
            // redirect customer to Mollie checkout page
            Logger::info("Webhook url : $payment->webhookUrl");

            return redirect()->to($payment->getCheckoutUrl(), 303);
        } catch (Exception $ex) {
            Logger::info('Mollie Payment failed');
            Logger::info($ex->getMessage());

            return to_route('cancel');
        }
    }

    /**
     * Mollie webhook function
     * Gives the response of the payment
     */
    public function handleMollieWebhookNotification(Request $request): void
    {
        //Get the mollie key
        $mollie_key = GatewayDetails::where('name', 'mollie_key')->first();
        //Mollie API call
        Mollie::api()->setApiKey("$mollie_key->value");
        $payment = Mollie::api()->payments->get($request->input('id'));
        $user = User::where('id', $payment->metadata->user_id)->first();
        $orderService = new OrderService();
        $price = $payment->amount->value;
        $gateway = Gateway::where('name', 'mollie')->first();
        $txId = $payment->id;
        $pricing = Pricing::find($payment->metadata->pricing_id);
        $plan = Plan::where('id', $pricing->plan_id)->first();
        // echo '$payment';exit;
        if ($payment->isPaid() && ! $payment->hasRefunds() && ! $payment->hasChargebacks()) {
            $paymentStatus = 'paid';
            Logger::info('Payment successful');
        } else {
            $paymentStatus = $payment->status;
            Logger::info("Payment status: $payment->status");
        }
        $orderService->confirmOrder($user, $plan, $pricing, $price, $txId, $paymentStatus, $gateway);
    }

    public function payPalPayment(Request $request, $id)
    {
        $gateway = Gateway::where('name', 'paypal')->first();
        /*
         * service class that interact with the Order model.
         * refer app/Models/Services/OrderService.php
         */
        $orderService = new OrderService();
        //Get user
        $user = User::find(auth()->id());
        //Get the price
        $pricing = Pricing::find($id);
        //Get the requested plan
        $plan = Plan::find($pricing->plan_id);
        //Get the price
        $price = $orderService->price($user, $pricing);
        // Creating an environment
        $paypal_client = GatewayDetails::where('name', 'paypal_client_id')->first();
        $paypal_secret = GatewayDetails::where('name', 'paypal_client_secret')->first();
        $clientId = $paypal_client->value;
        $clientSecret = $paypal_secret->value;

        if ($gateway->test_mode == 1) {
            $environment = new SandboxEnvironment($clientId, $clientSecret);
        } else {
            $environment = new ProductionEnvironment($clientId, $clientSecret);
        }
        $client = new PayPalHttpClient($environment);

        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
            'intent' => 'CAPTURE',
            'purchase_units' => [[
                'reference_id' => '1111',
                'custom_id' => $pricing->id,
                'amount' => [
                    'value' => sprintf('%.2f', $price),
                    'currency_code' => $pricing->currencies->currency,
                ],
            ]],
            'application_context' => [
                'cancel_url' => route('cancel'),
                'return_url' => route('getDone'),
            ],
        ];

        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            return redirect()->to($response->result->links[1]->href);
        } catch (Exception $ex) {
            Logger::info('Paypal Payment failed');
            Logger::info($ex->getMessage());

            return to_route('cancel');
        }
    }

    /**
     * Paypal response
     * Gives the response of the payment
     */
    public function payPalResponse(Request $request): \Illuminate\Http\RedirectResponse
    {
        $gateway = Gateway::where('name', 'paypal')->first();

        $user = User::find(auth()->id());

        $paypal_client = GatewayDetails::where('name', 'paypal_client_id')->first();
        $paypal_secret = GatewayDetails::where('name', 'paypal_client_secret')->first();
        $clientId = $paypal_client->value;
        $clientSecret = $paypal_secret->value;

        if ($gateway->test_mode == 1) {
            $environment = new SandboxEnvironment($clientId, $clientSecret);
        } else {
            $environment = new ProductionEnvironment($clientId, $clientSecret);
        }
        $client = new PayPalHttpClient($environment);

        $request = new OrdersCaptureRequest($request->input('token'));
        $request->prefer('return=representation');
        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);

            $orderService = new OrderService();
            $price = $response->result->purchase_units[0]->payments->captures[0]->amount->value;
            $txId = $response->result->purchase_units[0]->payments->captures[0]->id;
            $pricing_id = $response->result->purchase_units[0]->custom_id;
            $pricing = Pricing::find($pricing_id);
            Logger::info("Pricing : $pricing");
            $plan = Plan::where('id', $pricing->plan_id)->first();
            Logger::info("plan : $plan");
            $payment_status = $response->result->purchase_units[0]->payments->captures[0]->status;
            if ($payment_status == 'COMPLETED') {
                $paymentStatus = 'paid';
                $orderService->confirmOrder($user, $plan, $pricing, $price, $txId, $paymentStatus, $gateway);
            } elseif ($payment_status == 'REFUNDED') {
                Logger::info('This is a refund');
            } else {
                $paymentStatus = 'unpaid';
                $orderService->confirmOrder($user, $plan, $pricing, $price, $txId, $paymentStatus, $gateway);
            }
            // $result = array($response->result->purchase_units[0]->description, $response->result->purchase_units[0]->custom_id, $response->result->purchase_units[0]->payments->captures[0]->id, $response->result->purchase_units[0]->payments->captures[0]->status, $response->result->purchase_units[0]->payments->captures[0]->amount->currency_code, $response->result->purchase_units[0]->payments->captures[0]->amount->value);
            // If call returns body in response, y  ou can get the deserialized version from the result attribute of the response
            return to_route('success');
        } catch (Exception $ex) {
            Logger::info('Paypal Payment failed');
            Logger::info($ex->getMessage());

            return to_route('cancel');
        }
    }

    public function bankTransfer(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        /*
         * service class that interact with the Order model.
         * refer app/Models/Central/Services/OrderService.php
         */
        $orderService = new OrderService();
        //Get user
        $user = User::find(auth()->id());
        //Get the price
        $pricing = Pricing::find($id);
        //Get the requested plan
        $plan = Plan::find($pricing->plan_id);
        //Get the price
        $price = $orderService->price($user, $pricing);
        //Get the transaction id from the request
        $txId = $request->input('transactionid');
        //Get the gateway name
        $gateway = Gateway::where('name', 'bank_transfer')->first();

        $paymentStatus = 'paid';

        //Add complete order details using confirmOrder() function in OrderService
        $orderService->confirmOrder($user, $plan, $pricing, $price, $txId, $paymentStatus, $gateway);

        return to_route('success');
    }

    /**
     * success message page
     */
    public function success()
    {
        $order = Session::get('order');
        $params = [
            'order' => $order,
        ];

        return view('gateway.success', $params);
    }

    /**
     * Failure message page
     */
    public function cancel()
    {
        return view('gateway.cancel');
    }
}
