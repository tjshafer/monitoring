<?php

namespace App\Http\Controllers;

use App\Helpers\Logger;
use App\Models\DnsAMonitor;
use App\Models\DnsMonitoring;
use App\Models\DnsMonitorLogs;
use App\Models\Services\DnsMonitorService;
use App\Models\Services\PublicPageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DnsAaController extends Controller
{
    /*
     * Display Add new record page
     */
    public function add(Request $request, $uuid)
    {
        $server = DnsMonitoring::find($uuid);
        $param = [
            'request' => $request,
            'server' => $server,
        ];

        return view('dns_monitoring.add_a_record', $param);
    }

    /*
     * Store new record
     */
    public function store(Request $request, $uuid)
    {
        $server = DnsMonitoring::find($uuid);
        try {
            //Form validation
            $validator = Validator::make($request->all(), [
                'ip_address' => 'required',

            ]);
            if ($validator->fails()) {
                Logger::error('Server add form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            /*
             * service class that interact with the DNS Monitor model.
             * refer app/Models/Services/MonitorService.php
             */
            $monitorService = new DnsMonitorService();
            //Add Server using addARecord() function in the MonitorService
            //case of successfull addition of server
            if ($monitorService->addARecord($request, $uuid, $server)) {
                return to_route('manage', [$uuid])
                ->with('success', __('A Record added'));
            }
            //case were the server is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('DNS Server is not up', [
                            'server' => $server->website_domain,
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    /*
     * Display Edit record page
     */
    public function edit(Request $request, $uuid)
    {
        //Get the server
        $record = DnsAMonitor::find($uuid);
        $server = DnsMonitoring::find($record->website_id);

        $params = [
            'record' => $record,
            'server' => $server,
        ];

        return view('dns_monitoring.edit_a_record', $params);
    }

    /*
     * Store edited record
     */
    public function update(Request $request, $uuid)
    {
        try {
            $record = DnsAMonitor::find($uuid);
            $server = DnsMonitoring::find($record->website_id);
            //Form validation
            $validator = Validator::make($request->all(), [
                'ip_address' => 'required',
            ]);
            if ($validator->fails()) {
                Logger::error('Server update form is invalid: '.json_encode($request->all()));

                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
            }
            /*
             * service class that interact with the Monitor model.
             * refer app/Models/Services/DnsMonitorService.php
             */
            $monitorService = new DnsMonitorService();
            //Update server using updateARecord() function in MonitorService
            //case of successfull updation of server
            if ($monitorService->updateARecord($request, $record, $server)) {
                return to_route('manage', [$record->website_id])
                ->with('success', __('Record updated'));
            }
            //case were the server is currently not up
            return redirect()
                    ->back()
                    ->withInput()
                    ->with(
                        'error',
                        __('Server is not up', [
                            'server' => $server->website_domain,
                        ])
                    )
                    ->with('test_failed', 1);
        } catch (\Exception $e) {
            Logger::error($e->getMessage());

            return redirect()
                ->back()
                ->withInput()
                ->with('error', __('Something went wrong'));
        }
    }

    public function show(Request $request, $uuid)
    {
        //Get the  monitor
        $monitor = DnsAMonitor::find($uuid);
        //Get the server
        $server = DnsMonitoring::find($monitor->website_id);
        //Get the logs of the current monitor
        $logs = DnsMonitorLogs::where('rel_id', $uuid)->get();
        //Get the last 5 logs of the keyword
        $lastLogs = DnsMonitorLogs::where('rel_id', $uuid)->latest()->take(5)->get();
        //Get the latest log
        $lastLog = DnsMonitorLogs::where('rel_id', $uuid)->latest()->first();
        //Get last checked time
        if ($lastLog) {
            $lastChecked = $lastLog->created_at;
        } else {
            $lastChecked = null;
        }
        /*
         * service class that interact with the public pages.
         * refer app/Models/Services/PublicPageService.php
         */
        $recordDetails = new PublicPageService();
        //Get the server details details using showPage() function in PublicPageService
        $response = $recordDetails->showPage($logs);

        $params = [
            'upTime' => $response['upTime'],
            'downTime' => $response['downTime'],
            'avgResponseTime' => $response['avgResponseTime'],
            'record' => $monitor,
            'lastChecked' => $lastChecked,
            'lastLogs' => $lastLogs,
            'server' => $server,
        ];

        return view('dns_monitoring.view_a_record', $params);
    }

    /*
     * Delete record
     */
    public function delete($uuid): \Illuminate\Http\RedirectResponse
    {
        $monitor = DnsAMonitor::find($uuid);
        $server = DnsMonitoring::find($monitor->website_id);

        $monitor->delete();

        $dnsService = new DnsMonitorService();
        $dnsService->CheckAllRecords($server);

        return to_route('manage', [$monitor->website_id])
            ->with('success', __('Server deleted'));
    }
}
