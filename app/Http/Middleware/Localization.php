<?php

namespace App\Http\Middleware;

use App\Models\Addon;
use App\Models\Address;
use App\Models\Currency;
use App\Models\Gateway;
use App\Models\Language;
use App\Models\Service;
use App\Models\Setting;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (session()->has('locale')) {
            App::setLocale(session()->get('locale'));
        }

        $uri = $request->getRequestUri();

        View::share('languages', Language::all());
        View::share('currencies', Currency::all());
        View::share('logo', Setting::where('name', 'system_logo')
            ->value('value'));
        View::share('footer_text', Setting::where('name', 'footer_text')
            ->value('value'));
        $app_name = config('app.name');
        $words = explode(' ', $app_name);
        $app_name_short = '';
        if ($words[0] != null) {
            foreach ($words as $w) {
                $app_name_short .= $w[0];
            }
        }
        View::share('app_name_short', $app_name_short);

        $user = Auth::user();
        if ($user) {


            View::share('existPlan', Service::where('user_id', $user->id)
                ->where('status_id', 1)->latest()->first());
            View::share('availableAddress', Address::find(1));
            View::share('activeGateway', Gateway::where('status', true)->first());
        }

        return $next($request);
    }
}
